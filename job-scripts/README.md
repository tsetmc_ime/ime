## about

bash scripts in this folder perform post-session jobs. the scripts must be copied to and run on the database server, which has allowed connection to both AS/400 and MySQL databases.


## how it works

the entry point is the `check.sh` file. this file is run periodically as a crontab job, and checks jobs status in MySQL. it runs appropriate script when appropriate.

