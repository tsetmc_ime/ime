#!/bin/bash

# TODO exclude insertion of سها ایج افز from freglog resultset into investor
# TODO add unique idx_investor_storage to asset
# TODO add unique idx_asciiptcode_natid to investor
# TODO add unique idx_asciiptcode_letno_letdat_symbol to asset_transfer

JOB_NAME=ASSET

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


FUTURE_DELIVERY=0

usage() {
	echo "usage: $0 [-f] [-h]"
	echo "  run with -f switch for future delivery job."
}

OPTIND=1
while getopts "h?f" opt; do
	case "$opt" in
	h|\?)
		usage
		exit 0
		;;
	f)
		FUTURE_DELIVERY=1
		;;
	*)
		usage
		exit 1
		;;
	esac
done
shift $((OPTIND-1))


# echo check current state of jobs ...

# while IFS=$'\t' read -r ASSET; do
# 	if [ "$FUTURE_DELIVERY" -eq 1 ]; then
# 	  if [ "$ASSET" != "FINISHED" ]; then
# 			echo "ASSET is not executed"
# 			exit 1
# 		fi
# 	else
# 	  if [ "$ASSET" != "NOT_STARTED" ]; then
# 			echo "ASSET is already executed"
# 			exit 1
# 		fi
# 	fi
# done < <(mysql "select job_status('ASSET');")


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it "آغاز عملیات" "RUNNING"


log_it "ایجاد درخواست بر اساس دارایی‌های موقت" "RUNNING"


echo sending temp_asset records to asset_request ...
mysql "
	insert into asset_request(temp_asset_id, request_date) 
	select 
		ta.id, CURRENT_TIMESTAMP() 
	from temp_asset ta 
	inner join storage s on s.id = ta.storage_id 
	inner join instrument i on s.instrument_id = i.id 
	inner join convert_time ct on ct.id = i.convert_time 
	where 
		ta.converted = 0 
		and ta.is_final_confirmed 
		and (ct.id != 2 or (ct.id = 2 and DATE(ta.entrance_date) < DATE(now())))
		and ta.id not in ( 
			select temp_asset_id from asset_request 
			where response_status is null  
		)" 

if [ $? -ne 0 ]; then
	log_it "خطا در ایجاد درخواست بر اساس دارایی‌های موقت" 'FAILED'
	exit 1
fi


log_it "انتقال درخواست‌ها به سرور پس از معاملات" "RUNNING"

echo clearing KALA/K2ASETTN ...
echo "delete from KALA.K2ASETTN" | isql $MACHINE -b


echo sending asset_request to AS/400 ...
QRY=" 
		asset_request ar, 
		temp_asset ta, 
		storage st, 
		instrument it, 
		investor iv, 
		broker br 
	where 
		ar.temp_asset_id = ta.id 
		and ta.storage_id = st.id 
		and st.instrument_id = it.id 
		and iv.id = ta.investor_id 
		and br.id = ta.broker_id 
		and ar.response_status is null"

while IFS=$'\t' read -r seq tm dat ptcode14 qty symb brcode
do
	echo "\
		insert into KALA.K2ASETTN (KERQNM,KERQDT,KERQTM,KEACC1,KESHRS,KESYMB,KEBRKR) \
		select \
			'$seq', datef, '$tm', '$ptcode14', '$qty', '$symb', '$brcode' \
		from 
			lib934.taqvim 
		where 
			datel = '$dat'" | utf8_to_ebcdic | isql $MACHINE -b
done < <(mysql "
	select 
		concat(3, lpad(ar.id, 19, '0')) seq, 
		DATE_FORMAT(ar.request_date, '%H%i%s'), 
		DATE_FORMAT(ar.request_date, '%Y%m%d'), 
		iv.ascii_ptcode, 
		ta.quantity * it.lot, 
		replace(it.symbol, ' ', ''), 
		br.code 
	from $QRY"
)

CNT_AS400=`echo "select count(*) from KALA.K2ASETTN" isql $MACHINE -b -x0x09`
while IFS=$'\t' read -r CNT_MYSQL; do
	if [ "$CNT_AS400" -ne "$CNT_MYSQL" ]; then
		log_it "خطا در انتقال درخواست‌ها به سرور پس از معاملات" "FAILED"
		exit 1
	fi
done < <(mysql "select count(*) from $QRY;")


log_it "اجرای برنامه K2ASETCLN در سرور پس از معاملات" "RUNNING"
echo running KALA/K2ASETCLN ...
lftp -c "open $MACHINE && quote rcmd CALL KALA/K2ASETCLN && quit"


CNT_AS400_OUT=`echo "select count(*) from KALA.A2KSETTN" isql $MACHINE -b -x0x09`
if [ "$CNT_AS400_OUT" -ne "$CNT_AS400" ]; then
	log_it "خطا در اجرای برنامه K2ASETCLN در سرور پس از معاملات" "FAILED"
	exit 1
fi


log_it "دریافت پاسخ درخواست‌ها از سرور پس از معاملات" "RUNNING"

echo receiving asset_response from AS/400 and updating assets ...
while IFS=$'|' read -r seq ptcode14 symb brcode qty dat tm stat datl
do
	mysql "
		set autocommit = 0; 
		start transaction; 
		
		update asset_request set 
			persian_response_date = '$dat', 
			response_date = str_to_date($datl, '%Y%m%d'), 
			response_time = '$tm', 
			response_status = $stat 
		where 
			'$seq' = concat(3, lpad(id, 19, '0')); 
		
		insert into asset ( investor_id, storage_id, quantity, last_update_date ) 
		select 
			ta.investor_id, 
			ta.storage_id, 
			$qty / instrument.lot, 
			ta.convert_date 
		from 
			temp_asset ta, 
			asset_request ar, 
			storage, 
			instrument 
		where 
			ta.converted = 0 
			and ar.temp_asset_id = ta.id 
			and concat(3, lpad(ar.id, 19, '0')) = '$seq' 
			and ta.storage_id = storage.id 
			and storage.instrument_id = instrument.id 
		on duplicate key update 
			quantity = asset.quantity + $qty / instrument.lot, 
			last_update_date = str_to_date($datl, '%Y%m%d'); 
		
    insert into asset_transaction(investor_id,storage_id,quantity,transaction_date,asset_request_id)
		select 
			ta.investor_id, 
			ta.storage_id, 
			$qty / instrument.lot, 
			ta.convert_date, 
			ar.id 
		from 
			temp_asset ta, 
			asset_request ar, 
			storage, 
			instrument 
		where 
			ta.converted = 0 
			and ar.temp_asset_id = ta.id 
			and concat(3, lpad(ar.id, 19, '0')) = '$seq' 
			and ta.storage_id = storage.id 
			and storage.instrument_id = instrument.id 
		
		update temp_asset set 
			converted = 1, 
			convert_date = str_to_date($datl, '%Y%m%d') 
		where 
			'$stat' = '1' 
			and id in ( select temp_asset_id from asset_request where concat(3, lpad(id, 19, '0')) = '$seq' ); 
		
		commit;"

  if [ $? -ne 0 ]; then
		log_it "خطا در دریافت پاسخ درخواست‌ها از سرور پس از معاملات (شماره درخواست $seq)" 'FAILED'
		exit 1
  fi
done < <(echo "\
	select \
		trim(NSRQNM), \
		trim(NSACC1), \
		trim(NSSYMB), \
		trim(NSBRKR), \
		trim(NSSHRS), \
		trim(NSRDAT), \
		trim(NSRTIM), \
		trim(NSRQST), \
		DATEL \
	from \
		KALA.A2KSETTN, \
		lib934.taqvim \
	where \
		DATEF = trim(NSRDAT)" | isql $MACHINE -b -x0x7C | ebcdic_to_utf8
)

echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
