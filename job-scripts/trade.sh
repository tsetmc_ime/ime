#!/bin/bash

# TODO exclude insertion of سها ایج افز from freglog resultset into investor
# TODO add unique idx_investor_storage to asset
# TODO add unique idx_asciiptcode_natid to investor
# TODO add unique idx_asciiptcode_letno_letdat_symbol to asset_transfer

JOB_NAME=TRADE

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


# echo check current state of jobs ...

# while IFS=$'\t' read -r TRADE; do
# 	if [ "$TRADE" != "NOT_STARTED" ]; then
# 		echo "TRADE is already executed"
# 		exit 1
# 	fi
# done < <(mysql "select job_status('TRADE');")


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it 'آغاز عملیات' 'RUNNING'


log_it "اجرای برنامه A2KTRDCLN در سرور پس از معاملات" "RUNNING"

echo executing A2KTRDCLN ...
lftp -c "open $MACHINE && quote rcmd CALL KALA/A2KTRDCLN && quit"


log_it "دریافت معاملات از سرور پس از معاملات" "RUNNING"

echo fetching trades from AS/400 and updating assets ...
while IFS=$'|' read -r \
            reqNum symbol tradeDate tradeTime tradeSeq qty price ticketNo bPtCode sPtCode \
            bType bPtCode14 bClientCode bNationalCode bFirstName bLastName bIssueNo bBirthDate bFatherName bSerrie bIssuePlace bActive \
            sType sPtCode14 sClientCode sNationalCode sFirstName sLastName sIssueNo sBirthDate sFatherName sSerrie sIssuePlace sActive \
            retDate retTime
do
  mysql "
    set autocommit = 0;
    start transaction; 

    INSERT INTO trade(sequence_id,persian_trade_date,trade_time,trade_seq,symbol,ticket_number,quantity,unit_price,
                          b_person_type,b_ptcode,b_ascii_ptcode,b_client_code,b_national_id,b_last_name,b_first_name,b_id_number,b_father_name,b_birth_date,b_id_serie,b_issue_place,b_avtivity_state,
                          s_person_type,s_ptcode,s_ascii_ptcode,s_client_code,s_national_id,s_last_name,s_first_name,s_id_number,s_father_name,s_birth_date,s_id_serie,s_issue_place,s_activity_state) 
    VALUES ('$reqNum','$tradeDate','$tradeTime','$tradeSeq','$symbol','$ticketNo','$qty','$price',
            '$bType','$bPtCode','$bPtCode14','$bClientCode','$bNationalCode','$bFirstName','$bLastName','$bIssueNo','$bFatherName','$bBirthDate','$bSerrie','$bIssuePlace','$bActive',
            '$sType','$sPtCode','$sPtCode14','$sClientCode','$sNationalCode','$sFirstName','$sLastName','$sIssueNo','$sFatherName','$sBirthDate','$sSerrie','$sIssuePlace','$sActive');

    INSERT INTO investor(person_type,ptcode,ascii_ptcode,national_id,first_name,last_name,id_number,birth_date_greg,father_name,id_serie,issue_place,activity_state,last_update_date)
    VALUES ('$bType','$bPtCode','$bPtCode14','$bNationalCode','$bFirstName','$bLastName','$bIssueNo','$bBirthDate','$bFatherName','$bSerrie','$bIssuePlace','$bActive',current_timestamp)
    on duplicate key update 
            person_type='$bType', ptcode='$bPtCode',first_name='$bFirstName',last_name='$bLastName',id_number='$bIssueNo',birth_date_greg='$bBirthDate',father_name='$bFatherName',
            id_serie='$bSerrie',issue_place='$bIssuePlace', activity_state='$bActive',last_update_date=current_timestamp; 
    
    INSERT INTO investor(person_type,ptcode,ascii_ptcode,national_id,first_name,last_name,id_number,birth_date_greg,father_name,id_serie,issue_place,activity_state,last_update_date)
    VALUES ('$sType','$sPtCode','$sPtCode14','$sNationalCode','$sFirstName','$sLastName','$sIssueNo','$sBirthDate','$sFatherName','$sSerrie','$sIssuePlace','$sActive',current_timestamp)
    on duplicate key update 
            person_type='$sType', ptcode='$sPtCode',first_name='$sFirstName',last_name='$sLastName',id_number='$sIssueNo',birth_date_greg='$sBirthDate',father_name='$sFatherName',
            id_serie='$sSerrie',issue_place='$sIssuePlace', activity_state='$sActive',last_update_date=current_timestamp; 

   INSERT INTO asset(investor_id,storage_id,quantity,last_update_date)
   SELECT i.id,s.id,(tr.quantity/inst.lot),current_timestamp 
   FROM trade tr,investor i ,instrument inst,storage s 
   where i.ascii_ptcode=tr.b_ascii_ptcode 
        and REPLACE(inst.symbol,' ','') = REPLACE(tr.symbol,' ','') 
        and s.instrument_id=inst.id 
        and tr.sequence_id='$reqNum'
    on duplicate key update
        quantity = asset.quantity + tr.quantity / inst.lot, 
        last_update_date = current_timestamp; 
    
    insert into asset_transaction(investor_id,storage_id,quantity,transaction_date,trade_id)
        SELECT i.id,s.id,(tr.quantity/inst.lot),current_timestamp,tr.id 
        FROM 
            trade tr, 
            investor i , 
            instrument inst, 
            storage s 
        where 
            i.ascii_ptcode=tr.b_ascii_ptcode 
            and REPLACE(inst.symbol,' ','') = REPLACE(tr.symbol,' ','') 
            and s.instrument_id=inst.id 
            and tr.sequence_id='$reqNum';


    INSERT INTO asset(investor_id,storage_id,quantity,last_update_date)
    SELECT i.id,s.id,-(tr.quantity/inst.lot),current_timestamp 
    FROM 
        trade tr,
        investor i ,
        instrument inst, 
        storage s 
    WHERE 
        i.ascii_ptcode=tr.s_ascii_ptcode 
        and REPLACE(inst.symbol,' ','') = REPLACE(tr.symbol,' ','') 
        and s.instrument_id=inst.id 
        and tr.sequence_id='$reqNum'
    ON DUPLICATE KEY UPDATE
        quantity = asset.quantity - tr.quantity / inst.lot, 
        last_update_date = current_timestamp; 

    insert into asset_transaction(investor_id,storage_id,quantity,transaction_date,trade_id)
        SELECT i.id,s.id,-(tr.quantity/inst.lot),current_timestamp,tr.id 
        FROM 
            trade tr, 
            investor i , 
            instrument inst, 
            storage s 
        WHERE 
            i.ascii_ptcode=tr.s_ascii_ptcode 
            and REPLACE(inst.symbol,' ','') = REPLACE(tr.symbol,' ','') 
            and s.instrument_id=inst.id 
            and tr.sequence_id='$reqNum' ;

    INSERT INTO clearance_request( 
            register_date, 
            confirm_date, 
            is_final_confirmed, 
            investor_id, 
            storage_id, 
            quantity, 
            description, 
            trade_id) 
    select  current_timestamp, 
            current_timestamp, 
            1, 
            i.id, 
            st.id, 
            tr.quantity, 
            'ترخیص ناشی از فروش نماد با قابلیت ترخیص اتوماتیک در پایان جلسه معاملاتی جاری' as description , 
            tr.id 
    from  
        trade tr, 
        investor i, 
        instrument inst, 
        storage st 
    where  
        tr.b_ascii_ptcode =i.ascii_ptcode and  
        replace(tr.symbol , ' ','')=replace(inst.symbol,' ' ,'') and  
        i.id=st.instrument_id and  
        inst.clearance_time=1 and  
        inst.clearance_type = 2 and  
        tr.sequence_id='$reqNum' ; 
   
    commit;"

  if [ $? -ne 0 ]; then
		log_it "خطا در دریافت معاملات از سرور پس از معاملات (شماره اعلامیه $ticketNo)" 'FAILED'
		exit 1
  fi
done < <(echo "\
    select \
        trim(TKTRNM), \
        trim(TKSYMB), \
        concat(trim(TKCENT),concat(trim(TKYY##),concat(trim(TKMM##),trim(TKDD##)))), \
        concat(trim(TKHH##),concat(trim(TKMN##),trim(TKSS##))), \
        trim(TKSEQ#), \
        trim(TKSHRS), \
        trim(TKPRCE), \
        trim(TKTCKT), \
        trim(TKBAC#), \
        trim(TKSAC#), \
        trim(TBTYPE), \
        trim(TBACC1), \
        trim(TBTRID), \
        trim(TBNID), \
        trim(firstname(TBNAME)), \
        trim(lastname(TBNAME)), \
        trim(TBISNO), \
        trim(TBDATB), \
        trim(TBFATH), \
        trim(TBSERY), \
        trim(TBISPL), \
        case when TBACTV = 'Y' then 1 else 2 end, \
        trim(TSTYPE), \
        trim(TSACC1), \
        trim(TSTRID), \
        trim(TSNID), \
        trim(firstname(TSNAME)), \
        trim(lastname(TSNAME)), \
        trim(TSISNO), \
        trim(TSDATB), \
        trim(TSFATH), \
        trim(TSSERY), \
        trim(TSISPL), \
        case when TSACTV = 'Y' then 1 else 2 end, \
        trim(TKRDAT), \
        trim(TKRTIM) \
    from \
        KALA.A2KTRDTN" | isql $MACHINE -b -x0x7C | ebcdic_to_utf8
)

echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
