#!/bin/bash

JOB_NAME=NAZER

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it 'آغاز عملیات' 'RUNNING'


log_it 'اجرای A2KNZRCLN در سرور پس از معاملات' 'RUNNING'

echo executing A2KTRDCLN ...
lftp -c "open $MACHINE && quote rcmd CALL KALA/A2KNZRCLN && quit"


log_it 'دریافت لیست کارگزاران ناظر از سرور پس از معاملات' 'RUNNING'
echo fetching broker list from AS/400 ...

while IFS=$'|' read -r symbol ptcode14 brcode ptcode
do
    mysql "
        INSERT INTO ime.nazer_broker (symbol, ascii_ptcode, ptcode, broker_code)
        VALUES (symbol, ptcode14, ptcode, brcode) 
        on duplicate key update broker_code = brcode;
      "

  if [ $? -ne 0 ]; then
    log_it "خطا در دریافت لیست کارگزاران ناظر از سرور پس از معاملات ($symbol, $ptcode)" 'FAILED'
    exit 1
  fi
done < <(echo "select trim(SKSYMB), trim(SKACC1), trim(SKNAZR), trim(SKACC#) from KALA.A2KNZRTN"	| isql $MACHINE -b -x0x7C | ebcdic_to_utf8)

echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
