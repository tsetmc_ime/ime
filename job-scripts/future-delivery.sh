#!/bin/bash

JOB_NAME=FUTURE_DELIVERY

MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


# echo check current state of jobs ...

# while IFS=$'\t' read -r ASSET_REQUEST CLEARANCE_REQUEST FUTURE_DELIVERY; do
# 	if [ "$ASSET_REQUEST" != "FINISHED" ] || [ "$CLEARANCE_REQUEST" != "FINISHED" ]; then
# 		echo "ASSET_REQUEST and/or CLEARANCE_REQUEST not finished yet"
# 		exit 1
# 	fi
# 	if [ "$FUTURE_DELIVERY" != "NOT_STARTED" ] && [ "$FUTURE_DELIVERY" != "FAILED" ]; then
# 		echo "job is already running / finished"
# 		exit 1
# 	fi
# done < <(mysql "select job_status('ASSET_REQUEST'), job_status('CLEARANCE_REQUEST'), job_status('FUTURE_DELIVERY');")


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it 'آغاز عملیات' 'RUNNING'


echo running clear.sh ...
if ! ./clear.sh ; then
	echo clear.sh failed
	log_it 'خطا در اجرای عملیات ترخیص' 'FAILED'
	exit 1
fi

echo running asset.sh ...
if ! ./asset.sh -f ; then
	echo asset.sh failed
	log_it 'خطا در اجرای عملیات ایجاد دارایی' 'FAILED'
	exit 1
fi

echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
