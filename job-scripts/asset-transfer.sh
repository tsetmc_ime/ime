#!/bin/bash

# TODO exclude insertion of سها ایج افز from freglog resultset into investor
# TODO add unique idx_investor_storage to asset
# TODO add unique idx_asciiptcode_natid to investor
# TODO add unique idx_asciiptcode_letno_letdat_symbol to asset_transfer

JOB_NAME=ASSET_TRANSFER

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


# echo check current state of jobs ...

# while IFS=$'\t' read -r ASSET_TRANSFER; do
# 	if [ "$ASSET_TRANSFER" != "NOT_STARTED" ]; then
# 		echo "ASSET_TRANSFER is already executed"
# 		exit 1
# 	fi
# done < <(mysql "select job_status('ASSET_TRANSFER');")


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it 'آغاز عملیات' 'RUNNING'


log_it 'در حال دریافت نقل و انتقالات' 'RUNNING'

echo get FREGLOG from AS/400 ...
while IFS=$'|' read -r user dat symb ptcod onh letNum letDate invType natId ptcode14 datl fullname sin born father serial issuePlace active isin
do
	mysql "
		insert into investor ( 
			person_type, 
			ptcode, 
			ascii_ptcode, 
			national_id, 
			first_name, 
			last_name, 
			id_number, 
			birth_date, 
			father_name, 
			id_serial, 
			issue_place, 
			activity_state, 
			last_update_date 
		) values ( 
			'$invType', 
			'$ptcod', 
			'$ptcode14', 
			'$natId', 
			'$fullname', 
			'$fullname', 
			'$sin', 
			'$born', 
			'$father', 
			'$serial', 
			'$issuePlace', 
			'$active', 
			current_timestamp 
		) on duplicate key update 
			person_type = '$invType',  
			ascii_ptcode = '$ptcode14',  
			national_id = '$natId',  
			first_name = '$fullname',  
			last_name = '$fullname',  
			id_number = '$sin',  
			birth_date = '$born',  
			father_name = '$father',  
			id_serial = '$serial',  
			issue_place = '$issuePlace',  
			activity_state = '$active',  
			last_update_date = current_timestamp; 

		insert into asset_transfer( 
			ascii_ptcode, 
			letter_date, 
			letter_number, 
			persian_register_date, 
			ptcode, 
			quantity, 
			register_date, 
			symbol, 
			transaction_date, 
			is_applied, 
			isin 
		) values ( 
			'$ptcode14', 
			'$letDate', 
			'$letNum', 
			'$dat', 
			'$ptcod', 
			'$onh', 
			str_to_date($datl, '%Y%m%d'), 
			'$symb', 
			current_timestamp, 
			0, 
			'$isin' 
		);"

	if [$? -ne 0]; then
		log_it "خطا در ذخیره‌سازی نقل و انتقالات ($ptcod, $symb)" 'FAILED'
		exit 1
	fi
done < <(echo "\
	select \
		trim(REGUSR), \
		trim(REGDAT), \
		trim(REGCOR), \
		trim(REGCOD), \
		trim(REGONH), \
		trim(LETNUM), \
		trim(LETDAT), \
		case when trim(CIITYP) in ('1', '4', 'د', 'd', 'D') then 1 else 2 end, \
		trim(CISNAM), \
		qgpl.ime14(REGCOD), \
		DATEL, \
		trim(CIFNAM), \
		trim(CISIN#), \
		trim(CIBORN), \
		trim(CINNXT), \
		trim(CIBPHO), \
		trim(CIBPRO), \
		case when trim(CIACTV) = 'Y' then 1 else 2 end, \
		trim(AIINIS) \
	from  \
		TSELIB.FREGLOG,  \
		TSELIB.CPINDC, \
		LIB934.TAQVIM, \
		TRILIB.TRSYMB, \
		INTERFACE.SYMBMAP \
	where \
		DATEL = varchar_format(current_timestamp, 'YYYYMMDD') \
		and REGCOD = CIINDV  \
		and REGDAT = DATEF  \
		and REGCOR = TSKEY \
		and TSISIP = 'Z' \
		and REGUSR <> 'ANBA2AS400' \
		and REGCOD <> 'ایج‍00484' \
		and REGCOD <> 'افز00333' \
		and REGCOD <> 'کاه‍01880' \
		and AIMTYP = 'NO' \
		and AISYMB = REGCOR " | utf8_to_ebcdic | isql $MACHINE -b -x0x7C | ebcdic_to_utf8
)

log_it 'اعمال نقل و انتقالات در سامانه انبار' 'RUNNING'

echo inserting into asset_transaction table ...
echo insert/update asset table ...
echo mark asset_transfer records ...
mysql "
	set autocommit = 0; 
	start transaction; 
	
	insert into 
		asset_transaction (quantity, transaction_date, asset_transfer_id, investor_id, storage_id) 
	select  
		at.quantity, 
		current_timestamp, 
		at.id at_id, 
		iv.id iv_id, 
		st.id st_id 
	from  
		asset_transfer at, 
		investor iv, 
		storage st, 
		instrument it 
	where 
		at.ascii_ptcode = iv.ascii_ptcode 
		and at.isin = it.isin 
		and it.id = st.instrument_id 
		and is_applied = 0; 
	
	insert into 
		asset (quantity, last_update_date, investor_id, storage_id) 
	select 
		at.quantity / it.lot, 
		current_timestamp, 
		iv.id iv_id, 
		st.id st_id 
	from 
		asset_transfer at, 
		investor iv, 
		storage st, 
		instrument it 
	where 
		at.ascii_ptcode = iv.ascii_ptcode 
		and at.isin = it.isin 
		and it.id = st.instrument_id 
		and is_applied = 0 
	on duplicate key update 
		quantity = asset.quantity + at.quantity / it.lot, 
		last_update_date = current_timestamp; 
	
	update asset_transfer at 
	join investor iv on (iv.ascii_ptcode = at.ascii_ptcode) 
	join instrument it on (it.isin = at.isin) 
	join storage st on (st.instrument_id = it.id) 
	set is_applied = 1 
	where is_applied = 0; 
	
	commit;"

if [ $? -ne 0]; then
	log_it "خطا در اعمال نقل و انتقالات در سامانه انبار" 'FAILED'
	exit 1
fi

echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
