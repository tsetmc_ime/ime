#!/bin/bash

JOB_NAME=CLEARANCE_REQUEST

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db


DIR=`dirname "$0"`
source $DIR/common.sh


FUTURE_DELIVERY=0

usage() {
	echo "usage: $0 [-f] [-h]"
	echo "  run with -f switch for future delivery job."
}

OPTIND=1
while getopts "h?f" opt; do
	case "$opt" in
	h|\?)
		usage
		exit 0
		;;
	f)
		FUTURE_DELIVERY=1
		;;
	*)
		usage
		exit 1
		;;
	esac
done
shift $((OPTIND-1))


# echo check current state of jobs ...

# while IFS=$'\t' read -r CLEARANCE_REQUEST; do
# 	if [ "$FUTURE_DELIVERY" -eq 1 ]; then
# 	  if [ "$CLEARANCE_REQUEST" != "FINISHED" ]; then
# 			echo "CLEARANCE_REQUEST is not executed"
# 			exit 1
# 		fi
# 	else
# 	  if [ "$CLEARANCE_REQUEST" != "NOT_STARTED" ]; then
# 			echo "CLEARANCE_REQUEST is already executed"
# 			exit 1
# 		fi
# 	fi
# done < <(mysql "select job_status('CLEARANCE_REQUEST');")


set -x

mysql "update job set fire = 0 where name = '$JOB_NAME';"

echo running ...
log_it "آغاز عملیات" "RUNNING"


log_it "ارسال درخواست‌های ترخیص به سرور پس از معاملات" "RUNNING"

echo clearing KALA/K2ACLRTN  ...
echo "delete from KALA.K2ACLRTN" | isql $MACHINE -b


echo sending clearance_request to AS/400 ...

QRY="clearance_request cr, 
		storage st, 
		instrument it, 
		investor iv
	where 
		cr.is_final_confirmed =1
		and cr.request_date is null
		and cr.response_status is null
		and cr.storage_id = st.id 
		and st.instrument_id = it.id 
		and iv.id = cr.investor_id"

while IFS=$'\t' read -r seq tm dat ptcode14 ptcode qty symb
do
	echo "\
		insert into \
			KALA.K2ACLRTN (KCRQNM,KCRQDT,KCRQTM,KCACC1,KCSHRS,KCSYMB,KCACC#) \
		select 
			'$seq', datef, '$tm', '$ptcode14', '$qty', '$symb', '$ptcode' \
		from
			lib934.taqvim
		where
			datel = '$dat'" | utf8_to_ebcdic | isql $MACHINE -b
done < <(mysql "
	select 
		concat(4, lpad(cr.id, 19, '0')) seq, 
		DATE_FORMAT(now(), '%H%i%s'), 
		DATE_FORMAT(now(), '%Y%m%d'), 
		iv.ascii_ptcode, 
		iv.ptcode, 
		cr.quantity * it.lot, 
		replace(it.symbol, ' ', '')
	from 
		$QRY"
)

CNT_AS400=`echo "select count(*) from ALA.K2ACLRTN" isql $MACHINE -b -x0x09`
while IFS=$'\t' read -r CNT_MYSQL; do
	if [ "$CNT_AS400" -ne "$CNT_MYSQL" ]; then
		log_it "خطا در انتقال درخواست‌ها به سرور پس از معاملات" "FAILED"
		exit 1
	fi
done < <(mysql "select count(*) from $QRY;")


log_it "اجرای برنامه K2ACLRCLN در سرور پس از معاملات" "RUNNING"


echo running KALA/K2ACLRCLN ...
lftp -c "open $MACHINE && quote rcmd CALL KALA/K2ACLRCLN && quit"

CNT_AS400_OUT=`echo "select count(*) from KALA.A2KCLRTN" isql $MACHINE -b -x0x09`
if [ "$CNT_AS400_OUT" -ne "$CNT_AS400" ]; then
	log_it "خطا در اجرای برنامه K2ACLRCLN در سرور پس از معاملات" "FAILED"
	exit 1
fi


log_it "دریافت پاسخ درخواست‌های ترخیص از سرور پس از معاملات" "RUNNING"

echo receiving clearance_response from AS/400 and updating assets ...
while IFS=$'|' read -r seq ptcode14 symb qty reqdat reqtim stat stcost excost resdat restime reqdatl resdatl; do
	mysql "
		set autocommit = 0; 
		start transaction; 

		update clearance_request set 
			persian_request_date='$reqdat', 
			request_time='$reqtim', 
			request_date=str_to_date($reqdatl, '%Y%m%d'), 
			persian_response_date = '$resdat', 
			response_date = str_TO_DATE($resdatl, '%Y%m%d'), 
			response_time = '$restime', 
			response_status = $stat, 
			storage_cost = $stcost, 
			ex_cost = $excost 
		where 
			'$seq' = concat(4, lpad(id, 19, '0')); 
		
		insert into asset_transaction( investor_id, storage_id, quantity, transaction_date,clearance_request_id ) 
		select 
			cr.investor_id, 
			cr.storage_id, 
			-($qty / instrument.lot), 
			cr.response_date, 
			cr.id
		from 
			clearance_request cr, 
			storage, 
			instrument 
		where 
			concat(4, lpad(cr.id, 19, '0')) = '$seq' 
			and '$stat' = '1' 
			and cr.storage_id = storage.id 
			and storage.instrument_id = instrument.id ;
		
		insert into asset ( investor_id, storage_id, quantity, last_update_date ) 
		select 
			cr.investor_id, 
			cr.storage_id, 
			-($qty / instrument.lot),
			cr.response_date 
		from 
			clearance_request cr, 
			storage, 
			instrument 
		where 
			concat(4, lpad(cr.id, 19, '0')) = '$seq' 
			and '$stat' = '1' 
			and cr.storage_id = storage.id 
			and storage.instrument_id = instrument.id 
		on duplicate key update 
			quantity = asset.quantity - ($qty / instrument.lot), 
			last_update_date = cr.response_date;

		insert into out_asset(investor_id, storage_id, quantity, clearance_date, cost, clearance_request) 
		select investor_id,storage_id,quantity,response_date, ($stcost+$excost),id
		from clearance_request cr 
		where 
		concat(4, lpad(cr.id, 19, '0')) = '$seq' 
		and '$stat' = '1';

		commit;"

  if [ $? -ne 0 ]; then
		log_it "خطا در دریافت پاسخ درخواست‌ها از سرور پس از معاملات (شماره درخواست $seq)" 'FAILED'
		exit 1
  fi
done < <(echo "\
	select  \
		trim(NCRQNM), \
		trim(NCACC1), \
		trim(NCSYMB), \
		trim(NCSHRS), \
		trim(NCRQDT), \
		trim(NCRQTM), \
		trim(NCRQST), \
		trim(NCSTCO), \
		trim(NCEXCO), \
		trim(NCRDAT), \
		trim(NCRTIM), \
		reqT.DATEL ,\
		resT.DATEL \
	from \
		KALA.A2KCLRTN, \
		lib934.taqvim reqT, lib934.taqvim resT \
	where \
		reqT.DATEF = trim(NCRQDT) and resT.DATEF = trim(NCRDAT)" | isql $MACHINE -b -x0x7C | ebcdic_to_utf8
)


echo success
log_it 'عملیات با موفقیت به پایان رسید' 'FINISHED'

exit 0
