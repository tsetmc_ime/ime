#!/bin/bash

MACHINE=B100CB4P
MYSQL_HOST=172.30.109.15
MYSQL_DB=ime
MYSQL_USER=gsk-db

DIR=`dirname "$0"`

# Make your aliases available to the script
shopt -s expand_aliases

alias mysql="mysql -h $MYSQL_HOST -u $MYSQL_USER $MYSQL_DB --batch -s -N -e"

set -x

while IFS=$'\t' read -r JOB
do
	case "$JOB" in
	ASSET)
		"$DIR/asset.sh"
		;;
	ASSET_TRANSFER)
		"$DIR/asset-transfer.sh"
		;;
	CLEARANCE_REQUEST)
		"$DIR/clear.sh"
		;;
	FUTURE_DELIVERY)
		"$DIR/future-delivery.sh"
		;;
	INQUIRY)
		;;
	NAZER)
		"$DIR/nazer-broker.sh"
		;;
	TRADE)
		"$DIR/trade.sh"
		;;
	*)
		echo job $JOB is not defined
		;;
	esac
done < <(mysql "select name from job_view where fire = 1 order by id;")

