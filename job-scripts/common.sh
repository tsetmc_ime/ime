# Make your aliases available to the script
shopt -s expand_aliases

alias mysql="mysql -h $MYSQL_HOST -u $MYSQL_USER $MYSQL_DB --batch -s -N -e"

function cleanup {
    if ! rmdir /tmp/$JOB_NAME
    then
        echo "failed to remove lock /tmp/$JOB_NAME"
        exit 1
    fi
}

if ! mkdir /tmp/$JOB_NAME
then
    echo "another instance is already running"
    exit 1
fi

trap "cleanup" EXIT


log_it () {
  MSG=`mysql "insert into job (date, name, fire, status) values (current_date, '$JOB_NAME', 0, '$2') on duplicate key update status = '$2';" 2>&1`

  if [ $? -ne 0 ]; then
    MSG=`perl -pe 's|^(.*?): ||' <<< "$MSG"`
    echo "error occurred: $MSG"
    mysql "insert into ime.job_log (comments, date, name, time) values ('$MSG', current_date, '$JOB_NAME', current_time);"
    exit 1
  fi

  mysql "
    insert into job_log (comments, date, name, time) 
    VALUES ('$1', current_date, '$JOB_NAME', current_time);
  "
}

