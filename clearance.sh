#!/bin/sh

# set -x

MYSQL_DB=ime
MYSQL_PASSWORD=
MACHINE=B100CB4P

echo clearing KALA/K2ACLRTN ...
echo "delete from KALA.K2ACLRTN" | isql $MACHINE -b


echo sending clearance request to AS400 ...
mysql $MYSQL_DB --batch -s -p$MYSQL_PASSWORD -e "\
	select \
		concat(4, lpad(cr.id, 19, '0')) seq, \
 		DATE_FORMAT(cr.request_date, '%H%i%s'), \   
 		DATE_FORMAT(cr.request_date, '%Y%m%d'), \ 
 		iv.ascii_ptcode, \  
 		cr.quantity * it.lot, \
 		replace(it.symbol, ' ', '') \
	from \
		ime.clearance_request cr, \
		ime.storage st, \
		ime.instrument it, \  
		ime.investor iv \
	where \
		cr.storage_id = st.id \
		and st.instrument_id = it.id \
		and iv.id = cr.investor_id \
		and cr.response_status is null \
        and cr.is_final_confirmed"| while IFS=$'\t' read -r seq tm dat ptcode14 qty symb brcode
do
	echo "\
		Insert into \
			KALA.K2ASETTN (KERQNM,KERQDT,KERQTM,KEACC1,KESHRS,KESYMB,KEBRKR) \
			select '$seq', datef, '$tm', '$ptcode14', '$qty', '$symb', '$brcode' \
			from lib934.taqvim where datel = '$dat'" | utf8_to_ebcdic | isql $MACHINE -b
done