refresh = function () {
    $('#jobs-table tbody').html('');
    $.get('/post-session/list', function (data, status, xhr) {
        tbl = $('#jobs-table tbody');
        _.each(data, function (job) {
            if (!job.comments)
                job.comments = '-';
            if (!job.time)
                job.time = '-';
            let tr = `<tr><td scope="row" data-label="ردیف">${job.id}</td><td scope="row" data-label="عنوان">${job.title}</td><td scope="row" data-label="عملیات"><button onclick="startJob(this, '${job.name}', '${job.title}')" class="primary button" style="margin: 4px;" ${job.fire ? 'disabled' : ''}><i class="fa fa-bolt fa-fw" aria-hidden="true"></i>فرمان آغاز عملیات</button></td><td scope="row" data-label="آخرین وضعیت">${job.status}</td><td scope="row" data-label="زمان ثبت آخرین گزارش">${job.time}</td><td scope="row" data-label="آخرین پیام">${job.comments}</td></tr>`;
            if(!job.enable)
                tr = `<tr><td scope="row" data-label="ردیف">${job.id}</td><td scope="row" data-label="عنوان">${job.title}</td><td scope="row" data-label="عملیات"><button  class="primary button" style="margin: 4px;" disabled><i class="fa fa-bolt fa-fw" aria-hidden="true"></i>فرمان آغاز عملیات</button></td><td scope="row" data-label="آخرین وضعیت">${job.status}</td><td scope="row" data-label="زمان ثبت آخرین گزارش">${job.time}</td><td scope="row" data-label="آخرین پیام">${job.comments}</td></tr>`;
            tbl.append(tr);
        })
    }).fail(function () {
    });

    $.get('/post-session/current-date', function (data, status, xhr) {
        $('#jcal').html(new persianDate(new Date(data)).format('dddd، D MMMM YYYY'));
    }).fail(function () {
    });
};

$(document).ready(function () {
    refresh();
});

$('#btn-refresh').click(refresh);

startJob = function (e, jobName, jobDescription) {
    $.post(`/post-session/trigger?name=${jobName}`, function (data, status, xhr) {
        $(e).attr('disabled', true);
        $('#modal-message').html(`فرمان آغاز عملیات <span style='font-weight: bold;'>${jobDescription}</span> به سرور ارسال شد.`);

    }).fail(function (data, status, xhr) {
        $('#modal-message').text(data && data.responseText ? data.responseText : `فرمان ارسال نشد؛ یک خطای ناشناخته رخ داد.`);

    }).always(function () {
        window.triggerModal.open();
        setTimeout(refresh, 3000);
    });
};