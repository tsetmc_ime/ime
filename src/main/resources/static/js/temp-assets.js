setup_datatable = function(element, href) {
    let columnNames = [];
    $('#temp-asset-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = minDate.val();
            var max = maxDate.val();
            var date = new Date(data[2]);

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);
        }
    );

    window.table = element.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        aaSorting: [],
        ajax: {
            url: "/temp-assets/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {
                response.data.forEach(function (item, index) {
                    if (item["زمان ورود به انبار"] != null)
                        item["زمان ورود به انبار"] = new persianDate(new Date(item["زمان ورود به انبار"])).format('YYYY/MM/DD HH:mm:ss');
                    if (item["زمان تبدیل وضعیت"] != null)
                        item["زمان تبدیل وضعیت"] = new persianDate(new Date(item["زمان تبدیل وضعیت"])).format('YYYY/MM/DD HH:mm:ss');
                    if (item["زمان ثبت"] != null)
                        item["زمان ثبت"] = new persianDate(new Date(item["زمان ثبت"])).format('YYYY/MM/DD HH:mm:ss');
                    if (item["زمان تأیید"] != null)
                        item["زمان تأیید"] = new persianDate(new Date(item["زمان تأیید"])).format('YYYY/MM/DD HH:mm:ss');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.storageName = $('#storage_name').val();
                d.finalConfirmed = $('#temp-asset-table_status').val() ? $('#temp-asset-table_status').val() : '';
                d.entryFrom = $('#date_from_e').data('greg') ? $('#date_from_e').data('greg') : '';
                d.entryTo = $('#date_to_e').data('greg') ? $('#date_to_e').data('greg') : '';
                d.convertFrom = $('#date_from_c').data('greg') ? $('#date_from_c').data('greg') : '';
                d.convertTo = $('#date_to_c').data('greg') ? $('#date_to_c').data('greg') : '';
                d.columns = null;
                debugger;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&storageName=${d.storageName}&entryFrom=${d.entryFrom}&entryTo=${d.entryTo}&convertFrom=${d.convertFrom}&convertTo=${d.convertTo}&finalConfirmed=${d.finalConfirmed}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#temp-asset-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
