setup_datatable = function(href) {
    let columnNames = [];
    $('#activity-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#activity-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/activities/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    if (item["زمان"] !== null)
                        item["زمان"] = new persianDate(new Date(item["زمان"])).format('YYYY/MM/DD HH:mm:ss');
                    if (item["مقادیر جدید"] !== null)
                        item["مقادیر جدید"] = item["مقادیر جدید"].replaceAll('\n', '<br>');
                    if (item["مقادیر قبلی"] !== null)
                        item["مقادیر قبلی"] = item["مقادیر قبلی"].replaceAll('\n', '<br>');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.dateFrom = $('#date_from_j').data('greg') ? $('#date_from_j').data('greg') : '';
                d.dateTo = $('#date_to_j').data('greg') ? $('#date_to_j').data('greg') : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&dateFrom=${d.dateFrom}&dateTo=${d.dateTo}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#activity-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
