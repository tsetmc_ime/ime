setup_datatable = function(href) {
    let columnNames = [];
    $('#asset-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#asset-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        aaSorting: [],
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/assets/index",
            dataType: 'json',
            cache: false,
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

setup_asset_requests_datatable = function(href) {
    let columnNames = [];
    $('#asset-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#asset-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        aaSorting: [],
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/assets/asset-requests",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    if (item["تاریخ ثبت درخواست"] !== null)
                        item["تاریخ ثبت درخواست"] = new persianDate(new Date(item["تاریخ ثبت درخواست"])).format('YYYY/MM/DD');
                    if (item["تاریخ دریافت پاسخ"] !== null)
                        item["تاریخ دریافت پاسخ"] = new persianDate(new Date(item["تاریخ دریافت پاسخ"])).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.from = $('#date_from_j').data('greg') ? $('#date_from_j').data('greg') : '';
                d.to = $('#date_to_j').data('greg') ? $('#date_to_j').data('greg') : '';
                d.done = $('#response_status').val() ? $('#response_status').val() : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&from=${d.from}&to=${d.to}&done=${d.done}`);
            }
        },
        columns: columnNames,
    });
};

setup_investors_datatable = function(href) {
    let columnNames = [];
    $('#asset-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#asset-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        aaSorting: [],
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/assets/investor-assets",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#asset-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
