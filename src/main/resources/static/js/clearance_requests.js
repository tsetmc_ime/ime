setup_datatable = function (href) {
    let columnNames = [];
    $('#clearance-request-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    // Custom filtering function which will search data in column four between two values
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            let min = minDate.val();
            let max = maxDate.val();
            let date = new Date(data[2]);

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);
        }
    );

    window.table = $('#clearance-request-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        aaSorting: [],
        ajax: {
            url: window.admin ? "/clearance-requests/report" : "/clearance-requests/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {
                response.data.forEach(function (item, index) {
                    if (item["تاریخ ثبت درخواست"] !== null)
                        item["تاریخ ثبت درخواست"] = new persianDate(new Date(item["تاریخ ثبت درخواست"])).format('YYYY/MM/DD');
                    if (item["تاریخ دریافت پاسخ"] !== null)
                        item["تاریخ دریافت پاسخ"] = new persianDate(new Date(item["تاریخ دریافت پاسخ"])).format('YYYY/MM/DD');
                    if (item["تاریخ ارسال"] !== null)
                        item["تاریخ ارسال"] = new persianDate(new Date(item["تاریخ ارسال"])).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.from = $('#date_from_j').data('greg') ? $('#date_from_j').data('greg') : '';
                d.to = $('#date_to_j').data('greg') ? $('#date_to_j').data('greg') : '';
                d.finalConfirmed = $('#clearance-request-table_status').val() ? $('#clearance-request-table_status').val() : '';
                d.responseStatus = $('#clearance-response').val() ? $('#clearance-response').val() : '';
                d.clearType = $('#clearance-type').val() ? $('#clearance-type').val() : '';
                d.done = $('#response_status').val() ? $('#response_status').val() : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&from=${d.from}&to=${d.to}&finalConfirmed=${d.finalConfirmed}&responseStatus=${d.responseStatus}&clearType=${d.clearType}&done=${d.done}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#clearance-request-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
