setup_datatable = function(href) {
    let columnNames = [];
    $('#futures-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#futures-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/futures/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    const dt = item["زمان"];
                    if (dt !== null)
                        item["زمان"] = new persianDate(new Date(item["زمان"])).format('YYYY/MM/DD HH:mm:ss');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.date = $('#date').val();
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&date=${d.date}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#futures-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
