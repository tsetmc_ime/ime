setup_datatable = function(href) {
    let columnNames = [];
    $('#users-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    // Custom filtering function which will search data in column four between two values
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex) {
            let min = range.rbegin;
            let max = range.rend;
            let date = new Date( data[2] );

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);
        }
    );

    window.table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/users/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    if (item["آخرین تاریخ ورود"] !== null)
                        item["آخرین تاریخ ورود"] = new persianDate(new Date(item["آخرین تاریخ ورود"])).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#users-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
