setup_datatable = function () {
    let columnNames = [];
    $('#future-date-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#future-date-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/future-dates/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {

                response.data.forEach(function (item, index) {
                    item["تاریخ"] = new persianDate(new Date(item["تاریخ"])).format('YYYY/MM/DD');
                    if (item["تاریخ نامه"])
                        item["تاریخ نامه"] = new persianDate(new Date(item["تاریخ نامه"])).format('YYYY/MM/DD');
                    item['حذف'] = `<button type="button" class="alert button" onclick='discardFutureDeliveryDate(` + item["DT_RowId"]+`)'>
                        <i class="fa fa-trash fa-fw" aria-hidden="true"></i>
                        حذف
                    </button>`;
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;

            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#future-date-table').width('');
    window.table.columns.adjust().responsive.recalc();
};

discardFutureDeliveryDate = function(id){
    $.ajax({
        url: "/future-dates/discard/"+id,
        type: "POST",
        dataType: "text",
        cache: false,
        success: function (data) {

            $('#modal-message').text(`تاریخ تحویل آتی مورد نظر با موفقیت حذف شد`);
            window.triggerModal.open();

        },
        error: function (data) {
            $('#modal-message').text(data.responseText);
            window.triggerModal.open();
        }

    });
};

