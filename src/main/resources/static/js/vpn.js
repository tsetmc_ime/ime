setup_datatable = function () {
    let columnNames = [];
    $('#vpn-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#vpn-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/vpn/index",
            dataType: 'json',
            cache: false,
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;

            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#vpn-table').width('');
    window.table.columns.adjust().responsive.recalc();
};



