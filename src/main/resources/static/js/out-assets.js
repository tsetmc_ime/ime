setup_datatable = function(href) {
    let columnNames = [];
    $('#out-asset-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#out-asset-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        aaSorting: [],
        ajax: {
            url: window.admin ? "/out-assets/report" : "/out-assets/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {
                response.data.forEach(function (item, index) {
                    if (item["زمان ترخیص"] !== null)
                        item["زمان ترخیص"] = new persianDate(new Date(item["زمان ترخیص"])).format('YYYY/MM/DD');
                    if (item["زمان خروج نهایی"] !== null)
                        item["زمان خروج نهایی"] = new persianDate(new Date(item["زمان خروج نهایی"])).format('YYYY/MM/DD');
                    item["هزینه انبار بورسی (ریال)"] = parseFloat(item["هزینه انبار بورسی (ریال)"]).toLocaleString('fa');
                    item["هزینه انبار موقت (ریال)"] = parseFloat(item["هزینه انبار موقت (ریال)"]).toLocaleString('fa');
                    item["مالیات بر ارزش افزوده (ریال)"] = parseFloat(item["مالیات بر ارزش افزوده (ریال)"]).toLocaleString('fa');
                    item["هزینه کل (ریال)"] = parseFloat(item["هزینه کل (ریال)"]).toLocaleString('fa');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.clearanceFrom = $('#clearanceFrom_j').data('greg') ? $('#clearanceFrom_j').data('greg') : '';
                d.clearanceTo = $('#clearanceTo_j').data('greg') ? $('#clearanceTo_j').data('greg') : '';
                d.outFrom = $('#outFrom_j').data('greg') ? $('#outFrom_j').data('greg') : '';
                d.outTo = $('#putTo_j').data('greg') ? $('#putTo_j').data('greg') : '';
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&clearanceFrom=${d.clearanceFrom}&clearanceTo=${d.clearanceTo}&outFrom=${d.outFrom}&outTo=${d.outTo}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#out-asset-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
