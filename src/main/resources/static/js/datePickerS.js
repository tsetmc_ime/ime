$(document).ready(function() {
    $(".due_date_j").pDatepicker(
        {
            format: 'YYYY/MM/DD',
            autoClose: true,
            onSelect: function (unix) {
                let date = new Date(unix);
                // console.log(date);
                $(".due_date").val(date.toISOString().split('T')[0])
            }
        }
    );
});
// $( "#datepicker" ).datepicker();
// $( "#format" ).on( "change", function() {
//     $( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
// });