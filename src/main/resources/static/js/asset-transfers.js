setup_datatable = function(href) {
    let columnNames = [];
    $('#asset-transfer-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#asset-transfer-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        aaSorting: [],
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/asset-transfers/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    const dt = item["تاریخ نامه"];
                    if (dt !== null)
                        item["تاریخ نامه"] = new persianDate([parseInt(dt.substr(0,4)), parseInt(dt.substr(4,2)), parseInt(dt.substr(6,2))]).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#asset-transfer-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
