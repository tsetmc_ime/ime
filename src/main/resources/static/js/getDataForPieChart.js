getDataForPieChart = function (container, title, url, chart) {
    let hc_data = [];

    // Version 0.0.1
    if (chart == null) {
        chart = Highcharts.chart(container, {
            chart: {
                type: 'pie',
                style:
                    {
                        fontFamily: 'AlfaSans',
                        fontSize: '24px'
                    }
            },
            credits: {
                enabled: false
            },
            title: {
                text: title
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                formatter: function () {
                    return this.key + ': ' + this.y
                }
            },
            colors: ['#2C3D63', '#A6EBE7', '#F8D8D8', '#C1E6CF', '#FED67E', '#E086A6'],

            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        style: {
                            fontSize: '20px',
                            fontFamily: 'AlfaSans',
                            // fontweight: bold
                        },
                        enabled: true,
                        distance: 30,
                        formatter: function () {
                            if (this.y > 0)
                                return this.y;
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                data: hc_data,
                name: '',
                animation: false
            }]
        });
    }

    $.ajax({
        url: url,
        type: "GET",
        dataType: 'json',
        cache: false,
        success: function (data) {
            let items = Object.keys(data);
            for (let i = 0; i < items.length; i++) {
                hc_data.push({
                    name: items[i],
                    y: parseFloat(data[items[i]]),
                    sliced: false,
                    selected: false
                })
            }

            // update chart
            chart.series[0].setData(hc_data);
        }
    });

    return chart;
};
