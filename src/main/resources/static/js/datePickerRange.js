setup_jalali_datepicker = function(elem, elemHidden) {
    let pd = elem.pDatepicker({
        format: 'YYYY/MM/DD',
        autoClose: true,
        onSelect: function (unix) {
            elemHidden.val(new Date(unix).toISOString().slice(0, 10));
        }
    });
    if (elemHidden) {
        let v = elemHidden.val();
        if (v === '')
            elem.val('');
        else if (v.includes('-'))
            pd.setDate(new Date(v).getTime());
        else
            pd.setDate(new Date(`${v.substr(0, 4)}-${v.substr(4, 2)}-${v.substr(6, 2)}`).getTime());
    }
    elem.change(function () {
        if (this.value === '') {
            elem.data('greg', '');
            if (elemHidden)
                elemHidden.val('');
        }
    });
    if (elem.prop('readonly')) {
        elem.pDatepicker('destroy');
        elem.attr('data-readonly', true);
    }
    elem.prop('readonly', true);
};

setup_jalali_range_datepicker = function(from_elem, to_elem, ctx) {
    ctx.from = from_elem.pDatepicker({
        initialValue: false,
        format: 'YYYY/MM/DD',
        autoClose: true,
        onSelect: function (unix) {
            from_elem.data('greg', new Date(unix).toISOString().slice(0, 10));
            /* ctx.from.touched = true;
            if (ctx.to && ctx.to.options && ctx.to.options.minDate !== unix) {
                let cachedValue = ctx.to.getState().selected.unixDate;
                ctx.to.options = {minDate: unix};
                if (ctx.to.touched)
                    ctx.to.setDate(cachedValue);
            } */
        }
    });
    if (from_elem.prop('readonly')) {
        from_elem.pDatepicker('destroy');
        from_elem.attr('data-readonly', true);
    }
    from_elem.prop('readonly', true);

    ctx.to = to_elem.pDatepicker({
        initialValue: false,
        format: 'YYYY/MM/DD',
        autoClose: true,
        onSelect: function (unix) {
            to_elem.data('greg', new Date(unix).toISOString().slice(0, 10));
            /* ctx.to.touched = true;
            if (ctx.from && ctx.from.options && ctx.from.options.maxDate !== unix) {
                let cachedValue = ctx.from.getState().selected.unixDate;
                ctx.from.options = {maxDate: unix};
                if (ctx.from.touched)
                    ctx.from.setDate(cachedValue);
            } */
        }
    });
    if (to_elem.prop('readonly')) {
        to_elem.pDatepicker('destroy');
        to_elem.attr('data-readonly', true);
    }
    to_elem.prop('readonly', true);

};