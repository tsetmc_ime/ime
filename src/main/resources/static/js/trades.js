setup_datatable = function(href) {
    let columnNames = [];
    $('#trades-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    // Custom filtering function which will search data in column four between two values
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex) {
            let min = range.rbegin;
            let max = range.rend;
            let date = new Date( data[2] );

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);
        }
    );

    window.table = $('#trades-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/trades/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    const dt = item["تاریخ"];
                    if (dt !== null)
                        item["تاریخ"] = new persianDate([parseInt(dt.substr(0,4)), parseInt(dt.substr(4,2)), parseInt(dt.substr(6,2))]).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                const dtFrom = ctx1.from.getState().selected;
                d.tradeDateFrom = $('#date_from_j').data('greg') ? `${dtFrom.year.toString().lpad('0',4)}${dtFrom.month.toString().lpad('0',2)}${dtFrom.date.toString().lpad('0',2)}` : '';
                const dtTo = ctx1.to.getState().selected;
                d.tradeDateTo = $('#date_to_j').data('greg') ? `${dtTo.year.toString().lpad('0',4)}${dtTo.month.toString().lpad('0',2)}${dtTo.date.toString().lpad('0',2)}` : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&tradeDateFrom=${d.tradeDateFrom}&tradeDateTo=${d.tradeDateTo}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#trades-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
