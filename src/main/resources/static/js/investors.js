setup_datatable = function (href) {
    let columnNames = [];
    $('#investor-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#investor-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        aaSorting: [],
        ajax: {
            url: "/investors/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {
                response.data.forEach(function (item, index) {
                    if (item["آخرین تاریخ استعلام"] !== null)
                        item["آخرین تاریخ استعلام"] = new persianDate(new Date(item["آخرین تاریخ استعلام"])).format('YYYY/MM/DD HH:mm:ss');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.type = d.columns[5].search.value;
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&type=${d.type}`);
            }
        },
        columns: columnNames,
    });

    window.statusSelectInserted = false;

    window.table.on('draw', function () {
        if (!window.statusSelectInserted) {
            $('#investor-table_wrapper > .row:first-child > .columns.small-6').removeClass('small-6').addClass('small-4');
            $("#investor-table_wrapper > .row:first-child").append($("#filter-select"));
            window.statusSelectInserted = true;
            $('#investor-table_status').change(function () {
                window.table.columns(5).search(this.value).draw();
            });
        }
    });
};

adjustTable = function () {
    $('#investor-table').width('');
    window.table.columns.adjust().responsive.recalc();
};

setQueryResult = function(data) {
    if (typeof data !== 'object' || data == null) {
        $('#investor_ptcode,#investor_nationalId,#investor_personType,#investor_asciiPtcode').addClass('is-invalid-input').parent().addClass('is-invalid-label');

    } else {
        $('#investor_ptcode,#investor_nationalId,#investor_personType,#investor_asciiPtcode').removeClass('is-invalid-input').parent().removeClass('is-invalid-label');

        $('#ciindv-get').text(data.ptcode);
        $('#ime14-get').text(data.asciiPtcode);
        $('#firstname-get').text(data.firstName);
        $('#lastname-get').text(data.lastName);
        $('#national-id-get').text(data.nationalId);
        $('#investor_asciiPtcode').val(data.asciiPtcode);
        $('#father-get').text(data.fatherName);
        $('#bdate-get').text(data.birthDate);
        $('#idnum-get').text(data.idNumber);
        if (data.personType)
            $('#investor_personType').val(data.personType.id);
    }
};

$('#query-btn').click(function () {
    $('#query-btn').prop('disabled', true);
    $('#query-title').text('لطفاً صبر کنید...');
    $('#submit-btn').attr('disabled', true);
    $('#investor_ptcode,#investor_nationalId,#investor_personType').attr('disabled', true);

    setQueryResult({});

    $.get('/investors/query?ptcode=' + $('#investor_ptcode').val() + '&nationalCode=' + $('#investor_nationalId').val() + '&personType=' + $('#investor_personType').val(), function(data, status, xhr) {
        setQueryResult(data);
        if (data && data.ptcode)
            $('#investor_ptcode').val(data.ptcode);

    }).fail(function() {
        setQueryResult(null);

    }).always(function () {
        $('#query-btn').prop('disabled', false);
        $('#query-title').text('استعلام سهامدار');
        $('#submit-btn').attr('disabled', false);
        $('#investor_ptcode,#investor_nationalId,#investor_personType').attr('disabled', false);
    });
});
