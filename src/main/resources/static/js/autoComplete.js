auto_input = function (node, url) {
    node.autocomplete({
        minLength: 3,
        source: function (request, response) {
            let query = node.val();
            $.ajax({
                url: url + query,
                dataType: "json",
                success: function (data) {
                    let result = [];
                    for (const item of data)
                        result.push({label: item.name, value: item.name, id: item.id});
                    response(result);
                }
            });
        },
        select: function (event, ui) {
            node.val(ui.item.name);
        }
    });
};