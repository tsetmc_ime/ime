//
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="_csrf"]').attr('value'));
        }
    }
});

$(document).ajaxComplete(function(event, xhr, settings) {
    // https://stackoverflow.com/a/58360200/1994239
    if(xhr && xhr.responseText) {
        if (xhr.responseText.indexOf("LOGIN_PAGE_IDENTIFIER") !== -1)
            window.location.reload();
        else if (xhr.responseText.indexOf("ERROR_PAGE_IDENTIFIER") !== -1)
            window.location.href = "/error";
    }
});

$('.input-group .input-group-label.clear-input').click(function (e) {
    e.preventDefault();
    let inp = $(this).parent().find('input[type=text]');
    if (!inp.data('readonly'))
        inp.val('').data('greg', '').trigger('change');
}).hover( // https://css-tricks.com/snippets/jquery/addingremoving-class-on-hover/
    function() {
        let inp = $(this).parent().find('input[type=text]');
        if (!inp.data('readonly'))
            $(this).find('.fa-calendar').removeClass('fa-calendar').addClass('fa-times fa-cal');
    },
    function() {
        let inp = $(this).parent().find('input[type=text]');
        if (!inp.data('readonly'))
            $(this).find('.fa-cal').removeClass('fa-times fa-cal').addClass('fa-calendar')
    }
);

$(document).ready(function () {
    let nodes = $('ul.root-menu li.list-group-item.node');
    nodes.click(function() {
        let group = $(this).data('group');
        let family = $('ul.root-menu li.list-group-item').filter(function () {
            return $(this).data('group') === group;
        });
        let node = family.filter('.node');
        let items = family.not('.node');
        let icon = node.find('.fa');
        let is_open = icon.hasClass('fa-chevron-down');
        if (is_open) {
            items.addClass('hide');
            icon.removeClass('fa-chevron-down').addClass('fa-chevron-left');
            if (family.filter('.active').length)
                node.addClass('active-lite');
        } else {
            items.removeClass('hide');
            icon.removeClass('fa-chevron-left').addClass('fa-chevron-down');
            node.removeClass('active-lite');
        }
    });
    let group = $('ul.root-menu li.list-group-item.active').data('group');
    nodes.filter(function() { return $(this).data('group') === group; }).first().trigger('click');

    $("form").submit(function () {
        $(this).submit(false).find("[type=submit]").attr("disabled", true);
        return true;
    });
});

String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};