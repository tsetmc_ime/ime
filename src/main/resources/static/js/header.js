getDataForStorage = function (container, url) {

    $.ajax({
        url: url,
        type: "GET",
        dataType: "text",
        cache: false,
        success: function (data) {
            container.html(data);
        },
        error: function () {
            container.html('-');
        }
    });

};
