setup_datatable = function (href) {
    let columnNames = [];
    $('#storage-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#storage-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/storages/index",
            dataType: 'json',
            cache: false,
            dataSrc: function (response) {
                response.data.forEach(function (item, index) {
                    if (item["تاریخ سررسید"] !== null)
                        item["تاریخ سررسید"] = new persianDate(new Date(item["تاریخ سررسید"])).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.status = d.columns[7].search.value;
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&status=${d.status}`);
            }
        },
        columns: columnNames,
    });

    window.statusSelectInserted = false;

    window.table.on('draw', function () {
        if (!window.statusSelectInserted) {
            $('#storage-table_wrapper > .row:first-child > .columns.small-6').removeClass('small-6').addClass('small-4');
            $('#storage-table_wrapper > .row:first-child').append($("#filter-select"));
            window.statusSelectInserted = true;
            $('#storage-table_status').change(function () {
                window.table.columns(7).search(this.value).draw();
            });
        }
    });
};

setup_report_datatable = function(href) {
    let columnNames = [];
    $('#storage-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#storage-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        aaSorting: [],
        paging: true,
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/storages/report-index",
            dataType: 'json',
            cache: false,
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#storage-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
