setup_datatable = function (href) {
    let columnNames = [];
    $('#broker-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    window.table = $('#broker-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/brokers/index",
            dataType: 'json',
            cache: false,
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#broker-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
