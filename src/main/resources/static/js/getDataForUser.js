getDataForUser = function (container, title, url) {

    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (data) {
            let txt = '<div>';
            txt += `<h5>${title}</h5>`;
            _.each(data, function (v, k) {
                txt += `<p>${k}: ${v}</p>`
            });
            $("#" + container).html(txt);
        }
    });

};