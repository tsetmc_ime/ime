setup_datatable = function(href) {
    let columnNames = [];
    $('#instruments-table th').each(function (e, f) {
        columnNames.push({data: f.innerHTML});
    });

    // Custom filtering function which will search data in column four between two values
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex) {
            let min = range.rbegin;
            let max = range.rend;
            let date = new Date( data[2] );

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);
        }
    );

    window.table = $('#instruments-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        pagingType: "full_numbers",
        paging: true,
        aaSorting: [],
        language: {
            url: "/DataTables-1.10.25/Persian.lang"
        },
        ajax: {
            url: "/instruments/index",
            dataType: 'json',
            cache: false,
            dataSrc: function(response) {
                response.data.forEach(function(item, index) {
                    if (item["تاریخ سررسید"] !== null)
                        item["تاریخ سررسید"] = new persianDate(new Date(item["تاریخ سررسید"])).format('YYYY/MM/DD');
                });
                return response.data;
            },
            data: function (d) {
                d.search = d.search.value;
                d.orderKey = d.order.length ? columnNames[d.order[0].column].data : '';
                d.orderDir = d.order.length ? d.order[0].dir : '';
                d.from = $('#date_from_j').data('greg') ? $('#date_from_j').data('greg') : '';
                d.to = $('#date_to_j').data('greg') ? $('#date_to_j').data('greg') : '';
                d.columns = null;
                href.attr('href', `${href.data('href')}?search=${d.search}&orderKey=${d.orderKey}&orderDir=${d.orderDir}&from=${d.from}&to=${d.to}`);
            }
        },
        columns: columnNames,
    });
};

adjustTable = function () {
    $('#instruments-table').width('');
    window.table.columns.adjust().responsive.recalc();
};
