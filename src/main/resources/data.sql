-- delimiter $$

use ime; $$

create or replace view ime.job_view as
select
    row_number() over (ORDER BY n.id) id,
    time,
    n.name name,
    n.description name_fa,
    s.status status,
    s.description status_fa,
    comments,
    ifnull(fire, false) fire,
    case when ( enableFutureDeliveryJob = 1 and n.name = 'FUTURE_DELIVERY') or  n.name <> 'FUTURE_DELIVERY'  then true else false end as enable
from
    ime.job_name n
        left outer join (
        select
            date, fire, name, ifnull(status, 'NOT_STARTED') status
        from
            ime.job
    ) j on
        j.name = n.name
        and j.date = current_date
    join ime.job_status s on ifnull(j.status, 'NOT_STARTED') = s.status
    left outer join (
        select
            l.*,
            rank() over (partition by t.name order by l.id desc) as rnk
        from
            ime.job_log l,
            ime.job_log t
        where
            l.id = t.id
            and l.date = current_date
            and t.date = l.date
    ) l on
                n.name = l.name
            and l.rnk = 1
        left outer join (
        select 1 as enableFutureDeliveryJob
        from ime.future_delivery_date
        where Date(date) = current_date
    ) fdd on
            n.name = 'FUTURE_DELIVERY'
    where
        n.active = true
order by id
; $$

drop function if exists job_status; $$
create function job_status(job_name varchar(255))
    returns varchar(255)
    deterministic
begin
    declare str varchar(255) default null;
    select status into str from ime.job_view where name = job_name;
    return str;
end; $$

DROP PROCEDURE IF EXISTS createIndex; $$
create procedure createIndex()
begin
    declare m, n int;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'activity_log' and index_name = 'log_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'activity_log' and index_name = 'log_fultxt_idx' and column_name in ('action');
    if n <> 1 then
        if m > 0 then
            drop index log_fultxt_idx on ime.activity_log;
        end if;
        create fulltext index log_fultxt_idx on ime.activity_log(action);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'user' and index_name = 'user_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'user' and index_name = 'user_fultxt_idx' and column_name in ('fullname', 'username');
    if n <> 2 then
        if m > 0 then
            drop index user_fultxt_idx on ime.user;
        end if;
        create fulltext index user_fultxt_idx on ime.user(fullname, username);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'investor' and index_name = 'investor_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'investor' and index_name = 'investor_fultxt_idx' and column_name in ('ptcode','ascii_ptcode','national_id','first_name','last_name');
    if n <> 5 then
        if m > 0 then
            drop index investor_fultxt_idx on ime.investor;
        end if;
        create fulltext index investor_fultxt_idx on ime.investor(ptcode,ascii_ptcode,national_id,first_name,last_name);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'instrument' and index_name = 'instrument_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'instrument' and index_name = 'instrument_fultxt_idx' and column_name in ('symbol', 'symbol_desc');
    if n <> 2 then
        if m > 0 then
            drop index instrument_fultxt_idx on ime.instrument;
        end if;
        create fulltext index instrument_fultxt_idx on ime.instrument(symbol, symbol_desc);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'storage' and index_name = 'storage_name_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'storage' and index_name = 'storage_name_fultxt_idx' and column_name in ('name');
    if n <> 1 then
        if m > 0 then
            drop index storage_name_fultxt_idx on ime.storage;
        end if;
        create fulltext index storage_name_fultxt_idx on ime.storage(name);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'broker' and index_name = 'broker_name_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'broker' and index_name = 'broker_name_fultxt_idx' and column_name in ('name');
    if n <> 1 then
        if m > 0 then
            drop index broker_name_fultxt_idx on ime.broker;
        end if;
        create fulltext index broker_name_fultxt_idx on ime.broker(name);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'trade' and index_name = 'search_index';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'trade' and index_name = 'search_index' and column_name in ('symbol','b_broker_code','b_ptcode','b_ascii_ptcode','b_national_id','b_last_name','b_first_name','s_broker_code','s_ptcode','s_ascii_ptcode','s_client_code','s_national_id','s_last_name','s_first_name');
    if n <> 14 then
        if m > 0 then
            drop index search_index on ime.trade;
        end if;
        create FULLTEXT index search_index on ime.trade (symbol,b_broker_code,b_ptcode,b_ascii_ptcode,b_national_id,b_last_name,b_first_name,s_broker_code,s_ptcode,s_ascii_ptcode,s_client_code,s_national_id,s_last_name,s_first_name);
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'asset_transfer' and index_name = 'index2';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'asset_transfer' and index_name = 'index2' and column_name in ('symbol', 'ptcode');
    if n <> 2 then
        if m > 0 then
            drop index index2 on ime.asset_transfer;
        end if;
        ALTER TABLE ime.asset_transfer ADD FULLTEXT INDEX index2(symbol, ptcode) VISIBLE;
    END IF;

    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'future_delivery_date' and index_name = 'futureDeliveryDate_letterDescription_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'future_delivery_date' and index_name = 'futureDeliveryDate_letterDescription_fultxt_idx' and column_name in ('letterDescription');
    if n <> 1 then
        if m > 0 then
            drop index futureDeliveryDate_letterDescription_fultxt_idx on ime.future_delivery_date;
        end if;
        create FULLTEXT index futureDeliveryDate_letterDescription_fultxt_idx on ime.future_delivery_date(letterDescription);
    END IF;
    select count(*) into m from information_schema.statistics where table_schema = 'ime' and table_name = 'vpn' and index_name = 'vp_fultxt_idx';
    select count(*) into n from information_schema.statistics where table_schema = 'ime' and table_name = 'vpn' and index_name = 'vpn_fultxt_idx' and column_name in ('StorageName','UserName','StaticIP');
    if n <> 1 then
            if m > 0 then
    drop index vp_fultxt_idx on ime.vpn;
    end if;
            create FULLTEXT index vp_fultxt_idx on ime.vpn(StorageName,UserName,StaticIP);
    END IF;
END; $$
call createIndex(); $$
drop procedure createIndex; $$

insert ignore into ime.activity_state(id, description) values (1, 'فعال'); $$
insert ignore into ime.activity_state(id, description) values (2, 'غیر فعال'); $$
insert ignore into ime.activity_state(id, description) values (3, 'تست'); $$

insert ignore INTO ime.user_role (id, description, name) VALUES (1, 'مدیر سیستم', 'ADMIN'); $$
insert ignore INTO ime.user_role (id, description, name) VALUES (2, 'ناظر', 'SURVEILLANCE'); $$
insert ignore INTO ime.user_role (id, description, name) VALUES (3, 'کاربر سطح یک', 'USER_LEVEL_1'); $$
insert ignore INTO ime.user_role (id, description, name) VALUES (4, 'کاربر سطح دو', 'USER_LEVEL_2'); $$

insert ignore INTO ime.convert_time (id, description) VALUES (1, 'انتهای جلسه معاملاتی جاری'); $$
insert ignore INTO ime.convert_time (id, description) VALUES (2, 'انتهای جلسه معاملاتی بعد'); $$
insert ignore INTO ime.convert_time (id, description) VALUES (3, 'حین جلسه معاملاتی'); $$

insert ignore INTO ime.clearance_time (id, description) VALUES (1, 'پایان نشست معاملاتی'); $$
insert ignore INTO ime.clearance_time (id, description) VALUES (2, 'حین جلسه معاملاتی'); $$

insert ignore INTO ime.clearance_type (id, description) VALUES (1, 'دستی'); $$
insert ignore INTO ime.clearance_type (id, description) VALUES (2, 'اتوماتیک'); $$

insert ignore INTO ime.person_type (id, description) VALUES (1, 'حقیقی'); $$
insert ignore INTO ime.person_type (id, description) VALUES (2, 'غیر حقیقی'); $$

insert ignore INTO ime.clearance_response_status (id, description) VALUES (1, 'ترخیص موفق'); $$
insert ignore INTO ime.clearance_response_status (id, description) VALUES (2, 'عدم وجود کد بورسی'); $$
insert ignore INTO ime.clearance_response_status (id, description) VALUES (3, 'عدم یافتن نماد در فهرست نمادهای بورس کالا'); $$
insert ignore INTO ime.clearance_response_status (id, description) VALUES (4, 'عدم کفایت موجودی'); $$
insert ignore INTO ime.clearance_response_status (id, description) VALUES (5, 'ممنوع المعامله بودن کدبورسی'); $$

insert ignore INTO ime.asset_response_status (id, description) VALUES (1, 'تبدیل موفق'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (2, 'مشکل مرتبط با کد بورسی (عدم وجود یا غیرفعال بودن)'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (3, 'مشکل مرتبط با نماد معاملاتی'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (4, 'مانده ظرفیت انبار برای ایجاد داریی رکورد درخواست شده کفایت نمی کند.'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (5, 'کد بورسی ممنوع المعامله است'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (6, 'کارگزار ناظر اعلام شده برای گواهی سپرده در فهرست کارگزاران ناظر پیدا نشده است'); $$
insert ignore INTO ime.asset_response_status (id, description) VALUES (7, 'هزینه های مرتبط با نماد در جداول تعریف هزینه های انبارداری در سامانه پس از معاملات درج نشده است'); $$

insert ignore into job_name (id, name, description, active) VALUES (1, 'INQUIRY', 'استعلام', false); $$
insert ignore into job_name (id, name, description, active) VALUES (2, 'ASSET_TRANSFER', 'نقل و انتقالات', true); $$
insert ignore into job_name (id, name, description, active) VALUES (3, 'TRADE', 'دریافت معاملات', true); $$
insert ignore into job_name (id, name, description, active) VALUES (4, 'ASSET', 'ایجاد دارایی', true); $$
insert ignore into job_name (id, name, description, active) VALUES (5, 'CLEARANCE_REQUEST', 'ترخیص دارایی‌ها', true); $$
insert ignore into job_name (id, name, description, active) VALUES (6, 'NAZER', 'دریافت کارگزار ناظر از سرور پس از معاملات', true); $$
insert ignore into job_name (id, name, description, active) VALUES (7, 'FUTURE_DELIVERY', 'تحویل آتی / تسویه اختیار', true); $$

insert ignore into job_status (status, description) values ('NOT_STARTED', 'متوقف'); $$
insert ignore into job_status (status, description) values ('RUNNING', 'در حال اجرا'); $$
insert ignore into job_status (status, description) values ('FAILED', 'توقف با خطا'); $$
insert ignore into job_status (status, description) values ('FINISHED', 'تکمیل'); $$

INSERT ignore INTO ime.job_order (job_name_now, job_name_previous) VALUES ('CLEARANCE_REQUEST', 'ASSET'); $$
INSERT ignore INTO ime.job_order (job_name_now, job_name_previous) VALUES ('TRADE', 'ASSET_TRANSFER'); $$
INSERT ignore INTO ime.job_order (job_name_now, job_name_previous) VALUES ('NAZER', 'CLEARANCE_REQUEST'); $$
INSERT ignore INTO ime.job_order (job_name_now, job_name_previous) VALUES ('FUTURE_DELIVERY', 'NAZER'); $$
INSERT ignore INTO ime.job_order (job_name_now, job_name_previous) VALUES ('ASSET', 'TRADE'); $$

-- create administrator 'admin' with password '5iveL!fe'
insert ignore INTO ime.user (is_enabled, fullname, password, username, role_id, created_date) VALUES (true, 'مدیر سیستم', '$2a$10$w2sQkSQqfIJtjWBjLLOZj.r8/D1JFFlqbO2TR.iw8mItB7aoZnoeu', 'admin', 1, current_timestamp); $$


DROP PROCEDURE IF EXISTS jobCheck; $$
create procedure jobCheck(
    job_name varchar(255),
    new_status varchar(255),
    fire bit(1)
)
begin
    declare is_fired bit(1) default false;
    declare old_status varchar(255) default '';
    declare msg varchar(255) default '';
    declare job_in_run bit(1) default false;
    declare job_in_run_desc varchar(255) default '';
    declare job_previous_name varchar(255) default '';

    select o.job_name_previous into job_previous_name from ime.job_order o where o.job_name_now = job_name;
    if ifnull(job_previous_name,'')<>'' then
        select case when ifnull(j.status,'') <> 'FINISHED' then true else false end, j.name_fa  into job_in_run, job_in_run_desc
        from ime.job_view j
        where j.name = job_previous_name;

        if ifnull(job_in_run, false) = true then
            set msg = concat('تغییر وضعیت به علت عدم اجرا یا اتمام پیش‌نیاز «', job_in_run_desc, '» انجام نشد.');
            signal sqlstate '45000' set message_text = msg;
        end if;
    end if;


    select  true, j.name_fa  into job_in_run, job_in_run_desc
    from ime.job_view j
    where j.status <> 'FINISHED' and j.status <> 'NOT_STARTED';
    if ifnull(job_in_run, false) = true then
        set msg = concat('تغییر وضعیت این عملیات به علت عدم اتمام «', job_in_run_desc, '» انجام نشد.');
        signal sqlstate '45000' set message_text = msg;
    end if;


     select status, fire into old_status, is_fired from job_view where name = job_name;
     if new_status <> 'NOT_STARTED' then
         select concat('کارگران مشغول کار هستند. تغییر وضعیت این عملیات به «', description, '» انجام نشد.') into msg from job_status where status = new_status;
         signal sqlstate '45000' set message_text = msg;
     end if;
end; $$

drop trigger if exists job_trigger_insert; $$
create trigger job_trigger_insert
    before insert
    on ime.job
    for each row
begin
    call jobCheck(new.name, new.status, new.fire);
end; $$
drop trigger if exists job_trigger_update; $$
create trigger job_trigger_update
    before update
    on ime.job
    for each row
begin
    call jobCheck(new.name, new.status, new.fire);
end; $$


-- -------------------------------------
-- LOG INSTRUMENT INSERT --------------
-- -------------------------------------
drop trigger if exists log_instrument_insert; $$
create trigger log_instrument_insert
    after insert
    on ime.instrument
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'نماد: ', ifnull(new.symbol, ''), '\n');
    set new_str = concat(new_str, 'شرح نماد: ', ifnull(new.symbol_desc, ''), '\n');
    set new_str = concat(new_str, 'کد ۱۲ رقمی نماد: ', ifnull(new.isin, ''), '\n');
    set new_str = concat(new_str, 'مالیات بر ارزش افزوده هزینه انبارداری: ', ifnull(new.cost_vat, ''), '\n');
    set new_str = concat(new_str, 'اندازه بسته معاملاتی: ', ifnull(new.lot, ''), '\n');
    set new_str = concat(new_str, 'واحد اندازه‌گیری: ', ifnull(new.weight_unit, ''), '\n');
    set new_str = concat(new_str, 'تاریخ سررسید: ', ifnull(new.due_date, ''), '\n');
    set new_str = concat(new_str, 'تیک: ', ifnull(new.tick, ''), '\n');
    set new_str = concat(new_str, 'مالیات بر ارزش افزوده معاملات: ', ifnull(new.trades_vat, ''), '\n');
    set new_str = concat(new_str, 'زمان ترخیص: ', ifnull(new.clearance_time, ''), '\n');
    set new_str = concat(new_str, 'نوع ترخیص: ', ifnull(new.clearance_type, ''), '\n');
    set new_str = concat(new_str, 'زمان تبدیل: ', ifnull(new.convert_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('نماد ', new.id, ' (', ifnull(new.symbol,''), ') ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG INSTRUMENT UPDATE --------------
-- -------------------------------------
drop trigger if exists log_instrument_update; $$
create trigger log_instrument_update
    after update
    on ime.instrument
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.cost_vat <> old.cost_vat, true) and (new.cost_vat is not null or old.cost_vat is not null) then
        set field = 'مالیات بر ارزش افزوده هزینه انبارداری: ';
        set new_str = concat(new_str, field, ifnull(new.cost_vat, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.cost_vat, ''), '\n');
    end if;
    if ifnull(new.lot <> old.lot, true) and (new.lot is not null or old.lot is not null) then
        set field = 'اندازه بسته معاملاتی: ';
        set new_str = concat(new_str, field, ifnull(new.lot, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.lot, ''), '\n');
    end if;
    if ifnull(new.isin <> old.isin, true) and (new.isin is not null or old.isin is not null) then
        set field = 'کد ۱۲ رقمی نماد: ';
        set new_str = concat(new_str, field, ifnull(new.isin, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.isin, ''), '\n');
    end if;
    if ifnull(new.due_date <> old.due_date, true) and (new.due_date is not null or old.due_date is not null) then
        set field = 'تاریخ سررسید: ';
        set new_str = concat(new_str, field, ifnull(new.due_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.due_date, ''), '\n');
    end if;
    if ifnull(new.tick <> old.tick, true) and (new.tick is not null or old.tick is not null) then
        set field = 'تیک: ';
        set new_str = concat(new_str, field, ifnull(new.tick, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.tick, ''), '\n');
    end if;
    if ifnull(new.symbol <> old.symbol, true) and (new.symbol is not null or old.symbol is not null) then
        set field = 'نماد: ';
        set new_str = concat(new_str, field, ifnull(new.symbol, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.symbol, ''), '\n');
    end if;
    if ifnull(new.symbol_desc <> old.symbol_desc, true) and (new.symbol_desc is not null or old.symbol_desc is not null) then
        set field = 'شرح نماد: ';
        set new_str = concat(new_str, field, ifnull(new.symbol_desc, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.symbol_desc, ''), '\n');
    end if;
    if ifnull(new.trades_vat <> old.trades_vat, true) and (new.trades_vat is not null or old.trades_vat is not null) then
        set field = 'مالیات بر ارزش افزوده معاملات: ';
        set new_str = concat(new_str, field, ifnull(new.trades_vat, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.trades_vat, ''), '\n');
    end if;
    if ifnull(new.weight_unit <> old.weight_unit, true) and (new.weight_unit is not null or old.weight_unit is not null) then
        set field = 'واحد اندازه‌گیری: ';
        set new_str = concat(new_str, field, ifnull(new.weight_unit, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.weight_unit, ''), '\n');
    end if;
    if ifnull(new.clearance_time <> old.clearance_time, true) and (new.clearance_time is not null or old.clearance_time is not null) then
        set field = 'زمان ترخیص: ';
        set new_str = concat(new_str, field, ifnull(new.clearance_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.clearance_time, ''), '\n');
    end if;
    if ifnull(new.clearance_type <> old.clearance_type, true) and (new.clearance_type is not null or old.clearance_type is not null) then
        set field = 'نوع ترخیص: ';
        set new_str = concat(new_str, field, ifnull(new.clearance_type, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.clearance_type, ''), '\n');
    end if;
    if ifnull(new.convert_time <> old.convert_time, true) and (new.convert_time is not null or old.convert_time is not null) then
        set field = 'زمان تبدیل: ';
        set new_str = concat(new_str, field, ifnull(new.convert_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.convert_time, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('مشخصات نماد ', new.id, ' (', ifnull(new.symbol,''), ') تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG INSTRUMENT DELETE --------------
-- -------------------------------------
drop trigger if exists log_instrument_delete; $$
create trigger log_instrument_delete
    after delete
    on ime.instrument
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'نماد: ', ifnull(old.symbol, ''), '\n');
    set old_str = concat(old_str, 'شرح نماد: ', ifnull(old.symbol_desc, ''), '\n');
    set old_str = concat(old_str, 'کد ۱۲ رقمی نماد: ', ifnull(old.isin, ''), '\n');
    set old_str = concat(old_str, 'مالیات بر ارزش افزوده هزینه انبارداری: ', ifnull(old.cost_vat, ''), '\n');
    set old_str = concat(old_str, 'اندازه بسته معاملاتی: ', ifnull(old.lot, ''), '\n');
    set old_str = concat(old_str, 'واحد اندازه‌گیری: ', ifnull(old.weight_unit, ''), '\n');
    set old_str = concat(old_str, 'تاریخ سررسید: ', ifnull(old.due_date, ''), '\n');
    set old_str = concat(old_str, 'تیک: ', ifnull(old.tick, ''), '\n');
    set old_str = concat(old_str, 'مالیات بر ارزش افزوده معاملات: ', ifnull(old.trades_vat, ''), '\n');
    set old_str = concat(old_str, 'زمان ترخیص: ', ifnull(old.clearance_time, ''), '\n');
    set old_str = concat(old_str, 'نوع ترخیص: ', ifnull(old.clearance_type, ''), '\n');
    set old_str = concat(old_str, 'زمان تبدیل: ', ifnull(old.convert_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
        values (old.modified_by, concat('نماد ', old.id, ' (', ifnull(old.symbol,''), ') پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG STORAGE INSERT --------------
-- -------------------------------------
drop trigger if exists log_storage_insert; $$
create trigger log_storage_insert
    after insert
    on ime.storage
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد نماد: ', ifnull(new.instrument_id, ''), '\n');
    set new_str = concat(new_str, 'نام: ', ifnull(new.name, ''), '\n');
    set new_str = concat(new_str, 'ظرفیت: ', ifnull(new.max_cap, ''), '\n');
    set new_str = concat(new_str, 'هزینه انبارداری: ', ifnull(new.cost, ''), '\n');
    set new_str = concat(new_str, 'هزینه اولیه: ', ifnull(new.initial_cost, ''), '\n');
    set new_str = concat(new_str, 'آدرس: ', ifnull(new.address, ''), '\n');
    set new_str = concat(new_str, 'وضعیت: ', ifnull(new.is_active, ''), '\n');
    set new_str = concat(new_str, 'دارایی مبنای گواهی سپرده کالایی: ', ifnull(new.base_asset, ''), '\n');
    set new_str = concat(new_str, 'اولین روز معاملاتی: ', ifnull(new.first_trade_date, ''), '\n');
    set new_str = concat(new_str, 'آخرین روز معاملاتی: ', ifnull(new.last_trade_date, ''), '\n');
    set new_str = concat(new_str, 'تلفن تماس: ', ifnull(new.phone, ''), '\n');
    set new_str = concat(new_str, 'واحد قیمت: ', ifnull(new.price_unit, ''), '\n');
    set new_str = concat(new_str, 'آخرین روز نگهداری کالا: ', ifnull(new.last_storage_date, ''), '\n');
    set new_str = concat(new_str, 'مدت زمان ارزیابی کالا توسط انبار: ', ifnull(new.evaluate_time, ''), '\n');
    set new_str = concat(new_str, 'حداقل مقدار تحویلی از انبار: ', ifnull(new.min_deliverable, ''), '\n');
    set new_str = concat(new_str, 'نام انباردار: ', ifnull(new.store_keeper_name, ''), '\n');
    set new_str = concat(new_str, 'ساعت مراجعه به انبار: ', ifnull(new.working_hour, ''), '\n');
    set new_str = concat(new_str, 'زمان انبارگردانی: ', ifnull(new.warehousing_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('انبار ', new.id, ' (', ifnull(new.name,''), ') ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG STORAGE UPDATE ------------------
-- -------------------------------------
drop trigger if exists log_storage_update; $$
create trigger log_storage_update
    after update
    on ime.storage
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.instrument_id <> old.instrument_id, true) and (new.instrument_id is not null or old.instrument_id is not null) then
        set field = 'کد نماد: ';
        set new_str = concat(new_str, field, ifnull(new.instrument_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.instrument_id, ''), '\n');
    end if;
    if ifnull(new.name <> old.name, true) and (new.name is not null or old.name is not null) then
        set field = 'نام: ';
        set new_str = concat(new_str, field, ifnull(new.name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.name, ''), '\n');
    end if;
    if ifnull(new.max_cap <> old.max_cap, true) and (new.max_cap is not null or old.max_cap is not null) then
        set field = 'ظرفیت: ';
        set new_str = concat(new_str, field, ifnull(new.max_cap, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.max_cap, ''), '\n');
    end if;
    if ifnull(new.cost <> old.cost, true) and (new.cost is not null or old.cost is not null) then
        set field = 'هزینه انبارداری: ';
        set new_str = concat(new_str, field, ifnull(new.cost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.cost, ''), '\n');
    end if;
    if ifnull(new.initial_cost <> old.initial_cost, true) and (new.initial_cost is not null or old.initial_cost is not null) then
        set field = 'هزینه اولیه: ';
        set new_str = concat(new_str, field, ifnull(new.initial_cost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.initial_cost, ''), '\n');
    end if;
    if ifnull(new.address <> old.address, true) and (new.address is not null or old.address is not null) then
        set field = 'آدرس: ';
        set new_str = concat(new_str, field, ifnull(new.address, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.address, ''), '\n');
    end if;
    if ifnull(new.is_active <> old.is_active, true) and (new.is_active is not null or old.is_active is not null) then
        set field = 'وضعیت: ';
        set new_str = concat(new_str, field, ifnull(new.is_active, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.is_active, ''), '\n');
    end if;
    if ifnull(new.base_asset <> old.base_asset, true) and (new.base_asset is not null or old.base_asset is not null) then
        set field = 'دارایی مبنای گواهی سپرده کالایی: ';
        set new_str = concat(new_str, field, ifnull(new.base_asset, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.base_asset, ''), '\n');
    end if;
    if ifnull(new.first_trade_date <> old.first_trade_date, true) and (new.first_trade_date is not null or old.first_trade_date is not null) then
        set field = 'اولین روز معاملاتی: ';
        set new_str = concat(new_str, field, ifnull(new.first_trade_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.first_trade_date, ''), '\n');
    end if;
    if ifnull(new.last_trade_date <> old.last_trade_date, true) and (new.last_trade_date is not null or old.last_trade_date is not null) then
        set field = 'آخرین روز معاملاتی: ';
        set new_str = concat(new_str, field, ifnull(new.last_trade_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.last_trade_date, ''), '\n');
    end if;
    if ifnull(new.phone <> old.phone, true) and (new.phone is not null or old.phone is not null) then
        set field = 'تلفن تماس: ';
        set new_str = concat(new_str, field, ifnull(new.phone, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.phone, ''), '\n');
    end if;
    if ifnull(new.price_unit <> old.price_unit, true) and (new.price_unit is not null or old.price_unit is not null) then
        set field = 'واحد قیمت: ';
        set new_str = concat(new_str, field, ifnull(new.price_unit, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.price_unit, ''), '\n');
    end if;
    if ifnull(new.last_storage_date <> old.last_storage_date, true) and (new.last_storage_date is not null or old.last_storage_date is not null) then
        set field = 'آخرین روز نگهداری کالا: ';
        set new_str = concat(new_str, field, ifnull(new.last_storage_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.last_storage_date, ''), '\n');
    end if;
    if ifnull(new.evaluate_time <> old.evaluate_time, true) and (new.evaluate_time is not null or old.evaluate_time is not null) then
        set field = 'مدت زمان ارزیابی کالا توسط انبار: ';
        set new_str = concat(new_str, field, ifnull(new.evaluate_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.evaluate_time, ''), '\n');
    end if;
    if ifnull(new.min_deliverable <> old.min_deliverable, true) and (new.min_deliverable is not null or old.min_deliverable is not null) then
        set field = 'حداقل مقدار تحویلی از انبار: ';
        set new_str = concat(new_str, field, ifnull(new.min_deliverable, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.min_deliverable, ''), '\n');
    end if;
    if ifnull(new.store_keeper_name <> old.store_keeper_name, true) and (new.store_keeper_name is not null or old.store_keeper_name is not null) then
        set field = 'نام انباردار: ';
        set new_str = concat(new_str, field, ifnull(new.store_keeper_name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.store_keeper_name, ''), '\n');
    end if;
    if ifnull(new.working_hour <> old.working_hour, true) and (new.working_hour is not null or old.working_hour is not null) then
        set field = 'ساعت مراجعه به انبار: ';
        set new_str = concat(new_str, field, ifnull(new.working_hour, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.working_hour, ''), '\n');
    end if;
    if ifnull(new.warehousing_time <> old.warehousing_time, true) and (new.warehousing_time is not null or old.warehousing_time is not null) then
        set field = 'زمان انبارگردانی: ';
        set new_str = concat(new_str, field, ifnull(new.warehousing_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.warehousing_time, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('مشخصات انبار ', new.id, ' (', ifnull(new.name,''), ') تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG STORAGE DELETE ------------------
-- -------------------------------------
drop trigger if exists log_storage_delete; $$
create trigger log_storage_delete
    after delete
    on ime.storage
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد نماد: ', ifnull(old.instrument_id, ''), '\n');
    set old_str = concat(old_str, 'نام: ', ifnull(old.name, ''), '\n');
    set old_str = concat(old_str, 'ظرفیت: ', ifnull(old.max_cap, ''), '\n');
    set old_str = concat(old_str, 'هزینه انبارداری: ', ifnull(old.cost, ''), '\n');
    set old_str = concat(old_str, 'هزینه اولیه: ', ifnull(old.initial_cost, ''), '\n');
    set old_str = concat(old_str, 'آدرس: ', ifnull(old.address, ''), '\n');
    set old_str = concat(old_str, 'وضعیت: ', ifnull(old.is_active, ''), '\n');
    set old_str = concat(old_str, 'دارایی مبنای گواهی سپرده کالایی: ', ifnull(old.base_asset, ''), '\n');
    set old_str = concat(old_str, 'اولین روز معاملاتی: ', ifnull(old.first_trade_date, ''), '\n');
    set old_str = concat(old_str, 'آخرین روز معاملاتی: ', ifnull(old.last_trade_date, ''), '\n');
    set old_str = concat(old_str, 'تلفن تماس: ', ifnull(old.phone, ''), '\n');
    set old_str = concat(old_str, 'واحد قیمت: ', ifnull(old.price_unit, ''), '\n');
    set old_str = concat(old_str, 'آخرین روز نگهداری کالا: ', ifnull(old.last_storage_date, ''), '\n');
    set old_str = concat(old_str, 'مدت زمان ارزیابی کالا توسط انبار: ', ifnull(old.evaluate_time, ''), '\n');
    set old_str = concat(old_str, 'حداقل مقدار تحویلی از انبار: ', ifnull(old.min_deliverable, ''), '\n');
    set old_str = concat(old_str, 'نام انباردار: ', ifnull(old.store_keeper_name, ''), '\n');
    set old_str = concat(old_str, 'ساعت مراجعه به انبار: ', ifnull(old.working_hour, ''), '\n');
    set old_str = concat(old_str, 'زمان انبارگردانی: ', ifnull(old.warehousing_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
        values (old.modified_by, concat('انبار ', old.id, ' (', ifnull(old.name,''), ') پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG TEMP_ASSET INSERT ---------------
-- -------------------------------------
drop trigger if exists log_temp_asset_insert; $$
create trigger log_temp_asset_insert
    after insert
    on ime.temp_asset
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد سهامداری: ', ifnull(new.investor_id, ''), '\n');
    set new_str = concat(new_str, 'کد انبار: ', ifnull(new.storage_id, ''), '\n');
    set new_str = concat(new_str, 'مقدار: ', ifnull(new.quantity, ''), '\n');
    set new_str = concat(new_str, 'بارکد: ', ifnull(new.barcode, ''), '\n');
    set new_str = concat(new_str, 'تاریخ ورود به انبار: ', ifnull(new.entrance_date, ''), '\n');
    set new_str = concat(new_str, 'تبدیل وضعیت شده: ', ifnull(new.converted, ''), '\n');
    set new_str = concat(new_str, 'زمان تبدیل وضعیت: ', ifnull(new.convert_date, ''), '\n');
    set new_str = concat(new_str, 'کد کارگزار: ', ifnull(new.broker_id, ''), '\n');
    set new_str = concat(new_str, 'زمان تأیید: ', ifnull(new.confirm_date, ''), '\n');
    set new_str = concat(new_str, 'وضعیت تایید نهایی: ', ifnull(new.is_final_confirmed, ''), '\n');
    set new_str = concat(new_str, 'تاریخ و زمان ثبت: ', ifnull(new.register_date, ''), '\n');
    set new_str = concat(new_str, 'کاربر تایید کننده: ', ifnull(new.confirming_user, ''), '\n');
    set new_str = concat(new_str, 'کاربر ثبت کننده: ', ifnull(new.registrant_user, ''), '\n');
    set new_str = concat(new_str, 'کد تحویل آتی: ', ifnull(new.future_delivery_id, ''), '\n');
    set new_str = concat(new_str, 'شماره معامله: ', ifnull(new.trade_id, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('انبار موقت ', new.id, ' ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG TEMP_ASSET UPDATE ---------------
-- -------------------------------------
drop trigger if exists log_temp_asset_update; $$
create trigger log_temp_asset_update
    after update
    on ime.temp_asset
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.investor_id <> old.investor_id, true) and (new.investor_id is not null or old.investor_id is not null) then
        set field = 'کد سهامداری: ';
        set new_str = concat(new_str, field, ifnull(new.investor_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.investor_id, ''), '\n');
    end if;
    if ifnull(new.storage_id <> old.storage_id, true) and (new.storage_id is not null or old.storage_id is not null) then
        set field = 'کد انبار: ';
        set new_str = concat(new_str, field, ifnull(new.storage_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.storage_id, ''), '\n');
    end if;
    if ifnull(new.quantity <> old.quantity, true) and (new.quantity is not null or old.quantity is not null) then
        set field = 'مقدار: ';
        set new_str = concat(new_str, field, ifnull(new.quantity, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.quantity, ''), '\n');
    end if;
    if ifnull(new.barcode <> old.barcode, true) and (new.barcode is not null or old.barcode is not null) then
        set field = 'بارکد: ';
        set new_str = concat(new_str, field, ifnull(new.barcode, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.barcode, ''), '\n');
    end if;
    if ifnull(new.entrance_date <> old.entrance_date, true) and (new.entrance_date is not null or old.entrance_date is not null) then
        set field = 'تاریخ ورود به انبار: ';
        set new_str = concat(new_str, field, ifnull(new.entrance_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.entrance_date, ''), '\n');
    end if;
    if ifnull(new.converted <> old.converted, true) and (new.converted is not null or old.converted is not null) then
        set field = 'تبدیل وضعیت شده: ';
        set new_str = concat(new_str, field, ifnull(new.converted, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.converted, ''), '\n');
    end if;
    if ifnull(new.convert_date <> old.convert_date, true) and (new.convert_date is not null or old.convert_date is not null) then
        set field = 'زمان تبدیل وضعیت: ';
        set new_str = concat(new_str, field, ifnull(new.convert_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.convert_date, ''), '\n');
    end if;
    if ifnull(new.broker_id <> old.broker_id, true) and (new.broker_id is not null or old.broker_id is not null) then
        set field = 'کد کارگزار: ';
        set new_str = concat(new_str, field, ifnull(new.broker_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.broker_id, ''), '\n');
    end if;
    if ifnull(new.confirm_date <> old.confirm_date, true) and (new.confirm_date is not null or old.confirm_date is not null) then
        set field = 'زمان تأیید: ';
        set new_str = concat(new_str, field, ifnull(new.confirm_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.confirm_date, ''), '\n');
    end if;
    if ifnull(new.is_final_confirmed <> old.is_final_confirmed, true) and (new.is_final_confirmed is not null or old.is_final_confirmed is not null) then
        set field = 'وضعیت تایید نهایی: ';
        set new_str = concat(new_str, field, ifnull(new.is_final_confirmed, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.is_final_confirmed, ''), '\n');
    end if;
    if ifnull(new.register_date <> old.register_date, true) and (new.register_date is not null or old.register_date is not null) then
        set field = 'تاریخ و زمان ثبت: ';
        set new_str = concat(new_str, field, ifnull(new.register_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.register_date, ''), '\n');
    end if;
    if ifnull(new.confirming_user <> old.confirming_user, true) and (new.confirming_user is not null or old.confirming_user is not null) then
        set field = 'کاربر تایید کننده: ';
        set new_str = concat(new_str, field, ifnull(new.confirming_user, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.confirming_user, ''), '\n');
    end if;
    if ifnull(new.registrant_user <> old.registrant_user, true) and (new.registrant_user is not null or old.registrant_user is not null) then
        set field = 'کاربر ثبت کننده: ';
        set new_str = concat(new_str, field, ifnull(new.registrant_user, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.registrant_user, ''), '\n');
    end if;
    if ifnull(new.future_delivery_id <> old.future_delivery_id, true) and (new.future_delivery_id is not null or old.future_delivery_id is not null) then
        set field = 'کد تحویل آتی: ';
        set new_str = concat(new_str, field, ifnull(new.future_delivery_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.future_delivery_id, ''), '\n');
    end if;
    if ifnull(new.trade_id <> old.trade_id, true) and (new.trade_id is not null or old.trade_id is not null) then
        set field = 'شماره معامله: ';
        set new_str = concat(new_str, field, ifnull(new.trade_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.trade_id, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('مشخصات انبار موقت ', new.id, ' تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG TEMP_ASSET DELETE ---------------
-- -------------------------------------
drop trigger if exists log_temp_asset_delete; $$
create trigger log_temp_asset_delete
    after delete
    on ime.temp_asset
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد سهامداری: ', ifnull(old.investor_id, ''), '\n');
    set old_str = concat(old_str, 'کد انبار: ', ifnull(old.storage_id, ''), '\n');
    set old_str = concat(old_str, 'مقدار: ', ifnull(old.quantity, ''), '\n');
    set old_str = concat(old_str, 'بارکد: ', ifnull(old.barcode, ''), '\n');
    set old_str = concat(old_str, 'تاریخ ورود به انبار: ', ifnull(old.entrance_date, ''), '\n');
    set old_str = concat(old_str, 'تبدیل وضعیت شده: ', ifnull(old.converted, ''), '\n');
    set old_str = concat(old_str, 'زمان تبدیل وضعیت: ', ifnull(old.convert_date, ''), '\n');
    set old_str = concat(old_str, 'کد کارگزار: ', ifnull(old.broker_id, ''), '\n');
    set old_str = concat(old_str, 'زمان تأیید: ', ifnull(old.confirm_date, ''), '\n');
    set old_str = concat(old_str, 'وضعیت تایید نهایی: ', ifnull(old.is_final_confirmed, ''), '\n');
    set old_str = concat(old_str, 'تاریخ و زمان ثبت: ', ifnull(old.register_date, ''), '\n');
    set old_str = concat(old_str, 'کاربر تایید کننده: ', ifnull(old.confirming_user, ''), '\n');
    set old_str = concat(old_str, 'کاربر ثبت کننده: ', ifnull(old.registrant_user, ''), '\n');
    set old_str = concat(old_str, 'کد تحویل آتی: ', ifnull(old.future_delivery_id, ''), '\n');
    set old_str = concat(old_str, 'شماره معامله: ', ifnull(old.trade_id, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
        values (old.modified_by, concat('انبار موقت ', old.id, ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- ---------------------------------
-- LOG OUT_ASSET INSERT ------------
-- ---------------------------------
drop trigger if exists log_out_asset_insert; $$
create trigger log_out_asset_insert
    after insert
    on ime.out_asset
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد سهامداری: ', ifnull(new.investor_id, ''), '\n');
    set new_str = concat(new_str, 'کد انبار: ', ifnull(new.storage_id, ''), '\n');
    set new_str = concat(new_str, 'مقدار: ', ifnull(new.quantity, ''), '\n');
    set new_str = concat(new_str, 'زمان ترخیص: ', ifnull(new.clearance_date, ''), '\n');
    set new_str = concat(new_str, 'زمان خروج نهایی: ', ifnull(new.out_date, ''), '\n');
    set new_str = concat(new_str, 'هزینه انبار بورسی: ', ifnull(new.cost, ''), '\n');
    set new_str = concat(new_str, 'هزینه انبار موقت: ', ifnull(new.ocost, ''), '\n');
    set new_str = concat(new_str, 'مالیات بر ارزش افزوده: ', ifnull(new.vat, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('انبار خروجی ', new.id, ' ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG OUT_ASSET UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_out_asset_update; $$
create trigger log_out_asset_update
    after update
    on ime.out_asset
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.investor_id <> old.investor_id, true) and (new.investor_id is not null or old.investor_id is not null) then
        set field = 'کد سهامداری: ';
        set new_str = concat(new_str, field, ifnull(new.investor_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.investor_id, ''), '\n');
    end if;
    if ifnull(new.storage_id <> old.storage_id, true) and (new.storage_id is not null or old.storage_id is not null) then
        set field = 'کد انبار: ';
        set new_str = concat(new_str, field, ifnull(new.storage_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.storage_id, ''), '\n');
    end if;
    if ifnull(new.quantity <> old.quantity, true) and (new.quantity is not null or old.quantity is not null) then
        set field = 'مقدار: ';
        set new_str = concat(new_str, field, ifnull(new.quantity, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.quantity, ''), '\n');
    end if;
    if ifnull(new.clearance_date <> old.clearance_date, true) and (new.clearance_date is not null or old.clearance_date is not null) then
        set field = 'زمان ترخیص: ';
        set new_str = concat(new_str, field, ifnull(new.clearance_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.clearance_date, ''), '\n');
    end if;
    if ifnull(new.out_date <> old.out_date, true) and (new.out_date is not null or old.out_date is not null) then
        set field = 'زمان خروج نهایی: ';
        set new_str = concat(new_str, field, ifnull(new.out_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.out_date, ''), '\n');
    end if;
    if ifnull(new.cost <> old.cost, true) and (new.cost is not null or old.cost is not null) then
        set field = 'هزینه انبار بورسی: ';
        set new_str = concat(new_str, field, ifnull(new.cost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.cost, ''), '\n');
    end if;
    if ifnull(new.ocost <> old.ocost, true) and (new.ocost is not null or old.ocost is not null) then
        set field = 'هزینه انبار موقت: ';
        set new_str = concat(new_str, field, ifnull(new.ocost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.ocost, ''), '\n');
    end if;
    if ifnull(new.vat <> old.vat, true) and (new.vat is not null or old.vat is not null) then
        set field = 'مالیات بر ارزش افزوده: ';
        set new_str = concat(new_str, field, ifnull(new.vat, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.vat, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('مشخصات انبار خروجی ', new.id, ' تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG OUT_ASSET DELETE ----------------
-- -------------------------------------
drop trigger if exists log_out_asset_delete; $$
create trigger log_out_asset_delete
    after delete
    on ime.out_asset
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد سهامداری: ', ifnull(old.investor_id, ''), '\n');
    set old_str = concat(old_str, 'کد انبار: ', ifnull(old.storage_id, ''), '\n');
    set old_str = concat(old_str, 'مقدار: ', ifnull(old.quantity, ''), '\n');
    set old_str = concat(old_str, 'زمان ترخیص: ', ifnull(old.clearance_date, ''), '\n');
    set old_str = concat(old_str, 'زمان خروج نهایی: ', ifnull(old.out_date, ''), '\n');
    set old_str = concat(old_str, 'هزینه انبار بورسی: ', ifnull(old.cost, ''), '\n');
    set old_str = concat(old_str, 'هزینه انبار موقت: ', ifnull(old.ocost, ''), '\n');
    set old_str = concat(old_str, 'مالیات بر ارزش افزوده: ', ifnull(old.vat, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
        values (old.modified_by, concat('انبار خروجی ', old.id, ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG ‌BROKER INSERT --------------
-- -------------------------------------
drop trigger if exists log_broker_insert; $$
create trigger log_broker_insert
    after insert
    on ime.broker
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد: ', ifnull(new.code, ''), '\n');
    set new_str = concat(new_str, 'نام: ', ifnull(new.name, ''), '\n');
    set new_str = concat(new_str, 'وضعیت: ', ifnull(new.enabled, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('کارگزاری ', new.id, ' (', ifnull(new.name,''), ' کد ', ifnull(new.code,''), ') ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG BROKER UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_broker_update; $$
create trigger log_broker_update
    after update
    on ime.broker
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.code <> old.code, true) and (new.code is not null or old.code is not null) then
        set field = 'کد: ';
        set new_str = concat(new_str, field, ifnull(new.code, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.code, ''), '\n');
    end if;
    if ifnull(new.name <> old.name, true) and (new.name is not null or old.name is not null) then
        set field = 'نام: ';
        set new_str = concat(new_str, field, ifnull(new.name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.name, ''), '\n');
    end if;
    if ifnull(new.enabled <> old.enabled, true) and (new.enabled is not null or old.enabled is not null) then
        set field = 'وضعیت: ';
        set new_str = concat(new_str, field, ifnull(new.enabled, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.enabled, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('کارگزاری ', new.id, ' (', ifnull(new.name,''), ' کد ', ifnull(new.code,''), ') تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG BROKER DELETE ----------------
-- -------------------------------------
drop trigger if exists log_broker_delete; $$
create trigger log_broker_delete
    after delete
    on ime.broker
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد: ', ifnull(old.code, ''), '\n');
    set old_str = concat(old_str, 'نام: ', ifnull(old.name, ''), '\n');
    set old_str = concat(old_str, 'وضعیت: ', ifnull(old.enabled, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
    values (old.modified_by, concat('کارگزاری ', old.id, ' (', ifnull(old.name,''), ' کد ', ifnull(old.code,''), ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG INVESTOR INSERT --------------
-- -------------------------------------
drop trigger if exists log_investor_insert; $$
create trigger log_investor_insert
    after insert
    on ime.investor
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'نوع: ', ifnull(new.person_type, ''), '\n');
    set new_str = concat(new_str, 'کد بورسی: ', ifnull(new.ptcode, ''), '\n');
    set new_str = concat(new_str, 'کد ۱۴ رقمی: ', ifnull(new.ascii_ptcode, ''), '\n');
    set new_str = concat(new_str, 'کد ملی: ', ifnull(new.national_id, ''), '\n');
    set new_str = concat(new_str, 'نام: ', ifnull(new.first_name, ''), '\n');
    set new_str = concat(new_str, 'نام خانوادگی: ', ifnull(new.last_name, ''), '\n');
    set new_str = concat(new_str, 'شماره شناسنامه: ', ifnull(new.id_number, ''), '\n');
    set new_str = concat(new_str, 'تاریخ تولد: ', ifnull(new.birth_date, ''), '\n');
    set new_str = concat(new_str, 'تاریخ تولد میلادی: ', ifnull(new.birth_date_greg, ''), '\n');
    set new_str = concat(new_str, 'نام پدر: ', ifnull(new.father_name, ''), '\n');
    set new_str = concat(new_str, 'سری شناسنامه: ', ifnull(new.id_serie, ''), '\n');
    set new_str = concat(new_str, 'شماره سری شناسنامه: ', ifnull(new.id_serial, ''), '\n');
    set new_str = concat(new_str, 'زمان ثبت درخواست: ', ifnull(new.issue_place, ''), '\n');
    set new_str = concat(new_str, 'وضعیت: ', ifnull(new.activity_state, ''), '\n');
    set new_str = concat(new_str, 'تلفن تماس: ', ifnull(new.phone, ''), '\n');
    set new_str = concat(new_str, 'آدرس: ', ifnull(new.address, ''), '\n');
    set new_str = concat(new_str, 'شماره حساب: ', ifnull(new.account_number, ''), '\n');
    set new_str = concat(new_str, 'آخرین تاریخ استعلام: ', ifnull(new.last_update_date, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('سهامدار ', new.id, ' (', ifnull(new.first_name,''), ' ', ifnull(new.last_name,''), ') ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG INVESTOR UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_investor_update; $$
create trigger log_investor_update
    after update
    on ime.investor
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.person_type <> old.person_type, true) and (new.person_type is not null or old.person_type is not null) then
        set field = 'نوع: ';
        set new_str = concat(new_str, field, ifnull(new.person_type, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.person_type, ''), '\n');
    end if;
    if ifnull(new.ptcode <> old.ptcode, true) and (new.ptcode is not null or old.ptcode is not null) then
        set field = 'کد بورسی: ';
        set new_str = concat(new_str, field, ifnull(new.ptcode, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.ptcode, ''), '\n');
    end if;
    if ifnull(new.ascii_ptcode <> old.ascii_ptcode, true) and (new.ascii_ptcode is not null or old.ascii_ptcode is not null) then
        set field = 'کد ۱۴ رقمی: ';
        set new_str = concat(new_str, field, ifnull(new.ascii_ptcode, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.ascii_ptcode, ''), '\n');
    end if;
    if ifnull(new.national_id <> old.national_id, true) and (new.national_id is not null or old.national_id is not null) then
        set field = 'کد ملی: ';
        set new_str = concat(new_str, field, ifnull(new.national_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.national_id, ''), '\n');
    end if;
    if ifnull(new.first_name <> old.first_name, true) and (new.first_name is not null or old.first_name is not null) then
        set field = 'نام: ';
        set new_str = concat(new_str, field, ifnull(new.first_name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.first_name, ''), '\n');
    end if;
    if ifnull(new.last_name <> old.last_name, true) and (new.last_name is not null or old.last_name is not null) then
        set field = 'نام خانوادگی: ';
        set new_str = concat(new_str, field, ifnull(new.last_name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.last_name, ''), '\n');
    end if;
    if ifnull(new.id_number <> old.id_number, true) and (new.id_number is not null or old.id_number is not null) then
        set field = 'شماره شناسنامه: ';
        set new_str = concat(new_str, field, ifnull(new.id_number, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.id_number, ''), '\n');
    end if;
    if ifnull(new.birth_date <> old.birth_date, true) and (new.birth_date is not null or old.birth_date is not null) then
        set field = 'تاریخ تولد: ';
        set new_str = concat(new_str, field, ifnull(new.birth_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.birth_date, ''), '\n');
    end if;
    if ifnull(new.birth_date_greg <> old.birth_date_greg, true) and (new.birth_date_greg is not null or old.birth_date_greg is not null) then
        set field = 'تاریخ تولد میلادی: ';
        set new_str = concat(new_str, field, ifnull(new.birth_date_greg, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.birth_date_greg, ''), '\n');
    end if;
    if ifnull(new.father_name <> old.father_name, true) and (new.father_name is not null or old.father_name is not null) then
        set field = 'نام پدر: ';
        set new_str = concat(new_str, field, ifnull(new.father_name, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.father_name, ''), '\n');
    end if;
    if ifnull(new.id_serie <> old.id_serie, true) and (new.id_serie is not null or old.id_serie is not null) then
        set field = 'سری شناسنامه: ';
        set new_str = concat(new_str, field, ifnull(new.id_serie, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.id_serie, ''), '\n');
    end if;
    if ifnull(new.id_serial <> old.id_serial, true) and (new.id_serial is not null or old.id_serial is not null) then
        set field = 'شماره سری شناسنامه: ';
        set new_str = concat(new_str, field, ifnull(new.id_serial, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.id_serial, ''), '\n');
    end if;
    if ifnull(new.issue_place <> old.issue_place, true) and (new.issue_place is not null or old.issue_place is not null) then
        set field = 'زمان ثبت درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.issue_place, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.issue_place, ''), '\n');
    end if;
    if ifnull(new.activity_state <> old.activity_state, true) and (new.activity_state is not null or old.activity_state is not null) then
        set field = 'وضعیت: ';
        set new_str = concat(new_str, field, ifnull(new.activity_state, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.activity_state, ''), '\n');
    end if;
    if ifnull(new.phone <> old.phone, true) and (new.phone is not null or old.phone is not null) then
        set field = 'تلفن تماس: ';
        set new_str = concat(new_str, field, ifnull(new.phone, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.phone, ''), '\n');
    end if;
    if ifnull(new.address <> old.address, true) and (new.address is not null or old.address is not null) then
        set field = 'آدرس: ';
        set new_str = concat(new_str, field, ifnull(new.address, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.address, ''), '\n');
    end if;
    if ifnull(new.account_number <> old.account_number, true) and (new.account_number is not null or old.account_number is not null) then
        set field = 'شماره حساب: ';
        set new_str = concat(new_str, field, ifnull(new.account_number, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.account_number, ''), '\n');
    end if;
    if ifnull(new.last_update_date <> old.last_update_date, true) and (new.last_update_date is not null or old.last_update_date is not null) then
        set field = 'آخرین تاریخ استعلام: ';
        set new_str = concat(new_str, field, ifnull(new.last_update_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.last_update_date, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
    values (new.modified_by, concat('سهامدار ', new.id, ' (', ifnull(new.first_name,''), ' ', ifnull(new.last_name,''), ') تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG INVESTOR DELETE ----------------
-- -------------------------------------
drop trigger if exists log_investor_delete; $$
create trigger log_investor_delete
    after delete
    on ime.investor
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'نوع: ', ifnull(old.person_type, ''), '\n');
    set old_str = concat(old_str, 'کد بورسی: ', ifnull(old.ptcode, ''), '\n');
    set old_str = concat(old_str, 'کد ۱۴ رقمی: ', ifnull(old.ascii_ptcode, ''), '\n');
    set old_str = concat(old_str, 'کد ملی: ', ifnull(old.national_id, ''), '\n');
    set old_str = concat(old_str, 'نام: ', ifnull(old.first_name, ''), '\n');
    set old_str = concat(old_str, 'نام خانوادگی: ', ifnull(old.last_name, ''), '\n');
    set old_str = concat(old_str, 'شماره شناسنامه: ', ifnull(old.id_number, ''), '\n');
    set old_str = concat(old_str, 'تاریخ تولد: ', ifnull(old.birth_date, ''), '\n');
    set old_str = concat(old_str, 'تاریخ تولد میلادی: ', ifnull(old.birth_date_greg, ''), '\n');
    set old_str = concat(old_str, 'نام پدر: ', ifnull(old.father_name, ''), '\n');
    set old_str = concat(old_str, 'سری شناسنامه: ', ifnull(old.id_serie, ''), '\n');
    set old_str = concat(old_str, 'شماره سری شناسنامه: ', ifnull(old.id_serial, ''), '\n');
    set old_str = concat(old_str, 'زمان ثبت درخواست: ', ifnull(old.issue_place, ''), '\n');
    set old_str = concat(old_str, 'وضعیت: ', ifnull(old.activity_state, ''), '\n');
    set old_str = concat(old_str, 'تلفن تماس: ', ifnull(old.phone, ''), '\n');
    set old_str = concat(old_str, 'آدرس: ', ifnull(old.address, ''), '\n');
    set old_str = concat(old_str, 'شماره حساب: ', ifnull(old.account_number, ''), '\n');
    set old_str = concat(old_str, 'آخرین تاریخ استعلام: ', ifnull(old.last_update_date, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
    values (old.modified_by, concat('سهامدار ', old.id, ' (', ifnull(old.first_name,''), ' ', ifnull(old.last_name,''), ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG CLEARANCE_REQUEST INSERT --------------
-- -------------------------------------
drop trigger if exists log_clearance_request_insert; $$
create trigger log_clearance_request_insert
    after insert
    on ime.clearance_request
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد سهامداری: ', ifnull(new.investor_id, ''), '\n');
    set new_str = concat(new_str, 'کد انبار: ', ifnull(new.storage_id, ''), '\n');
    set new_str = concat(new_str, 'مقدار: ', ifnull(new.quantity, ''), '\n');
    set new_str = concat(new_str, 'پاسخ: ', ifnull(new.description, ''), '\n');
    set new_str = concat(new_str, 'تاریخ ثبت درخواست: ', ifnull(new.request_date, ''), '\n');
    set new_str = concat(new_str, 'زمان ثبت درخواست: ', ifnull(new.request_time, ''), '\n');
    set new_str = concat(new_str, 'تاریخ دریافت پاسخ: ', ifnull(new.response_date, ''), '\n');
    set new_str = concat(new_str, 'زمان دریافت پاسخ: ', ifnull(new.response_time, ''), '\n');
    set new_str = concat(new_str, 'وضعیت پاسخ: ', ifnull(new.response_status, ''), '\n');
    set new_str = concat(new_str, 'شماره معامله: ', ifnull(new.trade_id, ''), '\n');
    set new_str = concat(new_str, 'هزینه انبارداری: ', ifnull(new.storage_cost, ''), '\n');
    set new_str = concat(new_str, 'هزینه خروج: ', ifnull(new.ex_cost, ''), '\n');
    set new_str = concat(new_str, 'وضعیت تایید نهایی: ', ifnull(new.is_final_confirmed, ''), '\n');
    set new_str = concat(new_str, 'تاریخ ثبت درخواست شمسی: ', ifnull(new.persian_request_date, ''), '\n');
    set new_str = concat(new_str, 'تاریخ دریافت پاسخ شمسی: ', ifnull(new.persian_response_date, ''), '\n');
    set new_str = concat(new_str, 'شماره درخواست: ', ifnull(new.request_number, ''), '\n');
    set new_str = concat(new_str, 'کد تحویل آتی: ', ifnull(new.future_delivery_id, ''), '\n');
    set new_str = concat(new_str, 'کاربر ثبت کننده: ', ifnull(new.registrant_user, ''), '\n');
    set new_str = concat(new_str, 'تاریخ و زمان ثبت: ', ifnull(new.register_date, ''), '\n');
    set new_str = concat(new_str, 'کاربر تایید کننده: ', ifnull(new.confirming_user, ''), '\n');
    set new_str = concat(new_str, 'تایخ تایید: ', ifnull(new.confirm_date, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('درخواست ترخیص ', new.id, ' ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG CLEARANCE_REQUEST UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_clearance_request_update; $$
create trigger log_clearance_request_update
    after update
    on ime.clearance_request
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.investor_id <> old.investor_id, true) and (new.investor_id is not null or old.investor_id is not null) then
        set field = 'کد سهامداری: ';
        set new_str = concat(new_str, field, ifnull(new.investor_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.investor_id, ''), '\n');
    end if;
    if ifnull(new.storage_id <> old.storage_id, true) and (new.storage_id is not null or old.storage_id is not null) then
        set field = 'کد انبار: ';
        set new_str = concat(new_str, field, ifnull(new.storage_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.storage_id, ''), '\n');
    end if;
    if ifnull(new.quantity <> old.quantity, true) and (new.quantity is not null or old.quantity is not null) then
        set field = 'مقدار: ';
        set new_str = concat(new_str, field, ifnull(new.quantity, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.quantity, ''), '\n');
    end if;
    if ifnull(new.description <> old.description, true) and (new.description is not null or old.description is not null) then
        set field = 'پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.description, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.description, ''), '\n');
    end if;
    if ifnull(new.request_date <> old.request_date, true) and (new.request_date is not null or old.request_date is not null) then
        set field = 'تاریخ ثبت درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_date, ''), '\n');
    end if;
    if ifnull(new.request_time <> old.request_time, true) and (new.request_time is not null or old.request_time is not null) then
        set field = 'زمان ثبت درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_time, ''), '\n');
    end if;
    if ifnull(new.response_date <> old.response_date, true) and (new.response_date is not null or old.response_date is not null) then
        set field = 'تاریخ دریافت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_date, ''), '\n');
    end if;
    if ifnull(new.response_time <> old.response_time, true) and (new.response_time is not null or old.response_time is not null) then
        set field = 'زمان دریافت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_time, ''), '\n');
    end if;
    if ifnull(new.response_status <> old.response_status, true) and (new.response_status is not null or old.response_status is not null) then
        set field = 'وضعیت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_status, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_status, ''), '\n');
    end if;
    if ifnull(new.trade_id <> old.trade_id, true) and (new.trade_id is not null or old.trade_id is not null) then
        set field = 'شماره معامله: ';
        set new_str = concat(new_str, field, ifnull(new.trade_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.trade_id, ''), '\n');
    end if;
    if ifnull(new.storage_cost <> old.storage_cost, true) and (new.storage_cost is not null or old.storage_cost is not null) then
        set field = 'هزینه انبارداری: ';
        set new_str = concat(new_str, field, ifnull(new.storage_cost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.storage_cost, ''), '\n');
    end if;
    if ifnull(new.ex_cost <> old.ex_cost, true) and (new.ex_cost is not null or old.ex_cost is not null) then
        set field = 'هزینه خروج: ';
        set new_str = concat(new_str, field, ifnull(new.ex_cost, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.ex_cost, ''), '\n');
    end if;
    if ifnull(new.is_final_confirmed <> old.is_final_confirmed, true) and (new.is_final_confirmed is not null or old.is_final_confirmed is not null) then
        set field = 'وضعیت تایید نهایی: ';
        set new_str = concat(new_str, field, ifnull(new.is_final_confirmed, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.is_final_confirmed, ''), '\n');
    end if;
    if ifnull(new.persian_request_date <> old.persian_request_date, true) and (new.persian_request_date is not null or old.persian_request_date is not null) then
        set field = 'تاریخ ثبت درخواست شمسی: ';
        set new_str = concat(new_str, field, ifnull(new.persian_request_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.persian_request_date, ''), '\n');
    end if;
    if ifnull(new.persian_response_date <> old.persian_response_date, true) and (new.persian_response_date is not null or old.persian_response_date is not null) then
        set field = 'تاریخ دریافت پاسخ شمسی: ';
        set new_str = concat(new_str, field, ifnull(new.persian_response_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.persian_response_date, ''), '\n');
    end if;
    if ifnull(new.request_number <> old.request_number, true) and (new.request_number is not null or old.request_number is not null) then
        set field = 'شماره درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_number, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_number, ''), '\n');
    end if;
    if ifnull(new.future_delivery_id <> old.future_delivery_id, true) and (new.future_delivery_id is not null or old.future_delivery_id is not null) then
        set field = 'کد تحویل آتی: ';
        set new_str = concat(new_str, field, ifnull(new.future_delivery_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.future_delivery_id, ''), '\n');
    end if;
    if ifnull(new.registrant_user <> old.registrant_user, true) and (new.registrant_user is not null or old.registrant_user is not null) then
        set field = 'کاربر ثبت کننده: ';
        set new_str = concat(new_str, field, ifnull(new.registrant_user, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.registrant_user, ''), '\n');
    end if;
    if ifnull(new.register_date <> old.register_date, true) and (new.register_date is not null or old.register_date is not null) then
        set field = 'تاریخ و زمان ثبت: ';
        set new_str = concat(new_str, field, ifnull(new.register_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.register_date, ''), '\n');
    end if;
    if ifnull(new.confirming_user <> old.confirming_user, true) and (new.confirming_user is not null or old.confirming_user is not null) then
        set field = 'کاربر تایید کننده: ';
        set new_str = concat(new_str, field, ifnull(new.confirming_user, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.confirming_user, ''), '\n');
    end if;
    if ifnull(new.confirm_date <> old.confirm_date, true) and (new.confirm_date is not null or old.confirm_date is not null) then
        set field = 'تایخ تایید: ';
        set new_str = concat(new_str, field, ifnull(new.confirm_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.confirm_date, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('درخواست ترخیص ', new.id, ' تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG CLEARANCE_REQUEST DELETE ----------------
-- -------------------------------------
drop trigger if exists log_clearance_request_delete; $$
create trigger log_clearance_request_delete
    after delete
    on ime.clearance_request
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد سهامداری: ', ifnull(old.investor_id, ''), '\n');
    set old_str = concat(old_str, 'کد انبار: ', ifnull(old.storage_id, ''), '\n');
    set old_str = concat(old_str, 'مقدار: ', ifnull(old.quantity, ''), '\n');
    set old_str = concat(old_str, 'پاسخ: ', ifnull(old.description, ''), '\n');
    set old_str = concat(old_str, 'تاریخ ثبت درخواست: ', ifnull(old.request_date, ''), '\n');
    set old_str = concat(old_str, 'زمان ثبت درخواست: ', ifnull(old.request_time, ''), '\n');
    set old_str = concat(old_str, 'تاریخ دریافت پاسخ: ', ifnull(old.response_date, ''), '\n');
    set old_str = concat(old_str, 'زمان دریافت پاسخ: ', ifnull(old.response_time, ''), '\n');
    set old_str = concat(old_str, 'وضعیت پاسخ: ', ifnull(old.response_status, ''), '\n');
    set old_str = concat(old_str, 'شماره معامله: ', ifnull(old.trade_id, ''), '\n');
    set old_str = concat(old_str, 'هزینه انبارداری: ', ifnull(old.storage_cost, ''), '\n');
    set old_str = concat(old_str, 'هزینه خروج: ', ifnull(old.ex_cost, ''), '\n');
    set old_str = concat(old_str, 'وضعیت تایید نهایی: ', ifnull(old.is_final_confirmed, ''), '\n');
    set old_str = concat(old_str, 'تاریخ ثبت درخواست شمسی: ', ifnull(old.persian_request_date, ''), '\n');
    set old_str = concat(old_str, 'تاریخ دریافت پاسخ شمسی: ', ifnull(old.persian_response_date, ''), '\n');
    set old_str = concat(old_str, 'شماره درخواست: ', ifnull(old.request_number, ''), '\n');
    set old_str = concat(old_str, 'کد تحویل آتی: ', ifnull(old.future_delivery_id, ''), '\n');
    set old_str = concat(old_str, 'کاربر ثبت کننده: ', ifnull(old.registrant_user, ''), '\n');
    set old_str = concat(old_str, 'تاریخ و زمان ثبت: ', ifnull(old.register_date, ''), '\n');
    set old_str = concat(old_str, 'کاربر تایید کننده: ', ifnull(old.confirming_user, ''), '\n');
    set old_str = concat(old_str, 'تایخ تایید: ', ifnull(old.confirm_date, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
        values (old.modified_by, concat('درخواست ترخیص ', old.id, ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG ASSET_REQUEST INSERT --------------
-- -------------------------------------
drop trigger if exists log_asset_request_insert; $$
create trigger log_asset_request_insert
    after insert
    on ime.asset_request
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'کد دارایی موقت: ', ifnull(new.temp_asset_id, ''), '\n');
    set new_str = concat(new_str, 'تاریخ ثبت درخواست: ', ifnull(new.request_date, ''), '\n');
    set new_str = concat(new_str, 'تاریخ دریافت پاسخ: ', ifnull(new.response_date, ''), '\n');
    set new_str = concat(new_str, 'وضعیت پاسخ: ', ifnull(new.response_status, ''), '\n');
    set new_str = concat(new_str, 'تاریخ ثبت درخواست شمسی: ', ifnull(new.persian_request_date, ''), '\n');
    set new_str = concat(new_str, 'تاریخ دریافت پاسخ شمسی: ', ifnull(new.persian_response_date, ''), '\n');
    set new_str = concat(new_str, 'شماره درخواست: ', ifnull(new.request_number, ''), '\n');
    set new_str = concat(new_str, 'زمان ثبت درخواست: ', ifnull(new.request_time, ''), '\n');
    set new_str = concat(new_str, 'زمان دریافت پاسخ: ', ifnull(new.response_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('درخواست ایجاد دارایی ', new.id, ' ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG ASSET_REQUEST UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_asset_request_update; $$
create trigger log_asset_request_update
    after update
    on ime.asset_request
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.temp_asset_id <> old.temp_asset_id, true) and (new.temp_asset_id is not null or old.temp_asset_id is not null) then
        set field = 'کد دارایی موقت: ';
        set new_str = concat(new_str, field, ifnull(new.temp_asset_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.temp_asset_id, ''), '\n');
    end if;
    if ifnull(new.request_date <> old.request_date, true) and (new.request_date is not null or old.request_date is not null) then
        set field = 'تاریخ ثبت درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_date, ''), '\n');
    end if;
    if ifnull(new.response_date <> old.response_date, true) and (new.response_date is not null or old.response_date is not null) then
        set field = 'تاریخ دریافت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_date, ''), '\n');
    end if;
    if ifnull(new.response_status <> old.response_status, true) and (new.response_status is not null or old.response_status is not null) then
        set field = 'وضعیت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_status, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_status, ''), '\n');
    end if;
    if ifnull(new.persian_request_date <> old.persian_request_date, true) and (new.persian_request_date is not null or old.persian_request_date is not null) then
        set field = 'تاریخ ثبت درخواست شمسی: ';
        set new_str = concat(new_str, field, ifnull(new.persian_request_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.persian_request_date, ''), '\n');
    end if;
    if ifnull(new.persian_response_date <> old.persian_response_date, true) and (new.persian_response_date is not null or old.persian_response_date is not null) then
        set field = 'تاریخ دریافت پاسخ شمسی: ';
        set new_str = concat(new_str, field, ifnull(new.persian_response_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.persian_response_date, ''), '\n');
    end if;
    if ifnull(new.request_number <> old.request_number, true) and (new.request_number is not null or old.request_number is not null) then
        set field = 'شماره درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_number, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_number, ''), '\n');
    end if;
    if ifnull(new.request_time <> old.request_time, true) and (new.request_time is not null or old.request_time is not null) then
        set field = 'زمان ثبت درخواست: ';
        set new_str = concat(new_str, field, ifnull(new.request_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.request_time, ''), '\n');
    end if;
    if ifnull(new.response_time <> old.response_time, true) and (new.response_time is not null or old.response_time is not null) then
        set field = 'زمان دریافت پاسخ: ';
        set new_str = concat(new_str, field, ifnull(new.response_time, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.response_time, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
      values (new.modified_by, concat('درخواست ایجاد دارایی ', new.id, ' تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG ASSET_REQUEST DELETE ----------------
-- -------------------------------------
drop trigger if exists log_asset_request_delete; $$
create trigger log_asset_request_delete
    after delete
    on ime.asset_request
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'کد دارایی موقت: ', ifnull(old.temp_asset_id, ''), '\n');
    set old_str = concat(old_str, 'تاریخ ثبت درخواست: ', ifnull(old.request_date, ''), '\n');
    set old_str = concat(old_str, 'تاریخ دریافت پاسخ: ', ifnull(old.response_date, ''), '\n');
    set old_str = concat(old_str, 'وضعیت پاسخ: ', ifnull(old.response_status, ''), '\n');
    set old_str = concat(old_str, 'تاریخ ثبت درخواست شمسی: ', ifnull(old.persian_request_date, ''), '\n');
    set old_str = concat(old_str, 'تاریخ دریافت پاسخ شمسی: ', ifnull(old.persian_response_date, ''), '\n');
    set old_str = concat(old_str, 'شماره درخواست: ', ifnull(old.request_number, ''), '\n');
    set old_str = concat(old_str, 'زمان ثبت درخواست: ', ifnull(old.request_time, ''), '\n');
    set old_str = concat(old_str, 'زمان دریافت پاسخ: ', ifnull(old.response_time, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
    values (old.modified_by, concat('درخواست ایجاد دارایی ', old.id, ' پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------


-- -------------------------------------
-- LOG USER INSERT --------------
-- -------------------------------------
drop trigger if exists log_user_insert; $$
create trigger log_user_insert
    after insert
    on ime.user
    for each row
begin
    declare new_str varchar(2048) default '';

    set new_str = concat(new_str, 'نام کاربری: ', ifnull(new.username, ''), '\n');
    set new_str = concat(new_str, 'نام و نام خانوادگی: ', ifnull(new.fullname, ''), '\n');
    set new_str = concat(new_str, 'نشانی ایمیل: ', ifnull(new.email, ''), '\n');
    set new_str = concat(new_str, 'تلفن تماس: ', ifnull(new.phone, ''), '\n');
    set new_str = concat(new_str, 'کد انبار: ', ifnull(new.storage_id, ''), '\n');
    set new_str = concat(new_str, 'وضعیت: ', ifnull(new.is_enabled, ''), '\n');
    set new_str = concat(new_str, 'آخرین تاریخ ورود: ', ifnull(new.last_login_date, ''), '\n');
    set new_str = concat(new_str, 'کد نقش: ', ifnull(new.role_id, ''), '\n');

    insert into activity_log (`user_id`, `action`, `new_values`)
        values (new.created_by, concat('کاربر ', new.id, ' (', ifnull(new.username,''), ') ایجاد شد'), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG USER UPDATE ----------------
-- -------------------------------------
drop trigger if exists log_user_update; $$
create trigger log_user_update
    after update
    on ime.user
    for each row
begin
    declare new_str varchar(2048) default '';
    declare old_str varchar(2048) default '';
    declare field varchar(100) default '';

    if ifnull(new.username <> old.username, true) and (new.username is not null or old.username is not null) then
        set field = 'نام کاربری: ';
        set new_str = concat(new_str, field, ifnull(new.username, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.username, ''), '\n');
    end if;
    if ifnull(new.password <> old.password, true) and (new.password is not null or old.password is not null) then
        set field = 'رمز ورود: ';
        set new_str = concat(new_str, field, ifnull(new.password, ''), '\n');
        set old_str = concat(old_str, field, '********', '\n');
    end if;
    if ifnull(new.fullname <> old.fullname, true) and (new.fullname is not null or old.fullname is not null) then
        set field = 'نام و نام خانوادگی: ';
        set new_str = concat(new_str, field, ifnull(new.fullname, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.fullname, ''), '\n');
    end if;
    if ifnull(new.email <> old.email, true) and (new.email is not null or old.email is not null) then
        set field = 'نشانی ایمیل: ';
        set new_str = concat(new_str, field, ifnull(new.email, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.email, ''), '\n');
    end if;
    if ifnull(new.phone <> old.phone, true) and (new.phone is not null or old.phone is not null) then
        set field = 'تلفن تماس: ';
        set new_str = concat(new_str, field, ifnull(new.phone, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.phone, ''), '\n');
    end if;
    if ifnull(new.storage_id <> old.storage_id, true) and (new.storage_id is not null or old.storage_id is not null) then
        set field = 'کد انبار: ';
        set new_str = concat(new_str, field, ifnull(new.storage_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.storage_id, ''), '\n');
    end if;
    if ifnull(new.is_enabled <> old.is_enabled, true) and (new.is_enabled is not null or old.is_enabled is not null) then
        set field = 'وضعیت: ';
        set new_str = concat(new_str, field, ifnull(new.is_enabled, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.is_enabled, ''), '\n');
    end if;
    if ifnull(new.last_login_date <> old.last_login_date, true) and (new.last_login_date is not null or old.last_login_date is not null) then
        set field = 'آخرین تاریخ ورود: ';
        set new_str = concat(new_str, field, ifnull(new.last_login_date, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.last_login_date, ''), '\n');
    end if;
    if ifnull(new.role_id <> old.role_id, true) and (new.role_id is not null or old.role_id is not null) then
        set field = 'کد نقش: ';
        set new_str = concat(new_str, field, ifnull(new.role_id, ''), '\n');
        set old_str = concat(old_str, field, ifnull(old.role_id, ''), '\n');
    end if;

    insert into activity_log (`user_id`, `action`, `old_values`, `new_values`)
        values (new.modified_by, concat('کاربر ', new.id, ' (', ifnull(new.username,''), ') تغییر داده شد'), trim(trailing '\n' from old_str), trim(trailing '\n' from new_str));
end; $$
-- ---------------------------------
-- ---------------------------------

-- -------------------------------------
-- LOG USER DELETE ----------------
-- -------------------------------------
drop trigger if exists log_user_delete; $$
create trigger log_user_delete
    after delete
    on ime.user
    for each row
begin
    declare old_str varchar(2048) default '';

    set old_str = concat(old_str, 'نام کاربری: ', ifnull(old.username, ''), '\n');
    set old_str = concat(old_str, 'نام و نام خانوادگی: ', ifnull(old.fullname, ''), '\n');
    set old_str = concat(old_str, 'نشانی ایمیل: ', ifnull(old.email, ''), '\n');
    set old_str = concat(old_str, 'تلفن تماس: ', ifnull(old.phone, ''), '\n');
    set old_str = concat(old_str, 'کد انبار: ', ifnull(old.storage_id, ''), '\n');
    set old_str = concat(old_str, 'وضعیت: ', ifnull(old.is_enabled, ''), '\n');
    set old_str = concat(old_str, 'آخرین تاریخ ورود: ', ifnull(old.last_login_date, ''), '\n');
    set old_str = concat(old_str, 'کد نقش: ', ifnull(old.role_id, ''), '\n');

    insert into activity_log (`user_id`, `action`, `old_values`)
    values (old.modified_by, concat('کاربر ', old.id, ' (', ifnull(old.username,''), ') پاک شد'), trim(trailing '\n' from old_str));
end; $$
-- ---------------------------------
-- ---------------------------------
