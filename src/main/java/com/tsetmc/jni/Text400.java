package com.tsetmc.jni;

public class Text400 {

    private native static String decodeEbcdic(String str);
    private native static String encodeEbcdic(String str);
    private native static String encodeCp437(String str);

    public static void init() {
        System.loadLibrary("text400"); // text400.dll (Windows) or libtext400.so (UNIX)
    }

    public static String ebcdicToUnicode(String str) {
        return decodeEbcdic(str);
    }

    public static String unicodeToEbcdic(String str) {
        return encodeEbcdic(str);
    }

    public static String unicodeToCp437(String str) {
        return encodeCp437(str);
    }

}