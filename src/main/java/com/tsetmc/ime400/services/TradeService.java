package com.tsetmc.ime400.services;

import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@Service
public class TradeService {

    @Autowired
    StorageService storageService;

    @Autowired
    DataSource datasource;

    private String[] getQuery(String searchTerm, Integer storageId, String from, String to, String orderByColName, String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append(" select t.id as DT_RowId, t.id 'ردیف', t.symbol 'نماد', t.persian_trade_date 'تاریخ', t.ticket_number 'شماره اعلامیه', t.quantity 'مقدار', t.unit_price 'قیمت', " +
                "  t.b_ptcode 'کد بورسی خریدار', t.b_client_code 'کد مشتری خریدار', t.b_national_id 'شماره ملی خریدار', t.b_first_name 'نام خریدار', t.b_last_name 'نام خانوادگی خریدار', " +
                "  t.s_ptcode 'کد بورسی فروشنده', t.s_client_code 'کد مشتری فروشنده', t.s_national_id 'شماره ملی فروشنده', t.s_first_name 'نام فروشنده', t.s_last_name 'نام خانوادگی فروشنده' " +
                "from ime.trade t WHERE true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.trade t where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.trade t where true ");

        if (storageId != null) {
            String symbolSearch = storageService.getStorageSymbol(storageId).replaceAll(" ", "");
            query.append(String.format("and t.symbol='%s' ", symbolSearch));
            filteredRowCountQuery.append(String.format("and t.symbol='%s' ", symbolSearch));
            rowCountQuery.append(String.format("and t.symbol='%s' ", symbolSearch));
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format("and (MATCH(t.symbol,t.b_broker_code,t.b_ptcode,t.b_ascii_ptcode,t.b_national_id," +
                    "t.b_last_name,t.b_first_name,t.s_broker_code,t.s_ptcode,t.s_ascii_ptcode,t.s_client_code,t.s_national_id," +
                    "t.s_last_name,t.s_first_name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(t.symbol,t.b_broker_code,t.b_ptcode,t.b_ascii_ptcode,t.b_national_id," +
                    "t.b_last_name,t.b_first_name,t.s_broker_code,t.s_ptcode,t.s_ascii_ptcode,t.s_client_code,t.s_national_id," +
                    "t.s_last_name,t.s_first_name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (from != null && !from.isEmpty()) {
            // CAST("2020-08-29" AS Date)
            query.append(String.format(" and t.persian_trade_date>= '%s' ", from));
            filteredRowCountQuery.append(String.format(" and t.persian_trade_date>= '%s' ", from));
        }

        if (to != null && !to.isEmpty()) {
            query.append(String.format(" and t.persian_trade_date<>>= '%s' ", to));
            filteredRowCountQuery.append(String.format(" and t.persian_trade_date<>>= '%s' ", to));
        }

        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getTradeList(String searchTerm,
                               Integer storageId,
                               String from,
                               String to,
                               int offset,
                               int count,
                               String orderByColName,
                               String ascOrdesc) {
        String[] queries = getQuery(searchTerm, storageId, from, to, orderByColName, ascOrdesc);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getTradeList(String searchTerm,
                             Integer storageId,
                             String from,
                             String to,
                             String orderByColName,
                             String ascOrdesc, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(searchTerm, storageId, from, to, orderByColName, ascOrdesc);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
