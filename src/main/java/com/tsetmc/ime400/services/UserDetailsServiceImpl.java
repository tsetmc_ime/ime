package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.models.UserRole;
import com.tsetmc.ime400.repositories.UserRepository;
import com.tsetmc.ime400.repositories.UserRoleRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    DataSource datasource;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    String[] getQuery(Integer storageId, String search, String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append("select u.id as DT_RowId, u.id 'ردیف',username 'نام کاربری',fullname 'نام و نام خانوادگی',s.name 'نام انبار',last_login_date 'آخرین تاریخ ورود',case when u.is_enabled = 1 then 'فعال' else 'غیر فعال' end  'وضعیت', r.description 'نقش' " +
                "from ime.user u " +
                "left outer join ime.user_role r on u.role_id = r.id " +
                "left outer join ime.storage s on u.storage_id=s.id " +
                "where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.user u left outer join ime.storage s on u.storage_id=s.id where true ");

        String rowCountQuery = "select count(*) as total from ime.user u left outer join ime.storage s on u.storage_id=s.id where true ";

        if (storageId != null) {
            query.append(" and s.id= ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and s.id= ").append(storageId).append(' ');
        }

        if (!StringUtils.isEmptyOrWhitespace(search)) {
            query.append(" and (username like '%").append(search).append("%' or fullname like '%").append(search).append("%' or s.name like '%").append(search).append("%')");
            filteredRowCountQuery.append(" and (username like '%").append(search).append("%' or fullname like '%").append(search).append("%' or s.name like '%").append(search).append("%')");
        }

        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery, filteredRowCountQuery.toString()};
    }

    public String getUsersList(Integer storageId, String search, int start, int length, String orderKey, String orderDir) {
        String[] queries = getQuery(storageId, search, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getUsersList(Integer storageId, String search, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(storageId, search, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User does not exist."));
    }

    public JSONObject getUserInfo(int userId) throws SQLException, JSONException {
        final String query = "select u.id,rol.description,u.username,u.fullname,u.email,u.phone,\n" +
                "s.id storageId,s.name storageName,s.max_cap max_cap,s.is_active storageIsActive,\n" +
                "inst.id instrumentId,inst.symbol sumbol, inst.symbol_desc symbol_desc,inst.due_date due_date\n" +
                "from ime.user u \n" +
                "left outer join ime.user_role rol on rol.id = role_id \n" +
                "left outer join ime.storage s on u.storage_id=s.id\n" +
                "left outer join ime.instrument inst on s.instrument_id=inst.id\n" +
                "where u.id= " + userId;
        return JsonService.createObjectFromSQL(datasource, query);
    }


    public List<UserRole> getRoles() {
        return (List<UserRole>) userRoleRepository.findAll();
    }

    public User getUser() {
        return userRepository.findById(269).orElse(null);
    }

}
