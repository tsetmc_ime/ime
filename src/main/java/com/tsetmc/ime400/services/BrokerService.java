package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Broker;
import com.tsetmc.ime400.repositories.BrokerRepository;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@Service
public class BrokerService {

    @Autowired
    DataSource datasource;

    @Autowired
    BrokerRepository brokerRepository;

    public Broker getBrokerById(int id) {
        return brokerRepository.findById(id).orElse(null);
    }

    public Broker getBrokerByCode(String brokerCode) {
        return brokerRepository.findByCode(brokerCode).orElse(null);
    }

    public Broker getBrokerByName(String brokerName) {
        return brokerRepository.findByName(brokerName).orElse(null);
    }

    public String getBrokersByNameOrCode(String searchTerm) {
        try {
            String query = "select id, CONCAT( name,'  ',code) as name from ime.broker b ";
            if (searchTerm != null && !searchTerm.isEmpty()) {
                query += "where code like '%" + searchTerm + "%' or name like '%" + searchTerm + "%'";
            }
            return JsonService.createListFromSQL(datasource, query).toString();

        } catch (Exception ex) {
            return "";
        }
    }

    String[] getQuery(String search, String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append(" select b.id DT_RowId, b.id 'ردیف', b.code 'کد کارگزار', b.name 'نام کارگزار', case when b.enabled = 1 then 'فعال' else 'غیر فعال' end 'وضعیت' " +
                "from ime.broker b WHERE b.code like '%").append(search).append("%' or b.name like '%").append(search).append("%' ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.broker b WHERE b.code like '%").append(search).append("%' or b.name like '%").append(search).append("%' ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.broker b");

        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getBrokerList(String search, int offset, int count, String orderKey, String orderDir) throws SQLException, JSONException {
        String[] queries = getQuery(search, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getBrokerList(String search, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public void save(Broker broker) {
        brokerRepository.save(broker);
    }
}
