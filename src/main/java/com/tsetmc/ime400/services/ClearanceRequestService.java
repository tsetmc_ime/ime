package com.tsetmc.ime400.services;

import com.google.gson.JsonArray;
import com.tsetmc.ime400.models.ClearanceRequest;
import com.tsetmc.ime400.models.ClearanceResponseStatus;
import com.tsetmc.ime400.repositories.ClearanceRequestRepository;
import com.tsetmc.ime400.repositories.ClearanceResponseStatusRepository;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class ClearanceRequestService {

    @Autowired
    DataSource datasource;

    @Autowired
    ClearanceRequestRepository clearanceRequestRepository;

    @Autowired
    ClearanceResponseStatusRepository responseStatusRepository;

    public JSONObject getStats() throws SQLException, JSONException {
        final String query =
                " SELECT  \n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=1) as 'ترخیص موفق',\n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=0 or response_status is null) as 'در صف انتظار',\n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=2) as 'عدم وجود کد بورسی',\n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=3) as 'عدم وجود نماد',\n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=4) as 'عدم کفایت موجودی',\n" +
                        "(SELECT count(*) FROM ime.clearance_request where datediff(curdate(),request_date)<18000 and  response_status=5) as 'کد ممنوع المعامله'";
        return JsonService.createObjectFromSQL(datasource, query);
    }

    public JSONObject getStatByUserId(int userId) throws SQLException, JSONException {
        final String query =
                String.format(
                        "select \n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.clearance_request cr on cr.storage_id=s.id and cr.response_status is null) as 'در صف انتظار',\n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.clearance_request cr on cr.storage_id=s.id and cr.response_status is not null and cr.response_status!=1) as 'ناموفق',\n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.clearance_request cr on cr.storage_id=s.id and cr.response_status=1) as 'موفق'", userId);
        return JsonService.createObjectFromSQL(datasource, query);
    }

    public List<ClearanceRequest> getUnresponsedClearanceRequests() {
        return clearanceRequestRepository.findUnresponsedRequests();
    }

    public List<ClearanceRequest> getClearanceRequests(int storageId, int investorId) {
        return clearanceRequestRepository.findClearanceRequests(investorId, storageId);
    }

    public void createClearanceRequest(ClearanceRequest cr) {
        ClearanceRequest newCr = clearanceRequestRepository.save(cr);
        newCr.setRequestNumber("4" + String.format("%019d", newCr.getId()));
        clearanceRequestRepository.save(newCr);
    }

    public void updateClearanceRequest(ClearanceRequest cr) {
        clearanceRequestRepository.save(cr);
    }

    String[] getQuery(Integer storageId, String searchTerm, String from, String to,
                      Integer finalConfirmed, Integer responseStatus,
                      String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append(
                "SELECT cr.id as DT_RowId, cr.id 'ردیف', s.name 'نام انبار',i.ptcode 'کد سهامداری'," + "i.national_id 'کد ملی',i.first_name 'نام',i.last_name 'نام خانوادگی',cr.quantity 'مقدار',\n" +
                "IF(cr.is_final_confirmed=1 , 'تأیید نهایی', 'در انتظار تأیید') as 'وضعیت',request_date 'تاریخ ارسال',persian_response_date 'تاریخ دریافت پاسخ',crs.description 'پاسخ',cr.register_date 'تاریخ ثبت درخواست',cr.confirm_date ,\n" +
                "       case when cr.trade_id is not null then concat(' شماره معامله ',cr.trade_id)\n" +
                "            when cr.future_delivery_id is not null then concat('تحویل آتی / تسویه آپشن شماره ',cr.future_delivery_id)\n" +
                "            else 'ثبت شده توسط کاربر'\n" +
                "       end as 'توضیحات' \n" +
                "       , ins.symbol as 'نماد' " +
                "       , ins.symbol_desc as 'شرح نماد' " +
                "FROM ime.clearance_request cr\n" +
                "inner join ime.investor i on i.id=cr.investor_id\n" +
                "inner join ime.storage s on s.id=cr.storage_id\n" +
                "inner join ime.instrument ins on ins.id=s.instrument_id\n" +
                "left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("SELECT count(*) total FROM ime.clearance_request cr\n" +
                "inner join ime.investor i on i.id=cr.investor_id\n" +
                "inner join ime.storage s on s.id=cr.storage_id\n" +
                "left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("SELECT count(*) total FROM ime.clearance_request cr\n" +
                "inner join ime.investor i on i.id=cr.investor_id\n" +
                "inner join ime.storage s on s.id=cr.storage_id\n" +
                "left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ");

        if (storageId != null) {
            query.append(" and s.id= ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and s.id= ").append(storageId).append(' ');
            rowCountQuery.append(" and s.id= ").append(storageId).append(' ');
        }
        if (from != null && !from.isEmpty()) {
            query.append(" and Date(cr.request_date)>= CAST('").append(from).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(cr.request_date)>= CAST('").append(from).append("' as Date) ");
        }
        if (to != null && !to.isEmpty()) {
            query.append(" and Date(cr.request_date)<= CAST('").append(to).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(cr.request_date)<= CAST('").append(to).append("' as Date) ");
        }
        if (finalConfirmed != null) {
            query.append(" and cr.is_final_confirmed = ").append(finalConfirmed).append(' ');
            filteredRowCountQuery.append(" and cr.is_final_confirmed = ").append(finalConfirmed).append(' ');
        }
        if (responseStatus != null) {
            query.append(" and cr.response_status = ").append(responseStatus).append(' ');
            filteredRowCountQuery.append(" and cr.response_status = ").append(responseStatus).append(' ');
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) \n" +
                    " or MATCH(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format(" and (MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) \n" +
                    " or MATCH(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));

        }
        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getClearanceRequestList(Integer storageId, String searchTerm, String from, String to,
                                          Integer finalConfirmed, Integer responseStatus, int start, int length,
                                          String orderKey, String orderDir) {
        try {
            String[] queries = getQuery(storageId, searchTerm, from, to, finalConfirmed, responseStatus, orderKey, orderDir);

            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getClearanceRequestList(Integer storageId, String searchTerm, String from, String to,
                                        Integer finalConfirmed, Integer responseStatus,
                                        String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws IOException, SQLException, JSONException {
        String[] queries = getQuery(storageId, searchTerm, from, to, finalConfirmed, responseStatus, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public Optional<ClearanceRequest> findById(Long id) {
        return clearanceRequestRepository.findById(id);
    }

    public void deleteClearanceRequest(Long id) {
        clearanceRequestRepository.deleteById(id);
    }

    String[] getQueryReport(String searchTerm, String from, String to, Integer clearType, Boolean done, String orderKey, String orderDir) {
        String query = "select s.name 'نام انبار',inst.symbol 'نماد',inst.symbol_desc 'شرح نماد',cr.id as DT_RowId, cr.id 'ردیف',i.first_name 'نام',i.last_name 'نام خانوادگی',\n" +
                "       i.national_id 'کد ملی',i.ptcode 'کد سهامداری',i.ascii_ptcode 'کد ۱۴ رقمی',cr.quantity 'مقدار',cr.register_date 'تاریخ ثبت درخواست',cr.response_date  'تاریخ دریافت پاسخ'" +
                "       ,cr.request_date 'تاریخ ارسال',ct.description 'نوع ترخیص',crs.description 'وضعیت',\n" +
                "       case when cr.trade_id is not null then concat(' شماره معامله ',cr.trade_id)\n" +
                "            when cr.future_delivery_id is not null then concat('تحویل آتی / تسویه آپشن شماره ',cr.future_delivery_id)\n" +
                "            else 'ثبت شده توسط کاربر'\n" +
                "       end as 'توضیحات' \n" +
                "       , crs.description 'پاسخ' \n" +
                "           FROM ime.clearance_request cr\n" +
                "    inner join ime.investor i on i.id=cr.investor_id\n" +
                "    inner join ime.storage s on s.id=cr.storage_id\n" +
                "    inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "    inner join ime.clearance_type ct on inst.clearance_type=ct.id\n" +
                "    left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ";
        String filteredRowCountQuery = "select count(*) total \n" +
                "           FROM ime.clearance_request cr\n" +
                "    inner join ime.investor i on i.id=cr.investor_id\n" +
                "    inner join ime.storage s on s.id=cr.storage_id\n" +
                "    inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "    inner join ime.clearance_type ct on inst.clearance_type=ct.id\n" +
                "    left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ";
        String rowCountQuery = "select count(*) total \n" +
                "           FROM ime.clearance_request cr\n" +
                "    inner join ime.investor i on i.id=cr.investor_id\n" +
                "    inner join ime.storage s on s.id=cr.storage_id\n" +
                "    inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "    inner join ime.clearance_type ct on inst.clearance_type=ct.id\n" +
                "    left outer join ime.clearance_response_status crs on crs.id=cr.response_status\n" +
                "where true ";

        if (done != null) {
            if (done) {
                query += " and crs.id=1 ";
                filteredRowCountQuery += " and crs.id=1 ";
            } else {
                query += " and crs.id is not null and crs.id!=1 ";
                filteredRowCountQuery += " and crs.id is not null and crs.id!=1 ";
            }
        }

        if (clearType != null) {
            query += String.format(" and  ct.id = %d ", clearType);
            filteredRowCountQuery += String.format(" and  ct.id = %d ", clearType);
        }

        if (from != null && !from.isEmpty()) {
            query += " and Date(cr.request_date)>= CAST('" + from + "' as Date) ";
            filteredRowCountQuery += " and Date(cr.request_date)>= CAST('\" + from + \"' as Date) ";
        }
        if (to != null && !to.isEmpty()) {
            query += " and Date(cr.request_date)<= CAST('" + to + "' as Date) ";
            filteredRowCountQuery += " and Date(cr.request_date)<= CAST('" + to + "' as Date) ";
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query += String.format(" and (MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) \n" +
                    " or MATCH(s.name) AGAINST('%1$s' in boolean mode) or MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            filteredRowCountQuery += String.format(" and (MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) \n" +
                    " or MATCH(s.name) AGAINST('%1$s' in boolean mode) or MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());

        }
        query += " order by `" + orderKey + "` " + orderDir;

        return new String[]{query, rowCountQuery, filteredRowCountQuery};
    }

    public String getClearanceRequestsReport(String searchTerm, String from, String to, Integer clearType, Boolean done,
                                             int start, int length, String orderKey, String orderDir) {
        try {
            String[] queries = getQueryReport(searchTerm, from, to, clearType, done, orderKey, orderDir);

            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getClearanceRequestsReport(String searchTerm, String from, String to, Integer clearType, Boolean done,
                                           String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws IOException, SQLException, JSONException {
        String[] queries = getQueryReport(searchTerm, from, to, clearType, done, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public List<ClearanceResponseStatus> getAllResponseStatus() throws SQLException, JSONException {
        return responseStatusRepository.findAll();
    }

}
