package com.tsetmc.ime400.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.projections.StorageInstrument;
import com.tsetmc.ime400.repositories.StorageRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class StorageService {

    @Autowired
    DataSource datasource;

    @Autowired
    StorageRepository storageRepository;

    public JSONArray getActiveStoragesCount() throws SQLException, JSONException {
        final String query = "SELECT count(*) FROM ime.storage\n" +
                " where is_active=true";
        return JsonService.createListFromSQL(datasource, query);
    }

    public JSONObject getStorageStat(int storageId) throws SQLException, JSONException {
        final String query =
                String.format(
                        "select (total-`انبار خروجی`-`انبار موقت`-`انبار بورسی`) as `ظرفیت خالی`,`انبار بورسی`,`انبار موقت`,`انبار خروجی` from ( SELECT \n" +
                                // maxCap
                                "(SELECT max_cap FROM ime.storage WHERE id=%1$d ) as total,\n" +
                                // tempAsset
                                "(SELECT IFNULL(sum(ts.quantity),0)   from\n" +
                                "ime.temp_asset ts \n" +
                                "where storage_id=%1$d and converted=false\n" +
                                ") as `انبار موقت`,\n" +
                                // asset
                                "(SELECT  IFNULL(sum(quantity),0)  FROM ime.asset\n" +
                                "where storage_id=%1$d \n" +
                                ") as `انبار بورسی`,\n" +
                                // out asset
                                "(SELECT IFNULL( sum(quantity),0) FROM ime.out_asset\n" +
                                "where storage_id=%1$d and out_date is null) as `انبار خروجی`) as tbl", storageId);
        return JsonService.createObjectFromSQL(datasource, query);
    }

    public String getStorageList(String searchTerm,
                                 Integer status,
                                 int offset,
                                 int count,
                                 String orderByColName,
                                 String ascOrdesc) {
        String query = " select " +
                "s.id as DT_RowId, s.id 'ردیف', " +
                "s.name  'نام انبار', " +
                "i.symbol  'نماد', " +
                "i.symbol_desc  'شرح نماد', " +
                "i.due_date  'تاریخ سررسید', " +
                "s.max_cap  'سقف ظرفیت', " +
                "s.cost  'هزینه انبارداری', " +
                "i.cost_vat  'مالیات بر ارزش افزوده', " +
                "acs.description 'وضعیت' " +
                "from ime.storage s " +
                "left outer join activity_state acs on s.is_active = acs.id " +
                "inner join ime.instrument i on s.instrument_id=i.id " +
                "WHERE true ";
        String filteredRowCountQuery = "select count(*) as total " +
                "from ime.storage s " +
                "inner join ime.instrument i on s.instrument_id=i.id " +
                "WHERE true ";
        String rowCountQuery = "select count(*) as total " +
                "from ime.storage s " +
                "inner join ime.instrument i on s.instrument_id=i.id ";

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query += String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "or match(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            filteredRowCountQuery += String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "or match(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
        }

        if (status != null) {
            query += " and s.is_active=" + status;
            filteredRowCountQuery += " and s.is_active=" + status;
        }

        query += " order by `" + orderByColName + "` " + ascOrdesc;
        query += " limit " + offset + "," + count;

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, query);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, filteredRowCountQuery).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, rowCountQuery).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getStorageList(String searchTerm, Integer status, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String query = " select s.name as `نام انبار`,i.symbol as 'نماد',i.symbol_desc as `شرح نماد`," +
                "due_date as `تاریخ سررسید`" +
                ",s.max_cap as `سقف ظرفیت`,s.cost as `هزینه انبارداری`,i.cost_vat as `مالیات بر ارزش افزوده`, s.is_active As `وضعیت` " +
                "from ime.storage s " +
                "inner join ime.instrument i on s.instrument_id=i.id " +
                "WHERE true ";

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }
            query += String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "or match(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
        }
        if (status != null)
            query += " and s.is_active=" + status;

        JsonService.iterateOverSQL(datasource, query, iter);
    }

    public Optional<Storage> findById(Integer storageId) {
        return storageRepository.findById(storageId);
    }

    @Autowired
    EntityManager entityManager;

    public String getStoragesForAutoComplete(String name) throws SQLException, JSONException {
        String query = "select id,name from ime.storage where name like '%" + name + "%'";
        return JsonService.createListFromSQL(datasource, query).toString();
    }

    @Autowired
    InstrumentService instrumentService;

    public String getStorageSymbol(int storageId) {
        return storageRepository.findById(storageId).map(Storage::getInstrument).map(Instrument::getSymbol).orElse(null);
    }

    public Instrument getInstrumentOfStorage(int storageId) {
        return storageRepository.findById(storageId).map(Storage::getInstrument).orElse(null);
    }

    public Storage getStorageBySymbol(String symbol) {
        return storageRepository.findBySymbol(symbol).orElse(null);
    }

    public JSONArray getAllSymbols() throws SQLException, JSONException {
        final String query = "select * from ime.storage s\n" +
                "inner join ime.instrument i on s.instrument_id=i.id\n" +
                "where s.is_active";
        return JsonService.createListFromSQL(datasource, query);
    }

    String[] getReportQuery(String searchTerm, String orderKey, String orderDir) {
        String query = "select s.id as DT_RowId, s.id 'ردیف',s.name 'نام انبار',i.symbol 'نماد',i.symbol_desc 'شرح نماد',ifnull(taQuantity,0) 'موجودی موقت',ifnull(aQuantity,0) 'موجودی بورسی',ifnull(clearedQuantity,0) 'مقدار خارج شده',\n" +
                "       ifnull(oaQuantity,0) 'موجودی در آستانه خروج',s.max_cap 'سقف ظرفیت',(max_cap-ifnull(taQuantity,0)-ifnull(aQuantity,0)-ifnull(clearedQuantity,0)) 'ظرفیت باقی‌مانده' \n" +
                "from ime.storage s\n" +
                "    inner join ime.instrument i on i.id=s.instrument_id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) clearedQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by storage_id) oa on oa.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date!=cast('0000-00-00' as date) or out_date is not null\n" +
                "    group by investor_id, storage_id\n" +
                "    ) oa2 on oa2.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) aQuantity from ime.asset group by storage_id\n" +
                "    ) a on a.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) taQuantity from ime.temp_asset ta where !converted group by storage_id\n" +
                "    ) ta on ta.storage_id=s.id WHERE true \n";
        String filteredRowCountQuery = "select count(*) total\n" +
                "from ime.storage s\n" +
                "    inner join ime.instrument i on i.id=s.instrument_id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) clearedQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by storage_id) oa on oa.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date!=cast('0000-00-00' as date) or out_date is not null\n" +
                "    group by investor_id, storage_id\n" +
                "    ) oa2 on oa2.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) aQuantity from ime.asset group by storage_id\n" +
                "    ) a on a.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) taQuantity from ime.temp_asset ta where !converted group by storage_id\n" +
                "    ) ta on ta.storage_id=s.id WHERE true \n";
        String rowCountQuery = "select count(*) total\n" +
                "from ime.storage s\n" +
                "    inner join ime.instrument i on i.id=s.instrument_id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) clearedQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by storage_id) oa on oa.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date!=cast('0000-00-00' as date) or out_date is not null\n" +
                "    group by investor_id, storage_id\n" +
                "    ) oa2 on oa2.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) aQuantity from ime.asset group by storage_id\n" +
                "    ) a on a.storage_id=s.id\n" +
                "left outer join (\n" +
                "    select storage_id,sum(quantity) taQuantity from ime.temp_asset ta where !converted group by storage_id\n" +
                "    ) ta on ta.storage_id=s.id WHERE true \n";

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query += String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "or match(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            filteredRowCountQuery += String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "or match(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
        }

        query += " order by `" + orderKey + "` " + orderDir;

        return new String[]{query, rowCountQuery, filteredRowCountQuery};
    }

    public String getStorageAssetsReport(String searchTerm, int start, int length, String orderKey, String orderDir) {
        String[] queries = getReportQuery(searchTerm, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getStorageAssetsReport(String searchTerm, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getReportQuery(searchTerm, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
