package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.repositories.ClearanceTimeRepository;
import com.tsetmc.ime400.repositories.ClearanceTypeRepository;
import com.tsetmc.ime400.repositories.ConvertTimeRepository;
import com.tsetmc.ime400.repositories.InstrumentRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class InstrumentService {

    @Autowired
    DataSource datasource;

    @Autowired
    InstrumentRepository instrumentRepository;

    @Autowired
    StorageService storageServices;

    @Autowired
    ClearanceTypeRepository clearanceTypeRepository;

    @Autowired
    ClearanceTimeRepository clearanceTimeRepository;

    @Autowired
    ConvertTimeRepository convertTimeRepository;

    public Optional<Instrument> findById(Integer instrumentId) {
        return instrumentRepository.findById(instrumentId);
    }

    public Optional<Instrument> findByStorageId(int storageId) {
        return storageServices.findById(storageId).map(Storage::getInstrument);
    }

    public void saveInstrument(Instrument instrument) {
        Optional<Instrument> instrumentWithSameSymbol = instrumentRepository.findBySymbol(instrument.getSymbol());
        if (instrumentWithSameSymbol.isPresent() && !instrumentWithSameSymbol.get().getId().equals(instrument.getId()))
            return;
        instrumentRepository.save(instrument);
    }

    public String getInstrumentsBySymbol(String symbol) throws SQLException, JSONException {
        String query = "select id, symbol name from ime.instrument where symbol like '%" + symbol + "%'";
        return JsonService.createListFromSQL(datasource, query).toString();
    }

    public Optional<Instrument> findInstrumentBySymbol(String symbol) {
        return instrumentRepository.findBySymbol(symbol);
    }

    public List<?> getClearanceTime() {
        return clearanceTimeRepository.findAll();
    }

    public List<?> getClearanceType() {
        return clearanceTypeRepository.findAll();
    }

    public List<?> getConvertTime() {
        return convertTimeRepository.findAll();
    }

    String[] getQuery(String searchTerm, String from, String to, String orderByColName, String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append(" select i.id as DT_RowId, i.id 'ردیف', i.symbol 'نماد', i.symbol_desc 'شرح نماد', i.due_date 'تاریخ سررسید', i.lot 'اندازه بسته معاملاتی', i.weight_unit 'واحد وزن', i.cost_vat 'مالیات بر ارزش افزوده هزینه انبارداری', i.trades_vat 'مالیات بر ارزش افزوده معاملات' from ime.instrument i WHERE true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.instrument i where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.instrument i where true ");

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(i.symbol,i.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (from != null && !from.isEmpty()) {
            // CAST("2020-08-29" AS Date)
            query.append(" and Date(i.due_date)>= CAST('").append(from).append("' as Date)");
            filteredRowCountQuery.append(" and Date(i.due_date)>= CAST('").append(from).append("' as Date)");
        }

        if (to != null && !to.isEmpty()) {
            query.append(" and Date(i.due_date) <=CAST('").append(to).append("' as Date)");
            filteredRowCountQuery.append(" and Date(i.due_date) <=CAST('").append(to).append("' as Date)");
        }

        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getInstrumentList(String searchTerm,
                                    String from,
                                    String to,
                                    int offset,
                                    int count,
                                    String orderByColName,
                                    String ascOrdesc) {
        String[] queries = getQuery(searchTerm, from, to, orderByColName, ascOrdesc);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getInstrumentList(String searchTerm,
                                  String from,
                                  String to,
                                  String orderByColName,
                                  String ascOrdesc,
                                  JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(searchTerm, from, to, orderByColName, ascOrdesc);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
