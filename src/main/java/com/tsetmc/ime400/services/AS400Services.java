package com.tsetmc.ime400.services;

import com.tsetmc.ime400.interfaces.TextEncoder;
import com.tsetmc.ime400.models.FutureAsset;
import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.models.PTCodeInquiry;
import com.tsetmc.ime400.repositories.FutureAssetRepository;
import com.tsetmc.ime400.util.DateTimeService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AS400Services {

    @Qualifier("as400-datasource")
    @Autowired
    private DataSource as400;

    @Autowired
    TextEncoder textEncoder;

    @Autowired
    PTCodeInquiryService ptCodeInquiryService;

    @Autowired
    AssetServices assetServices;

    public String forExample(String cisnam) throws SQLException, JSONException {
        String stmt = String.format("select CIITYP as PersonType,CIINDV as PtCode,IME14(CIINDV) as PTcode2," +
                "CISNAM as NationalId,CIFNAM as FullName,CISIN# as IdNumber,CIBORN as BirthDate," +
                "CINNXT as FatherName,CIBPHO as IdSerial,CIBPRO as IssuePlace,CIACTV as ActivityState " +
                "from TSELIB.CPINDC where CISNAM = '%s' and  ", cisnam);
        return textEncoder.toUnicode(JsonService.createObjectFromSQL(as400, stmt).toString());
    }

    void updateInvestorFromAs400(PTCodeInquiry inq) throws SQLException, JSONException {
        String stmt = inq.getInvestorType() == 1 ?
                String.format("select " +
                        "  trim(CIINDV) CIINDV, qgpl.ime14(CIINDV) PTCODE14, CIITYP, CIACTV, DATEF, " +
                        "  trim(CISNAM) CISNAM, trim(CIBORN) CIBORN, " +
                        "  trim(CIFNAM) CIFNAM, trim(CISIN#) CISIN, trim(IMINVS) IMINVS, " +
                        "  trim(CIBPHO) CIBPHO, trim(CIBPRO) CIBPRO, trim(CINNXT) CINNXT " +
                        "from TSELIB.CPINDC, LIB934.TAQVIM, INTERFACE.INVSMAP " +
                        "where " +
                        "  IMACC# = CIINDV " +
                        "  and right(CIINDV, 5) = right('%s', 5) " +
                        "  and CISNAM = '%s' " +
                        "  and DATEL = varchar_format(current_timestamp, 'YYYYMMDD')", inq.getPtcode(), inq.getNationalId())
                :
                String.format("select " +
                        "  trim(CIINDV) CIINDV, qgpl.ime14(CIINDV) PTCODE14, CIITYP, CIACTV, DATEF, " +
                        "  trim(CISNAM) CISNAM, trim(CIBORN) CIBORN, " +
                        "  trim(CIFNAM) CIFNAM, trim(CISIN#) CISIN, trim(IMINVS) IMINVS, " +
                        "  trim(CIBPHO) CIBPHO, trim(CIBPRO) CIBPRO, trim(CINNXT) CINNXT " +
                        "from TSELIB.CPINDC, LIB934.TAQVIM, INTERFACE.INVSMAP " +
                        "where " +
                        "  IMACC# = CIINDV " +
                        "  and right(CIINDV, 5) = right('%s', 5) " +
                        "  and IMINVS='%s' " +
                        "  and DATEL = varchar_format(current_timestamp, 'YYYYMMDD')", inq.getPtcode(), inq.getNationalId());

        JSONArray result = JsonService.createListFromSQL(as400, textEncoder.toEbcdic(stmt));

        if (result.length() > 0) {
            JSONObject obj = result.getJSONObject(0);

            String ptcode14 = obj.getString("PTCODE14");
            String CIITYP = obj.getString("CIITYP");
            String CIINDV = textEncoder.toUnicode(obj.getString("CIINDV"));
            String CIFNAM = textEncoder.toUnicode(obj.getString("CIFNAM"));
            String CISNAM = obj.getString("CISNAM");
            String CISIN = textEncoder.toUnicode(obj.getString("CISIN"));
            String IMINVS = textEncoder.toUnicode(obj.getString("IMINVS"));
            String CIBORN = textEncoder.toUnicode(obj.getString("CIBORN"));
            String CIBPHO = textEncoder.toUnicode(obj.getString("CIBPHO"));
            String CIBPRO = textEncoder.toUnicode(obj.getString("CIBPRO"));
            String CINNXT = textEncoder.toUnicode(obj.getString("CINNXT"));
            String CIACTV = obj.getString("CIACTV");
            String DATEF = obj.getString("DATEF");

            inq.setResStatus(1);
            inq.setResInvestorType(investorService.getPersonType(CIITYP).getId());
            inq.setResAsciiPtcode(ptcode14);
            inq.setResPtcode(CIINDV);
            inq.setResTradingId(IMINVS);
            inq.setResNationalId(CISNAM);
            inq.setResFullname(CIFNAM);
            inq.setResIssueNo(CISIN);
            inq.setResBirthDate(CIBORN);
            inq.setResFatherName(CINNXT);
            inq.setResSerie(CIBPHO);
            inq.setResIssuePlace(CIBPRO);
            inq.setResActivityState("Y".equals(CIACTV) ? 1 : 2);
            inq.setPersianRequestDate(DATEF);
            inq.setPersianRequestTime();

            ptCodeInquiryService.updateInquiryRequest(inq);
        }
    }

    public Investor inquiryInvestor(int personType, String ptcode, String nationalCode) throws SQLException, JSONException {
        PTCodeInquiry inq = ptCodeInquiryService.insertNew(personType, ptcode, nationalCode);
        updateInvestorFromAs400(inq);
        return ptCodeInquiryService.pushResponseToInvestor(inq);
    }

    public void inquiryInvestors(List<Investor> investors) throws SQLException, JSONException {
        for (Investor inv : investors) {
            PTCodeInquiry inq = ptCodeInquiryService.insertNew(inv.getPersonType().getId(), inv.getPtcode(), inv.getNationalId());
            updateInvestorFromAs400(inq);
            ptCodeInquiryService.pushResponseToInvestor(inq);
        }
    }

    @Autowired
    AssetRequestService assetRequestService;

    @Autowired
    TempAssetService tempAssetService;

    @Autowired
    InvestorService investorService;

    @Autowired
    StorageService storageService;

    @Autowired
    BrokerService brokerService;

    @Autowired
    FutureAssetRepository futureAssetRepository;

    public void deleteFutureAsset() {
        futureAssetRepository.deleteAll();
    }

    public void getAsset(String symbol) throws SQLException, JSONException {
        String query = String.format("SELECT SPSYMB,SPDATE,SPACC#,QGPL.ime14(SPACC#) AS PTCODE14, SPTROH, " +
                "CISNAM,CIFNAM,CINNXT,CIBORN,CISIN#,CIBPRO,CIACTV " +
                "FROM STILIB.STREGS,TSELIB.CPINDC WHERE SPACC#=CIINDV " +
                "AND SPDATE =(SELECT MAX(SPDATE) FROM STILIB.STREGS) " +
                "AND SPSYMB='%s' ", symbol);
        String ebcdicQuery = textEncoder.toEbcdic(query);
        JSONArray answer = JsonService.createListFromSQL(as400, ebcdicQuery);

        List<FutureAsset> futAssetList = new ArrayList<>();
        for (int i = 0; i < answer.length(); i++) {
            JSONObject obj = answer.getJSONObject(i);
            FutureAsset futAsset = new FutureAsset();
            futAsset.setSymbol(textEncoder.toUnicode(obj.getString("SPSYMB")).trim());
            futAsset.setDate(textEncoder.toUnicode(obj.getString("SPDATE")).trim());
            futAsset.setQuantity(Integer.parseInt(textEncoder.toUnicode(obj.getString("SPTROH")).trim()));
            futAsset.setPtcode(textEncoder.toUnicode(obj.getString("SPACC#")).trim());
            futAsset.setAsciiPtcode(textEncoder.toUnicode(obj.getString("PTCODE14")).trim());
            futAsset.setNationalId(textEncoder.toUnicode(obj.getString("CISNAM")).trim());
            futAsset.setLastName(textEncoder.toUnicode(obj.getString("CIFNAM")).trim());
            futAsset.setBirthDate(textEncoder.toUnicode(obj.getString("CIBORN")).trim());
            futAsset.setIssueNumber(textEncoder.toUnicode(obj.getString("CISIN#")).trim());
            futAsset.setIssuePlace(textEncoder.toUnicode(obj.getString("CIBPRO")).trim());
            Byte actv = textEncoder.toUnicode(obj.getString("CIACTV")).trim().equals("Y") ? (byte) 1 : (byte) 0;
            futAsset.setIsActive(actv);

            futAssetList.add(futAsset);
        }
        futureAssetRepository.saveAll(futAssetList);
    }

    public List<FutureAsset> fetchFutureAssets() {
        return futureAssetRepository.findAll();
    }

    public int getAssetAmount(String ptcode14, String symbol, int lot) throws SQLException, JSONException {
        String date = DateTimeService.getDate(new Date());
        String stmt = String.format("SELECT SPTROH from STILIB.STREGS, lib934.taqvim where SPDATE=datef and spsymb like '%s' and qgpl.ime14(SPACC#)='%s' and datel = '%s' ", textEncoder.toEbcdic(symbol).replaceAll(" +", "%"), ptcode14, date);
        JSONArray answer = JsonService.createListFromSQL(as400, textEncoder.toEbcdic(stmt));
        if (answer.length() != 1)
            throw new RuntimeException("مقدار دارایی برای سهامدار و نماد مورد نظر قابل تشخیص نیست");
        return (Integer.parseInt(textEncoder.toUnicode(answer.getJSONObject(0).getString("SPTROH")).trim()) / lot);
    }

}
