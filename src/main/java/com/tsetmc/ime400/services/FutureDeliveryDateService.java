package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Broker;
import com.tsetmc.ime400.models.FutureDeliveryDate;
import com.tsetmc.ime400.repositories.FutureDeliveryDateRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Service
public class FutureDeliveryDateService {

    @Autowired
    FutureDeliveryDateRepository futureDeliveryDateRepository;
    @Autowired
    DataSource datasource;

    public FutureDeliveryDate FindById(Integer id) {
        return futureDeliveryDateRepository.findById(id).orElse(null);
    }

    String[] getQuery(String search, String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append(" select f.id DT_RowId, f.id 'ردیف', Date(f.date) 'تاریخ' " +
                ", f.letterNo  'شماره نامه', f.letterDate 'تاریخ نامه', f.letterDescription 'عنوان تحویل'" +
                ", 'delete'  'حذف'  " +
                "from ime.future_delivery_date f where f.date >= current_date ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.future_delivery_date f where f.date >= current_date ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.future_delivery_date f ");
        if (!StringUtils.isEmptyOrWhitespace(search)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = search.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(f.letterDescription) AGAINST('%1$s' in boolean mode))", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format( " and (MATCH(f.letterDescription) AGAINST('%1$s' in boolean mode))", querySearchTerm.toString()));
        }
        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getFutureDeliveryDateList(String search, int offset, int count, String orderKey, String orderDir) throws SQLException, JSONException {
        String[] queries = getQuery(search, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getFutureDeliveryDateList(String search, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public void save(FutureDeliveryDate futureDeliveryDate) {
        futureDeliveryDateRepository.save(futureDeliveryDate);
    }
    public void deleteFutureDeliveryDate(Integer id) {
        futureDeliveryDateRepository.deleteById(id);
    }
}
