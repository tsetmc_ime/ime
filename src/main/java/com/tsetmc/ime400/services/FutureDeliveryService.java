package com.tsetmc.ime400.services;

import com.tsetmc.ime400.dto.FutureDeliveryDto;
import com.tsetmc.ime400.models.FutureDelivery;
import com.tsetmc.ime400.repositories.ClearanceRequestRepository;
import com.tsetmc.ime400.repositories.FutureDeliveryRepository;
import com.tsetmc.ime400.repositories.TempAssetRepository;
import com.tsetmc.ime400.util.JDBCService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Service
public class FutureDeliveryService {

    @Autowired
    DataSource datasource;

    @Autowired
    FutureDeliveryRepository futureDeliveryRepository;

    @Autowired
    TempAssetRepository tempAssetRepository;

    @Autowired
    ClearanceRequestRepository clearanceRequestRepository;

    @Transactional(rollbackFor = {SQLException.class})
    public boolean insertNewRecords(List<FutureDelivery> futureDeliveryList) {
        try {
            if (futureDeliveryRepository.findAllByTransactionTime(LocalDate.now()).size() > 0)
                return false;
            else {
                futureDeliveryRepository.saveAll(futureDeliveryList);

                String clearQuery = "insert into ime.clearance_request(confirm_date, description, is_final_confirmed, quantity,register_date,future_delivery_id,investor_id, storage_id)\n" +
                        "select now(),'ثبت ناشی از تحویل آتی یا تسویه آپشن',1,fd.requested_quantiry,now(),fd.id,i.id,s.id\n" +
                        "from ime.future_delivery fd\n" +
                        "inner join ime.investor i on i.ascii_ptcode=fd.ptcode14\n" +
                        "inner join ime.instrument ins on replace(fd.symbol, ' ','')=replace(ins.symbol, ' ','')\n" +
                        "inner join ime.storage s on s.instrument_id=ins.id\n" +
                        "where cast(transaction_time as DATE)=cast(now() as date)\n" +
                        "and requested_quantiry<0 and delivered_quantity=0";
                JDBCService.execModifyingQuery(datasource, clearQuery);

                String assetQuery = "insert into ime.temp_asset(confirm_date, converted, entrance_date, is_final_confirmed, quantity, register_date, broker_id\n" +
                        ", future_delivery_id, investor_id, storage_id)\n" +
                        "select now(),0,now(),1,fd.requested_quantiry ,now(),b.id,fd.id,i.id,s.id\n" +
                        "from ime.future_delivery fd\n" +
                        "inner join ime.broker b on fd.broker_code=b.code\n" +
                        "inner join ime.investor i on i.ascii_ptcode=fd.ptcode14\n" +
                        "inner join ime.instrument ins on replace(ins.symbol,' ','')=replace(fd.symbol,' ','')\n" +
                        "inner join ime.storage s on s.instrument_id=ins.id\n" +
                        "where cast(now() as date)=cast(fd.transaction_time as date)\n" +
                        "and requested_quantiry>0 and delivered_quantity=0\n";
                JDBCService.execModifyingQuery(datasource, assetQuery);
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public List<FutureDelivery> getDeliveryOnDate(LocalDate date) {
        return futureDeliveryRepository.findAllByTransactionTime(date);
    }
    public List<FutureDeliveryDto> getAllDeliveryOnDate(LocalDate date) {
        return futureDeliveryRepository.getAllByTransactionTime(date);
    }

    private String[] getQuery(String searchTerm, String date, Integer storageId, String orderByColName, String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append("select fd.id as DT_RowId, fd.id 'ردیف',fd.symbol 'نماد',s.name 'انبار',i.ptcode 'کد بورسی',fd.ptcode14 'کد ۱۴ رقمی',i.national_id 'کد ملی',\n" +
                "    i.first_name 'نام', i.last_name 'نام خانوادگی',b.name 'کارگزار',fd.requested_quantiry 'مقدار تقاضا',\n" +
                "    fd.delivered_quantity 'مقدار تحویل شده',fd.ocost 'هزینه انبار (ریال)',fd.transaction_time 'زمان'\n" +
                "    from ime.future_delivery fd\n" +
                "    inner join ime.instrument inst on replace(fd.symbol,' ','')=replace(inst.symbol,' ','')\n" +
                "    inner join ime.storage s on s.instrument_id=inst.id\n" +
                "    left outer join ime.investor i on fd.ptcode14=i.ascii_ptcode\n" +
                "    left outer join ime.broker b on b.code=fd.broker_code where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total\n" +
                "    from ime.future_delivery fd\n" +
                "    inner join ime.instrument inst on replace(fd.symbol,' ','')=replace(inst.symbol,' ','')\n" +
                "    inner join ime.storage s on s.instrument_id=inst.id\n" +
                "    left outer join ime.investor i on fd.ptcode14=i.ascii_ptcode\n" +
                "    left outer join ime.broker b on b.code=fd.broker_code where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total\n" +
                "    from ime.future_delivery fd\n" +
                "    inner join ime.instrument inst on replace(fd.symbol,' ','')=replace(inst.symbol,' ','')\n" +
                "    inner join ime.storage s on s.instrument_id=inst.id\n" +
                "    left outer join ime.investor i on fd.ptcode14=i.ascii_ptcode\n" +
                "    left outer join ime.broker b on b.code=fd.broker_code ");

        if (storageId != null) {
            query.append(String.format("and s.id='%d' ", storageId));
            filteredRowCountQuery.append(String.format("and s.id='%d' ", storageId));
            rowCountQuery.append(String.format("and s.id='%d' ", storageId));
        }

        if (date != null) {
            // CAST("2020-08-29" AS Date)
            query.append(String.format(" and CAST(fd.transaction_time as Date) = CAST('%s' as Date) ", date));
            filteredRowCountQuery.append(String.format(" and CAST(fd.transaction_time as Date) = CAST('%s' as Date) ", date));
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(s.name) AGAINST('%1$s' in boolean mode) or " +
                    "MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode) or " +
                    "MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) ) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format(" and (MATCH(s.name) AGAINST('%1$s' in boolean mode) or " +
                    " MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode) or " +
                    " MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) ) ", querySearchTerm.toString()));
        }
        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getFutureDeliveryList(String search, String date, Integer storageId, int start, int length, String orderKey, String orderDir) {
        String[] queries = getQuery(search, date, storageId, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getFutureDeliveryList(String search, String date, Integer storageId, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, date, storageId, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
