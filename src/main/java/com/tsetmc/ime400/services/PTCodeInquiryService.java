package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.models.PTCodeInquiry;
import com.tsetmc.ime400.repositories.PTCodeInquiryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PTCodeInquiryService {

    @Autowired
    PTCodeInquiryRepository ptCodeInquiryRepository;

    @Autowired
    InvestorService investorService;

    public void deleteAllPTCodeInquiries() {
        ptCodeInquiryRepository.deleteAll();
    }

    public int getLastInquiryId() {
        return ptCodeInquiryRepository.findMaxId();
    }

    public PTCodeInquiry insertNew(int investorType, String ptCode, String nationalCode) {
        PTCodeInquiry newInquiry = new PTCodeInquiry();
        newInquiry.setInvestorType(investorType);
        newInquiry.setPtcode(ptCode.replaceAll("\\s", ""));
        newInquiry.setNationalId(nationalCode);
        newInquiry.setRequestDate(new Date());
        PTCodeInquiry insertedInquiry = ptCodeInquiryRepository.save(newInquiry);
        String reqNum = "1" + String.format("%019d", newInquiry.getId());
        insertedInquiry.setRequestNum(reqNum);
        ptCodeInquiryRepository.save(newInquiry);
        return newInquiry;
    }

    public List<PTCodeInquiry> getUnresponsedInquiries() {
        return ptCodeInquiryRepository.findByResStatusIsNull();
    }

    public List<PTCodeInquiry> getRecentInquiries(int maxId) {
        return ptCodeInquiryRepository.findByIdGreaterThan(maxId);
    }

    public PTCodeInquiry getInquiryRequest(String requestNum) {
        return ptCodeInquiryRepository.findByRequestNum(requestNum).orElse(null);
    }

    public void updateInquiryRequest(PTCodeInquiry inquiry) {
        ptCodeInquiryRepository.save(inquiry);
    }

    public Investor pushResponseToInvestor(PTCodeInquiry item) {
        Investor inv;
        if (item.getResAsciiPtcode() == null || item.getResAsciiPtcode().isEmpty()) {
            inv = investorService.findByPTCodeAndNationalId(item.getPtcode(), item.getNationalId()).orElse(null);
            if (inv != null) {
                inv.setActivityState(investorService.getActivityStateById(item.getResActivityState()));
                inv.setLastUpdateDate(new Date());
                investorService.saveInvestor(inv);
            }
            return inv;
        }

        inv = investorService.findByPTCode14(item.getResAsciiPtcode()).orElse(null);
        if (inv == null)
            inv = new Investor();

        inv.setPersonType(investorService.getPersonTypeById(item.getResInvestorType()));
        inv.setPtcode(item.getResPtcode());
        inv.setAsciiPtcode(item.getResAsciiPtcode());
        if (item.getResInvestorType() != null)
            inv.setNationalId(item.getResInvestorType() == 1 ? item.getResNationalId() : item.getResTradingId());

        String fullname = item.getResFullname();
        String[] parts = fullname.split(" ، ");
        inv.setLastName(parts[0]);
        if (parts.length > 1) inv.setFirstName(parts[1]);

        inv.setIdNumber(item.getResIssueNo());
        inv.setBirthDate(item.getResBirthDate());
        inv.setFatherName(item.getResFatherName());
        inv.setIdSerie(item.getResSerie());
        inv.setIssuePlace(item.getResIssuePlace());
        inv.setActivityState(investorService.getActivityStateById(item.getResActivityState()));
        inv.setLastUpdateDate(new Date());

        investorService.saveInvestor(inv);

        return inv;
    }

}
