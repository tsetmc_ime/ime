package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Asset;
import com.tsetmc.ime400.models.AssetTransaction;
import com.tsetmc.ime400.models.AssetTransfer;
import com.tsetmc.ime400.repositories.AssetRepository;
import com.tsetmc.ime400.repositories.AssetTransactionRepository;
import com.tsetmc.ime400.repositories.AssetTransferRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Service
public class AssetTransferService {

    @Autowired
    AssetTransferRepository assetTransferRepository;

    @Autowired
    AssetTransactionRepository assetTransactionRepository;

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    StorageService storageService;

    @Autowired
    DataSource datasource;

    public AssetTransferService() throws SQLException {
    }

    public void save(AssetTransfer assetTransfer) {
        assetTransferRepository.save(assetTransfer);
    }

    public List<AssetTransfer> getNotAppliedAssetTransfers() {
        return assetTransferRepository.findAllByIsApplied((byte) 0);
    }

    public AssetTransfer getAssetTransfer(String symbol, String asciiPtCode, String letterNumber, String letterDate) {
        return assetTransferRepository.findByParameter(symbol, asciiPtCode, letterNumber, letterDate).orElse(null);
    }

    @Transactional(rollbackFor = {SQLException.class})
    public void applyAssetTransferInDB(AssetTransfer assetTransfer, AssetTransaction assetTransaction, Asset asset) throws SQLException {
        assetTransferRepository.save(assetTransfer);
        assetTransactionRepository.save(assetTransaction);
        assetRepository.save(asset);
    }

    private String[] getQuery(String searchTerm, Integer storageId, String orderByColName, String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append("select at.id as DT_RowId, at.id 'ردیف',at.symbol 'نماد',at.letter_number 'شماره نامه',at.letter_date 'تاریخ نامه',at.quantity 'مقدار انتقال',i.ptcode 'کد بورسی',\n" +
                "       i.ascii_ptcode 'کد ۱۴ رقمی',i.national_id 'کد ملی',i.first_name 'نام',i.last_name 'نام خانوادگی' \n" +
                "       from ime.asset_transfer at inner join ime.investor i\n" +
                "on at.ascii_ptcode=i.ascii_ptcode ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total \n" +
                "from ime.asset_transfer at inner join ime.investor i\n" +
                "on at.ascii_ptcode=i.ascii_ptcode ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total \n" +
                "from ime.asset_transfer at inner join ime.investor i\n" +
                "on at.ascii_ptcode=i.ascii_ptcode ");

        if (storageId != null) {
            String symbolSearch = storageService.getStorageSymbol(storageId).replaceAll(" ", "");
            query.append(String.format("and at.symbol='%s' ", symbolSearch));
            filteredRowCountQuery.append(String.format("and at.symbol='%s' ", symbolSearch));
            rowCountQuery.append(String.format("and at.symbol='%s' ", symbolSearch));
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format("and (MATCH(at.symbol,at.ptcode) AGAINST('%1$s' in boolean mode) or " +
                    "MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) ) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(at.symbol,at.ptcode) AGAINST('%1$s' in boolean mode) or " +
                    "MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode) ) ", querySearchTerm.toString()));
        }
        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getAssetTransferList(String search, Integer storageId, int start, int length, String orderKey, String orderDir) {
        String[] queries = getQuery(search, storageId, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getAssetTransferList(String search, Integer storageId, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, storageId, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
