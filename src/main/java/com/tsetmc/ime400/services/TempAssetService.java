package com.tsetmc.ime400.services;


import com.tsetmc.ime400.models.TempAsset;
import com.tsetmc.ime400.repositories.TempAssetRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@Service
public class TempAssetService {

    @Autowired
    DataSource datasource;

    @Autowired
    TempAssetRepository tempAssetRepository;

    @Autowired
    EntityManager entityManager;

    String[] getQuery(Integer storageId, String searchTerm, String entryFrom, String entryTo, Integer finalConfirmed,
                      String convertFrom, String convertTo,
                      String orderKey, String order) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ta.id as DT_RowId, ta.id 'ردیف', st.name 'نام انبار', inv.first_name 'نام',inv.last_name 'نام خانوادگی',inv.ptcode 'کد سهامداری',inv.ascii_ptcode 'کد ۱۴ رقمی',\n" +
                "                inv.national_id 'کد ملی',ta.quantity 'مقدار',\n" +
                "                ta.entrance_date 'زمان ورود به انبار',ta.convert_date 'زمان تبدیل وضعیت',b.name as 'کارگزار ناظر',\n" +
                "                u1.fullname as 'کاربر ثبت کننده',\n" +
                "                u2.fullname as 'کاربر تأیید کننده',\n" +
                "                ta.register_date 'زمان ثبت',ta.confirm_date 'زمان تأیید',\n" +
                "                IF(ta.converted=1 || ta.converted=2 , 'تبدیل به دارایی بورسی', 'موجود در انبار موقت') as 'وضعیت تبدیل',\n" +
                "                IF(ta.is_final_confirmed=1 , 'تأیید نهایی', 'در انتظار تأیید') as 'وضعیت تأیید',\n" +
                "                case when ta.trade_id is not null then concat(' شماره معامله ',ta.trade_id)\n" +
                "                     when ta.future_delivery_id is not null then concat('تحویل آتی / تسویه آپشن شماره ',ta.future_delivery_id)\n" +
                "                     else 'ثبت شده توسط کاربر'\n" +
                "                end as 'توضیحات' \n"+
                "                FROM ime.temp_asset ta \n" +
                "                inner join ime.storage st on ta.storage_id = st.id\n" +
                "                inner join ime.investor inv on ta.investor_id=inv.id\n" +
                "                left outer join ime.broker b on b.id=ta.broker_id \n" +
                "                left outer join ime.user u1 on u1.id=ta.registrant_user\n" +
                "                left outer join ime.user u2 on u2.id=ta.confirming_user\n" +
                "                where true");
        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("SELECT COUNT(*) as total \n" +
                "                FROM ime.temp_asset ta \n" +
                "                inner join ime.investor inv on ta.investor_id=inv.id\n" +
                "                left outer join ime.broker b on b.id=ta.broker_id \n" +
                "                left outer join ime.user u1 on u1.id=ta.registrant_user\n" +
                "                left outer join ime.user u2 on u2.id=ta.confirming_user\n" +
                "                where true");
        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("SELECT COUNT(*) as total " +
                "                FROM ime.temp_asset ta \n" +
                "                inner join ime.investor inv on ta.investor_id=inv.id\n" +
                "                left outer join ime.broker b on b.id=ta.broker_id \n" +
                "                left outer join ime.user u1 on u1.id=ta.registrant_user\n" +
                "                left outer join ime.user u2 on u2.id=ta.confirming_user\n" +
                "                where true");

        if (storageId != null) {
            query.append(" and ta.storage_id=").append(storageId).append(' ');
            filteredRowCountQuery.append(" and ta.storage_id=").append(storageId).append(' ');
            rowCountQuery.append(" and ta.storage_id=").append(storageId).append(' ');
        }
        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,national_id) AGAINST('%1$s' in boolean mode) " +
                    "or match(b.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format(" and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,national_id) AGAINST('%1$s' in boolean mode) " +
                    "or match(b.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (entryFrom != null && !entryFrom.isEmpty()) {
            query.append(" and Date(ta.entrance_date)>= CAST('").append(entryFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(ta.entrance_date)>= CAST('").append(entryFrom).append("' as Date) ");
        }
        if (entryTo != null && !entryTo.isEmpty()) {
            query.append(" and Date(ta.entrance_date)<= CAST('").append(entryTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(ta.entrance_date)<= CAST('").append(entryTo).append("' as Date) ");
        }
        if (convertFrom != null && !convertFrom.isEmpty()) {
            query.append(" and Date(ta.convert_date) >= CAST('").append(convertFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(ta.convert_date) >= CAST('").append(convertFrom).append("' as Date) ");
        }
        if (convertTo != null && !convertTo.isEmpty()) {
            query.append(" and Date(ta.convert_date) <= CAST('").append(convertTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(ta.convert_date) <= CAST('").append(convertTo).append("' as Date) ");
        }
        if (finalConfirmed != null) {
            query.append(" and ta.is_final_confirmed = ").append(finalConfirmed).append(' ');
            filteredRowCountQuery.append(" and ta.is_final_confirmed = ").append(finalConfirmed).append(' ');
        }
        query.append(" order by `").append(orderKey).append("` ").append(order);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    /* public void saveTempAsset(TempAsset tempAsset) throws SQLException, JSONException {
        Investor investor = entityManager.getReference(Investor.class, tempAsset.getInvestor().getId());
        tempAsset.setInvestorId(investor);
        Storage storage = entityManager.getReference(Storage.class, tempAsset.getStorage().getId());
        tempAsset.setStorage(storage);
        Broker broker = entityManager.getReference(Broker.class, tempAsset.getBroker().getId());
        tempAsset.setBroker(broker);
        tempAssetRepository.save(tempAsset);
    } */

    public String getTempAssetList(Integer storageId, String searchTerm, String entryFrom, String entryTo,Integer finalConfirmed,
                                   String convertFrom, String convertTo, int start, int length,
                                   String orderKey, String order) {

        String[] queries = getQuery(storageId, searchTerm, entryFrom, entryTo,finalConfirmed, convertFrom, convertTo, orderKey, order);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + ',' + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getTempAssetList(Integer storageId, String searchTerm, String entryFrom, String entryTo,Integer finalConfirmed,
                                 String convertFrom, String convertTo,
                                 String orderKey, String order, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(storageId, searchTerm, entryFrom, entryTo,finalConfirmed, convertFrom, convertTo, orderKey, order);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public Optional<TempAsset> getTempAssetById(int id) {
        return tempAssetRepository.findById(id);
    }

    public void saveTempAsset(TempAsset tempAsset) {
        tempAssetRepository.save(tempAsset);
    }

//    public List<TempAsset> getUnconvertedTempAssets() throws SQLException,JSONException {
//        String query="SELECT ta.* from temp_asset ta\n" +
//                "inner join ime.storage s on s.id=ta.storage_id\n" +
//                "inner join ime.instrument i on s.instrument_id=i.id\n" +
//                "inner join ime.convert_time ct on ct.id=i.convert_time\n" +
//                "where ta.converted=0\n" +
//                "and ta.is_final_confirmed\n" +
//                "and (ct.id!=2 or (ct.id=2 and ta.entrance_date<now()))";
//        JSONArray jsArray = JsonService.createListFromSQL(datasource, query);
//        List<TempAsset> taList = tempAssetRepository.findAllByConvertedEquals((byte) 0);
//        return taList;
//    }

    public Optional<TempAsset> getTempAssetById(Integer tempAssetId) {
        return tempAssetRepository.findById(tempAssetId);
    }

    public void deleteTempAsset(int tempAssetId) {
        tempAssetRepository.deleteById(tempAssetId);
    }

    public JSONArray getCurrentDateTempAssets() throws SQLException, JSONException {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        String currDate = dt.format(new Date());

        String query = "SELECT inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,\n" +
                "                inv.national_id,ta.quantity,\n" +
                "                ta.entrance_date,ta.convert_date,b.name as brokerName,\n" +
                "                u1.fullname as registrantUser,\n" +
                "                u2.fullname as confirmingUser,\n" +
                "                ta.register_date,ta.confirm_date,\n" +
                "                IF(ta.converted=1 || ta.converted=2 , 'تبدیل به دارایی بورسی', 'موجود در انبار موقت') as convertState,\n" +
                "                IF(ta.is_final_confirmed=1 , 'تأیید نهایی', 'در انتظار تأیید') as convertState\n" +
                "                FROM ime.temp_asset ta \n" +
                "                inner join ime.investor inv on ta.investor_id=inv.id\n" +
                "                left outer join ime.broker b on b.id=ta.broker_id \n" +
                "                left outer join ime.user u1 on u1.id=ta.registrant_user\n" +
                "                left outer join ime.user u2 on u2.id=ta.confirming_user\n" +
                "                where entrance_date like '%'" + currDate + "'%'";
        return JsonService.createListFromSQL(datasource, query);
    }


}
