package com.tsetmc.ime400.services;

import com.tsetmc.ime400.repositories.ActivityLogRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@Service
public class LogService {

    @Autowired
    DataSource datasource;

    @Autowired
    ActivityLogRepository logRepository;

    String[] getQuery(String search, String orderKey, String orderDir, String dateFrom, String dateTo) {
        StringBuilder query = new StringBuilder();
        query.append(" select l.id DT_RowId, l.id 'ردیف', concat(fullname, ' (', username, ')') 'اپراتور', action 'عنوان عملیات', action_date_time 'زمان', old_values 'مقادیر قبلی', new_values 'مقادیر جدید' ")
                .append("from ime.activity_log l left outer join ime.user u on u.id = l.user_id where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.activity_log l left outer join ime.user u on u.id = l.user_id where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.activity_log ");

        if (dateFrom != null && !dateFrom.isEmpty()) {
            query.append(" and action_date_time >= cast('").append(dateFrom).append("' as date)");
            filteredRowCountQuery.append(" and action_date_time >= cast('").append(dateFrom).append("' as date)");
        }

        if (dateTo != null && !dateTo.isEmpty()) {
            query.append(" and action_date_time <= cast('").append(dateTo).append("' as date)");
            filteredRowCountQuery.append(" and action_date_time <= cast('").append(dateTo).append("' as date)");
        }

        if (!StringUtils.isEmptyOrWhitespace(search)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = search.split("\\s+");
            for (String s : splited)
                querySearchTerm.append("+*").append(s).append("* ");

            String str = String.format(" and (MATCH(l.action) AGAINST('%1$s' in boolean mode) or MATCH(u.username,u.fullname) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            query.append(str);
            filteredRowCountQuery.append(str);
        }

        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getLogList(String search, int offset, int count, String orderKey, String orderDir, String dateFrom, String dateTo) {
        String[] queries = getQuery(search, orderKey, orderDir, dateFrom, dateTo);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getLogList(String search, String orderKey, String orderDir, String dateFrom, String dateTo, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, orderKey, orderDir, dateFrom, dateTo);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
