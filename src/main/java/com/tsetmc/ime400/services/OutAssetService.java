package com.tsetmc.ime400.services;

import com.tsetmc.ime400.dto.OutAssetTotal;
import com.tsetmc.ime400.models.OutAsset;
import com.tsetmc.ime400.repositories.OutAssetRepository;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OutAssetService {
    @Autowired
    DataSource datasource;

    @Autowired
    OutAssetRepository outAssetRepository;

    String[] getQuery(Integer storageId,
                      String searchTerm,
                      String clearanceFrom,
                      String clearanceTo,
                      String outFrom,
                      String outTo,
                      String orderByColName,
                      String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append(" SELECT oa.id as  DT_RowId, oa.id 'ردیف',inv.first_name 'نام',inv.last_name 'نام خانوادگی',\n" +
                "inv.ptcode 'کد بورسی',inv.national_id 'کد ملی',oa.quantity 'مقدار',oa.clearance_date 'زمان ترخیص',IFNULL(oa.cost,0) 'هزینه انبار بورسی (ریال)',\n" +
                "oa.out_date 'زمان خروج نهایی',IFNULL(oa.ocost,0) 'هزینه انبار موقت (ریال)',IFNULL(oa.vat,0) 'مالیات بر ارزش افزوده (ریال)',IFNULL(oa.cost,0)+IFNULL(oa.ocost,0)+IFNULL(oa.vat,0) 'هزینه کل (ریال)' " +
                "FROM ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("SELECT count(*) total \n" +
                "FROM ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("SELECT count(*) total \n" +
                "FROM ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        if (storageId != null) {
            query.append(" and s.id= ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and s.id= ").append(storageId).append(' ');
            rowCountQuery.append(" and s.id= ").append(storageId).append(' ');
        }
        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (clearanceFrom != null && !StringUtils.isEmptyOrWhitespace(clearanceFrom)) {
            // CAST("2020-08-29" AS Date)
            query.append(" and Date(oa.clearance_date)>= CAST('").append(clearanceFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.clearance_date)>= CAST('").append(clearanceFrom).append("' as Date) ");
        }

        if (clearanceTo != null && !StringUtils.isEmptyOrWhitespace(clearanceFrom)) {
            query.append(" and Date(oa.clearance_date) <=CAST('").append(clearanceTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.clearance_date) <=CAST('").append(clearanceTo).append("' as Date) ");
        }

        if (outFrom != null && !StringUtils.isEmptyOrWhitespace(outFrom)) {
            // CAST("2020-08-29" AS Date)
            query.append(" and Date(oa.out_date)>= CAST('").append(outFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.out_date)>= CAST('").append(outFrom).append("' as Date) ");
        }

        if (outTo != null && !StringUtils.isEmptyOrWhitespace(outTo)) {
            query.append(" and Date(oa.out_date) <=CAST('").append(outTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.out_date) <=CAST('").append(outTo).append("' as Date) ");
        }

        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getOutAssetList(Integer storageId,
                                  String searchTerm,
                                  String clearanceFrom,
                                  String clearanceTo,
                                  String outFrom,
                                  String outTo,
                                  int offset,
                                  int count,
                                  String orderByColName,
                                  String ascOrdesc) {
        String[] queries = getQuery(storageId, searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderByColName, ascOrdesc);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getOutAssetList(Integer storageId,
                                String searchTerm,
                                String clearanceFrom,
                                String clearanceTo,
                                String outFrom,
                                String outTo,
                                String orderByColName,
                                String ascOrdesc, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(storageId, searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderByColName, ascOrdesc);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public Optional<OutAsset> findById(int id) {
        return outAssetRepository.findById(id);
    }
    public Optional<OutAssetTotal> getOutAssetTotalInGroup(int id) {
        return outAssetRepository.getOutAssetTotalInGroup(id);
    }
    public List<OutAsset> getAllOutAssetTotalInGroup(int id) {
        return outAssetRepository.getAllOutAssetTotalInGroup(id);
    }

    public void save(OutAsset oa) {
        outAssetRepository.save(oa);
    }

    String[] getReportQuery(String searchTerm, String clearanceFrom, String clearanceTo, String outFrom,
                            String outTo, String orderKey, String orderDir) {
        String query = "    SELECT min(oa.id) as  DT_RowId, row_number() over (order by Date(oa.clearance_date)) as 'ردیف',s.name 'نام انبار',inst.symbol 'نماد',inst.symbol_desc 'شرح نماد', inv.first_name 'نام',inv.last_name 'نام خانوادگی', " +
                "                 inv.ptcode 'کد بورسی',inv.national_id 'کد ملی',SUM(oa.quantity) 'مقدار',Date(oa.clearance_date) 'زمان ترخیص',Sum(IFNULL(oa.cost,0)) 'هزینه انبار بورسی (ریال)', " +
                "                 Date(oa.out_date) 'زمان خروج نهایی',SUM(IFNULL(oa.ocost,0)) 'هزینه انبار موقت (ریال)',Sum(IFNULL(oa.vat,0)) 'مالیات بر ارزش افزوده (ریال)',SUM(IFNULL(oa.cost,0)+IFNULL(oa.ocost,0)+IFNULL(oa.vat,0)) 'هزینه کل (ریال)' " +
                "                 FROM ime.out_asset oa " +
                "                 inner join ime.investor inv on inv.id=oa.investor_id " +
                "                 inner join ime.storage s on s.id=oa.storage_id  " +
                "                 inner join ime.instrument inst on inst.id=s.instrument_id " +
                "                 where true ";
        String filteredRowCountQuery = "SELECT count(*) total\n" +
                "    FROM (select 1 from ime.out_asset oa \n" +
                "    inner join ime.investor inv on inv.id=oa.investor_id \n" +
                "    inner join ime.storage s on s.id=oa.storage_id \n" +
                "    inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "    where true ";
        String rowCountQuery = "SELECT count(*) total\n" +
                "    FROM ( select 1 from ime.out_asset oa \n" +
                "    inner join ime.investor inv on inv.id=oa.investor_id \n" +
                "    inner join ime.storage s on s.id=oa.storage_id \n" +
                "    inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "    where true ";
        String out_date_condition =" and (out_date=cast('0000-00-00' as date) or out_date is null) ";
        boolean out_date_flg = false;
        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query += String.format("  and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)\n" +
                    "                 or MATCH(s.name) AGAINST('%1$s' in boolean mode) or match(inst.symbol ,inst.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            filteredRowCountQuery += String.format("  and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)\n" +
                    "                 or MATCH(s.name) AGAINST('%1$s' in boolean mode) or match(inst.symbol ,inst.symbol_desc) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
        }

        if (clearanceFrom != null && !clearanceFrom.isEmpty()) {
            // CAST("2020-08-29" AS Date)
            query += " and Date(oa.clearance_date)>= CAST('" + clearanceFrom + "' as Date)";
            filteredRowCountQuery += " and Date(oa.clearance_date)>= CAST('" + clearanceFrom + "' as Date)";
        }

        if (clearanceTo != null && !clearanceTo.isEmpty()) {
            query += " and Date(oa.clearance_date) <=CAST('" + clearanceTo + "' as Date)";
            filteredRowCountQuery += " and Date(oa.clearance_date) <=CAST('" + clearanceTo + "' as Date)";
        }

        if (outFrom != null && !outFrom.isEmpty()) {
            out_date_flg = true;
            // CAST("2020-08-29" AS Date)
            query += " and Date(oa.out_date)>= CAST('" + outFrom + "' as Date)";
            filteredRowCountQuery += " and Date(oa.out_date)>= CAST('" + outFrom + "' as Date)";
        }

        if (outTo != null && !outTo.isEmpty()) {
            out_date_flg = true;
            query += " and Date(oa.out_date) <=CAST('" + outTo + "' as Date)";
            filteredRowCountQuery += " and Date(oa.out_date) <=CAST('" + outTo + "' as Date)";
        }
        if(!out_date_flg){
            query += out_date_condition;
            filteredRowCountQuery += out_date_condition;
        }

        query += " group by s.name,inst.symbol,inst.symbol_desc,inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)";
        filteredRowCountQuery += " group by s.name,inst.symbol,inst.symbol_desc,inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)) as tb";
        rowCountQuery += " group by s.name,inst.symbol,inst.symbol_desc,inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)) as tb";


        query += " order by `" + orderKey + "` " + orderDir;

        return new String[]{query, rowCountQuery, filteredRowCountQuery};
    }

    public String getOutAssetReport(String searchTerm, String clearanceFrom, String clearanceTo, String outFrom,
                                    String outTo, int start, int length, String orderKey, String orderDir) {
        String[] queries = getReportQuery(searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getOutAssetReport(String searchTerm, String clearanceFrom, String clearanceTo, String outFrom,
                                  String outTo, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getReportQuery(searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }
    String[] getReportTotalQuery(Integer storageId,
                      String searchTerm,
                      String clearanceFrom,
                      String clearanceTo,
                      String outFrom,
                      String outTo,
                      String orderByColName,
                      String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append(" SELECT min(oa.id) as  DT_RowId, row_number() over (order by Date(oa.clearance_date)) 'ردیف',inv.first_name 'نام',inv.last_name 'نام خانوادگی',\n" +
                "inv.ptcode 'کد بورسی',inv.national_id 'کد ملی',SUM(oa.quantity) 'مقدار',Date(oa.clearance_date) 'زمان ترخیص',SUM(IFNULL(oa.cost,0)) 'هزینه انبار بورسی (ریال)',\n" +
                "Date(oa.out_date) 'زمان خروج نهایی',SUM(IFNULL(oa.ocost,0)) 'هزینه انبار موقت (ریال)',SUM(IFNULL(oa.vat,0)) 'مالیات بر ارزش افزوده (ریال)',SUM(IFNULL(oa.cost,0)+IFNULL(oa.ocost,0)+IFNULL(oa.vat,0)) 'هزینه کل (ریال)' " +
                "FROM ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("SELECT count(*) total \n" +
                "FROM (select 1 from ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("SELECT count(*) total \n" +
                "FROM (select 1 from ime.out_asset oa\n" +
                "inner join ime.investor inv on inv.id=oa.investor_id\n" +
                "inner join ime.storage s on s.id=oa.storage_id\n" +
                "where true ");

        if (storageId != null) {
            query.append(" and s.id= ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and s.id= ").append(storageId).append(' ');
            rowCountQuery.append(" and s.id= ").append(storageId).append(' ');
        }
        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(inv.first_name,inv.last_name,inv.ptcode,inv.ascii_ptcode,inv.national_id) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (clearanceFrom != null && !StringUtils.isEmptyOrWhitespace(clearanceFrom)) {
            // CAST("2020-08-29" AS Date)
            query.append(" and Date(oa.clearance_date)>= CAST('").append(clearanceFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.clearance_date)>= CAST('").append(clearanceFrom).append("' as Date) ");
        }

        if (clearanceTo != null && !StringUtils.isEmptyOrWhitespace(clearanceFrom)) {
            query.append(" and Date(oa.clearance_date) <=CAST('").append(clearanceTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.clearance_date) <=CAST('").append(clearanceTo).append("' as Date) ");
        }

        if (outFrom != null && !StringUtils.isEmptyOrWhitespace(outFrom)) {
            // CAST("2020-08-29" AS Date)
            query.append(" and Date(oa.out_date)>= CAST('").append(outFrom).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.out_date)>= CAST('").append(outFrom).append("' as Date) ");
        }

        if (outTo != null && !StringUtils.isEmptyOrWhitespace(outTo)) {
            query.append(" and Date(oa.out_date) <=CAST('").append(outTo).append("' as Date) ");
            filteredRowCountQuery.append(" and Date(oa.out_date) <=CAST('").append(outTo).append("' as Date) ");
        }
        query.append(" group by inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)");
        filteredRowCountQuery.append(" group by inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)) as tb");
        rowCountQuery.append(" group by inv.first_name,inv.last_name,inv.ptcode,inv.national_id,Date(oa.clearance_date),Date(oa.out_date)) as tb");

        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }
    public String getOutAssetTotalList(Integer storageId,
                                       String searchTerm,
                                       String clearanceFrom,
                                       String clearanceTo,
                                       String outFrom,
                                       String outTo,
                                       int offset,
                                       int count,
                                       String orderByColName,
                                       String ascOrdesc) {
        String[] queries = getReportTotalQuery(storageId, searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderByColName, ascOrdesc);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getOutAssetTotalList(Integer storageId,
                                     String searchTerm,
                                     String clearanceFrom,
                                     String clearanceTo,
                                     String outFrom,
                                     String outTo,
                                     String orderByColName,
                                     String ascOrdesc, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getReportTotalQuery(storageId, searchTerm, clearanceFrom, clearanceTo, outFrom, outTo, orderByColName, ascOrdesc);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }
}
