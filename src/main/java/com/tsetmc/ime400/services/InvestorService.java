package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.ActivityState;
import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.models.PersonType;
import com.tsetmc.ime400.repositories.ActivityStateRepository;
import com.tsetmc.ime400.repositories.InvestorRepository;
import com.tsetmc.ime400.repositories.PersonTypeRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class InvestorService {

    @Autowired
    DataSource datasource;

    @Autowired
    InvestorRepository investorRepository;

    @Autowired
    PersonTypeRepository personTypeRepository;

    @Autowired
    ActivityStateRepository activityStateRepository;

    public Optional<Investor> findByPTCodeAndNationalId(String ptCode, String nationalId) {
        return investorRepository.findByPtcodeAndNationalId(ptCode, nationalId);
    }

    public Optional<Investor> getInvestor(String ptCode, String nationalId) {
        return investorRepository.findByPtcodeAndNationalId(ptCode, nationalId);
    }

    public Optional<Investor> findByPTCode14(String ptcode14) {
        return investorRepository.findByAsciiPtcode(ptcode14);
    }

    public Optional<Investor> findById(Integer id) {
        return investorRepository.findById(id);
    }

    public void saveInvestor(Investor i) {
        investorRepository.save(i);
    }

    public Optional<Investor> getInvestorById(int id) {
        return investorRepository.findById(id);
    }

    String[] getQuery(String searchTerm,
                      Integer status,
                      Integer type,
                      String orderByColName,
                      String ascOrdesc) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT i.id as DT_RowId, i.id 'ردیف', i.first_name 'نام', i.last_name 'نام خانوادگی', i.national_id 'کد ملی', i.ptcode 'کد بورسی', i.ascii_ptcode 'کد ۱۴ رقمی', p.description as 'نوع', " +
                " ac.description as 'وضعیت', i.last_update_date 'آخرین تاریخ استعلام' " +
                " FROM ime.investor i left outer join activity_state ac on i.activity_state = ac.id " +
                " left outer join person_type p on i.person_type = p.id " +
                "WHERE true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("SELECT count(*) as total FROM ime.investor i WHERE true ");

        String rowCountQuery = "SELECT count(*) as total FROM ime.investor i WHERE true ";

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] split = searchTerm.split("\\s+");
            for (String s : split) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format("and (MATCH(ptcode,ascii_ptcode,national_id,first_name,last_name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format("and (MATCH(ptcode,ascii_ptcode,national_id,first_name,last_name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString()));
        }

        if (status != null) {
            query.append(" and i.activity_state=").append(status);
            filteredRowCountQuery.append(" and i.activity_state=").append(status);
        }

        if (type != null) {
            query.append(" and i.person_type=").append(type).append(' ');
            filteredRowCountQuery.append(" and i.person_type=").append(type).append(' ');
        }

        query.append(" order by `").append(orderByColName).append("` ").append(ascOrdesc);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getInvestorList(String searchTerm,
                                  Integer status,
                                  Integer type,
                                  int offset,
                                  int count,
                                  String orderByColName,
                                  String ascOrdesc) {
        String[] queries = getQuery(searchTerm, status, type, orderByColName, ascOrdesc);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getInvestorList(String searchTerm,
                                Integer status,
                                Integer type,
                                String orderByColName,
                                String ascOrdesc,
                                JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(searchTerm, status, type, orderByColName, ascOrdesc);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public PersonType getPersonType(String personTypeInAs400) {
        int personType;
        switch (personTypeInAs400) {
            case "1":
            case "4":
            case "د":
            case "d":
            case "D":
                personType = 1;
                break;
            default:
                personType = 2;
        }
        return personTypeRepository.findById(personType).orElse(null);
    }

    public ActivityState getActivityStateById(int id) {
        return activityStateRepository.findById(id).orElse(null);
    }

    public PersonType getPersonTypeById(int id) {
        return personTypeRepository.findById(id).orElse(null);
    }
}
