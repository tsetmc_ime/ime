package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.AssetRequest;
import com.tsetmc.ime400.repositories.AssetRequestRepository;
import com.tsetmc.ime400.util.JDBCService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Service
public class AssetRequestService {

    @Autowired
    AssetRequestRepository assetRequestRepository;

    @Autowired
    DataSource datasource;

    public JSONObject getStats() throws SQLException, JSONException {
        final String query =
                "SELECT  \n" +
                        "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<18000 and  response_status=1) as 'ایجاد دارایی موفق',\n" +
                        "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<18000 and  response_status=0 or response_status is null) as 'در صف انتظار',\n" +
                        "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<18000 and  response_status=2) as 'عدم وجود کد بورسی',\n" +
                        "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<18000 and  response_status=3) as 'عدم وجود نماد',\n" +
                        // "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<30 and  response_status=4) as '',\n" +
                        "(SELECT count(*) FROM ime.asset_request where datediff(curdate(),request_date)<18000 and  response_status=5) as 'کد ممنوع المعامله'";
        return JsonService.createObjectFromSQL(datasource, query);
    }

    public JSONObject getStatByUserId(int userId) throws SQLException, JSONException {
        final String query =
                String.format(
                        "select \n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.temp_asset ta on ta.storage_id=s.id\n" +
                                "inner join ime.asset_request ar on ar.temp_asset_id=ta.id and ar.response_status is null) as 'در صف انتظار',\n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.temp_asset ta on ta.storage_id=s.id\n" +
                                "inner join ime.asset_request ar on ar.temp_asset_id=ta.id and ar.response_status is not null and ar.response_status!=1) as 'ناموفق',\n" +
                                "(select count(*) from ime.user u \n" +
                                "inner join ime.storage s on u.id=%1$d and u.storage_id=s.id\n" +
                                "inner join ime.temp_asset ta on ta.storage_id=s.id\n" +
                                "inner join ime.asset_request ar on ar.temp_asset_id=ta.id and ar.response_status is not null and ar.response_status=1) as 'موفق'", userId);
        return JsonService.createObjectFromSQL(datasource, query);
    }

    public void generateAssetRequests() throws SQLException {
        String query = "insert into ime.asset_request(temp_asset_id)\n" +
                "SELECT ta.id from ime.temp_asset ta\n" +
                "inner join ime.storage s on s.id=ta.storage_id\n" +
                "inner join ime.instrument i on s.instrument_id=i.id\n" +
                "inner join ime.convert_time ct on ct.id=i.convert_time\n" +
                "where ta.converted=0\n" +
                "and ta.is_final_confirmed\n" +
                "and (ct.id!=2 or (ct.id=2 and ta.entrance_date<now()))\n" +
                "and ta.id not in\n" +
                "(\n" +
                "select temp_asset_id from ime.asset_request\n" +
                "where response_status is null \n" +
                ")";
        JDBCService.execModifyingQuery(datasource, query);
        List<AssetRequest> arList = assetRequestRepository.findByRequestNumberIsNullAndRequestDateIsNull();

        for (AssetRequest ar : arList) {
            ar.setRequestDate(new Date());
            String reqNum = "3" + String.format("%019d", ar.getId());
            ar.setRequestNumber(reqNum);
            assetRequestRepository.save(ar);
        }
    }

    public List<AssetRequest> getUnresponsedAssetRequests() {
        return assetRequestRepository.findAllByResponseStatusIsNull();
    }

    public void delete(int id) {
        assetRequestRepository.deleteById(id);
    }

    public AssetRequest getByReqestNumber(String reqnum) {
        return assetRequestRepository.findFirstByRequestNumber(reqnum).orElse(null);
    }

    public AssetRequest save(AssetRequest assetRequest) {
        return assetRequestRepository.save(assetRequest);
    }

    String[] getReportQuery(String searchTerm, String from, String to, Boolean done, String orderKey, String orderDir) {
        String query = "select ar.id DT_RowId, ar.id 'ردیف', s.name 'نام انبار',inst.symbol 'نماد',inst.symbol_desc 'شرح نماد',inv.first_name 'نام',inv.last_name 'نام خانوادگی',\n" +
                "       inv.national_id 'کد ملی',inv.ptcode 'کد سهامداری',inv.ascii_ptcode 'کد ۱۴ رقمی',ta.quantity 'مقدار',ar.request_date 'تاریخ ثبت درخواست',ar.response_date 'تاریخ دریافت پاسخ',ars.description 'وضعیت'\n" +
                "from ime.asset_request ar\n" +
                "         inner join ime.temp_asset ta on ta.id=ar.temp_asset_id\n" +
                "         inner join ime.storage s on s.id=ta.storage_id\n" +
                "         inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "         inner join ime.investor inv on inv.id=ta.investor_id\n" +
                "         inner join ime.asset_response_status ars on ars.id=ar.response_status " +
                " where true ";
        String filteredRowCountQuery = "select count(*) total \n" +
                "from ime.asset_request ar\n" +
                "         inner join ime.temp_asset ta on ta.id=ar.temp_asset_id\n" +
                "         inner join ime.storage s on s.id=ta.storage_id\n" +
                "         inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "         inner join ime.investor inv on inv.id=ta.investor_id\n" +
                "         inner join ime.asset_response_status ars on ars.id=ar.response_status " +
                " where true ";
        String rowCountQuery = "select count(*) total \n" +
                "from ime.asset_request ar\n" +
                "         inner join ime.temp_asset ta on ta.id=ar.temp_asset_id\n" +
                "         inner join ime.storage s on s.id=ta.storage_id\n" +
                "         inner join ime.instrument inst on inst.id=s.instrument_id\n" +
                "         inner join ime.investor inv on inv.id=ta.investor_id\n" +
                "         inner join ime.asset_response_status ars on ars.id=ar.response_status " +
                " where true ";

        if (done != null) {
            if (done) {
                query += " and ars.id=1 ";
                filteredRowCountQuery += " and ars.id=1 ";
            } else {
                query += " and ars.id is not null and ars.id!=1 ";
                filteredRowCountQuery += " and ars.id is not null and ars.id!=1 ";
            }
        }
        if (from != null && !from.isEmpty()) {
            query += " and Date(ar.request_date)>= CAST('" + from + "' as Date) ";
            filteredRowCountQuery += " and Date(ar.request_date)>= CAST('" + from + "' as Date) ";
        }
        if (to != null && !to.isEmpty()) {
            query += " and Date(ar.request_date)<= CAST('" + to + "' as Date) ";
            filteredRowCountQuery += " and Date(ar.request_date)<= CAST('" + to + "' as Date) ";
        }
        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited)
                querySearchTerm.append("+*").append(s).append("* ");

            query += String.format(" and (MATCH(inv.ptcode,inv.ascii_ptcode,inv.national_id,inv.first_name,inv.last_name) AGAINST('%1$s' in boolean mode) " +
                    "or MATCH(s.name) AGAINST('%1$s' in boolean mode) " +
                    "or MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode))\n", querySearchTerm.toString());
            filteredRowCountQuery += String.format(" and (MATCH(inv.ptcode,inv.ascii_ptcode,inv.national_id,inv.first_name,inv.last_name) AGAINST('%1$s' in boolean mode)\n" +
                    "                or MATCH(s.name) AGAINST('%1$s' in boolean mode)\n" +
                    "                or MATCH(inst.symbol,inst.symbol_desc) AGAINST('%1$s' in boolean mode))", querySearchTerm.toString());
        }

        query += " order by `" + orderKey + "` " + orderDir;

        return new String[]{query, rowCountQuery, filteredRowCountQuery};
    }

    public String getReport(String searchTerm, String from, String to, Boolean done, int start, int length, String orderKey, String orderDir) {
        String[] queries = getReportQuery(searchTerm, from, to, done, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getReport(String searchTerm, String from, String to, Boolean done, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getReportQuery(searchTerm, from, to, done, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}

