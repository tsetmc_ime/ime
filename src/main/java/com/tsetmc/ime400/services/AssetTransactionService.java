package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.AssetTransaction;
import com.tsetmc.ime400.repositories.AssetTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssetTransactionService {

    @Autowired
    AssetTransactionRepository assetTransactionRepository;

    public void save(AssetTransaction assetTransaction) {
        assetTransactionRepository.save(assetTransaction);
    }

}
