package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.Asset;
import com.tsetmc.ime400.repositories.AssetRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class AssetServices {

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    DataSource datasource;

    public int getAssetAmount(int investorId, int storageId) {
        return assetRepository.getAsset(investorId, storageId).map(Asset::getQuantity).orElse(0);
    }

    public Optional<Asset> getAsset(int investorId, int storageId) {
        return assetRepository.findByInvestorIdAndStorageId(investorId, storageId);
    }

    public Optional<Asset> getAssetById(int id) {
        return assetRepository.findById(id);
    }

    public void save(Asset asset) {
        assetRepository.save(asset);
    }

    String[] getQuery(Integer storageId, String searchTerm, String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append("select a.id DT_RowId, a.id 'ردیف', s.name 'نام انبار', ins.symbol 'نماد', a.quantity 'مقدار',i.ptcode 'کد سهامداری',i.ascii_ptcode 'کد ۱۴ رقمی',i.national_id 'کد ملی'," +
                "i.first_name 'نام',i.last_name 'نام خانوادگی' from \n" +
                "ime.asset a " + " inner join ime.investor i on a.investor_id=i.id " +
                "inner join ime.storage s on s.id=a.storage_id " +
                "inner join ime.instrument ins on s.instrument_id =ins.id " +
                "where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) total from \n" +
                "ime.asset a " + " inner join ime.investor i on a.investor_id=i.id " +
                "inner join ime.storage s on s.id=a.storage_id " +
                "inner join ime.instrument ins on s.instrument_id =ins.id " +
                "where true ");

        if (storageId != null) {
            query.append(" and s.id= ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and s.id= ").append(storageId).append(' ');
        }

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited)
                querySearchTerm.append("+*").append(s).append("* ");

            query.append(String.format(" and MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode)\n", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format(" and MATCH(i.ptcode,i.ascii_ptcode,i.national_id,i.first_name,i.last_name) AGAINST('%1$s' in boolean mode)\n", querySearchTerm.toString()));
        }

        if (storageId != null) {
            query.append(" and a.storage_id = ").append(storageId).append(' ');
            filteredRowCountQuery.append(" and a.storage_id = ").append(storageId).append(' ');
        }

        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        String rowCountQuery = "select count(*) total from \n" +
                "ime.asset a " + " inner join ime.investor i on a.investor_id=i.id " +
                "inner join ime.storage s on s.id=a.storage_id " +
                "inner join ime.instrument ins on s.instrument_id =ins.id " +
                "where true ";

        return new String[]{query.toString(), rowCountQuery, filteredRowCountQuery.toString()};
    }

    public String getAssets(Integer storageId, String searchTerm, int start, int length, String orderKey, String orderDir) {
        String[] queries = getQuery(storageId, searchTerm, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getAssetList(Integer storageId, String searchTerm, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(storageId, searchTerm, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    String[] getReportQuery(String searchTerm, String orderKey, String orderDir) {
        String query = "select inv.id as DT_RowId, inv.id 'ردیف', inv.ptcode 'کد سهامداری',inv.first_name 'نام',inv.last_name 'نام خانوادگی',inv.national_id 'کد ملی',inv.ascii_ptcode 'کد ۱۴ رقمی',\n" +
                "       ins.symbol 'نماد',ins.symbol_desc 'شرح نماد',s.name 'نام انبار',ifnull(ta.taQuantity,0) 'موجودی موقت',ifnull(a.quantity,0) 'موجودی بورسی',ifnull(oa.oaQuantity,0) 'موجودی در آستانه خروج'\n" +
                "from ime.investor inv\n" +
                "left outer join ime.asset a on a.investor_id=inv.id\n" +
                "left outer join\n" +
                "    (select investor_id,storage_id,sum(quantity) taQuantity\n" +
                "from ime.temp_asset ta where !converted group by investor_id, storage_id) ta\n" +
                "on ta.investor_id=a.investor_id and a.storage_id=ta.storage_id\n" +
                "left outer join\n" +
                "    (select investor_id,storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by investor_id, storage_id\n" +
                "        ) oa\n" +
                "on oa.investor_id=a.investor_id and a.storage_id=a.storage_id\n" +
                "inner join ime.storage s on ifnull(ifnull(a.storage_id,ta.storage_id),oa.storage_id)=s.id\n" +
                "inner join ime.instrument ins on ins.id=s.instrument_id\n" +
                "where ((a.quantity is not null and a.quantity>0) or\n" +
                "      (ta.taQuantity is not null and ta.taQuantity>0) or\n" +
                "      (oa.oaQuantity is not null and oa.oaQuantity>0)) \n";
        String filteredRowCountQuery = "select count(*) total\n" +
                "from ime.investor inv\n" +
                "left outer join ime.asset a on a.investor_id=inv.id\n" +
                "left outer join (select investor_id,storage_id,sum(quantity) taQuantity\n" +
                "                from ime.temp_asset ta where !converted group by investor_id, storage_id) ta\n" +
                "on ta.investor_id=a.investor_id and a.storage_id=ta.storage_id\n" +
                "left outer join\n" +
                "    (select investor_id,storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by investor_id, storage_id\n" +
                "        ) oa\n" +
                "on oa.investor_id=a.investor_id and a.storage_id=a.storage_id\n" +
                "inner join ime.storage s on ifnull(ifnull(a.storage_id,ta.storage_id),oa.storage_id)=s.id\n" +
                "inner join ime.instrument ins on ins.id=s.instrument_id\n" +
                "where ((a.quantity is not null and a.quantity>0) or\n" +
                "      (ta.taQuantity is not null and ta.taQuantity>0) or\n" +
                "      (oa.oaQuantity is not null and oa.oaQuantity>0)) ";
        String rowCountQuery = "select count(*) total\n" +
                "from ime.investor inv\n" +
                "left outer join ime.asset a on a.investor_id=inv.id\n" +
                "left outer join (select investor_id,storage_id,sum(quantity) taQuantity\n" +
                "                from ime.temp_asset ta where !converted group by investor_id, storage_id) ta\n" +
                "on ta.investor_id=a.investor_id and a.storage_id=ta.storage_id\n" +
                "left outer join\n" +
                "    (select investor_id,storage_id,sum(quantity) oaQuantity\n" +
                "    from out_asset where out_date=cast('0000-00-00' as date) or out_date is null\n" +
                "    group by investor_id, storage_id\n" +
                "        ) oa\n" +
                "on oa.investor_id=a.investor_id and a.storage_id=a.storage_id\n" +
                "inner join ime.storage s on ifnull(ifnull(a.storage_id,ta.storage_id),oa.storage_id)=s.id\n" +
                "inner join ime.instrument ins on ins.id=s.instrument_id\n" +
                "where ((a.quantity is not null and a.quantity>0) or\n" +
                "      (ta.taQuantity is not null and ta.taQuantity>0) or\n" +
                "      (oa.oaQuantity is not null and oa.oaQuantity>0)) ";

        if (!StringUtils.isEmptyOrWhitespace(searchTerm)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = searchTerm.split("\\s+");
            for (String s : splited)
                querySearchTerm.append("+*").append(s).append("* ");

            String str = String.format(" and (MATCH(inv.ptcode,inv.ascii_ptcode,inv.national_id,inv.first_name,inv.last_name) AGAINST('%1$s' in boolean mode) " +
                    "            or MATCH(ins.symbol,ins.symbol_desc) AGAINST('%1$s' in boolean mode) " +
                    "            or MATCH(s.name) AGAINST('%1$s' in boolean mode)) ", querySearchTerm.toString());
            query += str;
            filteredRowCountQuery += str;
        }

        query += " order by `" + orderKey + "` " + orderDir;

        return new String[]{query, rowCountQuery, filteredRowCountQuery};
    }

    public String getAssetReport(String searchTerm, int start, int length, String orderKey, String orderDir) {
        String[] queries = getReportQuery(searchTerm, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + start + "," + length);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getAssetReport(String searchTerm, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getReportQuery(searchTerm, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

}
