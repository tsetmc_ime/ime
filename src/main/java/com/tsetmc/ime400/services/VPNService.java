package com.tsetmc.ime400.services;

import com.tsetmc.ime400.models.VPN;
import com.tsetmc.ime400.repositories.VPNRepository;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@Service
public class VPNService {

    @Autowired
    VPNRepository vpnRepository;
    @Autowired
    DataSource datasource;

    public VPN FindById(Integer id) {
        return vpnRepository.findById(id).orElse(null);
    }

    String[] getQuery(String search, String orderKey, String orderDir) {
        StringBuilder query = new StringBuilder();
        query.append(" select v.id DT_RowId, v.id 'ردیف', v.storageName 'نام انبار' " +
                ", v.userName  'نام کاربری', v.password 'کلمه عبور', v.staticIP 'آی پی'" +
                "from ime.vpn v  where true ");

        StringBuilder filteredRowCountQuery = new StringBuilder();
        filteredRowCountQuery.append("select count(*) as total from ime.vpn v where true ");

        StringBuilder rowCountQuery = new StringBuilder();
        rowCountQuery.append("select count(*) as total from ime.vpn v ");
        if (!StringUtils.isEmptyOrWhitespace(search)) {
            StringBuilder querySearchTerm = new StringBuilder();
            String[] splited = search.split("\\s+");
            for (String s : splited) {
                querySearchTerm.append("+*").append(s).append("* ");
            }

            query.append(String.format(" and (MATCH(v.StorageName,v.UserName,v.StaticIP) AGAINST('%1$s' in boolean mode))", querySearchTerm.toString()));
            filteredRowCountQuery.append(String.format( " and (MATCH(v.StorageName,v.UserName,v.StaticIP) AGAINST('%1$s' in boolean mode))", querySearchTerm.toString()));
        }
        query.append(" order by `").append(orderKey).append("` ").append(orderDir);

        return new String[]{query.toString(), rowCountQuery.toString(), filteredRowCountQuery.toString()};
    }

    public String getVPNList(String search, int offset, int count, String orderKey, String orderDir) {
        String[] queries = getQuery(search, orderKey, orderDir);

        try {
            JSONArray jsArray = JsonService.createListFromSQL(datasource, queries[0] + " limit " + offset + "," + count);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("recordsFiltered", JsonService.createObjectFromSQL(datasource, queries[2]).getInt("total"));
            jsonObject.put("recordsTotal", JsonService.createObjectFromSQL(datasource, queries[1]).getInt("total"));
            jsonObject.put("data", jsArray);

            return jsonObject.toString();

        } catch (Exception e) {
            return "{\"recordsFiltered\":0,\"recordsTotal\":0,\"data\":[]}";
        }
    }

    public void getVPNList(String search, String orderKey, String orderDir, JsonService.ResultSetIterator iter) throws SQLException, JSONException, IOException {
        String[] queries = getQuery(search, orderKey, orderDir);
        JsonService.iterateOverSQL(datasource, queries[0], iter);
    }

    public void save(VPN vpn) {
        vpnRepository.save(vpn);
    }

}
