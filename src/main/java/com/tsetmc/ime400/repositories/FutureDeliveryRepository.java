package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.dto.FutureDeliveryDto;
import com.tsetmc.ime400.models.FutureDelivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FutureDeliveryRepository extends JpaRepository<FutureDelivery,Long> {

    List<FutureDelivery> findAllByTransactionTime(LocalDate date);
//    @Query(value = "SELECT f.ptcode14, i.ptcode,f.symbol,f.requested_quantiry,f.broker_code, f.transaction_time, f.ocost, f.delivered_quantity FROM future_delivery as f " +
//            " inner join investor i on f.ptcode14 = i.ascii_ptcode " +
//            " where f.transaction_time = ?1", nativeQuery = true)
//    List<FutureDeliveryDto> getAllByTransactionTime(LocalDate date);
    @Query(value = "SELECT new com.tsetmc.ime400.dto.FutureDeliveryDto( f.ptcode14, i.ptcode,f.symbol,f.requestedQuantiry,f.brokerCode, f.transactionTime, f.ocost, f.deliveredQuantity) " +
            " FROM FutureDelivery as f " +
            " inner join Investor i on f.ptcode14 = i.asciiPtcode " +
            " where f.transactionTime = ?1")
    List<FutureDeliveryDto> getAllByTransactionTime(LocalDate date);
}
