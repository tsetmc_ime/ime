package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.FutureDeliveryDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FutureDeliveryDateRepository extends JpaRepository<FutureDeliveryDate,Integer>{

}
