package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.AssetTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssetTransferRepository extends JpaRepository<AssetTransfer, Integer> {
    List<AssetTransfer> findAllByIsApplied(byte isApplies);

    @Query("select a from AssetTransfer a where a.symbol=?1 and a.ascii_ptcode=?2 and a.letterNumber=?3 and a.letterDate=?4")
    Optional<AssetTransfer> findByParameter(String symbol, String asciiPtCpde, String letterNumber, String letterDate);
}
