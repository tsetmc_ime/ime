package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ClearanceRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClearanceRequestRepository extends JpaRepository<ClearanceRequest, Long> {

    @Query("SELECT cr FROM ClearanceRequest cr WHERE cr.isFinalConfirmed=1 AND cr.responseStatus IS null")
    List<ClearanceRequest> findUnresponsedRequests();

    @Query("SELECT cr FROM ClearanceRequest cr WHERE cr.responseStatus IS null and cr.investor.id=?1 and cr.storage.id=?2")
    List<ClearanceRequest> findClearanceRequests(int investorId, int storageId);

}
