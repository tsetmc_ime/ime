package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.AssetRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssetRequestRepository extends JpaRepository<AssetRequest,Integer> {

List<AssetRequest> findAllByResponseStatusIsNull();
Optional<AssetRequest> findFirstByRequestNumber(String requestNumber);
    @Query("SELECT COALESCE(max(requestNumber),'00000000000000000000') FROM AssetRequest ")
    String findMaxRequestNumber();

    List<AssetRequest> findByRequestNumberIsNullAndRequestDateIsNull();
}
