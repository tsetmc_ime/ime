package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.PersonType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonTypeRepository extends JpaRepository<PersonType, Integer> {
}
