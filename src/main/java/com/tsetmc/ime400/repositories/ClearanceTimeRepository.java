package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ClearanceTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClearanceTimeRepository extends JpaRepository<ClearanceTime,Integer> {
}
