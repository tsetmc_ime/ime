package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.JobName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobNameRepository extends JpaRepository<JobName, String> {

}
