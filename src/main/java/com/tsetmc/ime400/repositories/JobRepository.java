package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Job;
import com.tsetmc.ime400.models.JobId;
import com.tsetmc.ime400.models.JobName;
import com.tsetmc.ime400.models.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface JobRepository extends JpaRepository<Job, JobId> {

    @Query("from Job where date = current_date and name = ?1")
    Optional<Job> findByName(JobName jobName);

    @Query("from Job where date = current_date")
    List<Job> getJobs();

}
