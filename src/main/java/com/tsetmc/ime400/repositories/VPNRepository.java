package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.VPN;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VPNRepository extends JpaRepository<VPN,Integer>{

}
