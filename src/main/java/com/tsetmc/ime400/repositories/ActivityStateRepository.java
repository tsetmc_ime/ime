package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ActivityState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityStateRepository extends JpaRepository<ActivityState, Integer> {
}
