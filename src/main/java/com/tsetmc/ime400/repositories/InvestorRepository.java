package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Investor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvestorRepository extends JpaRepository<Investor,Integer> {

    Optional<Investor> findByPtcodeAndNationalId(String PtCode, String nationalId);

    Optional<Investor> findByAsciiPtcode(String asciiPtCode);


}
