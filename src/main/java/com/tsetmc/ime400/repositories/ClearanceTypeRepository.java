package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ClearanceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClearanceTypeRepository extends JpaRepository<ClearanceType,Integer> {
}
