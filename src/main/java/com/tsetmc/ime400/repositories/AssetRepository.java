package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Asset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AssetRepository extends JpaRepository<Asset, Integer> {

    Optional<Asset> findByInvestorIdAndStorageId(Integer investorId, Integer storageId);

    @Query(value = "select COALESCE(sum(quantity),0) from Asset where investor.id =?1 and storage.id=?2")
    int getAssetAmount(int investorid, int storageid);

    @Query(value = "select a from Asset a join a.investor i join a.storage s where i.id=?1 and s.id=?2")
    Optional<Asset> getAsset(int investorId, int storageId);

}
