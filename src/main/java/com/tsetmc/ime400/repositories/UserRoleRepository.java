package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    UserRole findByName(String name);

}
