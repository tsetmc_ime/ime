package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Instrument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InstrumentRepository extends JpaRepository<Instrument, Integer> {

    @Query(value="select i.* from ime.instrument i where replace(i.symbol,' ' ,'')=replace(:symbol,' ','')", nativeQuery = true)
    Optional<Instrument> findBySymbol(@Param("symbol") String symbol);

    boolean existsBySymbol(String symbol);


}
