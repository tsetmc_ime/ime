package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.PTCodeInquiry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PTCodeInquiryRepository extends JpaRepository<PTCodeInquiry, Integer> {
    @Query("from PTCodeInquiry where resStatus is null or resStatus=0")
    List<PTCodeInquiry> findByResStatusIsNull();

    Optional<PTCodeInquiry> findByRequestNum(String requestNum);

    @Query("SELECT COALESCE(max(id),0) FROM PTCodeInquiry")
    Integer findMaxId();

    List<PTCodeInquiry> findByIdGreaterThan(int maxId);

}
