package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobStatusRepository extends JpaRepository<JobStatus, String> {

}
