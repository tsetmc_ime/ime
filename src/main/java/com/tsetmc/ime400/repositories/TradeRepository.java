package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeRepository extends JpaRepository<Trade,Integer> {
}
