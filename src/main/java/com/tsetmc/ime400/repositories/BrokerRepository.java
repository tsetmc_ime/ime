package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Broker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BrokerRepository extends JpaRepository<Broker, Integer> {

    Optional<Broker> findByCode(String code);

    Optional<Broker> findByName(String name);

}

