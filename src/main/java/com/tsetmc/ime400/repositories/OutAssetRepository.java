package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.dto.OutAssetTotal;
import com.tsetmc.ime400.models.OutAsset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OutAssetRepository  extends JpaRepository<OutAsset, Integer> {
    @Query(value = "SELECT MIN(o.id) as id , SUM(o.quantity) as quantity, Date(o.clearance_date) clearance_date" +
            ", Date(o.out_date) out_date, SUM(o.cost) cost, SUM(o.ocost) ocost, SUM(o.vat) vat" +
            ",o.storage_id , o.investor_id  " +
            "FROM out_asset AS o INNER JOIN out_asset AS o2 ON o.storage_id = o2.storage_id and o.investor_id = o2.investor_id " +
            "and Date(o.clearance_date) = Date(o2.clearance_date) and Date(o.out_date) = Date(o2.out_date) " +
            "WHERE o2.id=?1" +
            " GROUP BY Date(o.clearance_date),Date(o.out_date), o.storage_id,o.investor_id"
            , nativeQuery = true)
    Optional<OutAssetTotal> getOutAssetTotalInGroup(int id);

    @Query(value = "SELECT  o.*  " +
            " FROM out_asset AS o INNER JOIN out_asset AS o2 ON o.storage_id = o2.storage_id and o.investor_id = o2.investor_id " +
            " and Date(o.clearance_date) = Date(o2.clearance_date) " +
            " and (((o.out_date=cast('0000-00-00' as date) or o.out_date is null) " +
            "        and (o2.out_date=cast('0000-00-00' as date) or o2.out_date is null)" +
            "      ) " +
            "      or DATE(o.out_date) = DATE(o2.out_date)" +
            "     ) " +
            " WHERE o2.id=?1 "
            , nativeQuery = true)
    List<OutAsset> getAllOutAssetTotalInGroup(int id);
}
