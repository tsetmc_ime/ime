package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.Storage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StorageRepository extends JpaRepository<Storage, Integer> {

    @Query(value = "select count(*) from storage where replace(name, ' ', '') = replace(:name, ' ', '') and (:id is null or id <> :id)", nativeQuery = true)
    int existsStorageLike(@Param("id") Integer id, @Param("name") String name);

    Optional<Storage> findByName(String name);

    @Query(value = "select s.* from Storage s join Instrument i on s.instrument_id = i.id where REPLACE(i.symbol, ' ', '') = :symbol", nativeQuery = true)
    Optional<Storage> findBySymbol(@Param("symbol") String symbol);

}
