package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ConvertTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConvertTimeRepository extends JpaRepository<ConvertTime,Integer> {
}
