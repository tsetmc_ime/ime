package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.JobView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobViewRepository extends JpaRepository<JobView, String> {

    List<JobView> findAll();

    @Query(value = "select job_status(?1)", nativeQuery = true)
    String find_StatusByName(String jobName);

}
