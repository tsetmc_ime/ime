package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.FutureAsset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FutureAssetRepository extends JpaRepository<FutureAsset,Long> {
    Optional<FutureAsset> findByAsciiPtcodeAndSymbol(String ptcode14,String symbol);
}
