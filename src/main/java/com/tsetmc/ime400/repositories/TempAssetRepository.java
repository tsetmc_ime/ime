package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.TempAsset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TempAssetRepository  extends JpaRepository<TempAsset,Integer> {
}
