package com.tsetmc.ime400.repositories;

import com.tsetmc.ime400.models.ClearanceResponseStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClearanceResponseStatusRepository extends JpaRepository<ClearanceResponseStatus,Integer> {
}
