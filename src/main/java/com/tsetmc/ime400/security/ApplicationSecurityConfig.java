package com.tsetmc.ime400.security;

import com.tsetmc.ime400.controllers.ReloadUserPerRequestHttpSessionSecurityContextRepository;
import com.tsetmc.ime400.repositories.UserRepository;
import com.tsetmc.ime400.security.jwt.AuthEntryPointJwt;
import com.tsetmc.ime400.security.jwt.AuthTokenFilter;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;


@Configuration
@EnableWebSecurity
@Profile("!no-auth")
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // https://www.baeldung.com/spring-security-custom-access-denied-page#2-access-denied-handler
    public class CustomAccessDeniedHandler implements AccessDeniedHandler {

        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc) throws IOException {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            response.sendRedirect("/?unauthorized");
        }

    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Configuration()
    @Order(2)
    public class ApplicationSecurityConfig1 extends WebSecurityConfigurerAdapter {
        @Bean
        public UserDetailsService userDetailsService() {
            return new UserDetailsServiceImpl();
        }

        @Bean
        public DaoAuthenticationProvider authenticationProvider() {
            DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
            authProvider.setUserDetailsService(userDetailsService());
            authProvider.setPasswordEncoder(passwordEncoder());
            return authProvider;
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) {
            auth.authenticationProvider(authenticationProvider());
        }

        @Override
        public void configure(WebSecurity web) {
            web
                    .ignoring()
                    .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/fonts/**", "/error", "/favicon.ico")
            ;
        }

        @Autowired
        UserRepository userRepository;

        @Value("${ime400.work_time}")
        private String workTime;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.securityContext().securityContextRepository(new ReloadUserPerRequestHttpSessionSecurityContextRepository(userRepository, LocalTime.parse(workTime)));

            http
                    .addFilterBefore(
                            getBeforeAuthenticationFilter(), CustomBeforeAuthenticationFilter.class)
                    .authorizeRequests()
                    .antMatchers("/swagger-ui/**", "/access-denied", "/error", "/test", "/users/hash").permitAll()
                    .antMatchers("/users/hash").access("hasIpAddress('127.0.0.1') or hasIpAddress('::1')")
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/")
                    .permitAll()
                    .and()
                    .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                    .and()
                    .logout().permitAll()
            ;
        }

        // https://mail.codejava.net/frameworks/spring-boot/spring-security-authentication-failure-handler-examples
        // https://stackoverflow.com/questions/42181252/authentication-failure-redirect-with-request-params-not-working
        public UsernamePasswordAuthenticationFilter getBeforeAuthenticationFilter() throws Exception {
            CustomBeforeAuthenticationFilter filter = new CustomBeforeAuthenticationFilter();
            filter.setAuthenticationManager(authenticationManager());

            filter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler() {
                @Override
                public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                                    AuthenticationException exception) throws IOException, ServletException {
                    super.setDefaultFailureUrl("/login?error");
                    super.onAuthenticationFailure(request, response, exception);
                }
            });

            return filter;
        }

    }

    public boolean isLoggedIn(Authentication authentication) {
        return !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }


    @Configuration
    @Order(1)
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class ApplicationSecurityConfig2 extends WebSecurityConfigurerAdapter {

        @Autowired
        UserDetailsServiceImpl userDetailsService;

        @Autowired
        private AuthEntryPointJwt unauthorizedHandler;

        @Bean
        public AuthTokenFilter authenticationJwtTokenFilter() {
            return new AuthTokenFilter();
        }

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
            authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
        }

        @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        @Autowired
        UserRepository userRepository;

        @Value("${ime400.work_time}")
        private String workHour;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.securityContext().securityContextRepository(new ReloadUserPerRequestHttpSessionSecurityContextRepository(userRepository, LocalTime.parse(workHour)));

            http
                    .antMatcher("/api/**")
                    .cors().and().csrf().disable()
                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .authorizeRequests()
                    .antMatchers("/api/signin").permitAll()
                    .anyRequest().authenticated();

            http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        }
    }

}


@Configuration
@EnableWebSecurity
@Profile("no-auth")
class ConfigDevelopment extends WebSecurityConfigurerAdapter {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable();
    }

}
