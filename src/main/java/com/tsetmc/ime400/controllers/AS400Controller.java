package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.interfaces.TextEncoder;
import com.tsetmc.ime400.models.FutureAsset;
import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.services.AS400Services;
import com.tsetmc.ime400.services.InvestorService;
import com.tsetmc.ime400.services.PTCodeInquiryService;
import com.tsetmc.ime400.services.TempAssetService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class AS400Controller {

    @Autowired
    TextEncoder textEncoder;

    @Autowired
    PTCodeInquiryService ptCodeInquiryService;

    @Autowired
    AS400Services as400Services;

    @Autowired
    InvestorService investorService;

    @Autowired
    TempAssetService tempAssetService;

    @GetMapping("/investors/query")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE_W', 'USER_LEVEL_1_W', 'USER_LEVEL_2_W')")
    public Investor queryInvestor(
            @RequestParam String ptcode,
            @RequestParam String nationalCode,
            @RequestParam int personType) throws SQLException, JSONException {
        ptcode = ptcode.replaceAll("\\s", "");
        return as400Services.inquiryInvestor(personType, ptcode, nationalCode);
    }

    // todo swagger
    @PostMapping("/api/getFutureAssets") // TODO check authority
    public List<FutureAsset> getFutureAssets(@RequestBody List<String> symbols) throws SQLException, JSONException {
        as400Services.deleteFutureAsset();
        for (String s : symbols) {
            String symbol = s.replaceAll(" ", "");
            if (!symbol.isEmpty())
                as400Services.getAsset(symbol);
        }

        return as400Services.fetchFutureAssets();
    }

}
