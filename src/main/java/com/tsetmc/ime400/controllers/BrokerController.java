package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.Broker;
import com.tsetmc.ime400.services.BrokerService;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/brokers")
public class BrokerController {

    @Autowired
    BrokerService brokerService;

    @GetMapping(value = "/auto-complete")
    @ResponseBody
    public String GetBrokersBySearchTerm(@RequestParam(defaultValue = "") String searchTerm) {
        return brokerService.getBrokersByNameOrCode(searchTerm);
    }

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String indexPage() {
        return "brokers/index";
    }

    @GetMapping(value = "/index")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getBrokerList(@RequestParam(defaultValue = "") String search,
                                @RequestParam(defaultValue = "0") int start,
                                @RequestParam(defaultValue = "10") int length,
                                @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                @RequestParam(defaultValue = "desc") String orderDir) throws SQLException, JSONException {
        return brokerService.getBrokerList(search, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportBrokerList(@RequestParam(defaultValue = "") String search,
                                 @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                 @RequestParam(defaultValue = "desc") String orderDir,
                                 HttpServletResponse response) throws SQLException, JSONException, IOException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=brokers.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "کد کارگزار", false));
        columns.add(new CsvColumnsConfig(2, "نام کارگزار", false));
        columns.add(new CsvColumnsConfig(3, "وضعیت", false));

        brokerService.getBrokerList(search, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String newBrokerPage(Model model) {
        model.addAttribute("broker", new Broker());
        return "brokers/edit";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String editBrokerPage(@PathVariable int id, Model model) {
        model.addAttribute("broker", brokerService.getBrokerById(id));
        return "brokers/edit";
    }

    @PostMapping(path = {"/new", "/edit/{id}"})
    @PreAuthorize("hasAnyAuthority('ADMIN','SURVEILLANCE_W')")
    public String createOrUpdateBroker(@PathVariable(required = false) Integer id,
                                       @Valid Broker broker,
                                       Errors bindingErrors, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (errors.size() > 0)
            return "brokers/edit";

        final boolean inserting = broker.getId() == null;

        try {
            brokerService.save(broker);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "brokers/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("کارگزار %s (کد %s) با موفقیت %s", broker.getName(), broker.getCode(), inserting ? "ایجاد شد" : "تغییر یافت"));
        }});

        return "redirect:/brokers";
    }

}
