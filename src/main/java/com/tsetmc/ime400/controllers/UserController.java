package com.tsetmc.ime400.controllers;

import cn.apiclub.captcha.Captcha;
import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.repositories.StorageRepository;
import com.tsetmc.ime400.repositories.UserRepository;
import com.tsetmc.ime400.repositories.UserRoleRepository;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import com.tsetmc.ime400.types.ProfileResponse;
import com.tsetmc.ime400.util.*;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
public class UserController {

    private final UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    public UserController(UserDetailsServiceImpl userDetailsServiceImpl) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
    }

    @GetMapping(value = "/user-info")
    @ResponseBody
    public String getUserInfo() throws SQLException, JSONException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetailsServiceImpl.getUserInfo(user.getId()).toString();
    }

    private boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || AnonymousAuthenticationToken.class.
                isAssignableFrom(authentication.getClass())) {
            return false;
        }
        return authentication.isAuthenticated();
    }

    @GetMapping("/login")
    public String loginForm(Model model) {
        if (isAuthenticated())
            return "redirect:/";
        getCaptcha(model);
        return "authentication/login";
    }

    @GetMapping(value = "/reset-password")
    public String showResetPassword(Model model) {
        model.addAttribute("username", ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "authentication/reset-password";
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    @PostMapping(value = "/reset-password")
    @Transactional
    public String confirmResetPassword(String password, String password_confirmation, HttpServletRequest request) throws ServletException, IOException {
        if (!password.equals(password_confirmation))
            return "redirect:/reset-password?mismatch";

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);

        // https://stackoverflow.com/a/21415788/1994239
        request.logout();

        return "redirect:/";
    }

    @Autowired
    StorageRepository storageRepository;

    @Autowired
    UserRoleRepository roleRepository;

    public boolean authorizeEditUser(Authentication authentication, Integer id) {
        if (authentication == null)
            return false;

        final User currentUser = ((User) authentication.getPrincipal());
        final String currentRole = currentUser.getRole().getName();

        if ("ADMIN".equals(currentRole) || currentUser.getId().equals(id))
            return true;

        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(currentRole))
            return false;

        User user = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName());
    }

    @GetMapping("/users/edit/{id}")
    @PreAuthorize("@userController.authorizeEditUser(authentication, #id)")
    public String editUserPage(@PathVariable int id, Model model) {
        model.addAttribute("user", userRepository.findById(id).orElse(null));
        model.addAttribute("roles", roleRepository.findAll());
        return "authentication/register";
    }

    @PostMapping("/users/edit/{id}")
    @PreAuthorize("@userController.authorizeEditUser(authentication, #id)")
    public String editUser(@PathVariable(required = false) Integer id, @Valid User user, Errors bindingErrors, Model model, Authentication authentication, RedirectAttributes attrs) {
        return createOrEditUser(id, user, bindingErrors, model, authentication, attrs);
    }

    @GetMapping("/register")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String registerNewUserForm(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("roles", roleRepository.findAll());
        return "authentication/register";
    }

    // https://stackoverflow.com/a/29883178/1994239
    @PostMapping("/register")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String registerNewUser(@Valid User user, Errors bindingErrors, Model model, Authentication authentication, RedirectAttributes attrs) {
        return createOrEditUser(null, user, bindingErrors, model, authentication, attrs);
    }

    String createOrEditUser(Integer id, User user, Errors bindingErrors, Model model, Authentication authentication, RedirectAttributes attrs) {
        boolean inserting = id == null;
        user.setId(id);

        Map<String, String> errors = new HashMap<>();

        User user0 = userRepository.findByUsername(user.getUsername()).orElse(null);
        if (user0 == null && id != null)
            user0 = userRepository.findById(id).orElse(null);
        if (!inserting && user0 == null)
            errors.put(User.Fields.username, "کاربری با شناسه مورد نظر وجود ندارد");
        else if ((inserting && user0 != null) || (!inserting && !user0.getId().equals(id)))
            errors.put(User.Fields.username, "نام کاربری انتخابی در حال حاضر یک حساب در سایت دارد");
        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return "authentication/register";
        }

        boolean passChanged = inserting || !user.getPassword().isEmpty();
        if (!passChanged)
            user.setPassword(user0.getPassword());
        else if (!user.getPassword().equals(user.getPasswordConfirmation()))
            errors.put(User.Fields.password, "رمز با تکرار رمز مطابقت ندارد");

        user.setRole(roleRepository.findById(user.getRole().getId()).orElse(null));
        if (user.getRole() == null)
            errors.put(User.Fields.role, "سطح دسترسی کاربر مشخص نشده است");

        else {
            if ("ADMIN".equals(user.getRole().getName()) || (user0 != null && "ADMIN".equals(user0.getRole().getName())))
                errors.put(User.Fields.role, String.format("ایجاد یا تغییر کاربر با سطح دسترسی %s از طریق وب‌سایت مجاز نمی‌باشد", roleRepository.findByName("ADMIN").getDescription()));

            User currentUser = ((User) authentication.getPrincipal());
            if (currentUser.getId().equals(user.getId()) && !currentUser.getRole().getName().equals(user.getRole().getName()))
                errors.put(User.Fields.role, "تغییر دسترسی شخصی قابل انجام نیست");
        }

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return "authentication/register";
        }

        errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (!passChanged)
            errors.entrySet().removeIf(entry -> entry.getKey().equals(User.Fields.password));
        if (errors.size() > 0)
            return "authentication/register";

        if (passChanged)
            user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName())) {
            Storage storage = storageRepository.findByName(user.getStorage().getName()).orElse(null);
            if (storage == null)
                errors.put(Storage.class.getSimpleName().toLowerCase() + "." + Storage.Fields.name, "انبار مورد نظر وجود ندارد");
            else
                user.setStorage(storage);
        } else
            user.setStorage(null);

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return "authentication/register";
        }

        try {
            userRepository.save(user);

        } catch (Exception e) {
            errors.put("e4", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "authentication/register";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("حساب کاربری %s با موفقیت %s", user.getUsername(), id == null ? "ایجاد شد" : "تغییر یافت"));
        }});

        return "redirect:/users";
    }

    @GetMapping("/users")
    public String indexPage() {
        return "users/index";
    }

    @GetMapping(value = "/users/index")
    @ResponseBody
    public String getUsersList(@RequestParam(defaultValue = "") String search,
                               @RequestParam(defaultValue = "0") int start,
                               @RequestParam(defaultValue = "10") int length,
                               @RequestParam(defaultValue = "DT_RowId") String orderKey,
                               @RequestParam(defaultValue = "desc") String orderDir,
                               Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return userDetailsServiceImpl.getUsersList(storageId, search, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/users/export-csv")
    public void exportUsersList(@RequestParam(defaultValue = "") String search,
                                @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                @RequestParam(defaultValue = "desc") String orderDir,
                                HttpServletResponse response,
                                Authentication authentication) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=users.csv");

        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام کاربری", false));
        columns.add(new CsvColumnsConfig(3, "نام و نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(4, "نام انبار", false));
        columns.add(new CsvColumnsConfig(5, "آخرین تاریخ ورود", true));
        columns.add(new CsvColumnsConfig(6, "وضعیت", false));
        columns.add(new CsvColumnsConfig(7, "نقش", false));

        userDetailsServiceImpl.getUsersList(storageId, search, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @GetMapping("/user-profile")
    public String getProfile(Authentication authentication, Model model) {
        User u = ((User) authentication.getPrincipal());
        ProfileResponse profile = new ProfileResponse();
        if (u != null) {
            if (u.getStorage() != null)
                profile.setStorageName(u.getStorage().getName());
            profile.setFullName(u.getFullname());
            profile.setUserName(u.getUsername());
            profile.setRoleName(u.getRole().getDescription());
            profile.setEmail(u.getEmail());
            profile.setPhone(u.getPhone());
            profile.setInstrumentName(String.format("%s (%s)", u.getStorage().getInstrument().getSymbolDesc(), u.getStorage().getInstrument().getSymbol()));
            profile.setDueDate(u.getStorage().getInstrument().getDueDate());
        }
        model.addAttribute("profile", profile);
        return "users/profile";
    }

    // https://javatechonline.com/how-to-secure-spring-boot-application-by-captcha-validation/
    private void getCaptcha(Model model) {
        Captcha captcha = CaptchaUtil.createCaptcha(240, 70);

        model.addAttribute("realCaptcha", CaptchaUtil.encodeCaptcha(captcha));
        model.addAttribute("captcha", "");
        model.addAttribute("hiddenValue", HashUtil.getMd5(captcha.getAnswer()));
    }

    @GetMapping(path = "/users/hash")
    @ResponseBody
    public String computeHashForPassword(@RequestParam(name = "password") String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

}
