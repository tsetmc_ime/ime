package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.*;
import com.tsetmc.ime400.repositories.PersonTypeRepository;
import com.tsetmc.ime400.repositories.StorageRepository;
import com.tsetmc.ime400.services.*;
import com.tsetmc.ime400.types.ConvertedTempAsset;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

@Controller
@RequestMapping(path = "/temp-assets")
public class TempAssetController {

    @Autowired
    EntityManager entityManager;

    @Autowired
    StorageService storageService;

    @GetMapping("")
    public String indexPage() {
        return "temp-assets/index";
    }

    @Autowired
    InstrumentService instrumentService;

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W')")
    public String newTempAssetPage(Model model) {
        model.addAttribute("tempAsset", new TempAsset());
        return "temp-assets/edit";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @tempAssetController.checkAccessToStorage(authentication, #id)")
    public String editTempAssetPage(@PathVariable int id, Model model, Authentication authentication) {
        TempAsset tempAsset = tempAssetService.getTempAssetById(id).orElseThrow(EntityNotFoundException::new);

        if (tempAsset.getConverted() == 1) {
            ConvertedTempAsset cta = new ConvertedTempAsset();
            cta.setId(tempAsset.getId());
            cta.setConvertDate(tempAsset.getConvertDate());
            cta.setPrintDate(LocalDateTime.now());
            cta.setEntranceDate(tempAsset.getEntranceDate());
            cta.setStorageName(tempAsset.getStorage().getName());
            cta.setStorageAddress(tempAsset.getStorage().getAddress());
            cta.setSymbolDescription(tempAsset.getStorage().getInstrument().getSymbolDesc() + String.format(" (%s) ", tempAsset.getStorage().getInstrument().getSymbol()));
            cta.setQuantity(tempAsset.getQuantity());
            cta.setWeightUnit(tempAsset.getStorage().getInstrument().getWeightUnit());
            cta.setDueDate(tempAsset.getStorage().getInstrument().getDueDate());
            cta.setInvestorName(tempAsset.getInvestor().getFirstName() + " " + tempAsset.getInvestor().getLastName());
            cta.setNationalCode(tempAsset.getInvestor().getNationalId());
            cta.setPtCode(tempAsset.getInvestor().getPtcode());
            cta.setPtCode14(tempAsset.getInvestor().getAsciiPtcode());
            cta.setUserName(((User) authentication.getPrincipal()).getFullname());
            cta.setInvestorAddress(tempAsset.getInvestor().getAddress());
            model.addAttribute("cta", cta);

            return "temp-assets/print-govahi-sepordeh";

        } else {
            model.addAttribute("tempAsset", tempAsset);
            return "temp-assets/edit";
        }
    }

    @Autowired
    BrokerService brokerService;

    @Autowired
    InvestorService investorService;

    @Autowired
    PersonTypeRepository personTypeRepository;

    @ModelAttribute
    public void addAttributes(Model model, Authentication authentication) {
        User user = ((User) authentication.getPrincipal());
        model.addAttribute("personTypes", personTypeRepository.findAll());
    }

    @PostMapping(path = {"/new", "/edit/{id}"})
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @tempAssetController.checkAccessToStorage(authentication, #id)")
    public String createOrUpdateTempAsset(@PathVariable(required = false) Integer id, @Valid TempAsset tempAsset,
                                          Errors bindingErrors, Model model, Authentication authentication, RedirectAttributes attrs) {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);

        User user = ((User) authentication.getPrincipal());

        final boolean inserting = tempAsset.getId() == null;

        if (!inserting && tempAsset.getIsFinalConfirmed() != 0)
            errors.put("e5", "این رکورد به دارایی بورسی تبدیل شده و قابلیت ویرایش ندارد");

        String brokerCode = StringUtils.right(tempAsset.getBroker().getName(), 3);
        Broker broker;
        try { // TODO not working
            Integer.parseInt(brokerCode);
            broker = brokerService.getBrokerByCode(brokerCode);
        } catch (NumberFormatException e) {
            broker = brokerService.getBrokerByName(tempAsset.getBroker().getName());
        }
        if (broker == null)
            errors.put(Broker.class.getSimpleName().toLowerCase() + "." + Broker.Fields.name, "نام کارگزار ناظر را به درستی وارد نمایید");

        final Investor investor = tempAsset.getInvestor();
        Investor invstr = investorService.findByPTCode14(investor.getAsciiPtcode()).orElse(null);
        if (invstr == null)
            errors.put(Investor.class.getSimpleName().toLowerCase() + "." + Investor.Fields.asciiPtcode, "کد سهامداری معتبر نیست");
        else if (!investor.getNationalId().equals(invstr.getNationalId()) || !StringUtils.right(investor.getPtcode(), 5).equals(StringUtils.right(invstr.getPtcode(), 5)))
            errors.put(Investor.class.getSimpleName().toLowerCase() + "." + Investor.Fields.asciiPtcode, "کد ۱۴ رقمی سهامدار با کد ملی و کد بورسی انطباق ندارد");

        Storage storage = storageService.findById(user.getStorage().getId()).orElse(null);
        if (storage == null)
            errors.put(Storage.class.getSimpleName().toLowerCase() + "." + Storage.Fields.name, "انبار برای این دارایی تعریف نشده است");

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return "temp-assets/edit";
        }

        tempAsset.setStorage(storage);
        tempAsset.setBroker(broker);
        tempAsset.setInvestor(invstr);
        tempAsset.setRegistrantUser(user);
        tempAsset.setRegisterDate(LocalDateTime.now());
        tempAsset.setIsFinalConfirmed((byte) 0);

        try {
            tempAssetService.saveTempAsset(tempAsset);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "temp-assets/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("انبار موقت %d با موفقیت %s", tempAsset.getId(), inserting ? "ایجاد شد" : "تغییر یافت"));
        }});
        return "redirect:/temp-assets";
    }

    @PostMapping(path = {"/discard/{id}"})
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @tempAssetController.checkAccessToStorage(authentication, #id)")
    public String deleteTempAsset(@PathVariable Integer id, Model model, Authentication authentication, RedirectAttributes attrs) {
        Map<String, String> errors = new HashMap<>();

        TempAsset ta = tempAssetService.getTempAssetById(id).orElse(null);
        if (ta == null || ta.getIsFinalConfirmed() == 1 || ta.getConverted() == 1) {
            errors.put("e3", "درخواست مورد نظر قابل ابطال نیست");
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/temp-assets/edit/" + id;
        }

        try {
            tempAssetService.deleteTempAsset(id);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/temp-assets/edit/" + id;
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("قبض انبار موقت %d با موفقیت ابطال شد", ta.getId()));
        }});
        return "redirect:/temp-assets";
    }

    @Autowired
    TempAssetService tempAssetService;

    @Autowired
    StorageRepository storageRepository;

    @GetMapping(value = "/index")
    @ResponseBody
    public ResponseEntity<String> getTempAssetList(@RequestParam(required = false) String storageName,
                                                   @RequestParam(defaultValue = "") String search,
                                                   @RequestParam(required = false) String entryFrom,
                                                   @RequestParam(required = false) String entryTo,
                                                   @RequestParam(required = false) String convertFrom,
                                                   @RequestParam(required = false) String convertTo,
                                                   @RequestParam(required = false) Integer finalConfirmed,
                                                   @RequestParam(defaultValue = "0") int start,
                                                   @RequestParam(defaultValue = "10") int length,
                                                   @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                                   @RequestParam(defaultValue = "desc") String orderDir,
                                                   Authentication authentication) {
        User user = ((User) authentication.getPrincipal());
        Integer storageId;
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();
        else
            storageId = storageRepository.findByName(storageName).map(Storage::getId).orElse(null);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(tempAssetService.getTempAssetList(storageId, search, entryFrom, entryTo,finalConfirmed, convertFrom, convertTo, start, length, orderKey, orderDir), responseHeaders, HttpStatus.OK);
    }

    @GetMapping(value = "/export-csv")
    public void exportTempAssetList(@RequestParam(required = false) String storageName,
                                    @RequestParam(defaultValue = "") String search,
                                    @RequestParam(required = false) String entryFrom,
                                    @RequestParam(required = false) String entryTo,
                                    @RequestParam(required = false) String convertFrom,
                                    @RequestParam(required = false) String convertTo,
                                    @RequestParam(required = false) Integer finalConfirmed,
                                    @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                    @RequestParam(defaultValue = "desc") String orderDir,
                                    Authentication authentication,
                                    HttpServletResponse response) throws SQLException, JSONException, IOException {
        User user = ((User) authentication.getPrincipal());
        Integer storageId;
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();
        else
            storageId = storageRepository.findByName(storageName).map(Storage::getId).orElse(null);

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=temp-assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "زمان تبدیل وضعیت", true));
        columns.add(new CsvColumnsConfig(2, "وضعیت تبدیل", false));
        columns.add(new CsvColumnsConfig(3, "زمان ثبت", true));
        columns.add(new CsvColumnsConfig(4, "مقدار", false));
        columns.add(new CsvColumnsConfig(5, "نام انبار", false));
        columns.add(new CsvColumnsConfig(6, "نام", false));
        columns.add(new CsvColumnsConfig(7, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(8, "کد ملی", false));
        columns.add(new CsvColumnsConfig(9, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(10, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(11, "کارگزار ناظر", false));
        columns.add(new CsvColumnsConfig(12, "وضعیت تأیید", false));
        columns.add(new CsvColumnsConfig(13, "زمان تأیید", true));
        columns.add(new CsvColumnsConfig(14, "زمان ورود به انبار", true));

        tempAssetService.getTempAssetList(storageId, search, entryFrom, entryTo,finalConfirmed, convertFrom, convertTo, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    public boolean checkAccessToStorage(Authentication authentication, Integer tempAssetId) {
        return tempAssetId == null || tempAssetService.getTempAssetById(tempAssetId).map(TempAsset::getStorage).map(Storage::getId).map(id -> id.equals(((User) authentication.getPrincipal()).getStorage().getId())).orElse(false);
    }

    @PostMapping("/confirm/{id}")
    @PreAuthorize("hasAuthority('USER_LEVEL_1_W') and @tempAssetController.checkAccessToStorage(authentication, #id)")
    public String confirm(@PathVariable Integer id, Authentication authentication, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = new HashMap<>();

        TempAsset ta = tempAssetService.getTempAssetById(id).orElse(null);
        if (ta == null)
            errors.put("e1", "دارایی موقت یافت نشد");
        else if (ta.getIsFinalConfirmed() != 0)
            errors.put("e1", "وضعیت این دارایی قابل تغییر نیست");

        if (errors.size() > 0) {
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/temp-assets/edit/" + id;
        }

        User user = ((User) authentication.getPrincipal());

        ta.setConfirmingUser(user);
        ta.setConfirmDate(LocalDateTime.now());
        ta.setIsFinalConfirmed((byte) 1);

        try {
            tempAssetService.saveTempAsset(ta);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/temp-assets/edit/" + id;
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("دارایی %d با موفقیت تأیید شد", ta.getId()));
        }});
        return "redirect:/temp-assets";
    }

}
