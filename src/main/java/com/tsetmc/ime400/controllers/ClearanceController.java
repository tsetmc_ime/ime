package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.*;
import com.tsetmc.ime400.repositories.UserRepository;
import com.tsetmc.ime400.services.*;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

@Controller
@RequestMapping(path = "/clearance-requests")
public class ClearanceController {
    @Autowired
    AssetServices assetServices;
    @Autowired
    ClearanceRequestService clearanceRequestService;
    @Autowired
    InvestorService investorService;
    @Autowired
    AS400Services as400Services;

    @GetMapping(value = "/status")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String totalClearanceRequests() throws SQLException, JSONException {
        return clearanceRequestService.getStats().toString();
    }

    @GetMapping(value = "/user-status")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1', 'USER_LEVEL_2')")
    @ResponseBody
    public String ClearanceRequestInfo() throws SQLException, JSONException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return clearanceRequestService.getStatByUserId(user.getId()).toString();
    }

    @GetMapping("")
    public String indexPage() {
        return "clearance-requests/index";
    }

    @GetMapping("/index")
    @ResponseBody
    public String getClearanceRequestList(@RequestParam(required = false) Integer storageId,
                                          @RequestParam(defaultValue = "") String search,
                                          @RequestParam(required = false) String from,
                                          @RequestParam(required = false) String to,
                                          @RequestParam(required = false) Integer finalConfirmed,
                                          @RequestParam(required = false) Integer responseStatus,
                                          @RequestParam(defaultValue = "0") int start,
                                          @RequestParam(defaultValue = "10") int length,
                                          @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                          @RequestParam(defaultValue = "desc") String orderDir,
                                          Authentication authentication) {
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();
        return clearanceRequestService.getClearanceRequestList(storageId, search, from, to, finalConfirmed, responseStatus, start, length, orderKey, orderDir);
    }

    @GetMapping("/export-csv")
    public void getClearanceRequestList(@RequestParam(required = false) Integer storageId,
                                        @RequestParam(defaultValue = "") String search,
                                        @RequestParam(required = false) String from,
                                        @RequestParam(required = false) String to,
                                        @RequestParam(required = false) Integer finalConfirmed,
                                        @RequestParam(required = false) Integer responseStatus,
                                        @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                        @RequestParam(defaultValue = "desc") String orderDir,
                                        Authentication authentication,
                                        HttpServletResponse response) throws IOException, SQLException, JSONException {
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=clearance-requests.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "نام انبار", false));
        columns.add(new CsvColumnsConfig(5, "نام", false));
        columns.add(new CsvColumnsConfig(6, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(7, "کد ملی", false));
        columns.add(new CsvColumnsConfig(8, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(9, "وضعیت", false));
        columns.add(new CsvColumnsConfig(10, "مقدار", false));
        columns.add(new CsvColumnsConfig(11, "تاریخ ثبت درخواست", true));
        columns.add(new CsvColumnsConfig(12, "تاریخ ارسال", true));
        columns.add(new CsvColumnsConfig(13, "تاریخ دریافت پاسخ", true));
        columns.add(new CsvColumnsConfig(14, "confirm_date", true));

        clearanceRequestService.getClearanceRequestList(storageId, search, from, to, finalConfirmed, responseStatus, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @GetMapping("/report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getClearanceRequestReport(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) String from,
            @RequestParam(required = false) String to,
            @RequestParam(required = false) Integer clearType,
            @RequestParam(required = false) Boolean done,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "10") int length,
            @RequestParam(defaultValue = "DT_RowId") String orderKey,
            @RequestParam(defaultValue = "desc") String orderDir) {
        return clearanceRequestService.getClearanceRequestsReport(search, from, to, clearType, done, start, length, orderKey, orderDir).toString();
    }

    @GetMapping("/export-report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportClearanceRequestReport(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) String from,
            @RequestParam(required = false) String to,
            @RequestParam(required = false) Integer clearType,
            @RequestParam(required = false) Boolean done,
            @RequestParam(defaultValue = "DT_RowId") String orderKey,
            @RequestParam(defaultValue = "desc") String orderDir,
            HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=clearance-report.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "نام", false));
        columns.add(new CsvColumnsConfig(5, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(6, "کد ملی", false));
        columns.add(new CsvColumnsConfig(7, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(7, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(8, "مقدار", false));
        columns.add(new CsvColumnsConfig(9, "تاریخ ثبت درخواست", true));
        columns.add(new CsvColumnsConfig(10, "تاریخ دریافت پاسخ", true));
        columns.add(new CsvColumnsConfig(11, "نوع ترخیص", false));
        columns.add(new CsvColumnsConfig(12, "وضعیت", false));
        columns.add(new CsvColumnsConfig(13, "توضیحات", false));

        clearanceRequestService.getClearanceRequestsReport(search, from, to, clearType, done, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @Autowired
    InstrumentService instrumentService;

    String renderForm(Long id, ClearanceRequest cr, Model model) {
        Asset asset = assetServices.getAsset(cr.getInvestor().getId(), cr.getStorage().getId()).orElse(null);
        model.addAttribute("clearanceRequest", cr);
        Integer assetQnty = null;
        if (asset != null) {
            assetQnty = asset.getQuantity();
        }
        model.addAttribute("assetQnty", assetQnty);
        return "clearance-requests/edit";
    }

    public boolean checkAccessToStorage(Authentication authentication, Long clearanceRequestId) {
        return clearanceRequestId == null || clearanceRequestService.findById(clearanceRequestId).map(ClearanceRequest::getStorage).map(Storage::getId).map(id -> id.equals(((User) authentication.getPrincipal()).getStorage().getId())).orElse(false);
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE') or @clearanceController.checkAccessToStorage(authentication, #id)")
    public String editClearanceRequestPage(@PathVariable Long id, Model model) {
        return renderForm(id, clearanceRequestService.findById(id).orElseThrow(EntityNotFoundException::new), model);
    }

    @PostMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @clearanceController.checkAccessToStorage(authentication, #id)")
    public String updateClearanceRequest(@PathVariable(required = false) Long id,
                                         @Valid ClearanceRequest clearanceRequest, Errors bindingErrors,
                                         Model model, Authentication authentication, RedirectAttributes attrs) throws SQLException, JSONException {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);

        ClearanceRequest cr = clearanceRequestService.findById(id).orElseThrow(EntityNotFoundException::new);

        User user = ((User) authentication.getPrincipal());
        Storage storage = user.getStorage();
        Investor investor = investorService.findById(cr.getInvestor().getId()).orElseThrow(EntityNotFoundException::new);

        clearanceRequest.setStorage(storage);
        clearanceRequest.setInvestor(investor);
        clearanceRequest.setRegistrantUser(user);

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return renderForm(id, clearanceRequest, model);
        }

        if (cr.getIsFinalConfirmed() == (byte) 1) {
            errors.put("e0", "درخواست مورد نظر تأیید نهایی شده است و قابل ویرایش نیست");
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/clearance-requests";
        }

        // refresh investor details
        as400Services.inquiryInvestor(investor.getPersonType().getId(), investor.getPtcode(), investor.getNationalId());
        investor = investorService.findById(investor.getId()).orElseThrow(EntityNotFoundException::new);

        if (investor.getActivityState() == null || investor.getActivityState().getId() != 1) {
            errors.put(Investor.class.getSimpleName().toLowerCase() + "." + Investor.Fields.ptcode, "کد سهامداری وارد شده فعال نیست");
            model.addAttribute("errors", errors);
            return renderForm(id, clearanceRequest, model);
        }

        int assetAmount;
        try {
            assetAmount = as400Services.getAssetAmount(investor.getAsciiPtcode(), storage.getInstrument().getSymbol(), storage.getInstrument().getLot());
        } catch (RuntimeException e) {
            errors.put("e8", e.getMessage());
            model.addAttribute("errors", errors);
            return renderForm(id, clearanceRequest, model);
        }

        List<ClearanceRequest> existingCrList = clearanceRequestService.getClearanceRequests(storage.getId(), investor.getId());
        int clearedQty = existingCrList.stream().filter(x -> !x.getId().equals(id)).map(ClearanceRequest::getQuantity).reduce(0, Integer::sum);
        if (clearedQty + clearanceRequest.getQuantity() > assetAmount) {
            errors.put(ClearanceRequest.Fields.quantity, "مقدار وارد شده برای ترخیص بیش از کل دارایی بورسی است");
            model.addAttribute("errors", errors);
            return renderForm(id, clearanceRequest, model);
        }

        cr.setQuantity(clearanceRequest.getQuantity());
        cr.setDescription(clearanceRequest.getDescription());
        cr.setRegistrantUser(user);
        cr.setRegisterDate(new Date());

        try {
            clearanceRequestService.updateClearanceRequest(cr);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return renderForm(id, cr, model);
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("درخواست ترخیص شماره %d با موفقیت تغییر یافت", cr.getId()));
        }});
        return "redirect:/clearance-requests";
    }

    @PostMapping("/confirm/{id}")
    @PreAuthorize("hasAuthority('USER_LEVEL_1_W') and @clearanceController.checkAccessToStorage(authentication, #id)")
    public String confirm(@PathVariable Long id, Authentication authentication, RedirectAttributes attrs) {
        Map<String, String> errors = new HashMap<>();

        User user = ((User) authentication.getPrincipal());
        // User user=userRepository.findById(269).orElse(null);

        ClearanceRequest cr = clearanceRequestService.findById(id).orElseThrow(EntityNotFoundException::new);

        // TODO move to web security
        /* if (!cr.getStorage().getId().equals(user.getStorage().getId())) {
            attrs.addFlashAttribute("messages", new ArrayList<String>() {{
                add("شما اجازه دسترسی به درخواست مورد نظر را ندارید!");
            }});
            return "clearance/confirm";
        } */

        if (cr.getIsFinalConfirmed() == 1) {
            errors.put("e2", "درخواست مورد نظر قبلا تأیید نهایی شده است");
            return "redirect:/clearance-requests/edit/" + id;
        }

        // TODO move to web security
        if (!"USER_LEVEL_1".equals(user.getRole().getName())) {
            errors.put("e8", "شما دسترسی تأیید نهایی درخواست را ندارید");
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/clearance-requests/edit/" + id;
        }

        cr.setConfirmingUser(user);
        cr.setConfirmDate(new Date());
        cr.setIsFinalConfirmed((byte) 1);

        try {
            clearanceRequestService.updateClearanceRequest(cr);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/clearance-requests/edit/" + id;
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("درخواست ترخیص شماره %d به تأیید نهایی رسید؛ و به سامانه پس از معاملات ارسال خواهد شد", cr.getId()));
        }});
        return "redirect:/clearance-requests";
    }

    @Autowired
    UserRepository userRepository;

    @PostMapping("/discard/{id}")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @clearanceController.checkAccessToStorage(authentication, #id)")
    public String deleteRequest(@PathVariable(required = false) Long id, Authentication authentication, RedirectAttributes attrs) {
        Map<String, String> errors = new HashMap<>();

        clearanceRequestService.findById(id).ifPresent(cr -> {
            if (cr.getIsFinalConfirmed() != 0)
                errors.put("e1", "درخواست مورد نظر تأیید نهایی شده؛ و حذف آن امکان پذیر نمی باشد");
        });

        if (errors.size() > 0) {
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/clearance-requests/edit/" + id;
        }

        try {
            clearanceRequestService.deleteClearanceRequest(id);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            attrs.addFlashAttribute("errors", errors);
            return "redirect:/clearance-requests/edit/" + id;
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("درخواست ترخیص شماره %d ابطال شد", id));
        }});
        return "redirect:/clearance-requests";
    }

}
