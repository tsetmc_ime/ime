package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.models.VPN;
import com.tsetmc.ime400.services.VPNService;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.DateTimeService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(path = "/vpn")
@PreAuthorize("hasAuthority('ADMIN')")
public class VPNController {

    @Autowired
    VPNService vpnService;

    @GetMapping("")
    public String indexPage() {
        return "/vpn/index";
    }

    @GetMapping(value = "/index")
    @ResponseBody
    public String getVPNDateList(@RequestParam(defaultValue = "") String search,
                                                              @RequestParam(defaultValue = "0") int start,
                                                              @RequestParam(defaultValue = "10") int length,
                                                              @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                                              @RequestParam(defaultValue = "desc") String orderDir) {


        return vpnService.getVPNList(search, start, length, orderKey, orderDir);
    }

    @PostMapping(path = {"/new", "/edit/{id}"})
    public String createOrUpdateVPN( @PathVariable(required = false) Integer id,
                                       @Valid VPN vpn,
                                       Errors bindingErrors, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (errors.size() > 0)
            return "vpn/edit";



        final boolean inserting = vpn.getId() == null;

        try {
            vpnService.save(vpn);

        } catch (Exception e) {

            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "vpn/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("vpn %s با موفقیت ثبت شد", DateTimeService.fromGregoriantoJalali(vpn.getStorageName())));
        }});

        return "redirect:/vpn";
    }
    @GetMapping("/new")
    public String newVPNPage(Model model) {
        model.addAttribute("VPN", new VPN());
        return "vpn/edit";
    }
    @GetMapping("/edit/{id}")
    public String editVPNPage(@PathVariable int id, Model model) {
        model.addAttribute("VPN", vpnService.FindById(id));
        return "vpn/edit";
    }
}
