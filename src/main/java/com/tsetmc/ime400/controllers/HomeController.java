package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.models.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index(Model model, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        model.addAttribute("user", user);
        return Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()) ? "home/user" : "home/admin";
    }

}
