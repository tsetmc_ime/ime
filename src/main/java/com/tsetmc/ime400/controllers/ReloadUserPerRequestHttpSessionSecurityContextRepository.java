package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.repositories.UserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReloadUserPerRequestHttpSessionSecurityContextRepository extends HttpSessionSecurityContextRepository {

    private UserRepository userRepository;

    LocalTime workTime;

    public ReloadUserPerRequestHttpSessionSecurityContextRepository(UserRepository userRepository, LocalTime workTime) {
        this.userRepository = userRepository;
        this.workTime = workTime;
    }

    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        // Let the parent class actually get the SecurityContext from the HTTPSession first.
        SecurityContext context = super.loadContext(requestResponseHolder);

        Authentication authentication = context.getAuthentication();
        if (authentication != null) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            userRepository.findByUsername(userDetails.getUsername()).ifPresent(user -> {
                final String role = user.getRole().getName();
                List<SimpleGrantedAuthority> auth = new ArrayList<>(Collections.singletonList(new SimpleGrantedAuthority(role)));
                if (LocalTime.now().isBefore(workTime))
                    auth.add(new SimpleGrantedAuthority(role + "_W"));
                context.setAuthentication(new UsernamePasswordAuthenticationToken(user, null, auth));
            });
        }

        return context;
    }

}
