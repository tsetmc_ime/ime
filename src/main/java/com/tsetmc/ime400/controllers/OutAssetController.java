package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.dto.OutAssetTotal;
import com.tsetmc.ime400.models.Asset;
import com.tsetmc.ime400.models.OutAsset;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.InstrumentService;
import com.tsetmc.ime400.services.OutAssetService;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import lombok.ToString;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.DAYS;

@ToString
@Controller
@RequestMapping(path = "/out-assets")
public class OutAssetController {

    @Autowired
    OutAssetService outAssetService;

    @GetMapping("")
    public String indexPage() {
        return "out-assets/index";
    }

//    @GetMapping(value = "/index")
//    @ResponseBody
    private String getOutAssetList(@RequestParam(defaultValue = "") String search,
                                  @RequestParam(required = false) String clearanceFrom,
                                  @RequestParam(required = false) String clearanceTo,
                                  @RequestParam(required = false) String outFrom,
                                  @RequestParam(required = false) String outTo,
                                  @RequestParam(defaultValue = "0") int start,
                                  @RequestParam(defaultValue = "10") int length,
                                  @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                  @RequestParam(defaultValue = "desc") String orderDir,
                                  Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return outAssetService.getOutAssetList(storageId, search, clearanceFrom, clearanceTo, outFrom, outTo, start, length, orderKey, orderDir);
    }

//    @GetMapping(value = "/export-csv")
    private void exportOutAssetList(@RequestParam(defaultValue = "") String search,
                                   @RequestParam(required = false) String clearanceFrom,
                                   @RequestParam(required = false) String clearanceTo,
                                   @RequestParam(required = false) String outFrom,
                                   @RequestParam(required = false) String outTo,
                                   @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                   @RequestParam(defaultValue = "desc") String orderDir,
                                   Authentication authentication,
                                   HttpServletResponse response) throws IOException, SQLException, JSONException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=out-assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام", false));
        columns.add(new CsvColumnsConfig(3, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(4, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(5, "کد ملی", false));
        columns.add(new CsvColumnsConfig(6, "مقدار", false));
        columns.add(new CsvColumnsConfig(7, "زمان ترخیص", true));
        columns.add(new CsvColumnsConfig(8, "هزینه انبار بورسی (ریال)", false));
        columns.add(new CsvColumnsConfig(9, "زمان خروج نهایی", true));
        columns.add(new CsvColumnsConfig(10, "هزینه انبار موقت (ریال)", false));
        columns.add(new CsvColumnsConfig(11, "مالیات بر ارزش افزوده (ریال)", false));
        columns.add(new CsvColumnsConfig(12, "هزینه کل (ریال)", false));
        // columns.put("شماره مشتری","شماره مشتری");

        outAssetService.getOutAssetList(storageId, search, clearanceFrom, clearanceTo, outFrom, outTo, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @GetMapping(value = "/report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getOutAssetReportAdmin(@RequestParam(defaultValue = "") String search,
                                         @RequestParam(required = false) String clearanceFrom,
                                         @RequestParam(required = false) String clearanceTo,
                                         @RequestParam(required = false) String outFrom,
                                         @RequestParam(required = false) String outTo,
                                         @RequestParam(defaultValue = "0") int start,
                                         @RequestParam(defaultValue = "10") int length,
                                         @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                         @RequestParam(defaultValue = "desc") String orderDir) {
        return outAssetService.getOutAssetReport(search, clearanceFrom, clearanceTo, outFrom, outTo, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void getOutAssetReportAdmin(@RequestParam(defaultValue = "") String search,
                                       @RequestParam(required = false) String clearanceFrom,
                                       @RequestParam(required = false) String clearanceTo,
                                       @RequestParam(required = false) String outFrom,
                                       @RequestParam(required = false) String outTo,
                                       @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                       @RequestParam(defaultValue = "desc") String orderDir,
                                       HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=out-asset-report.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "نام", false));
        columns.add(new CsvColumnsConfig(5, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(6, "کد ملی", false));
        columns.add(new CsvColumnsConfig(7, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(8, "مقدار", false));
        columns.add(new CsvColumnsConfig(9, "زمان ترخیص", true));
        columns.add(new CsvColumnsConfig(10, "هزینه انبار بورسی (ریال)", false));
        columns.add(new CsvColumnsConfig(11, "زمان خروج نهایی", true));
        columns.add(new CsvColumnsConfig(12, "هزینه انبار موقت (ریال)", false));
        columns.add(new CsvColumnsConfig(13, "مالیات بر ارزش افزوده (ریال)", false));
        columns.add(new CsvColumnsConfig(14, "هزینه کل (ریال)", false));

        outAssetService.getOutAssetReport(search, clearanceFrom, clearanceTo, outFrom, outTo, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }
    @GetMapping(value = "/index")
    @ResponseBody
    public String getOutAssetTotalList(@RequestParam(defaultValue = "") String search,
                                       @RequestParam(required = false) String clearanceFrom,
                                       @RequestParam(required = false) String clearanceTo,
                                       @RequestParam(required = false) String outFrom,
                                       @RequestParam(required = false) String outTo,
                                       @RequestParam(defaultValue = "0") int start,
                                       @RequestParam(defaultValue = "10") int length,
                                       @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                       @RequestParam(defaultValue = "desc") String orderDir,
                                       Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return outAssetService.getOutAssetTotalList(storageId, search, clearanceFrom, clearanceTo, outFrom, outTo, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportOutAssetTotalList(@RequestParam(defaultValue = "") String search,
                                        @RequestParam(required = false) String clearanceFrom,
                                        @RequestParam(required = false) String clearanceTo,
                                        @RequestParam(required = false) String outFrom,
                                        @RequestParam(required = false) String outTo,
                                        @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                        @RequestParam(defaultValue = "desc") String orderDir,
                                        Authentication authentication,
                                        HttpServletResponse response) throws IOException, SQLException, JSONException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=out-assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام", false));
        columns.add(new CsvColumnsConfig(3, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(4, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(5, "کد ملی", false));
        columns.add(new CsvColumnsConfig(6, "مقدار", false));
        columns.add(new CsvColumnsConfig(7, "زمان ترخیص", true));
        columns.add(new CsvColumnsConfig(8, "هزینه انبار بورسی (ریال)", false));
        columns.add(new CsvColumnsConfig(9, "زمان خروج نهایی", true));
        columns.add(new CsvColumnsConfig(10, "هزینه انبار موقت (ریال)", false));
        columns.add(new CsvColumnsConfig(11, "مالیات بر ارزش افزوده (ریال)", false));
        columns.add(new CsvColumnsConfig(12, "هزینه کل (ریال)", false));
        // columns.put("شماره مشتری","شماره مشتری");

        outAssetService.getOutAssetTotalList(storageId, search, clearanceFrom, clearanceTo, outFrom, outTo, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }
    @Autowired
    InstrumentService instrumentService;

    public boolean checkAccessToStorage(Authentication authentication, Integer outAssetId) {
        return outAssetId == null || outAssetService.findById(outAssetId).map(OutAsset::getStorage).map(Storage::getId).map(id -> id.equals(((User) authentication.getPrincipal()).getStorage().getId())).orElse(false);
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE') or @outAssetController.checkAccessToStorage(authentication, #id)")
    public String viewOutAsset(@PathVariable int id, Model model) {
//        outAssetService.findById(id).ifPresent(outAsset -> model.addAttribute("outAsset", outAsset));
        List<OutAsset> outAssets =  outAssetService.getAllOutAssetTotalInGroup(id);
        OutAssetTotal total = new OutAssetTotal();
        total.setQuantity(0);
        total.setCost(0l);
        total.setOcost(0l);
        total.setVat(0l);
        total.setId(id);
        total.setTcost(0l);
        outAssets.stream().forEach(oa -> {
            total.setQuantity(total.getQuantity()+oa.getQuantity());
            total.setCost( total.getCost()+oa.getCost());
            total.setOcost(total.getOcost()+oa.getOcost());
            total.setVat(total.getVat() + Math.round(oa.getVat()== null ? 0 :oa.getVat()));
            total.setClearanceDate(oa.getClearanceDate());
            total.setOutDate(oa.getOutDate());
            total.setInvestor(oa.getInvestor());
            total.setStorage(oa.getStorage());
            total.setTcost(total.getTcost() + oa.getCost()+oa.getOcost()+ Math.round(oa.getVat() == null ? 0 :oa.getVat()));
        });
        model.addAttribute("outAsset", total);
//        outAssetService.getOutAssetTotalInGroup(id).ifPresent(outAsset -> model.addAttribute("outAsset", outAsset));

        return "out-assets/edit";
    }

    @PostMapping(path = {"/exit/{id}"})
    @PreAuthorize("hasAuthority('USER_LEVEL_1_W') and @outAssetController.checkAccessToStorage(authentication, #id)")
    public String finalExit(@PathVariable Integer id, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = new HashMap<>();

//        OutAsset oa = outAssetService.findById(id).orElseThrow(EntityNotFoundException::new);
        List<OutAsset> outAssets = outAssetService.getAllOutAssetTotalInGroup(id);
        LocalDateTime ldt = LocalDateTime.now();
        for(OutAsset oa : outAssets) {
            oa.setOutDate(ldt);
            long daysBetween = DAYS.between(oa.getClearanceDate(), oa.getOutDate());
            oa.setOcost(daysBetween * oa.getStorage().getCost() * (oa.getQuantity()));
            oa.setVat((oa.getOcost() + oa.getCost()) * oa.getStorage().getInstrument().getCostVat());

            try {
                outAssetService.save(oa);

            } catch (Exception e) {
                errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
                model.addAttribute("errors", errors);
                return "out-assets/edit";
            }
        }
        attrs.addFlashAttribute("messages", new ArrayList<String>() {
            {
                add(String.format("خروج کالای %d با موفقیت ثبت شد", id));
            }
        });
        return "redirect:/out-assets/edit/" + id;
    }

    @GetMapping({"/print/{id}.html", "/print/{id}"})
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE') or @outAssetController.checkAccessToStorage(authentication, #id)")
    public String printOutAssetHtml(@PathVariable int id, Model model, Authentication authentication) {
        User user = ((User) authentication.getPrincipal());
        model.addAttribute("username", user.getFullname());
//        outAssetService.findById(id).ifPresent(outAsset -> model.addAttribute("outAsset", outAsset));
        List<OutAsset> outAssets =  outAssetService.getAllOutAssetTotalInGroup(id);
        OutAssetTotal total = new OutAssetTotal();
        total.setQuantity(0);
        total.setCost(0l);
        total.setOcost(0l);
        total.setVat(0l);
        total.setTcost(0l);
        total.setId(id);
        outAssets.stream().forEach(oa -> {
            total.setQuantity(total.getQuantity()+oa.getQuantity());
            total.setCost( total.getCost()+oa.getCost());
            total.setOcost(total.getOcost()+oa.getOcost());
            total.setVat(total.getVat() + Math.round(oa.getVat()== null ? 0 :oa.getVat()));
            total.setClearanceDate(oa.getClearanceDate());
            total.setOutDate(oa.getOutDate());
            total.setInvestor(oa.getInvestor());
            total.setStorage(oa.getStorage());
            total.setTcost(total.getTcost() + oa.getCost()+oa.getOcost()+ Math.round(oa.getVat() == null ? 0 :oa.getVat()));
        });
        model.addAttribute("outAsset", total);
        return "out-assets/print";
    }

}
