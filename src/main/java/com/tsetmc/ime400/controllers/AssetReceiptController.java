package com.tsetmc.ime400.controllers;


import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import org.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

@Controller
@RequestMapping(path = "/asset_receipt")
public class AssetReceiptController {
    @GetMapping("")
    public String indexPage() {
        return "asset_receipt/index";
    }

//    @GetMapping(value = "/index")
//    @ResponseBody
//    public ResponseEntity<String> getAssetReceiptList(
//                                                   @RequestParam(defaultValue = "") String search,
//                                                   @RequestParam(required = false) String entryFrom,
//                                                   @RequestParam(required = false) String entryTo,
//                                                   @RequestParam(required = false) String convertFrom,
//                                                   @RequestParam(required = false) String convertTo,
//                                                   @RequestParam(defaultValue = "0") int start,
//                                                   @RequestParam(defaultValue = "10") int length,
//                                                   @RequestParam(defaultValue = "ta.id") String orderKey,
//                                                   @RequestParam(defaultValue = "desc") String orderDir,
//                                                   Authentication authentication) throws SQLException, JSONException {
//        try{
//            User user = ((User) authentication.getPrincipal());
//            Integer storageId;
//            storageId = user.getStorage().getId();
//        }
//        catch (Exception e)
//        {return null;}
//
//        HttpHeaders responseHeaders = new HttpHeaders();
//        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
//        return new ResponseEntity<String>(
//                tempAssetService.getTempAssetList(storageId, search, entryFrom, entryTo, convertFrom, convertTo, start, length, orderKey, orderDir).toString(), responseHeaders, HttpStatus.OK);
//    }

}
