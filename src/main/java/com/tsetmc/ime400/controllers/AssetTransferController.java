package com.tsetmc.ime400.controllers;


import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.AssetTransferService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/asset-transfers")
public class AssetTransferController {

    @GetMapping("")
    public String indexPage() {
        return "asset-transfers/index";
    }

    @Autowired
    AssetTransferService assetTransferService;

    @GetMapping(value = "/index")
    @ResponseBody
    public String getAssetTransferList(@RequestParam(defaultValue = "") String search,
                                       @RequestParam(defaultValue = "0") int start,
                                       @RequestParam(defaultValue = "10") int length,
                                       @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                       @RequestParam(defaultValue = "desc") String orderDir,
                                       Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return assetTransferService.getAssetTransferList(search, storageId, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportAssetTransferList(@RequestParam(defaultValue = "") String search,
                                        @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                        @RequestParam(defaultValue = "desc") String orderDir,
                                        Authentication authentication,
                                        HttpServletResponse response) throws IOException, SQLException, JSONException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=asset-transfers.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شماره نامه", false));
        columns.add(new CsvColumnsConfig(4, "تاریخ نامه", true));
        columns.add(new CsvColumnsConfig(5, "شماره اعلامیه", false));
        columns.add(new CsvColumnsConfig(6, "مقدار انتقال", false));
        columns.add(new CsvColumnsConfig(7, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(8, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(9, "کد ملی", false));
        columns.add(new CsvColumnsConfig(10, "نام", false));
        columns.add(new CsvColumnsConfig(11, "نام خانوادگی", false));

        assetTransferService.getAssetTransferList(search, storageId, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
