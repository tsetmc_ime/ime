package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.interfaces.TextEncoder;
import com.tsetmc.ime400.models.*;
import com.tsetmc.ime400.services.*;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

@Controller
@RequestMapping(path = "/assets")
public class AssetController {

    @Autowired
    AssetRequestService assetRequestService;

    @Autowired
    AssetServices assetServices;

    @Autowired
    AS400Services as400Services;

    @GetMapping(value = "/status")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String totalAssetRequests() throws SQLException, JSONException {
        return assetRequestService.getStats().toString();
    }

    @GetMapping(value = "/user-status")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1', 'USER_LEVEL_2')")
    @ResponseBody
    public String AssetRequestInfo() throws SQLException, JSONException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return assetRequestService.getStatByUserId(user.getId()).toString();
    }

    @GetMapping("")
    public String indexPage() {
        return "assets/index";
    }

    @GetMapping("/asset-request-report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String assetRequestsReportPage() {
        return "assets/asset-request-report";
    }

    @GetMapping("/investor-report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String investorsReportPage() {
        return "assets/investor-report";
    }

    /* @GetMapping("/edit/{id}")
    public String editAssetPage(@PathVariable Integer id, Model model, Authentication authentication) {
        assetServices.getAssetById(id).ifPresent(asset -> model.addAttribute("asset", asset));
        return "assets/edit";
    } */

    @Autowired
    InstrumentService instrumentService;

    @Autowired
    TextEncoder textEncoder;

    @GetMapping("/index")
    @ResponseBody
    public String getAssetList(@RequestParam(defaultValue = "") String search,
                               @RequestParam(defaultValue = "0") int start,
                               @RequestParam(defaultValue = "10") int length,
                               @RequestParam(defaultValue = "کد سهامداری") String orderKey,
                               @RequestParam(defaultValue = "desc") String orderDir,
                               Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();
        return assetServices.getAssets(storageId, search, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportAssetList(@RequestParam(defaultValue = "") String search,
                                @RequestParam(defaultValue = "کد سهامداری") String orderKey,
                                @RequestParam(defaultValue = "desc") String orderDir,
                                Authentication authentication,
                                HttpServletResponse response) throws SQLException, JSONException, IOException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام", false));
        columns.add(new CsvColumnsConfig(3, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(4, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(5, "کد ملی", false));
        columns.add(new CsvColumnsConfig(6, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(7, "مقدار", false));
        columns.add(new CsvColumnsConfig(8, "نماد", false));
        columns.add(new CsvColumnsConfig(9, "نام انبار", false));

        assetServices.getAssetList(storageId, search, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));

    }

    @GetMapping("/asset-requests")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getAssetRequestReport(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) String from,
            @RequestParam(required = false) String to,
            @RequestParam(required = false) Boolean done,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "10") int length,
            @RequestParam(defaultValue = "DT_RowId") String orderKey,
            @RequestParam(defaultValue = "desc") String orderDir) {
        return assetRequestService.getReport(search, from, to, done, start, length, orderKey, orderDir);
    }

    @GetMapping("/export-asset-requests")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportAssetRequestReport(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) String from,
            @RequestParam(required = false) String to,
            @RequestParam(required = false) Boolean done,
            @RequestParam(defaultValue = "DT_RowId") String orderKey,
            @RequestParam(defaultValue = "desc") String orderDir,
            HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=asset-requests.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "نام", false));
        columns.add(new CsvColumnsConfig(5, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(6, "کد ملی", false));
        columns.add(new CsvColumnsConfig(7, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(8, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(9, "مقدار", false));
        columns.add(new CsvColumnsConfig(10, "تاریخ ثبت درخواست", true));
        columns.add(new CsvColumnsConfig(11, "تاریخ دریافت پاسخ", true));
        columns.add(new CsvColumnsConfig(12, "وضعیت", false));

        assetRequestService.getReport(search, from, to, done, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));

    }

    @Autowired
    ClearanceRequestService clearanceRequestService;

    @Autowired
    InvestorService investorService;

    @Autowired
    StorageService storageService;

    String renderForm(Integer id, Model model, ClearanceRequest clearanceRequest, Map<String, String> errors) {
        model.addAttribute("clearanceRequest", clearanceRequest);
        model.addAttribute("asset", assetServices.getAssetById(id).orElse(null));
        model.addAttribute("errors", errors);
        return "assets/clear";
    }

    public boolean checkAccessToStorage(Authentication authentication, Integer assetId) {
        return assetId == null || assetServices.getAssetById(assetId).map(Asset::getStorage).map(Storage::getId).map(id -> id.equals(((User) authentication.getPrincipal()).getStorage().getId())).orElse(false);
    }

    @GetMapping("/clear/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE') or @assetController.checkAccessToStorage(authentication, #id)")
    public String clearAsset(@PathVariable Integer id, Model model) {
        return renderForm(id, model, new ClearanceRequest(), null);
    }

    @PostMapping("/clear/{id}")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1_W', 'USER_LEVEL_2_W') and @assetController.checkAccessToStorage(authentication, #id)")
    public String clearanceRequest(@PathVariable(required = false) Integer id,
                                   @Valid ClearanceRequest clearanceRequest, Errors bindingErrors, Model model,
                                   Authentication authentication, RedirectAttributes attrs) throws SQLException, JSONException {
        clearanceRequest.setId(null); // I don't know why it is set to asset id by spring/thymeleaf

        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (errors.size() > 0)
            return renderForm(id, model, clearanceRequest, errors);

        assetServices.getAssetById(id).ifPresent(asset -> clearanceRequest.setInvestor(asset.getInvestor()));

        User user = ((User) authentication.getPrincipal());
        // User user = userRepository.findById(269).orElse(null);

        Storage storage = user.getStorage();
        Investor investor = investorService.findById(clearanceRequest.getInvestor().getId()).orElseThrow(EntityNotFoundException::new);
        as400Services.inquiryInvestor(investor.getPersonType().getId(), investor.getPtcode(), investor.getNationalId());

        investor = investorService.findById(investor.getId()).orElseThrow(EntityNotFoundException::new);
        if (investor.getActivityState() == null || investor.getActivityState().getId() != 1) {
            errors.put(Investor.class.getSimpleName().toLowerCase() + "." + Investor.Fields.ptcode, "کد سهامداری وارد شده فعال نیست");
            return renderForm(id, model, clearanceRequest, errors);
            // attrs.addFlashAttribute("errors", errors); // TODO value cleared after redirect
            // return "redirect:/assets/clear/" + id;
        }

        int assetAmount;
        try {
            assetAmount = as400Services.getAssetAmount(investor.getAsciiPtcode(), storage.getInstrument().getSymbol(), storage.getInstrument().getLot());
        } catch (RuntimeException e) {
            errors.put("e8", e.getMessage());
            return renderForm(id, model, clearanceRequest, errors);
        }

        List<ClearanceRequest> existingCrList = clearanceRequestService.getClearanceRequests(storage.getId(), investor.getId());
        int clearedQty = existingCrList.stream().map(ClearanceRequest::getQuantity).reduce(0, Integer::sum);
        if (clearedQty + clearanceRequest.getQuantity() > assetAmount) {
            errors.put(ClearanceRequest.Fields.quantity, "مقدار وارد شده برای ترخیص بیش از کل دارایی بورسی است");
            return renderForm(id, model, clearanceRequest, errors);
        }

        clearanceRequest.setId(null);
        clearanceRequest.setRegistrantUser(user);
        clearanceRequest.setRegisterDate(new Date());
        clearanceRequest.setInvestor(investorService.getInvestorById(clearanceRequest.getInvestor().getId()).orElse(null));
        clearanceRequest.setIsFinalConfirmed((byte) 0);
        clearanceRequest.setStorage(storage);

        try {
            clearanceRequestService.createClearanceRequest(clearanceRequest);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            return renderForm(id, model, clearanceRequest, errors);
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("درخواست ترخیص %d با موفقیت ثبت شد. این درخواست برای اعمال باید حتماً به تأیید نهایی برسد.", clearanceRequest.getId()));
        }});
        return "redirect:/assets";
    }

    @GetMapping("/investor-assets")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getInvestorAssetsReport(@RequestParam(defaultValue = "") String search,
                                          @RequestParam(defaultValue = "0") int start,
                                          @RequestParam(defaultValue = "10") int length,
                                          @RequestParam(defaultValue = "کد سهامداری") String orderKey,
                                          @RequestParam(defaultValue = "desc") String orderDir) {
        return assetServices.getAssetReport(search, start, length, orderKey, orderDir);
    }

    @GetMapping("/export-investor-assets")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportInvestorAssetsReport(@RequestParam(defaultValue = "") String search,
                                           @RequestParam(defaultValue = "کد سهامداری") String orderKey,
                                           @RequestParam(defaultValue = "desc") String orderDir,
                                           HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=investor-assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "نام", false));
        columns.add(new CsvColumnsConfig(5, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(6, "کد ملی", false));
        columns.add(new CsvColumnsConfig(7, "کد سهامداری", false));
        columns.add(new CsvColumnsConfig(8, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(9, "موجودی موقت", false));
        columns.add(new CsvColumnsConfig(10, "موجودی بورسی", false));
        columns.add(new CsvColumnsConfig(11, "موجودی در آستانه خروج", false));

        assetServices.getAssetReport(search, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
