package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.models.Job;
import com.tsetmc.ime400.models.JobName;
import com.tsetmc.ime400.models.JobStatus;
import com.tsetmc.ime400.models.JobView;
import com.tsetmc.ime400.repositories.JobNameRepository;
import com.tsetmc.ime400.repositories.JobRepository;
import com.tsetmc.ime400.repositories.JobStatusRepository;
import com.tsetmc.ime400.repositories.JobViewRepository;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping(path = "/post-session")
@PreAuthorize("hasAuthority('ADMIN')")
public class PostSessionController {

    @Autowired
    JobViewRepository jobViewRepository;

    @GetMapping("")
    public String indexPage() {
        return "post-session/dashboard";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<JobView> getDayJobs() {
        return jobViewRepository.findAll();
    }

    @GetMapping("/current-date")
    @ResponseBody
    public LocalDate getSummary() {
        return LocalDate.now();
    }

    @Autowired
    JobRepository jobRepository;

    @Autowired
    JobStatusRepository jobStatusRepository;

    @Autowired
    JobNameRepository jobNameRepository;

    static Map<String,String> JobRunOrder = new HashMap<>();
    public PostSessionController(){
//        JobRunOrder.put("ASSET_TRANSFER","INQUIRY");
        JobRunOrder.put("TRADE","ASSET_TRANSFER");
        JobRunOrder.put("ASSET","TRADE");
        JobRunOrder.put("CLEARANCE_REQUEST","ASSET");
        JobRunOrder.put("NAZER","CLEARANCE_REQUEST");
        JobRunOrder.put("FUTURE_DELIVERY","NAZER");
    }


    @PostMapping("/trigger")
    public ResponseEntity<?> triggerStartJob(@RequestParam String name) {
        try {
            JobName jobName = jobNameRepository.findById(name).orElseThrow(EntityNotFoundException::new);

//            //برای آن که همزمان قابل اجرا نباشد
//            List<Job> jobsInRun = jobRepository.getJobs();
//            if(jobsInRun != null){
//                Optional<Job> jobInRun = jobsInRun.stream().filter(x-> x.getStatus().getDescription() != "FINISHED").findFirst();
//                if(jobInRun.isPresent()){
//                    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("احتراما، تا پایان اجرای فرآیند "+ jobInRun.get().getName().getDescription() +" منتظر بمانید");
//                }
//            }
//            //برای آن که به ترتیب قابل اجرا باشد
//            if(JobRunOrder.containsKey(name)) {
//                String lastJobRun = JobRunOrder.get(name);
//                JobName lastJobName = jobNameRepository.findById(lastJobRun).orElseThrow(EntityNotFoundException::new);
//                Job lastJob = jobRepository.findByName(lastJobName).orElse(new Job());
//                if (lastJob.getStatus().getDescription() != "FINISHED")
//                    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("احتراما، تا پایان اجرای فرآیندهای پیشین منتظر بمانید");
//            }

            JobStatus jobStatus = jobStatusRepository.findById("NOT_STARTED").orElseThrow(EntityNotFoundException::new);

            Job job = jobRepository.findByName(jobName).orElse(new Job());

            job.setName(jobName);
            job.setFire(true);
            job.setStatus(jobStatus);

            jobRepository.save(job);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(SQLErrorCheck.getRootCause(e).getMessage());
        }

        return ResponseEntity.ok().build();
    }

}
