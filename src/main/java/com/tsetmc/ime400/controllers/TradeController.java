package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.TradeService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/trades")
public class TradeController {

    @GetMapping("")
    public String indexPage() {
        return "trades/index";
    }

    @Autowired
    TradeService tradeService;

    @GetMapping(value = "/index")
    @ResponseBody
    public String getOutAssetList(@RequestParam(defaultValue = "") String search,
                                  @RequestParam(required = false) String tradeDateFrom,
                                  @RequestParam(required = false) String tradeDateTo,
                                  @RequestParam(defaultValue = "0") int start,
                                  @RequestParam(defaultValue = "10") int length,
                                  @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                  @RequestParam(defaultValue = "desc") String orderDir,
                                  Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return tradeService.getTradeList(search, storageId, tradeDateFrom, tradeDateTo, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportOutAssetList(@RequestParam(defaultValue = "") String search,
                                   @RequestParam(required = false) String tradeDateFrom,
                                   @RequestParam(required = false) String tradeDateTo,
                                   @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                   @RequestParam(defaultValue = "desc") String orderDir,
                                   Authentication authentication,
                                   HttpServletResponse response) throws IOException, SQLException, JSONException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=trades.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        // columns.add(new CsvColumnsConfig(3,"شرح نماد",false));
        columns.add(new CsvColumnsConfig(4, "تاریخ", false));
        columns.add(new CsvColumnsConfig(5, "شماره اعلامیه", false));
        columns.add(new CsvColumnsConfig(6, "مقدار", false));
        columns.add(new CsvColumnsConfig(7, "قیمت", false));
        columns.add(new CsvColumnsConfig(8, "کد بورسی خریدار", false));
        columns.add(new CsvColumnsConfig(9, "کد مشتری خریدار", false));
        columns.add(new CsvColumnsConfig(10, "شماره ملی خریدار", false));
        columns.add(new CsvColumnsConfig(11, "نام خانوادگی خریدار", false));
        columns.add(new CsvColumnsConfig(12, "نام خریدار", false));
        columns.add(new CsvColumnsConfig(13, "کد بورسی فروشنده", false));
        columns.add(new CsvColumnsConfig(14, "کد مشتری فروشنده", false));
        columns.add(new CsvColumnsConfig(15, "شماره ملی فروشنده", false));
        columns.add(new CsvColumnsConfig(16, "نام خانوادگی فروشنده", false));
        columns.add(new CsvColumnsConfig(17, "نام فروشنده", false));

        tradeService.getTradeList(search, storageId, tradeDateFrom, tradeDateTo, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
