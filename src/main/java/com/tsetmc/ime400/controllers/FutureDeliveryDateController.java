package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.models.Broker;
import com.tsetmc.ime400.models.FutureDeliveryDate;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.FutureDeliveryDateService;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.DateTimeService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@RequestMapping(path = "/future-dates")
@PreAuthorize("hasAuthority('ADMIN')")
public class FutureDeliveryDateController {

    @Autowired
    FutureDeliveryDateService futureDeliveryDateService;

    @GetMapping("")
    public String indexPage() {
        return "/future-dates/index";
    }

    @GetMapping(value = "/index")
    @ResponseBody
    public String getFutureDeliveryDateList(@RequestParam(defaultValue = "") String search,
                                                              @RequestParam(defaultValue = "0") int start,
                                                              @RequestParam(defaultValue = "10") int length,
                                                              @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                                              @RequestParam(defaultValue = "desc") String orderDir) throws SQLException, JSONException
    {


        return futureDeliveryDateService.getFutureDeliveryDateList(search, start, length, orderKey, orderDir);
    }

    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String createFutureDate(
                                       @Valid  FutureDeliveryDate futureDeliveryDate,
                                       Errors bindingErrors, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (errors.size() > 0)
            return "future-dates/edit";

        LocalDate date = LocalDate.now();
        if(futureDeliveryDate.getDate().compareTo(date) < 0) {
            errors.put("date", "تاریخ تحویل آتی نمی تواند قبل از تاریخ جاری باشد");
            model.addAttribute("errors", errors);
            return "future-dates/edit";
        }
        if(futureDeliveryDate.getLetterDate().compareTo(date) > 0) {
            errors.put("letterDate", "تاریخ نامه نمی تواند بعد از تاریخ جاری باشد");
            model.addAttribute("errors", errors);
            return "future-dates/edit";
        }
        try {
            futureDeliveryDateService.save(futureDeliveryDate);

        } catch (Exception e) {

            String msg = SQLErrorCheck.getRootCause(e).getMessage();
            if(msg.contains("Duplicate")){
                errors.put("e3", "تاریخ مورد نظر قبلا ثبت شده است");
            }
            else
                errors.put("e3", msg);
            model.addAttribute("errors", errors);
            return "future-dates/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("تحویل آتی روز %s با موفقیت ثبت شد", DateTimeService.fromGregoriantoJalali(futureDeliveryDate.getDate().format(DateTimeFormatter.ISO_DATE))));
        }});

        return "redirect:/future-dates";
    }
    @GetMapping("/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String newFutureDeliveryDatePage(Model model) {
        model.addAttribute("futureDeliveryDate", new FutureDeliveryDate());
        return "future-dates/edit";
    }

    @PostMapping("/discard/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable(required = true) Integer id) {
        Map<String, String> errors = new HashMap<>();

        FutureDeliveryDate date = futureDeliveryDateService.FindById(id);

        if(date!=null){
            if(date.getDate().compareTo(LocalDate.now())<0){
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("تاریخ تحویل آتی مورد نظر گذشته است ؛ و حذف آن امکان پذیر نمی باشد");

            }
        }

        try {
            futureDeliveryDateService.deleteFutureDeliveryDate(id);


        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(SQLErrorCheck.getRootCause(e).getMessage());
        }

        return ResponseEntity.ok().build();

    }
}
