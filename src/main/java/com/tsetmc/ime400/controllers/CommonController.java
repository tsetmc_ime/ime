package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.StorageService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Arrays;

@ControllerAdvice
public class CommonController {
    @ModelAttribute
    public void foo(Model model, Authentication authentication) {
        if (authentication != null) {
            User user = ((User) authentication.getPrincipal());
            model.addAttribute("role", user.getRole().getName());

            final Storage storage = user.getStorage();
            if (storage != null) {
                model.addAttribute("storageName", storage.getName());
                final Instrument instrument = storage.getInstrument();
                if (instrument != null) {
                    model.addAttribute("instrumentSymbol", instrument.getSymbol());
                    model.addAttribute("instrumentDescription", instrument.getSymbolDesc());
                    model.addAttribute("weightUnit", instrument.getWeightUnit());
                }
            }
        }
    }


}
