package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.repositories.ActivityStateRepository;
import com.tsetmc.ime400.repositories.StorageRepository;
import com.tsetmc.ime400.services.InstrumentService;
import com.tsetmc.ime400.services.StorageService;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/storages")
public class StorageController {

    @Autowired
    StorageService storageService;

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    InstrumentService instrumentService;

    @GetMapping(value = "/status")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1', 'USER_LEVEL_2')")
    @ResponseBody
    public String getStorageStats(Authentication authentication) throws SQLException, JSONException {
        return storageService.getStorageStat(((User) authentication.getPrincipal()).getStorage().getId()).toString();
    }

    @GetMapping(value = "/header-status")
    @PreAuthorize("hasAnyAuthority('USER_LEVEL_1', 'USER_LEVEL_2')")
    @ResponseBody
    public String getStorageStateForHeader(Authentication authentication) throws Exception {
        User user = ((User) authentication.getPrincipal());

        final Storage storage = user.getStorage();
        String weightUnit = storage.getInstrument().getWeightUnit();
        JSONObject storageState;
        StringBuilder storageStateStr = new StringBuilder("");
        storageState = storageService.getStorageStat(storage.getId());
        storageStateStr.append("انبار موقت");
        storageStateStr.append(" ");
        storageStateStr.append(storageState.get("انبار موقت"));
        storageStateStr.append(" ");
        storageStateStr.append(weightUnit);
        storageStateStr.append(" - ");
        storageStateStr.append("انبار بورسی");
        storageStateStr.append(" ");
        storageStateStr.append(storageState.get("انبار بورسی"));
        storageStateStr.append(" ");
        storageStateStr.append(weightUnit);
        storageStateStr.append(" - ");
        storageStateStr.append("انبار خروجی");
        storageStateStr.append(" ");
        storageStateStr.append(storageState.get("انبار خروجی"));
        storageStateStr.append(" ");
        storageStateStr.append(weightUnit);
        storageStateStr.append(" ");

        return storageStateStr.toString();
    }

    @GetMapping(value = "/index")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getStorageList(@RequestParam(defaultValue = "") String search,
                                 @RequestParam(required = false) Integer status,
                                 @RequestParam(defaultValue = "0") int start,
                                 @RequestParam(defaultValue = "10") int length,
                                 @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                 @RequestParam(defaultValue = "desc") String orderDir) {
        return storageService.getStorageList(search, status, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportStorageList(@RequestParam(defaultValue = "") String search,
                                  @RequestParam(required = false) Integer status,
                                  HttpServletResponse response) throws SQLException, JSONException, IOException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=storages.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(3, "نماد", false));
        columns.add(new CsvColumnsConfig(4, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(5, "سقف ظرفیت", false));
        columns.add(new CsvColumnsConfig(6, "هزینه انبارداری", false));
        columns.add(new CsvColumnsConfig(7, "مالیات بر ارزش افزوده", false));
        columns.add(new CsvColumnsConfig(8, "وضعیت", false));
        columns.add(new CsvColumnsConfig(9, "تاریخ سررسید", true));

        storageService.getStorageList(search, status, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

    @Autowired
    ActivityStateRepository activityStateRepository;

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String indexPage(Model model) {
        model.addAttribute("activityStates", activityStateRepository.findAll());
        return "storages/index";
    }

    @GetMapping("/report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String reportPage(Model model) {
        return "storages/report";
    }

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String newStoragePage(Model model) {
        model.addAttribute("storage", new Storage());
        return "storages/edit";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN','SURVEILLANCE')")
    public String editStoragePage(@PathVariable int id, Model model) {
        storageRepository.findById(id).ifPresent(storage -> model.addAttribute("storage", storage));
        return "storages/edit";
    }

    @Autowired
    StorageRepository storageRepository;

    @PostMapping(path = {"/new", "/edit/{id}"})
    @PreAuthorize("hasAnyAuthority('ADMIN','SURVEILLANCE_W')")
    public String createOrUpdateStorage(@PathVariable(required = false) Integer id,
                                        @Valid Storage storage, Errors bindingErrors, Model model, RedirectAttributes attrs) {
        storage.setId(id);

        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);

        Instrument instrument = instrumentService.findInstrumentBySymbol(storage.getInstrument().getSymbol()).orElse(null);
        if (instrument == null) {
            errors.put(Instrument.class.getSimpleName().toLowerCase() + "." + Instrument.Fields.symbol, "نماد مورد نظر وجود ندارد");
            model.addAttribute("errors", errors);
        }

        if (errors.size() > 0)
            return "storages/edit";

        storage.setInstrument(instrument);

        final boolean inserting = storage.getId() == null;

        try {
            storageRepository.save(storage);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "storages/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("رکورد %s با موفقیت %s", storage.getName(), inserting ? "ایجاد شد" : "تغییر یافت"));
        }});

        return "redirect:/storages";
    }

    @GetMapping(value = "/getByName")
    @ResponseBody
    public String getStorages(@RequestParam(defaultValue = "") String name) throws SQLException, JSONException {
        if (name.length() < 3)
            return "[]";
        return storageService.getStoragesForAutoComplete(name);
    }

    @GetMapping(value = "/report-index")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getStorageAssetsReport(@RequestParam(defaultValue = "") String search,
                                         @RequestParam(defaultValue = "0") int start,
                                         @RequestParam(defaultValue = "10") int length,
                                         @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                         @RequestParam(defaultValue = "desc") String orderDir) {
        return storageService.getStorageAssetsReport(search, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-report")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void reportStorageAssetsReport(@RequestParam(defaultValue = "") String search,
                                          @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                          @RequestParam(defaultValue = "desc") String orderDir,
                                          HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=storage-assets.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام انبار", false));
        columns.add(new CsvColumnsConfig(3, "نماد", false));
        columns.add(new CsvColumnsConfig(4, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(5, "موجودی موقت", false));
        columns.add(new CsvColumnsConfig(6, "موجودی بورسی", false));
        columns.add(new CsvColumnsConfig(7, "مقدار خارج شده", false));
        columns.add(new CsvColumnsConfig(8, "موجودی در آستانه خروج", false));
        columns.add(new CsvColumnsConfig(8, "سقف ظرفیت", false));
        columns.add(new CsvColumnsConfig(8, "ظرفیت باقی\u200Cمانده", false));

        storageService.getStorageAssetsReport(search, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
