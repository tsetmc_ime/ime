package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.repositories.PersonTypeRepository;
import com.tsetmc.ime400.services.InvestorService;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/investors")
public class InvestorController {

    @Autowired
    InvestorService investorService;

    @GetMapping("")
    public String indexPage() {
        return "investors/index";
    }

    @GetMapping(value = "/new")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE_W', 'USER_LEVEL_1_W', 'USER_LEVEL_2_W')")
    public String newInvestorPage(Model model) {
        model.addAttribute("investor", new Investor());
        return "investors/edit";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE_W', 'USER_LEVEL_1_W', 'USER_LEVEL_2_W')")
    public String editInvestorPage(@PathVariable int id, Model model) {
        investorService.getInvestorById(id).ifPresent(investor -> model.addAttribute("investor", investor));
        return "investors/edit";
    }

    @Autowired
    PersonTypeRepository personTypeRepository;

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("personTypes", personTypeRepository.findAll());
    }

    @PostMapping(path = {"/new", "/edit/{id}"})
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE_W', 'USER_LEVEL_1_W', 'USER_LEVEL_2_W')")
    public String createUpdateInvestor(@PathVariable(required = false) Integer id,
                                       @Valid Investor investor,
                                       Errors bindingErrors, Model model, RedirectAttributes attrs) {
        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);
        if (errors.size() > 0)
            return "investors/edit";

        try {
            Investor invstr = null;
            if (StringUtils.isNotBlank(investor.getAsciiPtcode()) && investor.getAsciiPtcode() != null) {
                invstr = investorService.findByPTCode14(investor.getAsciiPtcode()).orElse(null);
                if (invstr != null) {
                    if (!investor.getNationalId().equals(invstr.getNationalId()) || !StringUtils.right(investor.getPtcode(), 5).equals(StringUtils.right(invstr.getPtcode(), 5)))
                        invstr = null;
                    else {
                        invstr.setPhone(investor.getPhone());
                        invstr.setAddress(investor.getAddress());
                        invstr.setAccountNumber(investor.getAccountNumber());
                        investorService.saveInvestor(invstr);
                    }
                }
            }
            if (invstr == null) {
                errors.put(Investor.Fields.asciiPtcode, "کد ۱۴ رقمی سهامدار موجود نیست و یا با کد ملی و کد بورسی انطباق ندارد");
                model.addAttribute("errors", errors);
                return "investors/edit";
            }

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "investors/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("سهامدار %s با موفقیت %s", investor.getPtcode(), id == null ? "ایجاد شد" : "تغییر یافت"));
        }});
        return "redirect:/investors";
    }

    @GetMapping(value = "/index")
    @ResponseBody
    public String getInvestorList(@RequestParam(defaultValue = "") String search,
                                  @RequestParam(required = false) Integer status,
                                  @RequestParam(required = false) Integer type,
                                  @RequestParam(defaultValue = "0") int start,
                                  @RequestParam(defaultValue = "10") int length,
                                  @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                  @RequestParam(defaultValue = "desc") String orderDir) {
        return investorService.getInvestorList(search, status, type, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportInvestorList(@RequestParam(defaultValue = "") String search,
                                   @RequestParam(required = false) Integer status,
                                   @RequestParam(required = false) Integer type,
                                   @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                   @RequestParam(defaultValue = "desc") String orderDir,
                                   HttpServletResponse response) throws IOException, SQLException, JSONException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=investors.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نام", false));
        columns.add(new CsvColumnsConfig(3, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(4, "کد ملی", false));
        columns.add(new CsvColumnsConfig(5, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(6, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(7, "مالیات بر ارزش افزوده معاملات", false));
        columns.add(new CsvColumnsConfig(8, "نوع", false));
        columns.add(new CsvColumnsConfig(9, "وضعیت", false));
        columns.add(new CsvColumnsConfig(10, "آخرین تاریخ استعلام", true));

        investorService.getInvestorList(search, status, type, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
