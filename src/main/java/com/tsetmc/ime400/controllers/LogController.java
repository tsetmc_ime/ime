package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.services.LogService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
@RequestMapping(path = "/activities")
public class LogController {

    @Autowired
    LogService logService;

    @GetMapping("")
    public String indexPage() {
        return "activities/index";
    }

    @GetMapping(value = "/index")
    @ResponseBody
    public String getActivityList(@RequestParam(defaultValue = "") String search,
                                  @RequestParam(defaultValue = "0") int start,
                                  @RequestParam(defaultValue = "10") int length,
                                  @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                  @RequestParam(defaultValue = "desc") String orderDir,
                                  @RequestParam(required = false) String dateFrom,
                                  @RequestParam(required = false) String dateTo) {
        return logService.getLogList(search, start, length, orderKey, orderDir, dateFrom, dateTo);
    }

    @GetMapping(value = "/export-csv")
    public void exportBrokerList(@RequestParam(defaultValue = "") String search,
                                 @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                 @RequestParam(defaultValue = "desc") String orderDir,
                                 @RequestParam(required = false) String dateFrom,
                                 @RequestParam(required = false) String dateTo,
                                 HttpServletResponse response) throws SQLException, JSONException, IOException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=activities.csv");

        logService.getLogList(search, orderKey, orderDir, dateFrom, dateTo, JsonService.getCsvWriter(response));
    }

}
