package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.repositories.InstrumentRepository;
import com.tsetmc.ime400.services.InstrumentService;
import com.tsetmc.ime400.util.BindingErrorsCheck;
import com.tsetmc.ime400.util.JsonService;
import com.tsetmc.ime400.util.SQLErrorCheck;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/instruments")
public class InstrumentController {

    @Autowired
    InstrumentService instrumentService;

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String indexPage() {
        return "instruments/index";
    }

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String newInstrumentPage(Model model) {
        model.addAttribute("instrument", new Instrument());
        return "instruments/edit";
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("clearanceTypes", instrumentService.getClearanceType());
        model.addAttribute("clearanceTimes", instrumentService.getClearanceTime());
        model.addAttribute("convertTimes", instrumentService.getConvertTime());
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public String editInstrumentPage(@PathVariable int id, Model model) {
        Instrument instrument = instrumentRepository.findById(id).orElse(null);
        if(instrument != null) {
            instrument.setTradesVat(instrument.getTradesVat() * 100);
            instrument.setCostVat(instrument.getCostVat() * 100);
            model.addAttribute("instrument", instrument);
        }

        return "instruments/edit";
    }

    @Autowired
    InstrumentRepository instrumentRepository;

    /* @GetMapping("/{id}")
    @ResponseBody
    public Instrument getInstrumentInfo(@PathVariable Integer id) {
        return instrumentRepository.findById(id).orElse(null);
    } */

    @PostMapping(path = {"/new", "/edit/{id}"})
    @PreAuthorize("hasAnyAuthority('ADMIN','SURVEILLANCE_W')")
    public String createOrUpdateInstrument(@PathVariable(required = false) Integer id,
                                           @Valid Instrument instrument,
                                           Errors bindingErrors, Model model, RedirectAttributes attrs) {
        boolean inserting = id == null;
        instrument.setId(id);

        Map<String, String> errors = BindingErrorsCheck.getErrors(model, bindingErrors);

        if (errors.size() > 0)
            return "instruments/edit";

        try {
            instrument.setTradesVat(instrument.getTradesVat()/100);
            instrument.setCostVat(instrument.getCostVat()/100);
            instrumentRepository.save(instrument);

        } catch (Exception e) {
            errors.put("e3", SQLErrorCheck.getRootCause(e).getMessage());
            model.addAttribute("errors", errors);
            return "instruments/edit";
        }

        attrs.addFlashAttribute("messages", new ArrayList<String>() {{
            add(String.format("نماد %s با موفقیت %s", instrument.getSymbol(), inserting ? "ایجاد شد" : "تغییر یافت"));
        }});

        return "redirect:/instruments";
    }

    @GetMapping(value = "/getBySymbol")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String GetInstrumentsBySymbol(@RequestParam(defaultValue = "") String symbol) throws SQLException, JSONException {
        if (symbol.length() < 3)
            return "[]";
        return instrumentService.getInstrumentsBySymbol(symbol);
    }

    @GetMapping(value = "/index")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    @ResponseBody
    public String getInstrumentList(@RequestParam(defaultValue = "") String search,
                                    @RequestParam(required = false) String from,
                                    @RequestParam(required = false) String to,
                                    @RequestParam(defaultValue = "0") int start,
                                    @RequestParam(defaultValue = "10") int length,
                                    @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                    @RequestParam(defaultValue = "desc") String orderDir) {
        return instrumentService.getInstrumentList(search, from, to, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'SURVEILLANCE')")
    public void exportInstrumentList(@RequestParam(defaultValue = "") String search,
                                     @RequestParam(required = false) String from,
                                     @RequestParam(required = false) String to,
                                     @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                     @RequestParam(defaultValue = "desc") String orderDir,
                                     HttpServletResponse response) throws SQLException, JSONException, IOException {
        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=instruments.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "شرح نماد", false));
        columns.add(new CsvColumnsConfig(4, "اندازه بسته معاملاتی", false));
        columns.add(new CsvColumnsConfig(5, "واحد وزن", false));
        columns.add(new CsvColumnsConfig(6, "مالیات بر ارزش افزوده هزینه انبارداری", false));
        columns.add(new CsvColumnsConfig(7, "مالیات بر ارزش افزوده معاملات", false));
        columns.add(new CsvColumnsConfig(8, "تاریخ سررسید", true));

        instrumentService.getInstrumentList(search, from, to, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
