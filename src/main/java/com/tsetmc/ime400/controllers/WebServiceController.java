package com.tsetmc.ime400.controllers;

import com.tsetmc.ime400.dto.FutureDeliveryDto;
import com.tsetmc.ime400.models.*;
import com.tsetmc.ime400.repositories.FutureAssetRepository;
import com.tsetmc.ime400.repositories.JobViewRepository;
import com.tsetmc.ime400.repositories.UserRepository;
import com.tsetmc.ime400.security.jwt.JwtUtils;
import com.tsetmc.ime400.services.*;
import com.tsetmc.ime400.types.*;
import com.tsetmc.ime400.util.DateTimeService;
import io.swagger.v3.oas.annotations.Operation;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class WebServiceController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StorageService storageService;

    @Autowired
    InvestorService investorService;

    @Autowired
    AS400Services as400Services;

    @Autowired
    FutureAssetRepository futureAssetRepository;

    @Autowired
    FutureDeliveryService futureDeliveryService;

    @Autowired
    InstrumentService instrumentService;

    //todo swagger
    @PostMapping("/api/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        User userDetails = (User) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getFullname(),
                userDetails.getRole().getName(),
                "Bearer",
                jwt));
    }

    @Operation(summary = "Get All instruments information")
    @GetMapping("/api/getAllSymbol")
    @ResponseBody
    public String getAllSymbols() throws SQLException, JSONException {
        return storageService.getAllSymbols().toString();
    }

//    content = [
//            (Content(mediaType = "application/json", array = (
//            ArraySchema(schema = Schema(implementation = Foo::class)))))]),
    // todo test
//    @Operation(summary = "Get All Future/Option Deliverable Records from ime")
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "200", description = "records are received successfully",
////                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = FutureDelivery.class)))),
//                    content = @Content),
//            @ApiResponse(responseCode = "400", description = "Invalid data",
//                    content = @Content),
//            @ApiResponse(responseCode = "402", description = "internal server error",
//                    content = @Content) })

    @Autowired
    JobViewRepository jobViewRepository;

    /* @GetMapping("/test")
    @ResponseBody
    public String text() {
        return jobRepository.find_StatusByName(JobName.CLEARANCE_REQUEST.name()).name();
    } */

    @PostMapping("/api/putFutureDelivery") // TODO check authority
    @ResponseBody
    public String putFutureDelivery(@RequestBody List<FutureDeliveryRequest> futureDeliveryRequests) throws JSONException, SQLException {
        boolean result = true;
        String message = "";
        int responseCode = 200;
        JSONObject jobj = new JSONObject();

        /* TODO JobStatus clearJob = jobRepository.find_StatusByName(JobName.CLEARANCE_REQUEST.name());
        JobStatus assetJob = jobRepository.find_StatusByName(JobName.ASSET_REQUEST.name());
        JobStatus futureDeliveryJob = jobRepository.find_StatusByName(JobName.FUTURE_DELIVERY.name());
        if (!clearJob.equals(JobStatus.FINISHED) ||
                !assetJob.equals(JobStatus.FINISHED) ||
                !(futureDeliveryJob.equals(JobStatus.NOT_STARTED) || futureDeliveryJob.equals(JobStatus.FAILED))) {
            result = false;
            responseCode = 400;
            message = "در حال حاضر امکان اجرای این سرویس وجود ندارد";
            jobj.put("success", result);
            jobj.put("message", message);
            jobj.put("responseCode", responseCode);
            return jobj.toString();
        } */

        List<Investor> investors = new ArrayList<Investor>();
        List<FutureDelivery> futureDeliveryList = new ArrayList<>();

        ////////////////////////////////////////////////////////////////////////////
        /////////Inquiry Investors from AS400 //////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        for (FutureDeliveryRequest f : futureDeliveryRequests) {
            Investor inv = investorService.findByPTCode14(f.getPtcode14()).orElse(null);
            if (inv == null) {
                responseCode = 400;
                result = false;
                message = message.concat(String.format(" کد سهامداری %s اشتباه است , ", f.getPtcode14()));
            } else {
                investors.add(inv);
            }
        }

        if (!result) {
            jobj.put("success", result);
            jobj.put("message", message);
            jobj.put("responseCode", responseCode);
            return jobj.toString();

        } else {
            as400Services.inquiryInvestors(investors);
        }
        ////////////////////////////////////////////////////////////////////////////
        ////// checking activity status and asset of each investor /////////////////
        ////////////////////////////////////////////////////////////////////////////

        for (FutureDeliveryRequest f : futureDeliveryRequests) {
            Investor updatedInvestor = investorService.findByPTCode14(f.getPtcode14()).orElse(null);
            if (updatedInvestor.getActivityState().getId() == 2) {
                result = false;
                responseCode = 400;
                message = message.concat(String.format(" , کد سهامداری %s غیرفعال است", updatedInvestor.getAsciiPtcode()));
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////check future asset //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            Instrument instrument = instrumentService.findInstrumentBySymbol(f.getSymbol()).orElse(null);
            if (instrument == null) {
                result = false;
                message = String.format("نماد %s وجود ندارد", f.getSymbol());
                responseCode = 400;
                jobj.put("success", result);
                jobj.put("message", message);
                jobj.put("responseCode", responseCode);
                return jobj.toString();
            }
            if (f.getRequestedQuantiry() < 0) {
                FutureAsset asset = futureAssetRepository.findByAsciiPtcodeAndSymbol(f.getPtcode14(), f.getSymbol()).orElse(null);
                if (asset == null || (asset.getQuantity() / instrument.getLot()) < Math.abs(f.getRequestedQuantiry())) {
                    result = false;
                    responseCode = 400;
                    message = message.concat(String.format(" , دارایی کد %s برای ترخیص کافی نیست ", f.getPtcode14()));
                }
                if (asset != null && !DateTimeService.fromGregoriantoJalali(new Date()).equals(asset.getDate())) {
                    result = false;
                    responseCode = 400;
                    message = message.concat(String.format(" , دارایی کد %s به روز رسانی نشده است. ", f.getPtcode14()));
                }
            }

            FutureDelivery futureDelivery = new FutureDelivery();
            futureDelivery.setPtcode14(f.getPtcode14());
            futureDelivery.setSymbol(f.getSymbol());
            futureDelivery.setRequestedQuantiry(f.getRequestedQuantiry());
            futureDelivery.setBrokerCode(f.getBrokerCode());
            futureDelivery.setTransactionTime(LocalDate.now());
            futureDelivery.setOcost(null);
            futureDelivery.setDeliveredQuantity(null);

            futureDeliveryList.add(futureDelivery);
        }

        if (!result) {
            jobj.put("success", result);
            jobj.put("message", message);
            jobj.put("responseCode", responseCode);
            return jobj.toString();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////insert into futureDelivery /////////////////////////////////////////////////

        boolean insertResult = futureDeliveryService.insertNewRecords(futureDeliveryList);
        if (insertResult) {
            jobj.put("success", true);
            jobj.put("message", "ارسال رکوردها با موفقیت صورت گرفت");
            jobj.put("responseCode", 200);
            // LocalDate date = LocalDate.now();
            // List<FutureDelivery> insertedRecords = futureDeliveryService.getDeliveryOnDate(date);
            // jobj.put("data",insertedRecords);

        } else {
            jobj.put("success", false);
            jobj.put("responseCode", 402);
            jobj.put("message", "مشکلی در ثبت رکوردها در سمت سرور پیش آمده است. لطفا با ادمین سیستم تماس بگیرید");
        }

        return jobj.toString();
    }

    //todo test
    //todo swagger
    @GetMapping("/api/getFutureDelivery")
    @ResponseBody
    public String getFutureDelivery() throws JSONException {
        LocalDate date = LocalDate.now();
        JSONObject jsonObject = new JSONObject();
        try {
            List<FutureDeliveryDto> futureDeliveryList = futureDeliveryService.getAllDeliveryOnDate(date);
            jsonObject.put("success", true);
            jsonObject.put("message", "");
            jsonObject.put("data", futureDeliveryList);
        } catch (Exception e) {
            jsonObject.put("success", false);
            jsonObject.put("message", e.getMessage());
        }
        return jsonObject.toString();
    }
}
