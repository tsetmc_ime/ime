package com.tsetmc.ime400.controllers;


import com.tsetmc.ime400.dto.CsvColumnsConfig;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.services.FutureDeliveryService;
import com.tsetmc.ime400.util.JsonService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/futures")
public class FutureDeliveryController {

    @Autowired
    FutureDeliveryService futureDeliveryService;

    @GetMapping("")
    public String indexPage() {
        return "/futures/index";
    }

    @GetMapping(value = "/index")
    @ResponseBody
    public String getFutureDeliveryList(@RequestParam(defaultValue = "") String search,
                                        @RequestParam(required = false) String date,
                                        @RequestParam(defaultValue = "0") int start,
                                        @RequestParam(defaultValue = "10") int length,
                                        @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                        @RequestParam(defaultValue = "desc") String orderDir,
                                        Authentication authentication) {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        return futureDeliveryService.getFutureDeliveryList(search, date, storageId, start, length, orderKey, orderDir);
    }

    @GetMapping(value = "/export-csv")
    public void exportOutAssetList(@RequestParam(defaultValue = "") String search,
                                   @RequestParam(required = false) String date,
                                   @RequestParam(defaultValue = "DT_RowId") String orderKey,
                                   @RequestParam(defaultValue = "desc") String orderDir,
                                   Authentication authentication,
                                   HttpServletResponse response) throws IOException, SQLException, JSONException {
        Integer storageId = null;
        User user = ((User) authentication.getPrincipal());
        if (Arrays.asList("USER_LEVEL_1", "USER_LEVEL_2").contains(user.getRole().getName()))
            storageId = user.getStorage().getId();

        response.setContentType("application/csv, application/octet-stream");
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_16));
        response.setHeader("Content-Disposition", "attachment; filename=futures.csv");

        List<CsvColumnsConfig> columns = new ArrayList<>();
        columns.add(new CsvColumnsConfig(1, "ردیف", false));
        columns.add(new CsvColumnsConfig(2, "نماد", false));
        columns.add(new CsvColumnsConfig(3, "انبار", false));
        columns.add(new CsvColumnsConfig(4, "کد بورسی", false));
        columns.add(new CsvColumnsConfig(5, "کد ۱۴ رقمی", false));
        columns.add(new CsvColumnsConfig(6, "کد ملی", false));
        columns.add(new CsvColumnsConfig(7, "نام", false));
        columns.add(new CsvColumnsConfig(8, "نام خانوادگی", false));
        columns.add(new CsvColumnsConfig(9, "کارگزار", false));
        columns.add(new CsvColumnsConfig(10, "مقدار تقاضا", false));
        columns.add(new CsvColumnsConfig(11, "مقدار تحویل شده", false));
        columns.add(new CsvColumnsConfig(12, "هزینه انبار (ریال)", false));
        columns.add(new CsvColumnsConfig(13, "زمان", true));

        futureDeliveryService.getFutureDeliveryList(search, date, storageId, orderKey, orderDir, JsonService.getCsvWriter_WhitColumns(response, columns));
    }

}
