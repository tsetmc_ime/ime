package com.tsetmc.ime400;

import com.tsetmc.ime400.util.CustomAuditAware;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class MyConfiguration {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }

    @Autowired
    Environment env;

    @Bean
    @Primary
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create()
            .driverClassName(env.getProperty("spring.datasource.driver-class-name")) // https://stackoverflow.com/a/36352649/1994239
            .url(env.getProperty("spring.datasource.url"))
            .username(env.getProperty("spring.datasource.username"))
            .password(env.getProperty("spring.datasource.password"))
            .build();
    }


    // https://stackoverflow.com/a/48140893/1994239
    @Bean(name = "as400-datasource")
    public DataSource db2() {
        // https://stackoverflow.com/a/48140893/1994239
        return DataSourceBuilder.create()
            .driverClassName(env.getProperty("db2.datasource.driver-class-name"))
            .url(env.getProperty("db2.datasource.url"))
            .username(env.getProperty("db2.datasource.username"))
            .password(env.getProperty("db2.datasource.password"))
            .build();
    }

    // https://stackoverflow.com/a/59953650/1994239
    @Bean
    public AuditorAware<String> auditorAware(){
        return new CustomAuditAware();
    }

}
