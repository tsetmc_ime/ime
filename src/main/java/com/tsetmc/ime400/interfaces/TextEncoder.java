package com.tsetmc.ime400.interfaces;

public interface TextEncoder {

    String toUnicode(String ebcdic);
    String toEbcdic(String persian);
    String toCp437(String persian);

}
