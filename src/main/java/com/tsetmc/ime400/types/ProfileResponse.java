package com.tsetmc.ime400.types;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProfileResponse {
    String storageName;
    String fullName;
    String userName;
    String roleName;
    String email;
    String phone;
    String instrumentName;
    LocalDate dueDate;
}
