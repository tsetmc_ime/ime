package com.tsetmc.ime400.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConvertedTempAsset {
    int id;
    LocalDateTime convertDate;
    LocalDateTime printDate;
    LocalDateTime entranceDate;
    String storageName;
    String storageAddress;
    String symbolDescription;
    int quantity;
    String weightUnit;
    LocalDate dueDate;
    String investorName;
    String nationalCode;
    String ptCode;
    String ptCode14;
    String userName;
    String investorAddress;
}
