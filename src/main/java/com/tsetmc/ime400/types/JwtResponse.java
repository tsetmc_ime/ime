package com.tsetmc.ime400.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Date;

@Data
@AllArgsConstructor
public class JwtResponse {
    int id;
    String username;
    String fullname;
    String roleName;
    String type = "Bearer";
    String token;

}
