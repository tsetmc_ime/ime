package com.tsetmc.ime400.types;

import lombok.Data;

@Data
public class FutureDeliveryRequest {
    private String ptcode14;
    private String symbol;
    private Integer requestedQuantiry;
    private String brokerCode;
}
