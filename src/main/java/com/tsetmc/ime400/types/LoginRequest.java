package com.tsetmc.ime400.types;

import lombok.Data;
import org.springframework.context.annotation.Bean;

@Data
public class LoginRequest {
    String username;
    String password;
}
