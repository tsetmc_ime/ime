package com.tsetmc.ime400.runners;

import com.tsetmc.ime400.repositories.AssetRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    AssetRequestRepository assetRepository;

    @Override
    public void run(String... strings) {
        // this.assetRepository.save(new AssetRequest() );
    }

}