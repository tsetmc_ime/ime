package com.tsetmc.ime400.dto;

import com.tsetmc.ime400.models.ClearanceRequest;
import com.tsetmc.ime400.models.Investor;
import com.tsetmc.ime400.models.Storage;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
@Data
public class OutAssetTotal {

    int id;
    int quantity;
    LocalDateTime clearanceDate;
    LocalDateTime outDate;
    Long cost;
    Long ocost;
    Long tcost;
    Long vat;
    Investor investor;
    Storage storage;

}
