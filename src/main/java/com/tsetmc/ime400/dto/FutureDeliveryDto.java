package com.tsetmc.ime400.dto;

import com.tsetmc.ime400.models.ClearanceRequest;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

public class FutureDeliveryDto{
    public FutureDeliveryDto(String ptcode14, String ptcode, String symbol, Integer requestedQuantiry, String brokerCode, LocalDate transactionTime, Long ocost, Integer deliveredQuantity) {
        this.ptcode14 = ptcode14;
        this.ptcode = ptcode;
        this.symbol = symbol;
        this.requestedQuantiry = requestedQuantiry;
        this.brokerCode = brokerCode;
        this.transactionTime = transactionTime;
        this.ocost = ocost;
        this.deliveredQuantity = deliveredQuantity;
    }

    String ptcode14;
    String ptcode;
    String symbol;
    Integer requestedQuantiry;
    String brokerCode;
    LocalDate transactionTime;
    Long ocost;
    Integer deliveredQuantity;

    public String getPtcode14() {
        return ptcode14;
    }

    public void setPtcode14(String ptcode14) {
        this.ptcode14 = ptcode14;
    }

    public String getPtcode() {
        return ptcode;
    }

    public void setPtcode(String ptcode) {
        this.ptcode = ptcode;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getRequestedQuantiry() {
        return requestedQuantiry;
    }

    public void setRequestedQuantiry(Integer requestedQuantiry) {
        this.requestedQuantiry = requestedQuantiry;
    }

    public String getBrokerCode() {
        return brokerCode;
    }

    public void setBrokerCode(String brokerCode) {
        this.brokerCode = brokerCode;
    }

    public LocalDate getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(LocalDate transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Long getOcost() {
        return ocost;
    }

    public void setOcost(Long ocost) {
        this.ocost = ocost;
    }

    public Integer getDeliveredQuantity() {
        return deliveredQuantity;
    }

    public void setDeliveredQuantity(Integer deliveredQuantity) {
        this.deliveredQuantity = deliveredQuantity;
    }

    @Override
    public String toString() {
        return "{" +
                "ptcode14='" + ptcode14 + '\'' +
                ", ptcode='" + ptcode + '\'' +
                ", symbol='" + symbol + '\'' +
                ", requestedQuantiry=" + requestedQuantiry +
                ", brokerCode='" + brokerCode + '\'' +
                ", transactionTime=" + transactionTime +
                ", ocost=" + ocost +
                ", deliveredQuantity=" + deliveredQuantity +
                '}';
    }
}
