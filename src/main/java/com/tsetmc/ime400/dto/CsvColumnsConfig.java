package com.tsetmc.ime400.dto;

public class CsvColumnsConfig {
    private int order;
    private String keyName;
    private boolean isDate;

    public CsvColumnsConfig(){

    }
    public CsvColumnsConfig(int order,String keyName, boolean isDate) {
        this.order = order;
        this.keyName = keyName;
        this.isDate = isDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public boolean isDate() {
        return isDate;
    }

    public void setDate(boolean date) {
        isDate = date;
    }
}
