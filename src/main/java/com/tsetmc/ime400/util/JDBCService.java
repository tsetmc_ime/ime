package com.tsetmc.ime400.util;

import org.springframework.stereotype.Component;

import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class JDBCService {

    public static void execModifyingQuery(DataSource dataSource, String query) throws SQLException {
        LoggerFactory.getLogger(JDBCService.class).info(query);
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        }
    }

}
