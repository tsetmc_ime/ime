package com.tsetmc.ime400.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueStorageNameValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueStorageNameConstraint {
    String message() default "duplicate storage name";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}