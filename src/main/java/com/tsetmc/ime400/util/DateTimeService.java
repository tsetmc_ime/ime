package com.tsetmc.ime400.util;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.GregorianCalendar;
import com.ibm.icu.util.TimeZone;
import com.ibm.icu.util.ULocale;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
public class DateTimeService {

    //   http://linux-geek.blog.ir/1398/11/21/%D8%AA%D8%A8%D8%AF%DB%8C%D9%84-%D8%AA%D8%A7%D8%B1%DB%8C%D8%AE-%D8%AF%D8%B1-%D8%AC%D8%A7%D9%88%D8%A7-%D8%A8%D8%A7-%DA%A9%D8%AA%D8%A7%D8%A8%D8%AE%D8%A7%D9%86%D9%87-icu4j-%D8%B4%D8%B1%DA%A9%D8%AA-IBM


    public static String fromGregoriantoJalali(Date date) {
        ULocale PERSIAN_LOCALE = new ULocale("en@calendar=persian");
        ULocale GREGORIAN_LOCALE = new ULocale("en@calendar=gregorian");
        ZoneId IRAN_ZONE_ID = ZoneId.of("Asia/Tehran");

        Calendar gregorianCalendar = Calendar.getInstance(GREGORIAN_LOCALE);
        gregorianCalendar.setLenient(false);
        gregorianCalendar.clear();
        gregorianCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        gregorianCalendar.setTime(date);
        SimpleDateFormat df1 = new SimpleDateFormat("yyyyMMdd", PERSIAN_LOCALE);
        return df1.format(gregorianCalendar.getTime());
        //return JalaliCalendar.MiladiToShamsi(date, JalaliCalendar.DateFormat.WithoutDelimeter);

    }
    public static String fromGregoriantoJalali(String dateStr) {
        try {
            if(dateStr.length() >10) {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
                return JalaliCalendar.MiladiToShamsi(date, JalaliCalendar.DateFormat.Simple);
            }
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
            return JalaliCalendar.MiladiToShamsi(date, JalaliCalendar.DateFormat.Default);
        }
        catch (Exception pEx){
            return "";
        }
    }
    public static String getTime(Date date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmmss");
        String time = date.toInstant().atZone(ZoneId.of("Asia/Tehran")).toLocalTime().format(dtf);
        return time;
    }

    public static String getDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String format = formatter.format(date);
      return format;
    }

//    public static Date fromJalaliToGregorian(int year, int month, int day) {
//        ULocale PERSIAN_LOCALE = new ULocale("en@calendar=persian");
//        ULocale GREGORIAN_LOCALE = new ULocale("en@calendar=gregorian");
//        ZoneId IRAN_ZONE_ID = ZoneId.of("Asia/Tehran");
//
//        Calendar persianCalendar = Calendar.getInstance(PERSIAN_LOCALE);
//        persianCalendar.clear();
//        persianCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
//
//        persianCalendar.set(year, month, day);
//        // full date
//        return persianCalendar.getTime();
//    }

    public static Date  fromJalaliToGregorian(String persianDate)
    {
        int year = Integer.parseInt(persianDate.substring(0,4));
        int month = Integer.parseInt(persianDate.substring(4,6));
        int day = Integer.parseInt(persianDate.substring(6,8));
        return JalaliCalendar.ShamsiToMiladi(year+"/"+month+"/"+day);

    }
    public static Date fromJalaliToGregorian(int year, int month, int day,int hour,int minute,int second) {
        ULocale PERSIAN_LOCALE = new ULocale("en@calendar=persian");
        ULocale GREGORIAN_LOCALE = new ULocale("en@calendar=gregorian");
        ZoneId IRAN_ZONE_ID = ZoneId.of("Asia/Tehran");

        Calendar persianCalendar = Calendar.getInstance(PERSIAN_LOCALE);
        persianCalendar.clear();
        persianCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));

        persianCalendar.set(year, month, day);
        persianCalendar.set(Calendar.HOUR, hour);
        persianCalendar.set(Calendar.MINUTE, minute);
        persianCalendar.set(Calendar.SECOND,second);
        // full date
        return persianCalendar.getTime();
    }

}
