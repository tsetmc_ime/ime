package com.tsetmc.ime400.util;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import org.httprpc.sql.Parameters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.List;

@Component
public class JsonService {

    public static JSONArray createListFromSQL(DataSource dataSource, String query) throws SQLException, JSONException {
        try (Connection connection = dataSource.getConnection()) {
            Parameters params = Parameters.parse(query);

            PreparedStatement statement = connection.prepareStatement(params.getSQL());
            params.apply(statement);
            ResultSet resultSet = statement.executeQuery();

            ResultSetMetaData metadata = resultSet.getMetaData();
            int numberOfColumns = metadata.getColumnCount();

            JSONArray jArray = new JSONArray();
            while (resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 0; i < numberOfColumns; i++) {
                    final String columnName = metadata.getColumnLabel(i + 1);
                    final String value = resultSet.getString(i + 1);
                    jsonObject.put(columnName, value == null ? JSONObject.NULL : value);
                }
                jArray.put(jsonObject);
            }

            return jArray;
        }
    }

    public interface ResultSetIterator {
        void nextRecord(JSONObject obj) throws IOException, JSONException;

        void close() throws IOException;
    }
    public static JsonService.ResultSetIterator getCsvWriter_WhitColumns(HttpServletResponse response, List<CsvColumnsConfig> columns) throws IOException {
        return new JsonService.ResultSetIterator() {
            CsvWriter csvWriter = new CsvWriter(response.getOutputStream(), columns);

            @Override
            public void nextRecord(JSONObject obj) throws IOException {
                csvWriter.writeRecord(obj);
            }

            @Override
            public void close() throws IOException {
                csvWriter.close();
            }
        };
    }

    public static JsonService.ResultSetIterator getCsvWriter(HttpServletResponse response) throws IOException {
        return new JsonService.ResultSetIterator() {
            CsvWriter csvWriter = new CsvWriter(response.getOutputStream());

            @Override
            public void nextRecord(JSONObject obj) throws IOException {
                csvWriter.writeRecord(obj);
            }

            @Override
            public void close() throws IOException {
                csvWriter.close();
            }
        };
    }

    public static void iterateOverSQL(DataSource dataSource, String query, ResultSetIterator iter) throws SQLException, JSONException, IOException {
        try (Connection connection = dataSource.getConnection()) {
            Parameters params = Parameters.parse(query);

            PreparedStatement statement = connection.prepareStatement(params.getSQL());
            params.apply(statement);
            ResultSet resultSet = statement.executeQuery();

            ResultSetMetaData metadata = resultSet.getMetaData();
            int numberOfColumns = metadata.getColumnCount();

            while (resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 0; i < numberOfColumns; i++) {
                    final String columnName = metadata.getColumnLabel(i + 1);
                    final String value = resultSet.getString(i + 1);
                    jsonObject.put(columnName, value == null ? JSONObject.NULL : value);
                }
                iter.nextRecord(jsonObject);
            }

        } finally {
            iter.close();
        }
    }

    public static JSONObject createObjectFromSQL(DataSource dataSource, String query) throws SQLException, JSONException {
        try (Connection connection = dataSource.getConnection()) {
            Parameters params = Parameters.parse(query);

            PreparedStatement statement = connection.prepareStatement(params.getSQL());
            params.apply(statement);
            ResultSet resultSet = statement.executeQuery();

            ResultSetMetaData metadata = resultSet.getMetaData();
            int numberOfColumns = metadata.getColumnCount();

            JSONObject jsonObject = new JSONObject();
            if (resultSet.next()) {
                for (int i = 0; i < numberOfColumns; i++) {
                    final String columnName = metadata.getColumnName(i + 1);
                    final String value = resultSet.getString(i + 1);
                    jsonObject.put(columnName, value != null ? value : JSONObject.NULL);
                }
            }

            return jsonObject;
        }
    }

}
