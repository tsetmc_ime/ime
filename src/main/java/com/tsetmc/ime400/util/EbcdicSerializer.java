package com.tsetmc.ime400.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tsetmc.ime400.interfaces.TextEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EbcdicSerializer extends StdSerializer<String> {

    static TextEncoder textEncoder;

    // https://stackoverflow.com/a/5991240/1994239
    @Autowired
    public void setTextEncoder(TextEncoder textEncoder) {
        EbcdicSerializer.textEncoder = textEncoder;
    }

    public EbcdicSerializer() {
        this(null);
    }

    public EbcdicSerializer(Class<String> t) {
        super(t);
    }

    @Override
    public void serialize(String str, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
	/* if (str.contains("966657")) {
		System.out.println("**** " + str);
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);        
			System.out.print(" " + (int) c);
		}
		System.out.println("");
	} */
        jsonGenerator.writeString(textEncoder.toUnicode(str));
    }

}
