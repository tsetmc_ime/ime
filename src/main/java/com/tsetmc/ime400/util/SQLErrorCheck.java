package com.tsetmc.ime400.util;

public class SQLErrorCheck {

    // https://stackoverflow.com/a/4546473/1994239
    public static Throwable getRootCause(Throwable ex) {
        while (true) {
            Throwable cause = ex.getCause();
            if (cause == null) return ex;
            ex = cause;
        }
    }

}
