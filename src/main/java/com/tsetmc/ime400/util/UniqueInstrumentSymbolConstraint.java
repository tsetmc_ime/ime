package com.tsetmc.ime400.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueInstrumentSymbolValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueInstrumentSymbolConstraint {
    String message() default "duplicate symbol";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}