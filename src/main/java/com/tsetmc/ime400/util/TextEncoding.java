package com.tsetmc.ime400.util;

import com.tsetmc.jni.Text400;
import com.tsetmc.ime400.interfaces.TextEncoder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Profile("!prod & !no-ebcdic-codec")
class TextEncoding implements TextEncoder {

    public TextEncoding() {
        setupCache();
    }

    final static Map<Character, Character> ebcdic2unicode = Stream.of(new Object[][]{
            {'å', 'ﺍ'}, {'ç', 'ﺎ'}, {'ï', 'ﺏ'}, {'ì', 'ﺑ'}, {'Ä', 'ﺕ'}, {'À', 'ﺗ'}, {'Á', 'ﺙ'}, {'Ã', 'ﺛ'}, {'Å', 'ﺝ'}, {'Ç', 'ﺟ'}, {'É', 'ﺡ'}, {'Ê', 'ﺣ'}, {'Ë', 'ﺥ'}, {'È', 'ﺧ'}, {'Í', 'ﺩ'}, {'Î', 'ﺫ'}, {'Ï', 'ﺭ'}, {'Ì', 'ﺯ'}, {'ð', 'ﺱ'}, {'ý', 'ﺳ'}, {'þ', 'ﺵ'}, {'±', 'ﺷ'}, {'°', 'ﺹ'}, {'ª', 'ﺻ'}, {'º', 'ﺽ'}, {'æ', 'ﺿ'}, {'¸', 'ﻁ'}, {'Æ', 'ﻃ'}, {'¤', 'ﻅ'}, {'µ', 'ﻇ'}, {'¡', 'ﻉ'}, {'Ð', 'ﻋ'}, {'Ý', 'ﻌ'}, {'¿', 'ﻊ'}, {'Þ', 'ﻍ'}, {'^', 'ﻏ'}, {'£', 'ﻐ'}, {'®', 'ﻎ'}, {'¥', 'ﻑ'}, {'·', 'ﻓ'}, {'©', 'ﻕ'}, {'§', 'ﻗ'}, {'¯', 'ﻝ'}, {'¨', 'ﻟ'}, {'´', 'ﻡ'}, {'ô', 'ﻣ'}, {'ö', 'ﻥ'}, {'ò', 'ﻧ'}, {'õ', 'ﻩ'}, {'¹', 'ﻫ'}, {'û', 'ﻬ'}, {'ó', 'ﻭ'}, {'ß', 'ﭖ'}, {'Â', 'ﭘ'}, {'Ñ', 'ﭺ'}, {'ø', 'ﭼ'}, {'Ø', 'ﮊ'}, {'¶', 'ﮎ'}, {'¼', 'ﮐ'}, {'½', 'ﮒ'}, {'¾', 'ﮔ'}, {'ù', 'ﯼ'}, {'ÿ', 'ﯾ'}, {'ú', 'ﯽ'}, {'á', 'ﺂ'}, {'à', 'ﺁ'}, {'é', 'ء'}, {'î', 'ﺋ'}, {'í', 'ﺅ'}, {'ê', 'ﺃ'}, {'ë', 'ﺄ'}, {'÷', '؟'}, {'¢', '﷼'}, {'â', '،'}, {'²', 'ـ'}, {'ü', 'ﮤ'}, {'ñ', 'ﻻ'}, {'ã', 'ﻵ'}
    }).collect(Collectors.toMap(data -> (char) data[0], data -> (char) data[1]));

    final static Map<Character, Character> cp437 = Stream.of(new Object[][]{
            {'ì', 'ﺁ'}, {'Ä', 'ﺋ'}, {'É', 'ﺍ'}, {'æ', 'ﺎ'}, {'Æ', 'ﺏ'}, {'ô', 'ﺑ'}, {'ö', 'ﭖ'}, {'ò', 'ﭘ'}, {'û', 'ﺕ'}, {'ù', 'ﺗ'}, {'ÿ', 'ﺙ'}, {'Ö', 'ﺛ'}, {'Ü', 'ﺝ'}, {'¢', 'ﺟ'}, {'£', 'ﭺ'}, {'¥', 'ﭼ'}, {'₧', 'ﺡ'}, {'ƒ', 'ﺣ'}, {'á', 'ﺥ'}, {'í', 'ﺧ'}, {'ó', 'ﺩ'}, {'ú', 'ﺫ'}, {'ñ', 'ﺭ'}, {'Ñ', 'ﺯ'}, {'ª', 'ﮊ'}, {'º', 'ﺱ'}, {'¿', 'ﺳ'}, {'⌐', 'ﺵ'}, {'¬', 'ﺷ'}, {'½', 'ﺹ'}, {'¼', 'ﺻ'}, {'¡', 'ﺽ'}, {'«', 'ﺿ'}, {'»', 'ﻁ'}, {'α', 'ﻅ'}, {'ß', 'ﻉ'}, {'Γ', 'ﻊ'}, {'π', 'ﻌ'}, {'Σ', 'ﻋ'}, {'σ', 'ﻍ'}, {'µ', 'ﻎ'}, {'τ', 'ﻐ'}, {'Φ', 'ﻏ'}, {'Θ', 'ﻑ'}, {'Ω', 'ﻓ'}, {'δ', 'ﻕ'}, {'∞', 'ﻗ'}, {'φ', 'ﮎ'}, {'ε', 'ﮐ'}, {'∩', 'ﮒ'}, {'≡', 'ﮔ'}, {'±', 'ﻝ'}, {'≥', 'ﻻ'}, {'≤', 'ﻟ'}, {'⌠', 'ﻡ'}, {'⌡', 'ﻣ'}, {'÷', 'ﻥ'}, {'≈', 'ﻧ'}, {'°', 'ﻭ'}, {'∙', 'ﻩ'}, {'·', 'ﻬ'}, {'√', 'ﻫ'}, {'ⁿ', 'ﯽ'}, {'²', 'ﯼ'}, {'■', 'ﯾ'}, {'è', '،'}, {'Å', 'ء'}, { 'ï', 'ـ' }
    }).collect(Collectors.toMap(data -> (char) data[0], data -> (char) data[1]));

    static Map<Character, Character> unicode2cp437 = cp437.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    static Map<Character, Character> unicode2ebcdic = ebcdic2unicode.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));

    static final int NORMAL = 0, INITIAL = 1, MEDIAL = 2, FINAL = 3, ISOLATED = 4;

    static class PersianCharset {
        char[] forms;

        PersianCharset(char normal, char initial, char medial, char final_, char isolated) {
            forms = new char[]{normal, initial, medial, final_, isolated};
        }

        boolean hasForm(int idx) {
            return forms[idx] != 0;
        }
    }

    static final Set<PersianCharset> persianCharacters = new HashSet<>(Arrays.asList(
            new PersianCharset('ا', '\0', '\0', 'ﺎ', 'ﺍ'), //
            new PersianCharset('آ', '\0', '\0', 'ﺂ', 'ﺁ'), //
            new PersianCharset('أ', '\0', '\0', 'ﺄ', 'ﺃ'), //
            new PersianCharset('إ', '\0', '\0', 'ﺈ', 'ﺇ'), //
            new PersianCharset('ب', 'ﺑ', 'ﺒ', 'ﺐ', 'ﺏ'), //
            new PersianCharset('پ', 'ﭘ', 'ﭙ', 'ﭗ', 'ﭖ'), //
            new PersianCharset('ت', 'ﺗ', 'ﺘ', 'ﺖ', 'ﺕ'), //
            new PersianCharset('ث', 'ﺛ', 'ﺜ', 'ﺚ', 'ﺙ'), //
            new PersianCharset('ح', 'ﺣ', 'ﺤ', 'ﺢ', 'ﺡ'), //
            new PersianCharset('خ', 'ﺧ', 'ﺨ', 'ﺦ', 'ﺥ'), //
            new PersianCharset('ج', 'ﺟ', 'ﺠ', 'ﺞ', 'ﺝ'), //
            new PersianCharset('چ', 'ﭼ', 'ﭽ', 'ﭻ', 'ﭺ'), //
            new PersianCharset('د', '\0', '\0', 'ﺪ', 'ﺩ'), //
            new PersianCharset('ذ', '\0', '\0', 'ﺬ', 'ﺫ'), //
            new PersianCharset('ر', '\0', '\0', 'ﺮ', 'ﺭ'), //
            new PersianCharset('ز', '\0', '\0', 'ﺰ', 'ﺯ'), //
            new PersianCharset('ژ', '\0', '\0', 'ﮋ', 'ﮊ'), //
            new PersianCharset('س', 'ﺳ', 'ﺴ', 'ﺲ', 'ﺱ'), //
            new PersianCharset('ش', 'ﺷ', 'ﺸ', 'ﺶ', 'ﺵ'), //
            new PersianCharset('ص', 'ﺻ', 'ﺼ', 'ﺺ', 'ﺹ'), //
            new PersianCharset('ض', 'ﺿ', 'ﻀ', 'ﺾ', 'ﺽ'), //
            new PersianCharset('ط', 'ﻃ', 'ﻄ', 'ﻂ', 'ﻁ'), //
            new PersianCharset('ظ', 'ﻇ', 'ﻈ', 'ﻆ', 'ﻅ'), //
            new PersianCharset('ع', 'ﻋ', 'ﻌ', 'ﻊ', 'ﻉ'), //
            new PersianCharset('غ', 'ﻏ', 'ﻐ', 'ﻎ', 'ﻍ'), //
            new PersianCharset('ف', 'ﻓ', 'ﻔ', 'ﻒ', 'ﻑ'), //
            new PersianCharset('ق', 'ﻗ', 'ﻘ', 'ﻖ', 'ﻕ'), //
            new PersianCharset('ک', 'ﮐ', 'ﮑ', 'ﮏ', 'ﮎ'), //
            new PersianCharset('گ', 'ﮔ', 'ﮕ', 'ﮓ', 'ﮒ'), //
            new PersianCharset('ل', 'ﻟ', 'ﻠ', 'ﻞ', 'ﻝ'), //
            new PersianCharset('م', 'ﻣ', 'ﻤ', 'ﻢ', 'ﻡ'), //
            new PersianCharset('ن', 'ﻧ', 'ﻨ', 'ﻦ', 'ﻥ'), //
            new PersianCharset('و', '\0', '\0', 'ﻮ', 'ﻭ'), //
            new PersianCharset('ه', 'ﻫ', 'ﻬ', 'ﻪ', 'ﻩ'), //
            new PersianCharset('ی', 'ﯾ', 'ﯿ', 'ﯽ', 'ﯼ'), //
            new PersianCharset('ئ', 'ﺋ', 'ﺌ', 'ﺊ', 'ﺉ'), //
            new PersianCharset('ؤ', '\0', '\0', 'ﺆ', 'ﺅ'), //
            new PersianCharset('ۀ', '\0', '\0', 'ﮥ', 'ﮤ'), //
            new PersianCharset('ة', '\0', '\0', 'ﺔ', 'ﺓ'), //
            new PersianCharset('\0', '\0', '\0', 'ﻼ', 'ﻻ'), //
            new PersianCharset('\0', '\0', '\0', 'ﻶ', 'ﻵ'), //
            new PersianCharset('\0', '\0', '\0', 'ﻸ', 'ﻷ'), //
            new PersianCharset('\0', '\0', '\0', 'ﻺ', 'ﻹ'), //
            new PersianCharset('\0', '\0', '\0', '\0', 'ء'), //
            new PersianCharset('\0', '\0', '\0', '\0', '،'), //
            new PersianCharset('\0', '\0', '\0', '\0', 'ـ'), //
            new PersianCharset('\0', '\0', '\0', '\0', '؟'), //
            new PersianCharset('\0', '\0', '\0', '\0', '﷼')
    ));

    static class Char {
        char c;
        PersianCharset persianCharset;
        int idx;

        Char(char c) {
            this.c = c;
            this.idx = 0;
            this.persianCharset = null;
        }

        Char(char c, PersianCharset persianCharset, int idx) {
            this.c = c;
            this.persianCharset = persianCharset;
            this.idx = idx;
        }

        int form() {
            return idx;
        }

        char form(int idx) {
            return persianCharset.forms[idx];
        }

        boolean isPersian() {
            return persianCharacters.contains(persianCharset);
        }

        boolean hasForm(int idx) {
            return persianCharset.hasForm(idx);
        }
    }

    static Map<Character, Char> characterCache;

    public static void setupCache() {
        characterCache = new HashMap<>();
        for (PersianCharset charset : persianCharacters) {
            for (int idx = 0; idx < 5; idx++) {
                char c = charset.forms[idx];
                characterCache.put(c, new Char(c, charset, idx));
            }
        }
    }

    static Char makeCharFormEbcdic(char d) {
        Character c = ebcdic2unicode.get(d);
        if (c == null)
            return new Char(d);

        Char ch = characterCache.get(c);
        return ch != null ? ch : new Char(c);
    }

    static Char makeCharFormUnicode(char d) {
        Char c = characterCache.get(d);
        return c != null ? c : new Char(d);
    }

    static class CharacterFormatter {
        Char previous;
        Char current;
        Char next;

        CharacterFormatter(Char previous, Char current, Char next) {
            this.previous = previous;
            this.current = current;
            this.next = next;
        }

        String getUnicode() {
            String r = Character.toString(unicodeForm());

            if (makeJoiner())
                return makeJoined() ? (ZWJ + r + ZWJ) : (r + ZWJ);

            if (makeNonJoiner())
                return makeJoined() ? (ZWJ + r + ZWNJ) : (r + ZWNJ);

            return makeJoined() ? (ZWJ + r) : r;
        }

        final char ZWJ = 0x200D, ZWNJ = 0x200C;

        public boolean persian;

        char unicodeForm() {
            persian = current.isPersian();
            if (!persian)
                return current.c;

            char c = current.form(TextEncoding.NORMAL);
            if (c != 0)
                return c;

            return current.c;
        }

        boolean makeJoined() {
            if (!current.isPersian())
                return false;

            if (current.form() != MEDIAL && current.form() != FINAL)
                return false;

            if (!previous.isPersian())
                return true;

            if (previous.form() != INITIAL && previous.form() != MEDIAL)
                return true;

            return false;
        }

        boolean makeJoiner() {
            if (!current.isPersian())
                return false;

            if (current.form() != INITIAL && current.form() != MEDIAL)
                return false;

            if (!next.isPersian() || (!next.hasForm(MEDIAL) && !next.hasForm(FINAL))) {
                char c = current.form(TextEncoding.NORMAL);
                return c != 'ط' && c != 'ظ';
            }

            return false;
        }

        boolean makeNonJoiner() {
            if (!current.isPersian())
                return false;

            if (current.form() != FINAL && current.form() != ISOLATED)
                return false;

            if (!current.hasForm(INITIAL) && !current.hasForm(MEDIAL))
                return false;

            if (!next.isPersian())
                return false;

            return next.hasForm(MEDIAL) || next.hasForm(FINAL);
        }

        char getEbcdic() {
            if (isMedialOrFinal())
                return isInitialOrMedial() ? ebcdicForm(unicode2ebcdic, MEDIAL) : ebcdicForm(unicode2ebcdic, FINAL);
            else
                return isInitialOrMedial() ? ebcdicForm(unicode2ebcdic, INITIAL) : ebcdicForm(unicode2ebcdic, ISOLATED);
        }

        char getCp437() {
            if (isMedialOrFinal())
                return isInitialOrMedial() ? ebcdicForm(unicode2cp437, MEDIAL) : ebcdicForm(unicode2cp437, FINAL);
            else
                return isInitialOrMedial() ? ebcdicForm(unicode2cp437, INITIAL) : ebcdicForm(unicode2cp437, ISOLATED);
        }

        boolean isPrintable() {
            return current.c != ZWJ && current.c != ZWNJ;
        }

        char ebcdicForm(Map<Character, Character> unicode2xxx, int idx) {
            if (current.persianCharset != null) {
                Character r = unicode2xxx.get(current.form(idx));
                if (r == null && idx == MEDIAL)
                    r = unicode2xxx.get(current.form(INITIAL));
                if (r == null && idx == MEDIAL)
                    r = unicode2xxx.get(current.form(FINAL));
                if (r == null)
                    r = unicode2xxx.get(current.form(ISOLATED));
                if (r == null)
                    r = unicode2xxx.get(current.form(INITIAL));

                return r == null ? current.c : r;

            } else {
                Character d = unicode2xxx.get(current.c);
                return d != null ? d : current.c;
            }
        }

        boolean isInitialOrMedial() {
            return next.c != ZWNJ && (ZWJ == next.c || (next.persianCharset != null && (next.persianCharset.hasForm(MEDIAL) || next.persianCharset.hasForm(FINAL))));
        }

        boolean isMedialOrFinal() {
            return previous.c == ZWJ || (previous.persianCharset != null && (previous.persianCharset.hasForm(INITIAL) || previous.persianCharset.hasForm(MEDIAL)));
        }
    }

    static final Map<String, String> ebcdicReplacements = Stream.of(new Object[][]{
            {"ñ", "ç"}, {"ã", "á"}, {"\u0083" /* NBH */, " "}, {"\u0000", " "}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    static final Map<String, String> unicodeReplacements = Stream.of(new Object[][]{
            {"لا", "لﻻ"}, {"لآ", "لﻵ"}, {"ي", "ی"}, {"ك", "ک"}, {"۰", "0"}, {"۱", "1"}, {"۲", "2"}, {"۳", "3"}, {"۴", "4"}, {"۵", "5"}, {"۶", "6"}, {"۷", "7"}, {"۸", "8"}, {"۹", "9"}, {"٠", "0"}, {"١", "1"}, {"٢", "2"}, {"٣", "3"}, {"٤", "4"}, {"٥", "5"}, {"٦", "6"}, {"٧", "7"}, {"٨", "8"}, {"٩", "9"}, {"ـ", "\u200d"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    static final Map<String, String> unicodeCp437Replacements = Stream.of(new Object[][]{
            {"ؤ", "و"}, {"أ", "ا"}, {"إ", "ا"}, {"لا", "ﻻ"}, {"لآ", "ﻻ"}, {"ي", "ی"}, {"ك", "ک"}, {"۰", "0"}, {"۱", "1"}, {"۲", "2"}, {"۳", "3"}, {"۴", "4"}, {"۵", "5"}, {"۶", "6"}, {"۷", "7"}, {"۸", "8"}, {"۹", "9"}, {"٠", "0"}, {"١", "1"}, {"٢", "2"}, {"٣", "3"}, {"٤", "4"}, {"٥", "5"}, {"٦", "6"}, {"٧", "7"}, {"٨", "8"}, {"٩", "9"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    @Override
    public String toUnicode(String wide) {
        if (wide == null)
            return null;

        for (String r : ebcdicReplacements.keySet())
            wide = wide.replaceAll(r, ebcdicReplacements.get(r));

        StringBuilder sb = new StringBuilder();

        wide = wide + ' ';
        Char previous, current = makeCharFormEbcdic(' '), next = makeCharFormEbcdic(wide.charAt(0));
        boolean rtl = false;

        for (int i = 1; i < wide.length(); i++) {
            previous = current;
            current = next;
            next = makeCharFormEbcdic(wide.charAt(i));

            CharacterFormatter f = new CharacterFormatter(previous, current, next);
            sb.append(f.getUnicode());

            rtl |= f.persian;
        }

        final String str = sb.toString();

        if (rtl) {
            StringBuilder sb2 = new StringBuilder(str.length());
            for (int i = 0; i < str.length(); i++) {
                final char c = str.charAt(i);
                sb2.append(c == '(' ? ')' : (c == ')' ? '(' : c));
            }
            return sb2.toString();

        } else
            return str;
    }

    @Override
    public String toEbcdic(String wide) {
        if (wide == null)
            return null;

        for (String r : unicodeReplacements.keySet())
            wide = wide.replaceAll(r, unicodeReplacements.get(r));

        StringBuilder sb = new StringBuilder();

        wide = wide + ' ';
        Char previous, current = makeCharFormUnicode(' '), next = makeCharFormUnicode(wide.charAt(0));

        for (int i = 1; i < wide.length(); i++) {
            previous = current;
            current = next;
            next = makeCharFormUnicode(wide.charAt(i));

            CharacterFormatter f = new CharacterFormatter(previous, current, next);
            if (f.isPrintable())
                sb.append(f.getEbcdic());
        }

        return sb.toString();
    }

    @Override
    public String toCp437(String wide) {
        if (wide == null)
            return null;

        for (String r : unicodeCp437Replacements.keySet())
            wide = wide.replaceAll(r, unicodeCp437Replacements.get(r));

        wide = new StringBuilder(wide).reverse().toString();

        StringBuilder sb = new StringBuilder();
        boolean prepend = false;

        wide = wide + ' ';
        Char previous, current = makeCharFormUnicode(' '), next = makeCharFormUnicode(wide.charAt(0));

        char d = wide.charAt(0);
        for (int i = 1; i < wide.length(); i++) {
            char c = wide.charAt(i);

            previous = current;
            current = next;
            next = makeCharFormUnicode(c);

            if (d != ' ')
                prepend = current.isPersian();

            CharacterFormatter f = new CharacterFormatter(next, current, previous);
            if (f.isPrintable()) {
                if (prepend)
                    sb.append(f.getCp437());
                else
                    sb.insert(0, current.c);
            }

            d = c;
        }

        return sb.toString();
    }

}

@Component
@Profile("no-ebcdic-codec")
class PassiveTextEncoding implements TextEncoder {

    @Override
    public String toUnicode(String ebcdic) {
        return ebcdic;
    }

    @Override
    public String toEbcdic(String wide) {
        return wide;
    }

    @Override
    public String toCp437(String persian) {
        return persian;
    }

}

@Component
@Profile("prod & !no-ebcdic-codec")
class NativeTextEncoding implements TextEncoder {

    NativeTextEncoding() {
        Text400.init();
    }

    @Override
    public String toUnicode(String ebcdic) {
        return Text400.ebcdicToUnicode(ebcdic);
    }

    @Override
    public String toEbcdic(String wide) {
        return Text400.unicodeToEbcdic(wide);
    }

    @Override
    public String toCp437(String persian) {
        return Text400.unicodeToCp437(persian);
    }

}
