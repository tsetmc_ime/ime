package com.tsetmc.ime400.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PersianDateValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PersianDateConstraint {
    String message() default "Invalid persian date";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}