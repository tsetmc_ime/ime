package com.tsetmc.ime400.util;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

@Component
public class BindingErrorsCheck {

    public static Map<String, String> getErrors(Model model, Errors bindingErrors) {
        Map<String, String> errors = new HashMap<>();
        for (FieldError e : bindingErrors.getFieldErrors())
            errors.put(e.getField(), e.getDefaultMessage());

        if (errors.size() > 0)
            model.addAttribute("errors", errors);

        return errors;
    }

}
