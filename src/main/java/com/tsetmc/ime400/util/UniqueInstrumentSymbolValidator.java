package com.tsetmc.ime400.util;

import com.tsetmc.ime400.repositories.InstrumentRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

// https://stackoverflow.com/a/62650045/1994239

public class UniqueInstrumentSymbolValidator implements ConstraintValidator<UniqueInstrumentSymbolConstraint, String> {

    @Override
    public void initialize(UniqueInstrumentSymbolConstraint contactNumber) {
    }

    @Autowired
    InstrumentRepository instrumentRepository;

    @Override
    public boolean isValid(String symbol, ConstraintValidatorContext cxt) {
        return !instrumentRepository.existsBySymbol(symbol);
    }

}
