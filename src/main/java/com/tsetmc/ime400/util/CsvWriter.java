package com.tsetmc.ime400.util;

import com.tsetmc.ime400.dto.CsvColumnsConfig;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

public class CsvWriter {
    Boolean headerWritten = false;
    OutputStreamWriter writer;
    OutputStream stream;
    List<CsvColumnsConfig> columns;

    public CsvWriter(OutputStream stream) {
        this.stream = stream;
        this.writer = new OutputStreamWriter(stream, StandardCharsets.UTF_16LE);
    }
    public CsvWriter(OutputStream stream,List<CsvColumnsConfig> columns) {
        this.columns = columns;
        this.stream = stream;
        this.writer = new OutputStreamWriter(stream, StandardCharsets.UTF_16LE);
    }

    public void writeRecord(JSONObject obj) throws IOException {
        boolean hasColumns = (columns != null && columns.size()>0)?true: false;
        if (!headerWritten) {
            stream.write(new byte[]{(byte) 0xff, (byte) 0xfe}, 0, 2);
            if(hasColumns) {
                int i = 0;
                for(CsvColumnsConfig entry: columns){
                    if (i != 0)
                        writer.write('\t');
                    writer.write(entry.getKeyName());
                    i++;
                }

                writer.write("\r\n");
                headerWritten = true;
            }
            else {
                int i = 0;
                for (Iterator keys = obj.keys(); keys.hasNext(); i++) {
                    if (i != 0)
                        writer.write('\t');
                    writer.write((String) keys.next());
                }
                writer.write("\r\n");
                headerWritten = true;
            }
        }
        if(hasColumns) {
            int i = 0;
            for(CsvColumnsConfig entry: columns){
                if (i != 0)
                    writer.write('\t');
                String v = obj.optString(entry.getKeyName());
                if(entry.isDate()){
                    v = DateTimeService.fromGregoriantoJalali(v);
//                    v = DateTimeService.fromGregoriantoJalali(new Date());
                }
                writer.write(v.replace("\r", "  ").replace("\n", "  ").replace("\t", "  "));
                i++;
            }

            writer.write("\r\n");
        }
        else {
            int i = 0;
            for (Iterator keys = obj.keys(); keys.hasNext(); i++) {
                if (i != 0)
                    writer.write('\t');
                final String v = obj.optString((String) keys.next());
                writer.write(v.replace("\r", "  ").replace("\n", "  ").replace("\t", "  "));
            }
            writer.write("\r\n");
        }
    }

    public void close() throws IOException {
        writer.close();
    }

}
