package com.tsetmc.ime400.util;

import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.repositories.StorageRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

// https://stackoverflow.com/a/62650045/1994239

public class UniqueStorageNameValidator implements ConstraintValidator<UniqueStorageNameConstraint, Storage> {

    String message;

    @Override
    public void initialize(UniqueStorageNameConstraint constraint) {
        message = constraint.message();
    }

    @Autowired
    StorageRepository storageRepository;

    @Override
    public boolean isValid(Storage storage, ConstraintValidatorContext cxt) {
        final boolean valid = storageRepository.existsStorageLike(storage.getId(), storage.getName()) == 0;
        if (!valid) {
            cxt.disableDefaultConstraintViolation();
            cxt.buildConstraintViolationWithTemplate(message).addPropertyNode(Storage.Fields.name).addConstraintViolation();
        }
        return valid;
    }

}
