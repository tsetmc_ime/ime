package com.tsetmc.ime400.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PersianDateValidator implements ConstraintValidator<PersianDateConstraint, String> {

    @Override
    public void initialize(PersianDateConstraint contactNumber) {
    }

    @Override
    public boolean isValid(String persianDateField, ConstraintValidatorContext cxt) {
        return persianDateField != null && persianDateField.length() == 8;
    }

}
