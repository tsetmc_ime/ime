package com.tsetmc.ime400.aspects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Aspect
@Order(1)
@Component
// @ConditionalOnExpression("${endpoint.aspect.enabled:true}")
public class EndpointAspect {
    static Logger log = LoggerFactory.getLogger(EndpointAspect.class);

    @Before("within(com.tsetmc.ime400.controllers..*) && !within(com.tsetmc.ime400.controllers.CommonController)")
    public void endpointBefore(JoinPoint p) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        CodeSignature sig = (CodeSignature) p.getSignature();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null)
            ipAddress = request.getRemoteAddr();
        log.info(String.format("start %s %s.%s request from %s by %s", request.getMethod(), p.getTarget().getClass().getSimpleName(), sig.getName(), ipAddress, request.getRemoteUser()));
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = params.nextElement();
            log.info(String.format("  %s: %s", paramName, request.getParameter(paramName)));
        }
    }

    @AfterReturning(value = ("within(com.tsetmc.ime400.controllers..*) && !within(com.tsetmc.ime400.controllers.CommonController)"), returning = "returnValue")
    public void endpointAfterReturning(JoinPoint p, Object returnValue) {
        ObjectMapper mapper = new ObjectMapper();
        // mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            log.info(String.format("response object: %s", returnValue != null ? mapper.writeValueAsString(mapper.readValue(returnValue.toString(), Object.class)) : "null"));
        } catch (JsonProcessingException e) {
            log.info(String.format("response object: %s", returnValue.toString()));
        }
        log.info(String.format("end %s.%s", p.getTarget().getClass().getSimpleName(), p.getSignature().getName()));
    }

    @AfterThrowing(pointcut = ("within(com.tsetmc.ime400.controllers..*) && !within(com.tsetmc.ime400.controllers.CommonController)"), throwing = "e")
    public void endpointAfterThrowing(JoinPoint p, Exception e) {
        log.error(String.format("%s.%s thrown an exception: %s", p.getTarget().getClass().getSimpleName(), p.getSignature().getName(), e.getMessage()));
    }

}
