package com.tsetmc.ime400;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Ime400Application {

	public static void main(String[] args) {
		SpringApplication.run(Ime400Application.class, args);
	}

}
