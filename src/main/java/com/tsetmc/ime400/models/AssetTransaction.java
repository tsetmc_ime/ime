package com.tsetmc.ime400.models;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "asset_transaction", schema = "ime")
public class AssetTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    int id;

    @Basic
    @Column(name = "quantity", nullable = false)
    Integer quantity;

    @Column(name = "transaction_date", columnDefinition = "DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    Date transactionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "investor_id", referencedColumnName = "id", nullable = false)
    Investor investor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storage_id", referencedColumnName = "id", nullable = false)
    Storage storage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "asset_request_id", referencedColumnName = "id")
    AssetRequest assetRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clearance_request_id", referencedColumnName = "id")
    ClearanceRequest clearanceRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "asset_transfer_id", referencedColumnName = "id")
    AssetTransfer assetTransfer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trade_id", referencedColumnName = "id")
    Trade trade;

}
