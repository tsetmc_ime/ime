package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

// https://www.baeldung.com/hibernate-inheritance

@Data
@Entity
@Table(name = "job_status", schema = "ime")
public class JobStatus {

    @Id
    @Column
    String status;

    @Column
    String description;

}
