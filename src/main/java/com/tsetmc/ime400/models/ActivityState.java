package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "activity_state", schema = "ime")
public class ActivityState {

    @Id
    @Column(nullable = false)
    int id;

    @Column(name = "description", length = 50, unique = true)
    String desc;

}
