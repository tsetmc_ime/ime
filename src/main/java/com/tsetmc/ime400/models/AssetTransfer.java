package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Data
@Table(name = "asset_transfer", schema = "ime")
public class AssetTransfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @Basic
    @Column(name = "persian_register_date", length = 8)
    String persianRegisterDate;

    public void setpersianRegisterDate(String persianRegisterDate) {
        this.persianRegisterDate = persianRegisterDate;
        this.registerDate = DateTimeService.fromJalaliToGregorian(persianRegisterDate);
    }

    @Column(name = "register_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date registerDate;

    @Basic
    @Column(name = "symbol", nullable = false, length = 20)
    String symbol;

    @Basic
    @Column(name = "ptcode", nullable = false, length = 10)
    String ptcode;

    @Basic
    @Column(name = "quantity", nullable = false)
    int quantity;

    @Basic
    @Column(name = "is_applied", nullable = false)
    Byte isApplied;

    @Basic
    @Column(name = "ascii_ptcode", nullable = false, length = 14)
    String ascii_ptcode;

    @Basic
    @Column(name = "letter_number", length = 20)
    String letterNumber;

    @Basic
    @Column(name = "letter_date", nullable = false, length = 8)
    String letterDate;

    @Basic
    @Column(name = "transaction_date", nullable = false, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date transactionDate;

    @Column
    String isin; // کد ۱۲ رقمی نماد AIINIS

}
