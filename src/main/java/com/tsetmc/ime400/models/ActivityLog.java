package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "activity_log", schema = "ime")
public class ActivityLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;

    @Column
    String action;

    @Column(name = "action_date_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false)
    LocalDateTime actionDateTime;

    @Column(name = "old_values", length = 2048)
    String oldValues;

    @Column(name = "new_values", length = 2048)
    String newValues;

}
