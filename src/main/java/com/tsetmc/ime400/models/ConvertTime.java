package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "convert_time", schema = "ime")
public class ConvertTime {

    @Id
    @Column(nullable = false)
    int id;

    @Basic
    @Column(name = "description", length = 50, unique = true)
    String description;

}
