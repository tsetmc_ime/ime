package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "ptcode_inquiry", schema = "ime")
public class PTCodeInquiry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    int id;

    @Basic
    @Column(name = "request_num", length = 20)
    String requestNum;

    @Basic
    @Column(name = "investor_type", nullable = false)
    int investorType;

    @Basic
    @Column(name = "ptcode", nullable = false, length = 20)
    String ptcode;

    @Basic
    @Column(name = "national_id", nullable = false, length = 11)
    String nationalId;

    @Column(name = "request_date", nullable = false, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date requestDate;

    public void setRequestDate(Date date) {
        this.requestDate = date;
        this.persianRequestDate = DateTimeService.fromGregoriantoJalali(this.requestDate);
    }

    @Column(name = "persian_request_date")
    String persianRequestDate;

    @Basic
    @Column(name = "persian_request_time")
    String persianRequestTime;

    public void setPersianRequestTime() {
        this.persianRequestTime = DateTimeService.getTime(this.requestDate);
    }

    @Basic
    @Column(name = "res_status")
    int resStatus;

    @Basic
    @Column(name = "res_investor_type", nullable = true)
    Integer resInvestorType;

    @Basic
    @Column(name = "res_ascii_ptcode", nullable = true, length = 14)
    String resAsciiPtcode;

    @Basic
    @Column(name = "res_ptcode", nullable = true, length = 20)
    String resPtcode;

    @Basic
    @Column(name = "res_trading_id", nullable = true, length = 20)
    String resTradingId;

    @Basic
    @Column(name = "res_national_id", nullable = true, length = 10)
    String resNationalId;

    @Basic
    @Column(name = "res_fullname", nullable = true, length = 200)
    String resFullname;

    @Basic
    @Column(name = "res_issue_no", nullable = true, length = 20)
    String resIssueNo;

    @Basic
    @Column(name = "res_birth_date", nullable = true, length = 8)
    String resBirthDate;

    @Basic
    @Column(name = "res_father_name", nullable = true, length = 100)
    String resFatherName;

    @Basic
    @Column(name = "res_serie", nullable = true, length = 20)
    String resSerie;

    @Basic
    @Column(name = "res_issue_place", nullable = true, length = 45)
    String resIssuePlace;

    @Basic
    @Column(name = "res_activity_state", nullable = true)
    int resActivityState;

    @Basic
    @Column(name = "res_persian_date", nullable = true, length = 8)
    String resPersianDate;

    @Basic
    @Column(name = "res_persian_time", nullable = true, length = 8)
    String resPersianTime;

    @Column(name = "res_date", nullable = true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date resDate;

}
