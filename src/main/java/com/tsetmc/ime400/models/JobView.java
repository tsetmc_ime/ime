package com.tsetmc.ime400.models;

import lombok.Data;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@Entity
@Immutable
@Subselect("select id, time, name, name_fa, fire, status_fa, comments, enable from job_view")
public class JobView {

    @Column
    int id;

    @Id
    @Column
    String name;

    @Column(name = "name_fa")
    String title;

    @Column(name = "status_fa")
    String status;

    @Column
    String comments;

    @Column
    LocalTime time;

    @Column
    Boolean fire;

    @Column
    Boolean enable;

}
