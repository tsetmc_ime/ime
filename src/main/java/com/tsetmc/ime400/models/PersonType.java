package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "person_type", schema = "ime")
public class PersonType {

    @Id
    @Column(nullable = false)
    int id;

    @Basic
    @Column(name = "description", length = 20, unique = true)
    private String desc;

}
