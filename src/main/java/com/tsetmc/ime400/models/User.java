package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.PasswordConstraint;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;
import java.util.ArrayList;

@Data
@Entity
@FieldNameConstants
@Table(name = "user", schema = "ime")
@EntityListeners(AuditingEntityListener.class)
public class User extends LogFields implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @Column(unique = true, nullable = false, length = 20)
    @Length(min = 6, max = 24, message = "نام کاربری باید دارای حداقل ۶ و حداکثر ۲۴ حرف باشد")
    @Pattern(regexp = "^[A-Za-z][A-Za-z0-9_]*$", message = "نام کاربری می‌تواند متشکل از حروف انگلیسی و اعداد باشد")
    String username;

    @PasswordConstraint(message = "رمز عبور باید حداقل ۶ حرف و شامل حداقل یک حرف بزرگ و یک حرف کوچک از حروف انگلیسی و حداقل یک عدد باشد")
    @Column(nullable = false, length = 80)
    String password;

    @Transient
    private String passwordConfirmation;

    @Override
    public ArrayList<SimpleGrantedAuthority> getAuthorities() {
        return new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority(role.getName()));
        }};
    }

    @Column(length = 100)
    String fullname;

    @Column(length = 50)
    String email;

    @Column(length = 20)
    String phone;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Column(name = "is_enabled")
    boolean enabled = true;

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Column(name = "last_login_date")
    Timestamp lastLoginDate;

    @ManyToOne
    @JoinColumn(name = "storage_id", referencedColumnName = "id")
    Storage storage;

    @ManyToOne
    @JoinColumn(name = "role_id", updatable = false)
    UserRole role;

    public User() {
    }

}
