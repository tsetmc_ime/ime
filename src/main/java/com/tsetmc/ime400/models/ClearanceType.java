package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "clearance_type", schema = "ime")
public class ClearanceType {

    @Id
    @Column(nullable = false)
    int id;

    @Column(name = "description", nullable = false, length = 100, unique = true)
    String description;

}
