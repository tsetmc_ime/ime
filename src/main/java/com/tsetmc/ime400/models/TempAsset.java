package com.tsetmc.ime400.models;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "temp_asset", schema = "ime")
@EntityListeners(AuditingEntityListener.class)
public class TempAsset extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @NotNull(message = "مقدار کالا وارد نشده است")
    @Column(nullable = false)
    Integer quantity;

    @Column(length = 20)
    String barcode;

    // @NotNull(message = "تاریخ ورود به انبار را ثبت کنید")
    @Column(name = "entrance_date", columnDefinition = "TIMESTAMP", nullable = false)
    // @DateTimeFormat(pattern = "yyyyMMdd")
    LocalDateTime entranceDate = LocalDateTime.now();

    @Column(nullable = false)
    Byte converted = 0;

//    @Column(name = "convert_date")
//    // @DateTimeFormat(pattern = "yyyyMMdd")
//    LocalDate convertDate;


    @Column(name = "convert_date", columnDefinition = "TIMESTAMP")
    LocalDateTime convertDate;

    @Column(name = "register_date", columnDefinition = "TIMESTAMP")
    LocalDateTime registerDate;

    @Column(name = "confirm_date", columnDefinition = "TIMESTAMP")
    // @DateTimeFormat(pattern = "yyyyMMdd")
    LocalDateTime confirmDate;

    @NotNull
    @Column(name = "is_final_confirmed", nullable = false)
    Byte isFinalConfirmed = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "investor_id", referencedColumnName = "id")
    Investor investor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "storage_id", referencedColumnName = "id")
    // @NotNull(message = "نام انبار باید مقدار داشته باشد")
    Storage storage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "broker_id", referencedColumnName = "id")
    @NotNull(message = "کارگزار ناظر مشخص نشده است")
    Broker broker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "registrant_user", referencedColumnName = "id")
    User registrantUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "confirming_user", referencedColumnName = "id")
    User confirmingUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trade_id", referencedColumnName = "id")
    Trade trade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "future_delivery_id", referencedColumnName = "id")
    FutureDelivery futureDelivery;

}
