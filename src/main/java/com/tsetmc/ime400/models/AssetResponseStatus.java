package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "asset_response_status", schema = "ime")
public class AssetResponseStatus {

    @Id
    @Column(name = "id", nullable = false)
    int id;

    @Basic
    @Column(name = "description", length = 200)
    private String desc;

}
