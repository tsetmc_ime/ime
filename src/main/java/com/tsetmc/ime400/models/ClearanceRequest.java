package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@FieldNameConstants
@Table(name = "clearance_request", schema = "ime")
@EntityListeners(AuditingEntityListener.class)
public class ClearanceRequest extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Long id;

    @Column(name = "request_number", length = 20)
    String requestNumber;

    @ManyToOne
    @JoinColumn(name = "registrant_user", referencedColumnName = "id")
    User registrantUser;

    @Column(name = "register_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date registerDate;

    @ManyToOne
    @JoinColumn(name = "confirming_user", referencedColumnName = "id")
    User confirmingUser;

    @Column(name = "confirm_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date confirmDate;

    @Column(name = "is_final_confirmed")
    Byte isFinalConfirmed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "investor_id", referencedColumnName = "id")
    Investor investor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storage_id", referencedColumnName = "id")
    Storage storage;

    @Column
    @NotNull(message = "مقدار درخواستی برای ترخیص کالا مشخص نشده است")
    @Range(min = 1, message = "مقدار وارد شده باید عددی بزرگتر از صفر باشد")
    Integer quantity;

    @Column(name = "description", length = 500)
    String description;

    @Column(name = "request_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date requestDate;

    public void setRequestDate(Date reqDate) {
        this.requestDate = reqDate;
        this.persianRequestDate = DateTimeService.fromGregoriantoJalali(reqDate);
        this.requestTime = DateTimeService.getTime(reqDate);
    }

    @Column(name = "persian_request_date")
    String persianRequestDate;

    @Column(name = "request_time")
    String requestTime;

    @Column(name = "response_date")
    Timestamp responseDate;

    @Column(name = "persian_response_date")
    String persianResponseDate;

    @Column(name = "response_time")
    String responseTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "response_status", referencedColumnName = "id")
    ClearanceResponseStatus responseStatus;

    @Column(name = "storage_cost")
    Long storageCost;

    @Column(name = "ex_cost")
    Long exCost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trade_id", referencedColumnName = "id")
    Trade trade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "future_delivery_id", referencedColumnName = "id")
    FutureDelivery futureDelivery;

}
