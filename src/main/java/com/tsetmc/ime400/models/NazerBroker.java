package com.tsetmc.ime400.models;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "nazer_broker", schema = "ime", uniqueConstraints = {@UniqueConstraint(columnNames = {"symbol", "ascii_ptcode", "broker_code"})})
public class NazerBroker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @Column(nullable = false, length = 15)
    String symbol;

    @Column(name = "ascii_ptcode", nullable = false, length = 14)
    String asciiPtCode;

    @Column(nullable = false, length = 10)
    String ptcode;

    @Column(name = "broker_code", nullable = false, length = 3)
    String brokerCode;

}
