package com.tsetmc.ime400.models;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "out_asset", schema = "ime")
@EntityListeners(AuditingEntityListener.class)
public class OutAsset extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    int id;

    @Column(nullable = false)
    int quantity;

    @Column(name = "clearance_date", columnDefinition = "TIMESTAMP")
    LocalDateTime clearanceDate;



    @Column(name = "out_date", columnDefinition = "TIMESTAMP")
    LocalDateTime outDate;



    @Column
    Long cost;

    @Column
    Long ocost;

    @Column
    Double vat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "investor_id", referencedColumnName = "id")
    Investor investor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="storage_id",referencedColumnName = "id")
    Storage storage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="clearance_request",referencedColumnName = "id")
    ClearanceRequest clearanceRequest;



}
