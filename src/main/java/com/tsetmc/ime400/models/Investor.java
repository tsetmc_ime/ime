package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "investor", schema = "ime", uniqueConstraints = {@UniqueConstraint(columnNames = {"ascii_ptcode", "national_id"})})
@FieldNameConstants
@EntityListeners(AuditingEntityListener.class)
public class Investor extends LogFields {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NotNull(message = "نوع شخص مشخص نشده است")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_type", referencedColumnName = "id")
    PersonType personType;

    @Column(length = 20)
    // @Length(min = 8, max = 8, message = "کدبورسی باید ۸ کاراکتر باشد")
    @NotBlank(message = "کد بورسی باید مقدار داشته باشد")
    String ptcode;

    @Column(name = "ascii_ptcode", length = 14)
    String asciiPtcode;

    @Column(name = "national_id", length = 11)
    @Length(min = 6, max = 10, message = "کد ملی شخص حقیقی باید ۱۰ کاراکتر و کد مشتری شخص حقوقی باید ۶ کاراکتر باشد")
    @NotBlank(message = "برای سهامدار حقیقی کد ملی و برای سهامدار حقوقی کد مشتری باید وارد شود")
    String nationalId;

    @Column(name = "first_name", length = 100)
    String firstName;

    @Column(name = "last_name", length = 100)
    String lastName;

    @Column(name = "id_number", length = 20)
    String idNumber;

    @Column(name = "birth_date", length = 20)
    String birthDate;

    public void setBirthDate(String _birthDate) {
        if (_birthDate != null && !_birthDate.isEmpty()) {
            this.birthDate = _birthDate;
            try {
                this.birthDateGreg = new Timestamp(DateTimeService.fromJalaliToGregorian(_birthDate).getTime());
            }
            catch (Exception ex) {
                this.birthDateGreg = null;
            }
        } else {
            this.birthDateGreg = null;
        }
    }

    @Column(name = "birth_date_greg")
    Timestamp birthDateGreg;

    @Column(name = "father_name", length = 100)
    String fatherName;

    @Column(name = "id_serie", length = 20)
    String idSerie;

    @Column(name = "id_serial", length = 20)
    String idSerial;

    @Column(name = "issue_place", length = 20)
    String issuePlace;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "activity_state", referencedColumnName = "id")
    ActivityState activityState;

    @Column(length = 50)
    String phone;

    @Column(length = 200)
    String address;

    @Column(name = "account_number", length = 200)
    String accountNumber;

    @Column(name = "last_update_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date lastUpdateDate;

}
