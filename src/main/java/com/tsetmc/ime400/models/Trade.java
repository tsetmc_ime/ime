package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "trade", schema = "ime")
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Long id;

    @Basic
    @Column(name = "sequence_id", nullable = true, length = 20)
    String sequenceId;

    @Column(name = "trade_date", nullable = true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date tradeDate;

    @Basic
    @Column(name = "persian_trade_date", nullable = true, length = 8)
    String persianTradeDate;
    public void setPersianTradeDate(String trade_date) {
        this.persianTradeDate = trade_date;
        this.tradeDate = DateTimeService.fromJalaliToGregorian(trade_date);
    }

    @Basic
    @Column(name = "trade_time", nullable = true, length = 6)
    String tradeTime;

    @Basic
    @Column(name = "trade_seq", nullable = true, length = 4)
    String tradeSeq;

    @Basic
    @Column(name = "symbol", nullable = false, length = 12)
    String symbol;

    @Basic
    @Column(name = "ticket_number", nullable = true, length = 10)
    String ticketNumber;

    @Basic
    @Column(name = "quantity", nullable = false)
    int quantity;

    @Basic
    @Column(name = "unit_price", nullable = false)
    int unitPrice;

    @Basic
    @Column(name = "b_broker_code", nullable = false, length = 3)
    String bBrokerCode;

    @Basic
    @Column(name = "b_person_type", nullable = false)
    int bPersonType;

    @Basic
    @Column(name = "b_ptcode", nullable = false, length = 12)
    String bPtcode;

    @Basic
    @Column(name = "b_ascii_ptcode", nullable = false, length = 14)
    String bAsciiPtcode;

    @Basic
    @Column(name = "b_client_code", nullable = false, length = 11)
    String bClientCode;

    @Basic
    @Column(name = "b_national_id", nullable = false, length = 20)
    String bNationalId;

    @Basic
    @Column(name = "b_last_name", nullable = true, length = 50)
    String bLastName;

    @Basic
    @Column(name = "b_first_name", nullable = true, length = 50)
    String bFirstName;

    @Basic
    @Column(name = "b_id_number", nullable = true, length = 12)
    String bIdNumber;

    @Basic
    @Column(name = "b_father_name", nullable = true, length = 50)
    String bFatherName;

    @Basic
    @Column(name = "b_birth_date", nullable = true, length = 10)
    String bBirthDate;

    @Basic
    @Column(name = "b_id_serial", nullable = true, length = 10)
    String bIdSerial;

    @Basic
    @Column(name = "b_id_serie", nullable = true, length = 3)
    String bIdSerie;

    @Basic
    @Column(name = "b_issue_place", nullable = true, length = 50)
    String bIssuePlace;

    @Basic
    @Column(name = "b_avtivity_state", nullable = true)
    Integer bAvtivityState;

    @Basic
    @Column(name = "s_broker_code", nullable = false, length = 3)
    String sBrokerCode;

    @Basic
    @Column(name = "s_person_type", nullable = true)
    Integer sPersonType;

    @Basic
    @Column(name = "s_ptcode", nullable = false, length = 12)
    String sPtcode;

    @Basic
    @Column(name = "s_ascii_ptcode", nullable = false, length = 14)
    String sAsciiPtcode;

    @Basic
    @Column(name = "s_client_code", nullable = false, length = 11)
    String sClientCode;

    @Basic
    @Column(name = "s_national_id", nullable = false, length = 20)
    String sNationalId;

    @Basic
    @Column(name = "s_last_name", nullable = true, length = 50)
    String sLastName;

    @Basic
    @Column(name = "s_first_name", nullable = true, length = 50)
    String sFirstName;

    @Basic
    @Column(name = "s_id_number", nullable = true, length = 12)
    String sIdNumber;

    @Basic
    @Column(name = "s_father_name", nullable = true, length = 50)
    String sFatherName;

    @Basic
    @Column(name = "s_birth_date", nullable = true, length = 10)
    String sBirthDate;

    @Basic
    @Column(name = "s_id_serial", nullable = true, length = 10)
    String sIdSerial;

    @Basic
    @Column(name = "s_id_serie", nullable = true, length = 3)
    String sIdSerie;

    @Basic
    @Column(name = "s_issue_place", nullable = true, length = 50)
    String sIssuePlace;

    @Basic
    @Column(name = "s_activity_state", nullable = true)
    Integer sActivityState;

}
