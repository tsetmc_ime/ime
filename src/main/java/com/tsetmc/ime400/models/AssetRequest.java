package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.DateTimeService;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "asset_request", schema = "ime")
@EntityListeners(AuditingEntityListener.class)
public class AssetRequest extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "temp_asset_id", referencedColumnName = "id", nullable = false)
    TempAsset tempAsset;

    @Column(name = "request_number", nullable = true, length = 20)
    String requestNumber;

    @Column(name = "request_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date requestDate;

    public void setRequestDate(Date value) {
        this.requestDate = value;
        this.setPersianRequestDate(DateTimeService.fromGregoriantoJalali(this.requestDate));
        this.setRequestTime(DateTimeService.getTime(this.requestDate));
    }

    @Basic
    @Column(name = "persian_request_date", nullable = true, length = 8)
    String persianRequestDate;

    @Basic
    @Column(name = "request_time", nullable = true, length = 6)
    String requestTime;

    @Basic
    @Column(name = "persian_response_date", nullable = true, length = 8)
    @Length(min = 8, max = 8, message = "فرمت قابل قبول برای تاریخ شمسی به صورت yyyymmdd است")
    String persianResponseDate;

    @Column(name = "response_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date responseDate;

    public void setResponseDate() {
        int year = 0, month = 0, day = 0, min = 0, hour = 0, sec = 0;
        if (getPersianResponseDate() != null && !getPersianResponseDate().isEmpty()) {
            String _responseDate = getPersianResponseDate();
            year = Integer.parseInt(_responseDate.substring(0, 4));
            month = Integer.parseInt(_responseDate.substring(4, 6));
            day = Integer.parseInt(_responseDate.substring(6, 8));
        }
        if (getResponseTime() != null && !getResponseTime().isEmpty()) {
            String _responseTime = getResponseTime();
            hour = Integer.parseInt(_responseTime.substring(0, 2));
            min = Integer.parseInt(_responseTime.substring(2, 4));
            sec = Integer.parseInt(_responseTime.substring(4, 6));
        }
        this.responseDate = DateTimeService.fromJalaliToGregorian(year, month, day, hour, min, sec);
    }

    @Basic
    @Column(name = "response_time", nullable = true, length = 6)
    String responseTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "response_status", referencedColumnName = "id", nullable = false)
    AssetResponseStatus responseStatus;

}
