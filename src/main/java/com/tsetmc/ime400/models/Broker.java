package com.tsetmc.ime400.models;

import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "broker", schema = "ime")
@FieldNameConstants
@EntityListeners(AuditingEntityListener.class)
public class Broker extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @Column(nullable = false, length = 3)
    @Length(min = 3, max = 3, message = "کد کارگزار باید ۳ رقم باشد")
    @NotBlank(message = "کد ۳ رقمی کارگزار الزامی است")
    String code;

    @Column(nullable = false, length = 100)
    @NotBlank(message = "نام کارگزار الزامی است")
    String name;

    @Column(nullable = false)
    Byte enabled;

}
