package com.tsetmc.ime400.models;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "asset", schema = "ime", uniqueConstraints = {@UniqueConstraint(columnNames = {"investor_id", "storage_id"})})
@EntityListeners(AuditingEntityListener.class)
public class Asset extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "investor_id", referencedColumnName = "id", nullable = false)
    Investor investor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "storage_id", referencedColumnName = "id", nullable = false)
    Storage storage;

    @Column(nullable = false)
    int quantity;

    @Column(name = "convert_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date convertDate;

    @Column(name = "Last_update_date", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date lastUpdateDate;

}
