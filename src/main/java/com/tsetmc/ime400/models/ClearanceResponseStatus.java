package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "clearance_response_status", schema = "ime")
public class ClearanceResponseStatus {

    @Id
    @Column(nullable = false)
    int id;

    @Column(name = "description", length = 200, unique = true)
    String description;

}
