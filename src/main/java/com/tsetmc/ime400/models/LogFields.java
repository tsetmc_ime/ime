package com.tsetmc.ime400.models;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

// https://www.baeldung.com/hibernate-inheritance

@MappedSuperclass
public class LogFields {

    @Column(name = "created_by")
    @CreatedBy
    String createdBy;

    @Column(name = "created_date")
    @CreatedDate
    Long createdDate;

    @Column(name = "modified_by")
    @LastModifiedBy
    String modifiedBy;

    @Column(name = "modified_date")
    @LastModifiedDate
    Long modifiedDate;

}
