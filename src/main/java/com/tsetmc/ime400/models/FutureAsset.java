package com.tsetmc.ime400.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "future_asset", schema = "ime")
public class FutureAsset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    long id;

    @Basic
    @Column(name = "symbol", nullable = false)
    String symbol;

    @Basic
    @Column(name = "date", nullable = false)
    String date;

    @Basic
    @Column(name = "quantity", nullable = false)
    int quantity;

    @Basic
    @Column(name = "ptcode", nullable = false)
    String ptcode;

    @Basic
    @Column(name = "ascii_ptcode", nullable = false)
    String asciiPtcode;

    @Basic
    @Column(name = "national_id")
    String nationalId;

    @Basic
    @Column(name = "client_code")
    String clientCode;

    @Basic
    @Column(name = "first_name")
    String firstName;

    @Basic
    @Column(name = "last_name")
    String lastName;

    @Basic
    @Column(name = "father_name")
    String fatherName;

    @Basic
    @Column(name = "birth_date")
    String birthDate;

    @Basic
    @Column(name = "issue_number")
    String issueNumber;

    @Basic
    @Column(name = "issue_place")
    String issuePlace;

    @Basic
    @Column(name = "is_active")
    Byte isActive;

}
