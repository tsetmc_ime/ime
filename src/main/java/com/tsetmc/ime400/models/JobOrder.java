package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "job_order", schema = "ime")
public class JobOrder implements Serializable {
    @Id
    @OneToOne
    @JoinColumn(name = "job_name_now", nullable = false, insertable = false, updatable = false)
    JobName JobNameNow;
    @OneToOne
    @JoinColumn(name = "job_name_previous", nullable = false, insertable = false, updatable = false)
    JobName JobNamePreviuos;
}
