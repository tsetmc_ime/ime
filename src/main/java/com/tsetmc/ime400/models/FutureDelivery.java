package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Data
@Table(name = "future_delivery", schema = "ime")
public class FutureDelivery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Long id;

    @Basic
    @Column(name = "ptcode14", length = 14)
    String ptcode14;

    @Basic
    @Column(name = "symbol", length = 20)
    String symbol;

    @Basic
    @Column(name = "requested_quantiry")
    Integer requestedQuantiry;

    @Basic
    @Column(name = "broker_code", length = 3)
    String brokerCode;

    @Basic
    @Column(name = "transaction_time")
    LocalDate transactionTime;

    @Basic
    @Column(name = "ocost")
    Long ocost;

    @Basic
    @Column(name = "delivered_quantity")
    Integer deliveredQuantity;

    @OneToOne(mappedBy = "futureDelivery")
    ClearanceRequest clearanceRequest;

}
