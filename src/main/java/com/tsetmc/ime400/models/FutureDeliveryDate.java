package com.tsetmc.ime400.models;

import com.ibm.icu.text.SimpleDateFormat;
import com.tsetmc.ime400.util.DateTimeService;
import com.tsetmc.ime400.util.UniqueInstrumentSymbolConstraint;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
@Table(name = "future_delivery_date", schema = "ime")
public class FutureDeliveryDate {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, unique = true)
    @NotNull(message = "تاریخ تحویل مقدار ندارد")
    LocalDate date;
    @Column(nullable = false, length = 255)
    String letterNo;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    LocalDate letterDate;
    @Column(nullable = false, length = 255)
    @NotBlank(message = "عنوان نامه نمی تواند خالی باشد")
    String letterDescription;


}
