package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "clearance_time", schema = "ime")
public class ClearanceTime {

    @Id
    @Column(nullable = false)
    int id;

    @Column(name = "description", length = 80, unique = true)
    String description;

}
