package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "job_log", schema = "ime")
public class JobLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    Integer id;

    @Column
    LocalDate date;

    @ManyToOne
    @JoinColumn(name = "name", nullable = false, insertable = false, updatable = false)
    JobName name;

    @Column
    String comments;

    @Column
    LocalTime time;

}
