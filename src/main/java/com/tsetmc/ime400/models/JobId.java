package com.tsetmc.ime400.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class JobId implements Serializable {

    private LocalDate date;
    private String name;

    public JobId(LocalDate date, JobName jobName) {
        this.date = date;
        this.name = jobName.name;
    }

    public JobId() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobId jobId = (JobId) o;
        return date.equals(jobId.date) && name.equals(jobId.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, name);
    }

}
