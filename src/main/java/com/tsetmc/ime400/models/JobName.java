package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

// https://www.baeldung.com/hibernate-inheritance

@Data
@Entity
@Table(name = "job_name", schema = "ime")
public class JobName {

    @Column(nullable = false)
    int id;

    @Id
    @Column
    String name;

    @Column
    String description;

    @Column
    Boolean active;

}
