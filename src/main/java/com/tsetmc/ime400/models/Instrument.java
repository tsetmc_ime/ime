package com.tsetmc.ime400.models;

import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "instrument", schema = "ime",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"symbol"})})
@FieldNameConstants
@EntityListeners(AuditingEntityListener.class)
public class Instrument extends LogFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @NaturalId
    @Column(nullable = false, length = 20, unique = true)
    @NotNull(message = "نماد باید مقدار داشته باشد")
    // @UniqueInstrumentSymbolConstraint(message = "در حال حاضر نماد دیگری با همین نام وجود دارد") TODO
    // https://stackoverflow.com/a/62650045/1994239
    @Size(min = 7, max = 12, message = "نماد باید بین ۷ تا ۱۲ کاراکتر باشد")
    String symbol;

    @Column(name = "symbol_desc", nullable = false, length = 100)
    @NotNull(message = "شرح نماد باید مقدار داشته باشد")
    @NotBlank(message = "شرح نماد باید مقدار داشته باشد")
    @Size(max = 100, message = "شرح نماد نمی‌تواند بیش از ۱۰۰ کاراکتر باشد")
    String symbolDesc;

    @Column(name = "due_date")
    // @NotNull(message = "تاریخ سررسید باید مقدار داشته باشد")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate dueDate;

    /* @Transient
    @PersianDateConstraint(message = "مقدار تاریخ شمسی معتبر نیست")
    String dueDateJ;

    public void setDueDateJ(String jdt) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        dueDate = df.parse(jdt).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public String getDueDateJ() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(dueDate);
    } */

    @Column(nullable = false)
    @NumberFormat
    @NotNull(message = "اندازه بسته معاملاتی باید مقدار داشته باشد")
    @Positive
    Integer lot;

    @Column
    @NumberFormat
    @Positive
    Integer tick;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clearance_type", referencedColumnName = "id")
    ClearanceType clearanceType;

    @NotBlank(message = "واحد اندازه‌گیری باید مقدار داشته باشد")
    @Column(name = "weight_unit", length = 20)
    String weightUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clearance_time", referencedColumnName = "id")
    ClearanceTime clearanceTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "convert_time", referencedColumnName = "id")
    ConvertTime convertTime;

    @Column(name = "trades_vat", nullable = false, precision = 0)
    @NumberFormat
    @NotNull(message = "مالیات بر ارزش افزوده معاملات باید مقدار داشته باشد")
    @Range(min = 0, max = 100, message = "مالیات بر ارزش افزوده معاملات باید عددی بین ۰ تا ۱۰۰ باشد")
    Double tradesVat;

    @Column(name = "cost_vat", nullable = false, precision = 0)
    @NumberFormat
    @NotNull(message = "مالیات بر ارزش افزوده هزینه انبارداری باید مقدار داشته باشد")
    @Range(min = 0, max = 100, message = "مالیات بر ارزش افزوده هزینه انبارداری باید عددی بین ۰ تا ۱۰۰ باشد")
    Double costVat;

    @Column
    @Pattern(regexp = "^[A-Z0-9]{12}$", message = "کد ۱۲ رقمی نماد باید از اعداد و حروف بزرگ انگلیسی تشکیل شده باشد")
    @NotNull(message = "کد ۱۲ رقمی نماد وارد نشده است")
    String isin;  // کد 12 رقمی نماد AIINIS

}
