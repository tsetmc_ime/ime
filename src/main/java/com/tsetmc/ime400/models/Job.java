package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "job", schema = "ime", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"date", "name"})
})
@IdClass(JobId.class)
public class Job {

    @Id
    @Column
    LocalDate date = LocalDate.now();

    @ManyToOne
    @JoinColumn(name = "name", nullable = false)
    JobName name;

    @ManyToOne
    @JoinColumn(name = "status", nullable = false)
    JobStatus status;

    @Column
    Boolean fire;

}
