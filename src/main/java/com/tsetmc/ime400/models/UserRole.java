package com.tsetmc.ime400.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_role", schema = "ime", catalog = "ime")
public class UserRole {

    @Id
    @Column(nullable = false)
    int id;

    @Column(nullable = false, unique = true)
    String name;

    @Column(nullable = false)
    String description;

}
