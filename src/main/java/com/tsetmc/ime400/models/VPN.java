package com.tsetmc.ime400.models;

import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@Data
@FieldNameConstants
@Table(name = "vpn", schema = "ime",  uniqueConstraints = {@UniqueConstraint(columnNames = {"UserName"})
})
public class VPN {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(nullable = false, length = 1000)
    @NotBlank(message = "نام انبار نمی تواند خالی باشد")
    String StorageName;
    @Pattern(regexp="gskuser[0-9]+",message="gskuser به همراه عدد")
    @NotBlank(message = "نام کاربری نمی تواند خالی باشد")
    String UserName;
    @Column(nullable = false, length = 100)
    @NotBlank(message = "کلمه عبور نمی تواند خالی باشد")
    String Password;
    @Column(nullable = false, length = 1000)
    @NotBlank(message = "IP نمیتواند خالی باشد")
    String StaticIP;


}
