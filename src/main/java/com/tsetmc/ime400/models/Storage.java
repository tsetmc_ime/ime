package com.tsetmc.ime400.models;

import com.tsetmc.ime400.util.UniqueStorageNameConstraint;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@FieldNameConstants
@Table(name = "storage", schema = "ime")
// https://stackoverflow.com/a/2783859/1994239
//todo storage name should not be unique, test that unique index where has been used and remove it with checking all side effects
//@UniqueStorageNameConstraint(message = "در حال حاضر انبار دیگری با نام مشابه وجود دارد")
@EntityListeners(AuditingEntityListener.class)
public class Storage extends LogFields {

    public Storage() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Integer id;

    @Column(nullable = false, length = 100, unique = true)
    @NotBlank(message = "نام انبار باید مقدار داشته باشد")
    String name;

    @Column(name = "max_cap", nullable = false)
    @NotNull(message = "سقف ظرفیت باید مقدار داشته باشد")
    @NumberFormat
    Integer maxCap;

    @Column(nullable = false)
    @NotNull(message = "نرخ انبارداری روزانه باید مقدار داشته باشد")
    @NumberFormat
    Integer cost;

    @Column(name = "initial_cost")
    // @NotNull(message = "هزینه اولیه باید مقدار داشته باشد")
    @NumberFormat
    Integer initialCost;

    @Column(length = 500)
    String address;

    @Column(name = "is_active")
    Byte isActive=(byte) 1;

    @Column(name = "base_asset")
    Long baseAsset;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "first_trade_date")
    LocalDate firstTradeDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "last_trade_date")
    LocalDate lastTradeDate;

    @Column(nullable = true, length = 40)
    String phone;

    @Column(name = "price_unit", length = 10)
    String priceUnit;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "last_storage_date")
    LocalDate lastStorageDate;

    @Column(name = "evaluate_time")
    Integer evaluateTime;

    @Column(name = "min_deliverable")
    Integer minDeliverable;

    @Column(name = "store_keeper_name", length = 50)
    String storeKeeperName;

    @Column(name = "working_hour", length = 50)
    String workingHour;

    @Column(name = "warehousing_time", length = 50)
    String warehousingTime;

    @ManyToOne
    @JoinColumn(name = "instrument_id", referencedColumnName = "id", updatable = false)
    Instrument instrument;

}
