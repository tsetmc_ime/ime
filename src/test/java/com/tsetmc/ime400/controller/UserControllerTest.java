package com.tsetmc.ime400.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsetmc.ime400.controllers.UserController;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.models.UserRole;
import com.tsetmc.ime400.security.jwt.JwtUtils;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest

@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private UserController controller;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    User user_user_test1= new User();
    User user_user_test2= new User();
    User user_admin= new User();
    User user_SURVEILLANCE= new User();
    @BeforeEach
    void init(){
        user_admin.setId(1);
        user_admin.setUsername("admin");
        user_admin.setPassword("5iveL!fe");
        user_admin.setEnabled(true);
        UserRole r = new UserRole();
        r.setId(1);
        r.setName("ADMIN");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_admin.setRole(r);

        user_SURVEILLANCE.setId(3);
        user_SURVEILLANCE.setUsername("bagherzadeh");
        user_SURVEILLANCE.setPassword("Tsetmc@0012171832");
        user_SURVEILLANCE.setEnabled(true);
        r = new UserRole();
        r.setId(2);
        r.setName("SURVEILLANCE");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_SURVEILLANCE.setRole(r);

        user_user_test1.setId(5);
        user_user_test1.setUsername("test_user_1");
        user_user_test1.setPassword("Tsetmc@0012171832");
        user_user_test1.setEnabled(true);
        Storage s = new Storage();
        s.setId(3);
        Instrument i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test1.setStorage(s);
        r = new UserRole();
        r.setId(3);
        r.setName("USER_LEVEL_1");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test1.setRole(r);

        user_user_test2.setId(23);
        user_user_test2.setUsername("test_user_2");
        user_user_test2.setPassword("Tsetmc@0012171832");
        user_user_test2.setEnabled(true);
        s = new Storage();
        s.setId(3);
        i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test2.setStorage(s);
        r = new UserRole();
        r.setId(4);
        r.setName("USER_LEVEL_2");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test2.setRole(r);
    }

    @Test
    public void getUserInfo_shouldbegetUserInfo_admin() throws Exception {

        mockMvc.perform(get("/user-info").with(user(user_admin))).andExpect(status().isOk())
                .andExpect(content().string(containsString(user_admin.getUsername())));

    }
    @Test
    public void getUserInfo_shouldbegetUserInfo_SURVEILLANCE() throws Exception {

        mockMvc.perform(get("/user-info").with(user(user_SURVEILLANCE))).andExpect(status().isOk())
                .andExpect(content().string(containsString(user_SURVEILLANCE.getUsername())));

    }

    @Test
    public void getUserInfo_shouldbegetUserInfo_Level1() throws Exception {

        mockMvc.perform(get("/user-info").with(user(user_user_test1)))
                .andExpect(redirectedUrl("/?unauthorized"));

    }
    @Test
    public void getUserInfo_shouldbegetUserInfo_Level2() throws Exception {

        mockMvc.perform(get("/user-info").with(user(user_user_test2)))
                .andExpect(redirectedUrl("/?unauthorized"));

    }
    @Test
    public void getUsersList_shouldbeGetListOfUser_admin() throws Exception {

        mockMvc.perform(get("/users/index?draw=1&columns=&start=0&length=10&search=&orderKey=&orderDir=")
                        .with(user(user_admin))).andDo(print())//andExpect(view().name("users"))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    public void getUsersList_shouldbeGetListOfUser_SURVEILLANCE() throws Exception {

        mockMvc.perform(get("/users/index?draw=1&columns=&start=0&length=10&search=&orderKey=&orderDir=")
                        .with(user(user_SURVEILLANCE))).andDo(print())//andExpect(view().name("users"))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    public void getUsersList_shouldbeGetListOfUser_Level_1() throws Exception {

        mockMvc.perform(get("/users/index?draw=1&columns=&start=0&length=10&search=&orderKey=&orderDir=")
                        .with(user(user_user_test1))).andDo(print())//andExpect(view().name("users"))
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    public void getUsersList_shouldbeGetListOfUser_Level_2() throws Exception {

        mockMvc.perform(get("/users/index?draw=1&columns=&start=0&length=10&search=&orderKey=&orderDir=")
                        .with(user(user_user_test2))).andDo(print())//andExpect(view().name("users"))
                .andExpect(redirectedUrl("/?unauthorized"));
    }

    @Test
    void EditUserRole() throws Exception{
        mockMvc.perform(post("/users/edit/{id}",1).with(user(user_admin))
                        .with(csrf())
                        .param("role.name", user_user_test2.getRole().getName())
                        .param("username", user_admin.getUsername())
                        .param("password", user_admin.getPassword())
                )
                .andExpect(model().attributeExists("errors"));
    }

}
