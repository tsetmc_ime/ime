package com.tsetmc.ime400.controller;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsetmc.ime400.controllers.TempAssetController;
import com.tsetmc.ime400.models.*;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest

@AutoConfigureMockMvc
public class TempAssetControllerTest {
    @Autowired
    private TempAssetController controller;
    @Autowired
    private MockMvc mockMvc;
    User user_user_test1= new User();
    User user_user_test2= new User();
    User user_admin= new User();
    User user_SURVEILLANCE= new User();
    @BeforeEach
    void init(){
        user_admin.setId(1);
        user_admin.setUsername("admin");
        user_admin.setPassword("5iveL!fe");
        user_admin.setEnabled(true);
        UserRole r = new UserRole();
        r.setId(1);
        r.setName("ADMIN");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_admin.setRole(r);

        user_SURVEILLANCE.setId(3);
        user_SURVEILLANCE.setUsername("bagherzadeh");
        user_SURVEILLANCE.setPassword("Tsetmc@0012171832");
        user_SURVEILLANCE.setEnabled(true);
        r = new UserRole();
        r.setId(2);
        r.setName("SURVEILLANCE");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_SURVEILLANCE.setRole(r);

        user_user_test1.setId(5);
        user_user_test1.setUsername("test_user_1");
        user_user_test1.setPassword("Tsetmc@0012171832");
        user_user_test1.setEnabled(true);
        Storage s = new Storage();
        s.setId(3);
        Instrument i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test1.setStorage(s);
        r = new UserRole();
        r.setId(3);
        r.setName("USER_LEVEL_1");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test1.setRole(r);

        user_user_test2.setId(23);
        user_user_test2.setUsername("test_user_2");
        user_user_test2.setPassword("Tsetmc@0012171832");
        user_user_test2.setEnabled(true);
        s = new Storage();
        s.setId(3);
        i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test2.setStorage(s);
        r = new UserRole();
        r.setId(4);
        r.setName("USER_LEVEL_2");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test2.setRole(r);
    }

    @Test
    public void createOrUpdateTempAsset_User_Level_1() throws Exception {
        
        mockMvc.perform(post("/temp-assets/new").with(user(user_user_test1))
                    .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(model().attributeDoesNotExist("errors"))
                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/temp-assets"));
    }
    @Test
    public void createOrUpdateTempAsset_User_Admin() throws Exception {

        mockMvc.perform(post("/temp-assets/new").with(user(user_admin))
                        .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    public void createOrUpdateTempAsset_User_Surveillance() throws Exception {

        mockMvc.perform(post("/temp-assets/new").with(user(user_SURVEILLANCE))
                        .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    public void createOrUpdateTempAsset_Edit_User_Surveillance() throws Exception {

        mockMvc.perform(post("/temp-assets/edit/{id}", 1).with(user(user_SURVEILLANCE))
                        .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    public void createOrUpdateTempAsset_Edit_User_Admin() throws Exception {

        mockMvc.perform(post("/temp-assets/edit/{id}", 1).with(user(user_admin))
                        .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    public void createOrUpdateTempAsset_Edit_User_Level_1() throws Exception {

        mockMvc.perform(post("/temp-assets/edit/{id}", 1).with(user(user_user_test1))
                        .with(csrf())
                        .param("quantity","100")
                        .param("investor.asciiPtcode", "16213822910488")
                        .param("broker.id","1")
                        .param("broker.code","112")
                        .param("broker.name","112")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(model().attributeDoesNotExist("errors"))
                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/temp-assets"));
    }


    @Test
    public void Index_User_Level_1() throws Exception {

        mockMvc.perform(get("/temp-assets").with(user(user_user_test1)))
                .andExpect(status().isOk());
    }
    @Test
    public void Index_User_Admin() throws Exception {

        mockMvc.perform(get("/temp-assets").with(user(user_admin)))
                .andExpect(status().isOk());
    }
    @Test
    public void Index_User_Surveillance() throws Exception {

        mockMvc.perform(get("/temp-assets").with(user(user_SURVEILLANCE)))
                .andExpect(status().isOk());
    }

///confirm/{id}
    @Test
    void Confirm_User_Level_2() throws Exception {
        mockMvc.perform(post("/temp-assets/confirm/{id}", 1).with(user(user_user_test2))
                        .with(csrf())
                )
                .andExpect(model().attributeDoesNotExist("errors"))
                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/temp-assets"));
    }
    @Test
    void Confirm_User_Level_1() throws Exception{
        mockMvc.perform(post("/temp-assets/confirm/{id}", 1).with(user(user_user_test1))
                        .with(csrf())
                )
                .andExpect(model().attributeDoesNotExist("errors"))
                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/temp-assets"));
    }

    @Test
    void Confirm_User_Admin() throws Exception{
        mockMvc.perform(post("/temp-assets/confirm/{id}", 1).with(user(user_admin))
                        .with(csrf())
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
    @Test
    void Confirm_User_Surveillance() throws Exception{
        mockMvc.perform(post("/temp-assets/confirm/{id}", 1).with(user(user_SURVEILLANCE))
                        .with(csrf())
                )
                .andExpect(redirectedUrl("/?unauthorized"));
    }
}
