package com.tsetmc.ime400.controller;

import com.tsetmc.ime400.controllers.AssetController;
import com.tsetmc.ime400.controllers.InvestorController;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.models.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest

@AutoConfigureMockMvc
public class InvestorControllerTest {

    @Autowired
    private InvestorController controller;
    @Autowired
    private MockMvc mockMvc;
    User user_user_test1= new User();
    User user_user_test2= new User();
    User user_admin= new User();
    User user_SURVEILLANCE= new User();
    @BeforeEach
    void init(){
        user_admin.setId(1);
        user_admin.setUsername("admin");
        user_admin.setPassword("5iveL!fe");
        user_admin.setEnabled(true);
        UserRole r = new UserRole();
        r.setId(1);
        r.setName("ADMIN");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_admin.setRole(r);

        user_SURVEILLANCE.setId(3);
        user_SURVEILLANCE.setUsername("bagherzadeh");
        user_SURVEILLANCE.setPassword("Tsetmc@0012171832");
        user_SURVEILLANCE.setEnabled(true);
        r = new UserRole();
        r.setId(2);
        r.setName("SURVEILLANCE");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_SURVEILLANCE.setRole(r);

        user_user_test1.setId(5);
        user_user_test1.setUsername("test_user_1");
        user_user_test1.setPassword("Tsetmc@0012171832");
        user_user_test1.setEnabled(true);
        Storage s = new Storage();
        s.setId(3);
        Instrument i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test1.setStorage(s);
        r = new UserRole();
        r.setId(3);
        r.setName("USER_LEVEL_1");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test1.setRole(r);

        user_user_test2.setId(23);
        user_user_test2.setUsername("test_user_2");
        user_user_test2.setPassword("Tsetmc@0012171832");
        user_user_test2.setEnabled(true);
        s = new Storage();
        s.setId(3);
        i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test2.setStorage(s);
        r = new UserRole();
        r.setId(4);
        r.setName("USER_LEVEL_2");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test2.setRole(r);
    }
    @Test
    public void SaveNewInvestor_User_Level_1() throws Exception{
        mockMvc.perform(post("/investors/new").with(user(user_user_test1))
                        .with(csrf())
                        .param("ptcode","زار64889")
                        .param("nationalId", "0491794924")
                        .param("personType","1")
                        .param("firstName","مجید")
                        .param("lastName","زارع ")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/investors"));
    }
    @Test
    public void SaveNewInvestor_Admin() throws Exception{
        mockMvc.perform(post("/investors/new").with(user(user_admin))
                        .with(csrf())
                        .param("ptcode","مح م 14288")
                        .param("nationalId", "3230690516")
                        .param("personType","1")
                        .param("firstName","جوانه")
                        .param("lastName","محمدمرادی")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(model().attributeDoesNotExist("e3"))
//                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/investors"));
    }

    @Test
    public void SaveNewInvestor_Surveillance() throws Exception{
        mockMvc.perform(post("/investors/new").with(user(user_SURVEILLANCE))
                        .with(csrf())
                        .param("ptcode","بتکـ00035")
                        .param("nationalId", "3340901767")
                        .param("personType","1")
                        .param("firstName","علی")
                        .param("lastName","بتکده")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(model().attributeDoesNotExist("e3"))
//                .andExpect(flash().attributeExists("messages"))
                .andExpect(redirectedUrl("/investors"));
    }
}
