package com.tsetmc.ime400.controller;
import com.tsetmc.ime400.controllers.AssetController;
import com.tsetmc.ime400.models.Instrument;
import com.tsetmc.ime400.models.Storage;
import com.tsetmc.ime400.models.User;
import com.tsetmc.ime400.models.UserRole;
import com.tsetmc.ime400.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest

@AutoConfigureMockMvc
public class AssetControllerTest {
    @Autowired
    private AssetController controller;
    @Autowired
    private MockMvc mockMvc;

    User user_user_test1= new User();
    User user_admin= new User();
    User user_SURVEILLANCE= new User();
    @BeforeEach
    void init(){
        user_admin.setId(1);
        user_admin.setUsername("admin");
        user_admin.setPassword("5iveL!fe");
        user_admin.setEnabled(true);
        UserRole r = new UserRole();
        r.setId(1);
        r.setName("ADMIN");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_admin.setRole(r);

        user_SURVEILLANCE.setId(3);
        user_SURVEILLANCE.setUsername("bagherzadeh");
        user_SURVEILLANCE.setPassword("Tsetmc@0012171832");
        user_SURVEILLANCE.setEnabled(true);
        r = new UserRole();
        r.setId(2);
        r.setName("SURVEILLANCE");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_SURVEILLANCE.setRole(r);

        user_user_test1.setId(5);
        user_user_test1.setUsername("test_user_1");
        user_user_test1.setPassword("Tsetmc@0012171832");
        user_user_test1.setEnabled(true);
        Storage s = new Storage();
        s.setId(3);
        Instrument i = new Instrument();
        i.setId(4);
        s.setInstrument(i);
        user_user_test1.setStorage(s);
        r = new UserRole();
        r.setId(3);
        r.setName("USER_LEVEL_1");//"ADMIN", "USER_LEVEL_1","SURVEILLANCE","USER_LEVEL_2"
        user_user_test1.setRole(r);
    }
    @Test
    public void getTotalAssetRequest_adminUser() throws Exception {

        mockMvc.perform(get("/assets/status")
                .with(user(user_admin)))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    public void getTotalAssetRequest_surveillanceUser() throws Exception {

        mockMvc.perform(get("/assets/status")
                .with(user(user_SURVEILLANCE)))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    public void getTotalAssetRequest_level1_User() throws Exception {

        mockMvc.perform(get("/assets/status")
                .with(user(user_user_test1)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getAssetRequsetInfo_admin_user_status() throws Exception{

        mockMvc.perform(get("/assets/user-status").with(user(user_admin))).andExpect(status().isOk());

    }
    @Test
    public void getAssetRequsetInfo_surveillance_user_status() throws Exception{

        mockMvc.perform(get("/assets/user-status").with(user(user_SURVEILLANCE))).andExpect(status().isOk());

    }
    @Test
    public void getAssetRequsetInfo_userLevel1_user_status() throws Exception{

        mockMvc.perform(get("/assets/user-status").with(user(user_user_test1))).andExpect(status().isOk());

    }
    //asset-request-report
    @Test
    public void assetRequestsReportPage_adminAndSurveillanceUser() throws Exception{

        mockMvc.perform(get("/assets/asset-request-report").with(user(user_admin))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/asset-request-report").with(user(user_SURVEILLANCE))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/asset-request-report").with(user(user_user_test1))).andExpect(redirectedUrl("/?unauthorized"));

    }

    @Test
    public void assetInvestorReport_adminAndSurveillanceUser() throws Exception{

        mockMvc.perform(get("/assets/investor-report").with(user(user_admin))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/investor-report").with(user(user_SURVEILLANCE))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/investor-report").with(user(user_user_test1))).andExpect(redirectedUrl("/?unauthorized"));

    }

    @Test
    public void assetIndex_anyUser() throws Exception{

        mockMvc.perform(get("/assets/index").with(user(user_admin))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/index").with(user(user_SURVEILLANCE))).andExpect(status().isOk());

        mockMvc.perform(get("/assets/index").with(user(user_user_test1))).andExpect(status().isOk());

    }
    //asset-requests
    @Test
    public void assetRequest_anyUser() throws Exception{

        mockMvc.perform(get("/assets/asset-requests").with(user(user_admin))).andExpect(status().is2xxSuccessful());

        mockMvc.perform(get("/assets/asset-requests").with(user(user_SURVEILLANCE))).andExpect(status().is2xxSuccessful());

        mockMvc.perform(get("/assets/asset-requests").with(user(user_user_test1))).andExpect(status().is2xxSuccessful());

    }
//renderForm
    @Test
    public void assetClearGet_anyUser() throws Exception{

        mockMvc.perform(get("/assets/clear/{id}",1).with(user(user_admin))).andExpect(redirectedUrl("/?unauthorized"));

        mockMvc.perform(get("/assets/clear/{id}",1).with(user(user_SURVEILLANCE))).andExpect(redirectedUrl("/?unauthorized"));

        mockMvc.perform(get("/assets/clear/{id}",1).with(user(user_user_test1))).andExpect(status().is2xxSuccessful());

    }

    @Test
    public void assetClearPost_anyUser() throws Exception{
        mockMvc.perform(post("/assets/clear/{id}",1).with(user(user_admin)))
                .andExpect(redirectedUrl("/?unauthorized"));

        mockMvc.perform(post("/assets/clear/{id}",1).with(user(user_SURVEILLANCE)))
                .andExpect(redirectedUrl("/?unauthorized"));

        mockMvc.perform(post("/assets/clear/{id}",1).with(user(user_user_test1)))
                .andExpect(redirectedUrl("/assets")).andExpect(status().isFound());

    }
//    @Test
//    public void assetExportCsv_anyUser() throws Exception{
//
////        mockMvc.perform(get("/assets/export-csv").with(user(user_admin))).andExpect(status().is2xxSuccessful());
////
////        mockMvc.perform(get("/assets/export-csv").with(user(user_SURVEILLANCE))).andExpect(status().is2xxSuccessful());
//
//        mockMvc.perform(get("/assets/export-csv").with(user(user_user_test1)).with(new RequestPostProcessor() {
//                    @Override
//                    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest mockHttpServletRequest) {
//                        mockHttpServletRequest.setRemoteAddr("12345");
//                        return mockHttpServletRequest;
//                    }
//                })
//                .accept("application/csv, application/octet-stream")).andReturn().getResponse();
//
//    }


}
