# Introduction

This is the IME web application.

# Build Instructions

There are some environment variables that you have to define. Find them in resources/application.properties file. They are sensitive passwords, database locations, etc.

Example environment variables for MySQL mock database are here:

```bash
export SPRING_DATASOURCE_URL=jdbc:mysql://localhost:3306/ime
export SPRING_DATASOURCE_USERNAME=user
export SPRING_DATASOURCE_PASSWORD=1234
export WORK_TIME=16:00 # time of post-session start; since when users cannot send POST requests
```

You can use IntelliJ IDEA or eclipse. 

You can even run it directly from command line:

```bash
# cd project root directory
mvn compile
mvn libsass:compile
mvn spring-boot:run
```

To run without security, run with `no-auth` profile enabled:

```bash
mvn spring-boot:run -Dspring-boot.run.profiles=no-auth
```

For the command line, you have to install JDK 8 and [maven](https://maven.apache.org/install.html).

## Connecting to DB2 on AS/400

Some controller actions require DB2 on AS/400 as a secondary datasource. Those actions are defined in `AS400Controller` class. 


```bash
export DB2_DATASOURCE_DRIVER_CLASS_NAME=com.ibm.as400.access.AS400JDBCDriver
export DB2_DATASOURCE_URL=jdbc:as400://172.22.77.90
export DB2_DATASOURCE_USERNAME=USER
export DB2_DATASOURCE_PASSWORD=PASSWORD
```

## Running Tests

```bash
mvn test -Dtest="com.tsetmc.ime400.controller.**"
```
